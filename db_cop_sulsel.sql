-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 30, 2016 at 01:51 
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_cop_sulsel`
--

-- --------------------------------------------------------

--
-- Table structure for table `bobot_temp`
--

CREATE TABLE `bobot_temp` (
  `ID_KASUS` varchar(10) NOT NULL DEFAULT '-',
  `BOBOT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bobot_temp`
--

INSERT INTO `bobot_temp` (`ID_KASUS`, `BOBOT`) VALUES
('-', 0);

-- --------------------------------------------------------

--
-- Table structure for table `capu`
--

CREATE TABLE `capu` (
  `id` smallint(5) UNSIGNED NOT NULL,
  `username` varchar(40) NOT NULL,
  `passwd` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `dates`
--
CREATE TABLE `dates` (
`date` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `digits`
--
CREATE TABLE `digits` (
`digit` bigint(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `numbers`
--
CREATE TABLE `numbers` (
`number` bigint(25)
);

-- --------------------------------------------------------

--
-- Table structure for table `point_berkas_temp`
--

CREATE TABLE `point_berkas_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `BOBOT_POINT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `point_berkas_temp`
--

INSERT INTO `point_berkas_temp` (`ID_KASUS`, `BOBOT_POINT`) VALUES
('K160910003', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prosentase_berkas_temp`
--

CREATE TABLE `prosentase_berkas_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `PROSENTASE` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prosentase_berkas_temp`
--

INSERT INTO `prosentase_berkas_temp` (`ID_KASUS`, `PROSENTASE`) VALUES
('K160716002', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prosentase_realisasi_temp`
--

CREATE TABLE `prosentase_realisasi_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `PROSENTASE` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prosentase_realisasi_temp`
--

INSERT INTO `prosentase_realisasi_temp` (`ID_KASUS`, `PROSENTASE`) VALUES
('K160716002', 0);

-- --------------------------------------------------------

--
-- Table structure for table `prosentase_temp`
--

CREATE TABLE `prosentase_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `PROSENTASE` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `prosentase_temp`
--

INSERT INTO `prosentase_temp` (`ID_KASUS`, `PROSENTASE`) VALUES
('K160716002', 100);

-- --------------------------------------------------------

--
-- Table structure for table `real_lidik_temp`
--

CREATE TABLE `real_lidik_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `rab` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_lidik_temp`
--

INSERT INTO `real_lidik_temp` (`ID_KASUS`, `rab`) VALUES
('K160716001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `real_sidik_temp`
--

CREATE TABLE `real_sidik_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `rab` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `real_sidik_temp`
--

INSERT INTO `real_sidik_temp` (`ID_KASUS`, `rab`) VALUES
('K160716001', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ren_lidik_temp`
--

CREATE TABLE `ren_lidik_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `rab` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ren_lidik_temp`
--

INSERT INTO `ren_lidik_temp` (`ID_KASUS`, `rab`) VALUES
('aa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `ren_sidik_temp`
--

CREATE TABLE `ren_sidik_temp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `rab` int(12) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ren_sidik_temp`
--

INSERT INTO `ren_sidik_temp` (`ID_KASUS`, `rab`) VALUES
('aa', 0);

-- --------------------------------------------------------

--
-- Table structure for table `sc_chat_data`
--

CREATE TABLE `sc_chat_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `sender_id` int(21) UNSIGNED NOT NULL,
  `reciever_id` int(21) UNSIGNED NOT NULL,
  `text` text NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tb_anggota_spkt`
--

CREATE TABLE `tb_anggota_spkt` (
  `NRP` varchar(10) NOT NULL,
  `NAMA_ANGGOTA` varchar(50) NOT NULL,
  `PANGKAT_ANGGOTA` varchar(50) NOT NULL,
  `NO_TELEPON` varchar(50) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_anggota_spkt`
--

INSERT INTO `tb_anggota_spkt` (`NRP`, `NAMA_ANGGOTA`, `PANGKAT_ANGGOTA`, `NO_TELEPON`, `tgl_masuk_data`) VALUES
('78112322', 'Andi Suroso', 'AIPTU', '123455', '2016-06-22 05:23:37'),
('77933432', 'Khoirul Anam', 'AIPDA', '', '2016-06-22 08:19:11'),
('87061030', 'i made arta sutyawan', 'Brigadir Polisi Dua', '', '2016-06-24 01:28:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_interogasi`
--

CREATE TABLE `tb_ba_interogasi` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC04',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `LOKASI` varchar(200) NOT NULL,
  `TGL_BA` datetime NOT NULL,
  `NAMA_LENGKAP` varchar(100) NOT NULL,
  `ALAMAT` varchar(200) DEFAULT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_interogasi`
--

INSERT INTO `tb_ba_interogasi` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `LOKASI`, `TGL_BA`, `NAMA_LENGKAP`, `ALAMAT`, `NRP`, `tgl_masuk_data`, `LATITUDE`, `LONGITUDE`) VALUES
('K160624001', 'DOC12', 5, 'fgg', '2016-10-21 00:00:00', 'dgg', 'xhv', '76040065', '2016-11-15 01:52:07', '-7.2887263', '111.809326'),
('K160624001', 'DOC12', 4, 'fgg', '2016-10-21 00:00:00', 'dgg', 'xhv', '76040065', '2016-11-07 05:35:04', '-7.2887263', '112.809326'),
('K160624001', 'DOC12', 3, 'fgg', '2016-10-21 00:00:00', 'dgg', 'xhv', '76040065', '2016-11-07 05:35:04', '-7.2887263', '112.809326'),
('K160624001', 'DOC12', 2, 'fgg', '2016-10-21 00:00:00', 'dgg', 'xhv', '76040065', '2016-11-07 05:35:04', '-7.2887263', '112.809326'),
('K160624001', 'DOC12', 1, 'lokasi', '2016-10-21 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.2887254', '112.80933'),
('K160624001', 'DOC12', 6, 'fgg', '2016-10-21 00:00:00', 'dgg', 'xhv', '76040065', '2016-11-07 05:35:04', '-7.2887263', '112.809326'),
('K160624001', 'DOC12', 7, 'lokasi', '2016-10-21 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.2887254', '112.80933'),
('K160624001', 'DOC12', 8, 'lokasi', '2016-10-21 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.2887254', '112.80933'),
('K160624001', 'DOC12', 9, 'lokasi', '2016-10-21 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.2887254', '112.80933'),
('K160624001', 'DOC12', 10, 'lokasi', '2016-10-21 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.2887254', '112.80933'),
('K160624001', 'DOC12', 11, 'lokasi', '2016-10-23 00:00:00', 'Agung Imamudin', 'Tuban', '76040065', '2016-11-07 05:35:04', '-7.288727', '112.80933'),
('K160624001', 'DOC12', 12, 'di kampus pens', '2016-10-23 00:00:00', 'budi suharji', 'keputih sukolill', '76040065', '2016-11-07 05:35:04', '-7.288729', '112.809326'),
('K160624001', 'DOC12', 13, 'lokasi tkp', '2016-10-23 00:00:00', 'agung Imamudin', 'kaputih', '76040065', '2016-11-07 05:35:04', '-7.2887287', '112.809326'),
('K160624001', 'DOC12', 14, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn s', '2016-10-23 00:00:00', 'agung Imamudin', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.288733', '112.809326'),
('K160624001', 'DOC12', 15, 'lokasi', '2016-10-23 00:00:00', 'agung Imamudin', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.288729', '112.809326'),
('K160624001', 'DOC12', 16, 'tkp', '2016-10-23 00:00:00', 'agung Imamudin', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.288732', '112.809326'),
('K160624001', 'DOC12', 17, 'lokasi', '2016-10-23 00:00:00', 'agung Imamudin', 'alamat', '76040065', '2016-11-07 05:35:04', '-7.2887273', '112.80933'),
('K160624001', 'DOC12', 18, 'lhvg', '2016-10-23 00:00:00', 'cgv', 'cvv', '76040065', '2016-11-07 05:35:04', '-7.2887273', '112.80933'),
('K160624001', 'DOC12', 19, 'ghb', '2016-10-23 00:00:00', 'gjb', 'vjn', '76040065', '2016-11-07 05:35:04', '-7.288727', '112.80933'),
('K160624001', 'DOC12', 20, 'lokasi', '2016-10-24 00:00:00', 'absbns', 'shsbz', '76040065', '2016-11-07 05:35:04', '-7.288728', '112.809326'),
('K160624001', 'DOC12', 21, 'lokasi', '2016-10-24 00:00:00', 'absbns', 'shsbz', '76040065', '2016-11-07 05:35:04', '-7.288728', '112.809326'),
('K160716001', 'DOC12', 1, 'lokasi', '2016-10-25 00:00:00', 'nama', 'agung', '76040065', '2016-11-07 05:35:04', '109.6226079761982', '-7.448587027008919'),
('K160627001', 'DOC12', 1, 'lokasi', '2016-10-25 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.8092943', '-7.2887624'),
('K160910003', 'DOC12', 1, 'takp', '2016-10-25 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.73931875824928', '-7.3069395661611996'),
('K160910003', 'DOC12', 2, 'takp', '2016-10-25 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.73931875824928', '-7.3069395661611996'),
('K160716002', 'DOC12', 1, 'lokasii', '2016-10-25 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.8093271', '-7.2887289'),
('K160624001', 'DOC12', 22, 'lokaso', '2016-10-26 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.8093322', '-7.288728'),
('K160627001', 'DOC12', 2, 'bbb', '2016-10-26 00:00:00', 'gh', 'ghb', '76040065', '2016-11-07 05:35:04', '112.8093273', '-7.2887288'),
('K160624001', 'DOC12', 23, 'lokasi', '2016-10-26 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.8093255', '-7.2887262'),
('K160624001', 'DOC12', 24, 'lokasi', '2016-10-26 00:00:00', 'agung', 'alamat', '76040065', '2016-11-07 05:35:04', '112.8093287', '-7.2887267'),
('K160624001', 'DOC12', 25, 'lokasi', '2016-10-26 00:00:00', 'agung Imamudin', 'keputih', '76040065', '2016-11-07 05:35:04', '112.7797142', '-7.2956564'),
('K160922002', 'DOC12', 1, 'lokasi', '2016-10-26 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.7797142', '-7.2956564'),
('K160622001', 'DOC12', 1, 'lokasi', '2016-10-26 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.7797142', '-7.2956564'),
('K160910003', 'DOC12', 3, 'circleK', '2016-10-26 00:00:00', 'hadi sucipto', 'surabaya', '76040065', '2016-11-07 05:35:04', '112.7796857', '-7.2956748'),
('K160627001', 'DOC12', 3, 'lokasi', '2016-10-26 00:00:00', 'agung imamudin', 'alamat', '76040065', '2016-11-07 05:35:04', '112.809328', '-7.2887277'),
('K160923001', 'DOC12', 1, 'keputih', '2016-10-26 00:00:00', 'agung Imamudin', 'tuban', '76040065', '2016-11-07 05:35:04', '112.98636194318533', '-6.963553814239088'),
('K160716002', 'DOC12', 2, 'lolasi', '2016-10-26 00:00:00', 'nama', 'alamat', '76040065', '2016-11-07 05:35:04', '112.8093342', '-7.2887255'),
('K160624001', 'DOC12', 26, 'Jalan KembangGung 68 Tuban', '2016-10-05 00:00:00', 'Agus Prayetno', 'Jalan Mawar Merah 78 Surabaya', '76040065', '2016-11-07 05:35:04', '112.03103989362717', '-6.901778868512271'),
('K160910002', 'DOC12', 1, 'Desa kembang bilo Tuban', '2016-11-05 00:00:00', 'Agung Imamudin', 'Tuban', '76040065', '2016-11-07 05:35:04', '-6.9017351', '112.0316155'),
('K160716001', 'DOC12', 2, 'keputih permai 3 surabaya', '2016-11-07 00:00:00', 'agung Imamudin', 'tuban', '76040065', '2016-11-07 05:35:04', '-7.2887231', '112.8093216'),
('K160624001', 'DOC12', 27, 'Lokasi', '2016-11-11 00:00:00', 'Agung', 'Alamat', '78040126', '2016-11-11 10:07:17', '-7.2887217', '112.8093256'),
('K160624001', 'DOC12', 28, 'Yib', '2016-11-11 00:00:00', 'Fhjj', 'Vjjkg', '78040126', '2016-11-11 10:09:44', '-7.2887231', '112.8093207'),
('K160627001', 'DOC12', 4, 'Yuvv', '2016-11-11 00:00:00', 'F0hkgjc', 'Jfjcjc', '78040126', '2016-11-11 10:12:37', '-7.2887217', '112.8093268'),
('K160627001', 'DOC12', 5, 'Yuvv', '2016-11-11 00:00:00', 'F0hkgjc', 'Jfjcjc', '78040126', '2016-11-11 10:13:15', '-7.2887217', '112.8093268'),
('K160627001', 'DOC12', 6, 'Yuvv', '2016-11-11 00:00:00', 'F0hkgjc', 'Jfjcjc', '78040126', '2016-11-11 10:26:33', '-7.2887217', '112.8093268'),
('K160627001', 'DOC12', 7, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:23', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 8, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:31', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 9, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:44', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 10, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:48', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 11, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:52', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 12, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:53', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 13, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:53', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 14, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:54', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 15, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:54', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 16, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:54', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 17, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:00:55', '-7.2887224', '112.8093215'),
('K160627001', 'DOC12', 18, 'Tkp', '2016-11-12 00:00:00', 'Nama', 'Alamat', '78040126', '2016-11-12 09:01:04', '-7.2887224', '112.8093215'),
('K160624001', 'DOC12', 29, 'Lokasi', '2016-11-14 00:00:00', 'Sasaran', 'Alamat', '78040126', '2016-11-14 05:17:44', '-7.284055', '112.80539'),
('K160624001', 'DOC12', 30, 'Lokasi', '2016-11-14 00:00:00', 'Sasaran', 'Alamat', '78040126', '2016-11-14 05:18:42', '-7.284055', '112.80539'),
('K160624001', 'DOC12', 31, 'Lokasi', '2016-11-14 00:00:00', 'Sasaran', 'Alamat', '78040126', '2016-11-14 05:20:18', '-7.284055', '112.80539'),
('K160624001', 'DOC12', 32, 'Lokasi', '2016-11-14 00:00:00', 'Sasaran', 'Alamat', '78040126', '2016-11-14 05:20:33', '-7.284055', '112.80539'),
('K160719001', 'DOC12', 1, 'Rumah mbokmu', '2016-11-15 00:00:00', 'Rudiment antar', 'Makassar', '78040126', '2016-11-15 07:11:34', '-7.295662', '112.77979'),
('K160627001', 'DOC12', 19, 'Uf', '2016-11-25 11:32:31', 'Uuc', 'Cyc', '78040126', '2016-11-25 10:32:31', '-7.288718', '112.80932'),
('K160624001', 'DOC12', 33, 'Jznzn', '2016-11-25 12:45:48', 'Zjznns', 'Sjsbzn', '78040126', '2016-11-25 11:45:48', '-7.289007', '112.809296'),
('K160716002', 'DOC12', 3, 'Lokasi', '2016-11-25 12:50:14', 'Agung Imamudin', 'Apamat', '78040126', '2016-11-25 11:50:14', '-7.288762', '112.809296');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_interogasi_detail`
--

CREATE TABLE `tb_ba_interogasi_detail` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC04',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `PERTANYAAN` varchar(1000) NOT NULL,
  `JAWABAN` varchar(2000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_interogasi_detail`
--

INSERT INTO `tb_ba_interogasi_detail` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `PERTANYAAN`, `JAWABAN`, `tgl_masuk_data`) VALUES
('K160624001', 'DOC14', 1, 2, 'tanyaq', 'jawab2', '2016-10-21 11:17:25'),
('K160624001', 'DOC14', 1, 1, 'tanya', 'jawab', '2016-10-21 11:17:25'),
('K160624001', 'DOC14', 2, 1, 'tanya 1 ', 'jawab 1', '2016-10-21 11:26:00'),
('K160624001', 'DOC14', 2, 2, 'tanya 2', 'jawab 2', '2016-10-21 11:26:00'),
('K160624001', 'DOC14', 3, 1, 'tanya 1 ', 'jawab 1', '2016-10-21 11:27:09'),
('K160624001', 'DOC14', 3, 2, 'tanya 2', 'jawab 2', '2016-10-21 11:27:09'),
('K160624001', 'DOC14', 4, 1, 'tanya 1 ', 'jawab 1', '2016-10-21 11:27:32'),
('K160624001', 'DOC14', 4, 2, 'tanya 2', 'jawab 2', '2016-10-21 11:27:32'),
('K160624001', 'DOC14', 5, 1, 'tanya 1 ', 'jawab 1', '2016-10-21 11:31:24'),
('K160624001', 'DOC14', 5, 2, 'tanya 2', 'jawab 2', '2016-10-21 11:31:24'),
('K160624001', 'DOC14', 6, 1, 'tanya 1 ', 'jawab 1', '2016-10-21 11:33:11'),
('K160624001', 'DOC14', 6, 2, 'tanya 2', 'jawab 2', '2016-10-21 11:33:11'),
('K160624001', 'DOC14', 7, 1, 'tanya', 'jawab', '2016-10-21 11:34:13'),
('K160624001', 'DOC14', 7, 2, 'tanyaq', 'jawab2', '2016-10-21 11:34:13'),
('K160624001', 'DOC14', 8, 1, 'tanya', 'jawab', '2016-10-21 11:35:02'),
('K160624001', 'DOC14', 8, 2, 'tanyaq', 'jawab2', '2016-10-21 11:35:02'),
('K160624001', 'DOC14', 9, 1, 'tanya', 'jawab', '2016-10-21 11:35:36'),
('K160624001', 'DOC14', 9, 2, 'tanyaq', 'jawab2', '2016-10-21 11:35:36'),
('K160624001', 'DOC14', 10, 1, 'tanya', 'jawab', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 10, 2, 'tanyaq', 'jawab2', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 11, 1, 'Mengapa kejadian terjadi', 'kurang tahu', '2016-10-23 09:14:35'),
('K160624001', 'DOC14', 11, 2, 'vzbana', 'hsms xbz', '2016-10-23 09:14:35'),
('K160624001', 'DOC14', 12, 1, 'mengapa bisa terjadi?', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:28:22'),
('K160624001', 'DOC14', 12, 2, 'vsjanambs sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:28:22'),
('K160624001', 'DOC14', 13, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:30:43'),
('K160624001', 'DOC14', 13, 2, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:30:43'),
('K160624001', 'DOC14', 14, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:37:34'),
('K160624001', 'DOC14', 14, 2, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:37:34'),
('K160624001', 'DOC14', 15, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:45:41'),
('K160624001', 'DOC14', 15, 2, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 09:45:41'),
('K160624001', 'DOC14', 16, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 10:29:26'),
('K160624001', 'DOC14', 17, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-23 12:18:12'),
('K160624001', 'DOC14', 18, 1, 'ex', 'ds', '2016-10-23 14:53:15'),
('K160624001', 'DOC14', 19, 1, 'zz', 'dns', '2016-10-23 14:55:32'),
('K160624001', 'DOC14', 20, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-24 00:28:28'),
('K160624001', 'DOC14', 21, 1, 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', 'sgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sjsgsja sjssn sjshsns shsjsn shjs sk sjs sjss shsjs shsks shsks sjjs shsbs hs sjs sjs sj', '2016-10-24 00:31:53'),
('K160716001', 'DOC14', 1, 1, 'tanya', 'jawab', '2016-10-25 13:12:49'),
('K160627001', 'DOC14', 1, 1, 'tanya', 'jawab', '2016-10-25 13:23:36'),
('K160910003', 'DOC14', 1, 1, 'tanya', 'alamat', '2016-10-25 13:27:54'),
('K160910003', 'DOC14', 2, 1, 'tanya', 'alamat', '2016-10-25 13:30:14'),
('K160716002', 'DOC14', 1, 1, 'tanya', 'jawab', '2016-10-25 13:44:35'),
('K160624001', 'DOC14', 22, 1, 'tanya', 'jawab', '2016-10-26 03:29:34'),
('K160627001', 'DOC14', 2, 1, 'bj', 'fjj', '2016-10-26 03:31:31'),
('K160624001', 'DOC14', 23, 1, 'pertamyaan', 'jawaban', '2016-10-26 05:14:50'),
('K160624001', 'DOC14', 24, 1, 'hsbsbxkz', 'hsksns', '2016-10-26 05:23:25'),
('K160624001', 'DOC14', 25, 1, 'tanya', 'jawab', '2016-10-26 06:35:02'),
('K160922002', 'DOC14', 1, 1, 'tanya', 'jawab', '2016-10-26 06:37:52'),
('K160622001', 'DOC14', 1, 1, 'tanya', 'jawab', '2016-10-26 06:39:03'),
('K160910003', 'DOC14', 3, 1, 'ada di tkp ?', 'oke ada di tkp saat ini sudah berada di lokasi', '2016-10-26 06:42:50'),
('K160910003', 'DOC14', 3, 2, 'berapa lama ?', 'iya selama 1 jam', '2016-10-26 06:42:50'),
('K160627001', 'DOC14', 3, 1, 'tanya', 'ahaba', '2016-10-26 13:18:34'),
('K160923001', 'DOC14', 1, 1, 'tanya', 'imanan', '2016-10-26 13:23:39'),
('K160716002', 'DOC14', 2, 1, 'bzbz', 'gsjsns', '2016-10-26 13:41:14'),
('K160624001', 'DOC14', 26, 1, 'Mengapa kejadian bisa terjadi?', 'Pelaku tidak menjelaskan', '2016-10-04 23:17:05'),
('K160910002', 'DOC14', 1, 1, 'kenapa?', 'saya tidak tahu', '2016-11-05 01:00:16'),
('K160716001', 'DOC14', 2, 1, 'pertanyaan 1', 'pertanyaan 2', '2016-11-07 05:19:04'),
('K160624001', 'DOC12', 27, 1, '5an', 'Sbnka', '2016-11-11 10:07:17'),
('K160624001', 'DOC12', 28, 1, 'Gjv', 'Gjb', '2016-11-11 10:09:44'),
('K160627001', 'DOC12', 4, 1, 'N j', 'Jcjvj', '2016-11-11 10:12:37'),
('K160627001', 'DOC12', 5, 1, 'N j', 'Jcjvj', '2016-11-11 10:13:15'),
('K160627001', 'DOC12', 6, 1, 'N j', 'Jcjvj', '2016-11-11 10:26:33'),
('K160627001', 'DOC12', 7, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:23'),
('K160627001', 'DOC12', 8, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:31'),
('K160627001', 'DOC12', 9, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:44'),
('K160627001', 'DOC12', 10, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:48'),
('K160627001', 'DOC12', 11, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:52'),
('K160627001', 'DOC12', 12, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:53'),
('K160627001', 'DOC12', 13, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:53'),
('K160627001', 'DOC12', 14, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:54'),
('K160627001', 'DOC12', 15, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:54'),
('K160627001', 'DOC12', 16, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:54'),
('K160627001', 'DOC12', 17, 1, 'Tanya', 'Jawabab', '2016-11-12 09:00:55'),
('K160627001', 'DOC12', 18, 1, 'Tanya', 'Jawabab', '2016-11-12 09:01:04'),
('K160624001', 'DOC12', 29, 1, 'Pertanyaan', 'Jawan', '2016-11-14 05:17:44'),
('K160624001', 'DOC12', 30, 1, 'Pertanyaan', 'Jawan', '2016-11-14 05:18:42'),
('K160624001', 'DOC12', 31, 1, 'Pertanyaan', 'Jawan', '2016-11-14 05:20:18'),
('K160624001', 'DOC12', 32, 1, 'Pertanyaan', 'Jawan', '2016-11-14 05:20:33'),
('K160719001', 'DOC12', 1, 1, 'Hair Kamu keapa', 'Oyi Jeh manyus', '2016-11-15 07:11:34'),
('K160719001', 'DOC12', 1, 2, 'Terus GMn ?', 'Terserah deh', '2016-11-15 07:11:34'),
('K160627001', 'DOC12', 19, 1, 'Cj', 'Jcjc', '2016-11-25 10:32:31'),
('K160624001', 'DOC12', 33, 1, 'Jsnbs', 'Hajss', '2016-11-25 11:45:48'),
('K160716002', 'DOC12', 3, 1, 'Tanya', 'Jawaban', '2016-11-25 11:50:14'),
('K160716002', 'DOC12', 3, 2, 'Hsnsbsk sjajs shs susbshs hs', 'Hs s usbsuhsubs hsbsuhs shsb', '2016-11-25 11:50:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_interogasi_gambar`
--

CREATE TABLE `tb_ba_interogasi_gambar` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `GAMBAR` varchar(200) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_interogasi_gambar`
--

INSERT INTO `tb_ba_interogasi_gambar` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `GAMBAR`, `tgl_masuk_data`) VALUES
('K160622001', 'DOC14', 1, 1, 'DOC14_2016-10-26_1_K160622001_1.jpeg', '2016-10-26 06:39:03'),
('K160624001', 'DOC12', 33, 1, 'DOC12_2016-11-25 12:45:48_33_K160624001_1.jpeg', '2016-11-25 11:45:48'),
('K160624001', 'DOC14', 1, 1, 'DOC14_2016-10-21_1_K160624001_1.png', '2016-10-21 11:17:25'),
('K160624001', 'DOC14', 1, 2, 'DOC14_2016-10-21_1_K160624001_2.png', '2016-10-21 11:17:25'),
('K160624001', 'DOC14', 2, 1, 'DOC14_2016-10-21_2_K160624001_1.png', '2016-10-21 11:26:00'),
('K160624001', 'DOC14', 3, 1, 'DOC14_2016-10-21_3_K160624001_1.png', '2016-10-21 11:27:09'),
('K160624001', 'DOC14', 4, 1, 'DOC14_2016-10-21_4_K160624001_1.png', '2016-10-21 11:27:32'),
('K160624001', 'DOC14', 5, 1, 'DOC12_2016-11-12_9_K160627001_1.jpeg', '2016-11-14 09:50:28'),
('K160624001', 'DOC14', 6, 1, 'DOC14_2016-10-21_6_K160624001_1.png', '2016-10-21 11:33:11'),
('K160624001', 'DOC14', 7, 1, 'DOC14_2016-10-21_7_K160624001_1.png', '2016-10-21 11:34:13'),
('K160624001', 'DOC14', 8, 1, 'DOC14_2016-10-21_8_K160624001_1.jpeg', '2016-10-21 11:35:02'),
('K160624001', 'DOC14', 9, 1, 'DOC14_2016-10-21_9_K160624001_1.jpeg', '2016-10-21 11:35:36'),
('K160624001', 'DOC14', 9, 2, 'DOC14_2016-10-21_9_K160624001_2.jpeg', '2016-10-21 11:35:36'),
('K160624001', 'DOC14', 10, 1, 'DOC14_2016-10-21_10_K160624001_1.jpeg', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 10, 2, 'DOC14_2016-10-21_10_K160624001_2.jpeg', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 10, 3, 'DOC14_2016-10-21_10_K160624001_3.jpeg', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 10, 4, 'DOC14_2016-10-21_10_K160624001_4.jpeg', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 10, 5, 'DOC14_2016-10-21_10_K160624001_5.jpeg', '2016-10-21 11:36:29'),
('K160624001', 'DOC14', 11, 1, 'DOC14_2016-10-23_11_K160624001_1.jpeg', '2016-10-23 09:14:35'),
('K160624001', 'DOC14', 12, 1, 'DOC14_2016-10-23_12_K160624001_1.jpeg', '2016-10-23 09:28:22'),
('K160624001', 'DOC14', 12, 2, 'DOC14_2016-10-23_12_K160624001_2.jpeg', '2016-10-23 09:28:22'),
('K160624001', 'DOC14', 13, 1, 'DOC14_2016-10-23_13_K160624001_1.jpeg', '2016-10-23 09:30:43'),
('K160624001', 'DOC14', 13, 2, 'DOC14_2016-10-23_13_K160624001_2.jpeg', '2016-10-23 09:30:43'),
('K160624001', 'DOC14', 14, 1, 'DOC14_2016-10-23_14_K160624001_1.jpeg', '2016-10-23 09:37:34'),
('K160624001', 'DOC14', 14, 2, 'DOC14_2016-10-23_14_K160624001_2.jpeg', '2016-10-23 09:37:35'),
('K160624001', 'DOC14', 15, 1, 'DOC14_2016-10-23_15_K160624001_1.jpeg', '2016-10-23 09:45:41'),
('K160624001', 'DOC14', 15, 2, 'DOC14_2016-10-23_15_K160624001_2.jpeg', '2016-10-23 09:45:41'),
('K160624001', 'DOC14', 16, 1, 'DOC14_2016-10-23_16_K160624001_1.jpeg', '2016-10-23 10:29:26'),
('K160624001', 'DOC14', 16, 2, 'DOC14_2016-10-23_16_K160624001_2.jpeg', '2016-10-23 10:29:26'),
('K160624001', 'DOC14', 17, 1, 'DOC14_2016-10-23_17_K160624001_1.jpeg', '2016-10-23 12:18:12'),
('K160624001', 'DOC14', 17, 2, 'DOC14_2016-10-23_17_K160624001_2.jpeg', '2016-10-23 12:18:13'),
('K160624001', 'DOC14', 18, 1, 'DOC14_2016-10-23_18_K160624001_1.jpeg', '2016-10-23 14:53:15'),
('K160624001', 'DOC14', 19, 1, 'DOC14_2016-10-23_19_K160624001_1.jpeg', '2016-10-23 14:55:32'),
('K160624001', 'DOC14', 20, 1, 'DOC14_2016-10-24_20_K160624001_1.jpeg', '2016-10-24 00:28:28'),
('K160624001', 'DOC14', 21, 1, 'DOC14_2016-10-24_21_K160624001_1.jpeg', '2016-10-24 00:31:53'),
('K160624001', 'DOC14', 22, 1, 'DOC14_2016-10-26_22_K160624001_1.jpeg', '2016-10-26 03:29:35'),
('K160624001', 'DOC14', 23, 1, 'DOC14_2016-10-26_23_K160624001_1.jpeg', '2016-10-26 05:14:50'),
('K160624001', 'DOC14', 24, 1, 'DOC14_2016-10-26_24_K160624001_1.jpeg', '2016-10-26 05:23:25'),
('K160624001', 'DOC14', 24, 2, 'DOC14_2016-10-26_24_K160624001_2.jpeg', '2016-10-26 05:23:25'),
('K160624001', 'DOC14', 24, 3, 'DOC14_2016-10-26_24_K160624001_3.jpeg', '2016-10-26 05:23:25'),
('K160624001', 'DOC14', 24, 4, 'DOC14_2016-10-26_24_K160624001_4.jpeg', '2016-10-26 05:23:25'),
('K160624001', 'DOC14', 24, 5, 'DOC14_2016-10-26_24_K160624001_5.jpeg', '2016-10-26 05:23:25'),
('K160624001', 'DOC14', 25, 1, 'DOC14_2016-10-26_25_K160624001_1.jpeg', '2016-10-26 06:35:02'),
('K160624001', 'DOC14', 25, 2, 'DOC14_2016-10-26_25_K160624001_2.jpeg', '2016-10-26 06:35:02'),
('K160624001', 'DOC14', 26, 1, 'DOC14_2016-10-05_26_K160624001_1.jpeg', '2016-10-04 23:17:05'),
('K160627001', 'DOC12', 7, 1, 'DOC12_2016-11-12_7_K160627001_1.jpeg', '2016-11-12 09:00:23'),
('K160627001', 'DOC12', 8, 1, 'DOC12_2016-11-12_8_K160627001_1.jpeg', '2016-11-12 09:00:31'),
('K160627001', 'DOC12', 9, 1, 'DOC12_2016-11-12_9_K160627001_1.jpeg', '2016-11-12 09:00:44'),
('K160627001', 'DOC12', 19, 1, 'DOC12_2016-11-25_19_K160627001_1.jpeg', '2016-11-25 12:48:40'),
('K160627001', 'DOC14', 2, 1, 'DOC14_2016-10-26_2_K160627001_1.jpeg', '2016-10-26 03:31:31'),
('K160627001', 'DOC14', 3, 1, 'DOC14_2016-10-26_3_K160627001_1.jpeg', '2016-10-26 13:18:34'),
('K160716001', 'DOC14', 1, 1, 'DOC14_2016-10-25_1_K160716001_1.jpeg', '2016-10-25 13:12:49'),
('K160716001', 'DOC14', 2, 1, 'DOC14_2016-11-07_2_K160716001_1.jpeg', '2016-11-07 05:19:04'),
('K160716002', 'DOC12', 3, 1, 'DOC12_2016-11-25_3_K160716002_1.jpeg', '2016-11-25 12:48:51'),
('K160716002', 'DOC12', 3, 2, 'DOC12_2016-11-25_3_K160716002_2.jpeg', '2016-11-25 12:49:04'),
('K160716002', 'DOC12', 3, 3, 'DOC12_2016-11-25_3_K160716002_3.jpeg', '2016-11-25 12:49:13'),
('K160716002', 'DOC14', 1, 1, 'DOC14_2016-10-25_1_K160716002_1.jpeg', '2016-10-25 13:44:35'),
('K160716002', 'DOC14', 2, 1, 'DOC14_2016-10-26_2_K160716002_1.jpeg', '2016-10-26 13:41:14'),
('K160716002', 'DOC14', 2, 2, 'DOC14_2016-10-26_2_K160716002_2.jpeg', '2016-10-26 13:41:14'),
('K160716002', 'DOC14', 2, 3, 'DOC14_2016-10-26_2_K160716002_3.jpeg', '2016-10-26 13:41:14'),
('K160716002', 'DOC14', 2, 4, 'DOC14_2016-10-26_2_K160716002_4.jpeg', '2016-10-26 13:41:14'),
('K160719001', 'DOC12', 1, 1, 'DOC12_2016-11-15_1_K160719001_1.jpeg', '2016-11-15 07:11:34'),
('K160719001', 'DOC12', 1, 2, 'DOC12_2016-11-15_1_K160719001_2.jpeg', '2016-11-15 07:11:34'),
('K160910002', 'DOC14', 1, 1, 'DOC14_2016-11-05_1_K160910002_1.jpeg', '2016-11-05 01:00:16'),
('K160910002', 'DOC14', 1, 2, 'DOC14_2016-11-05_1_K160910002_2.jpeg', '2016-11-05 01:00:16'),
('K160910003', 'DOC14', 1, 1, 'DOC14_2016-10-25_1_K160910003_1.jpeg', '2016-10-25 13:27:54'),
('K160910003', 'DOC14', 2, 1, 'DOC14_2016-10-25_2_K160910003_1.jpeg', '2016-10-25 13:30:14'),
('K160910003', 'DOC14', 3, 1, 'DOC14_2016-10-26_3_K160910003_1.jpeg', '2016-10-26 06:42:50'),
('K160910003', 'DOC14', 3, 2, 'DOC14_2016-10-26_3_K160910003_2.jpeg', '2016-10-26 06:42:50'),
('K160922002', 'DOC14', 1, 1, 'DOC14_2016-10-26_1_K160922002_1.jpeg', '2016-10-26 06:37:52'),
('K160923001', 'DOC14', 1, 1, 'DOC14_2016-10-26_1_K160923001_1.jpeg', '2016-10-26 13:23:39');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_lit_dokumen`
--

CREATE TABLE `tb_ba_lit_dokumen` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC04',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `LOKASI` varchar(200) NOT NULL,
  `TGL_BA` datetime NOT NULL,
  `DASAR` varchar(200) NOT NULL,
  `SASARAN_BARANG` varchar(500) DEFAULT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_lit_dokumen`
--

INSERT INTO `tb_ba_lit_dokumen` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `LOKASI`, `TGL_BA`, `DASAR`, `SASARAN_BARANG`, `NRP`, `tgl_masuk_data`, `LATITUDE`, `LONGITUDE`) VALUES
('K160716002', 'DOC16', 1, '', '2016-11-08 00:00:00', 'dasar penelitin', 'sasaran barang', '76040065', '2016-11-08 05:41:28', '-7.2889863', '112.8093063'),
('K160923002', 'DOC16', 1, '', '2016-11-08 00:00:00', 'dasar berita acara', 'sasaran barang', '76040065', '2016-11-08 05:42:31', '-7.2881909', '112.8115621'),
('K160716002', 'DOC16', 2, '', '2016-11-08 00:00:00', 'sadjbs', 'sasarab barang', '76040065', '2016-11-08 05:44:45', '-7.2887251', '112.8093229'),
('K160716002', 'DOC16', 3, '', '2016-11-08 00:00:00', 'fasar', 'sasaran', '76040065', '2016-11-08 05:48:17', '-7.2887235', '112.8093241'),
('K160716001', 'DOC16', 1, '', '2016-11-12 00:00:00', 'Dasar', 'Sasaran barang', '78040126', '2016-11-12 08:59:18', '-7.2887216', '112.8093236'),
('K160716001', 'DOC16', 2, '', '2016-11-14 00:00:00', 'Dasar', 'Sasaran', '78040126', '2016-11-14 05:27:27', '-7.288723', '112.80932'),
('K160716001', 'DOC16', 3, '', '2016-11-14 00:00:00', 'Dasar', 'Sasaran', '78040126', '2016-11-14 05:27:33', '-7.288723', '112.80932'),
('K160716001', 'DOC16', 4, '', '2016-11-14 00:00:00', 'Dasar', 'Sasaran', '78040126', '2016-11-14 05:28:00', '-7.288723', '112.80932'),
('K160716001', 'DOC16', 5, '', '2016-11-14 00:00:00', 'Dasar', 'Sasaran', '78040126', '2016-11-14 05:29:44', '-7.288723', '112.80932'),
('K160716001', 'DOC16', 6, '', '2016-11-14 00:00:00', 'Dasar', 'Sasaran', '78040126', '2016-11-14 05:30:04', '-7.288723', '112.80932'),
('K160627001', 'DOC16', 1, '', '2016-11-14 00:00:00', 'Dasar', 'Sasara', '78040126', '2016-11-14 05:31:07', '-7.288722', '112.809326'),
('K160627001', 'DOC16', 2, '', '2016-11-21 00:00:00', 'Dasar', 'Sasaran barang', '78040126', '2016-11-21 05:56:33', '-7.296535', '112.78058');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_lit_dokumen_detail`
--

CREATE TABLE `tb_ba_lit_dokumen_detail` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC04',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `TEMUAN` varchar(1000) NOT NULL,
  `ANALISA` varchar(2000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_lit_dokumen_detail`
--

INSERT INTO `tb_ba_lit_dokumen_detail` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `TEMUAN`, `ANALISA`, `tgl_masuk_data`) VALUES
('K160716002', 'DOC16', 1, 1, 'hsvaj a', 'hasil analisa', '2016-11-08 05:41:28'),
('K160923002', 'DOC16', 1, 1, 'tuan', 'dmndjna', '2016-11-08 05:42:31'),
('K160716002', 'DOC16', 2, 1, 'banbs', 'gskns', '2016-11-08 05:44:45'),
('K160716002', 'DOC16', 3, 1, 'hcnnz', 'fjvn', '2016-11-08 05:48:17'),
('K160716001', 'DOC16', 1, 1, 'Temuan', 'Analisa', '2016-11-12 08:59:18'),
('K160716001', 'DOC16', 2, 1, 'Bnvb', 'Hjjvb', '2016-11-14 05:27:27'),
('K160716001', 'DOC16', 3, 1, 'Bnvb', 'Hjjvb', '2016-11-14 05:27:33'),
('K160716001', 'DOC16', 4, 1, 'Bnvb', 'Hjjvb', '2016-11-14 05:28:00'),
('K160716001', 'DOC16', 5, 1, 'Bnvb', 'Hjjvb', '2016-11-14 05:29:44'),
('K160716001', 'DOC16', 6, 1, 'Bnvb', 'Hjjvb', '2016-11-14 05:30:04'),
('K160627001', 'DOC16', 1, 1, 'Chbb', 'Xbb', '2016-11-14 05:31:07'),
('K160627001', 'DOC16', 2, 1, 'Temuan', 'Analisa', '2016-11-21 05:56:33');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_lit_dokumen_gambar`
--

CREATE TABLE `tb_ba_lit_dokumen_gambar` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `GAMBAR` varchar(200) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_lit_dokumen_gambar`
--

INSERT INTO `tb_ba_lit_dokumen_gambar` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `GAMBAR`, `tgl_masuk_data`) VALUES
('K160716002', 'DOC16', 2, 1, 'DOC16_2016-11-08_2_K160716002_1.jpeg', '2016-11-08 05:44:45'),
('K160716002', 'DOC16', 3, 1, 'DOC16_2016-11-08_3_K160716002_1.jpeg', '2016-11-08 05:48:17');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_observasi`
--

CREATE TABLE `tb_ba_observasi` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC03',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `DASAR` varchar(200) NOT NULL,
  `TGL_BA` datetime NOT NULL,
  `SASARAN_TEMPAT` varchar(500) NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_observasi`
--

INSERT INTO `tb_ba_observasi` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `DASAR`, `TGL_BA`, `SASARAN_TEMPAT`, `NRP`, `tgl_masuk_data`, `LATITUDE`, `LONGITUDE`) VALUES
('K160716001', 'DOC11', 1, 'dasar berita acara', '2016-11-07 00:00:00', 'sasarab tempat', '76040065', '2016-11-07 06:55:16', '-7.2826492', '112.8057185'),
('K160716001', 'DOC11', 2, 'dasar', '2016-11-07 00:00:00', 'sasaran tempatku', '76040065', '2016-11-07 06:59:16', '-7.2887241', '112.8093258'),
('K160622001', 'DOC11', 1, 'addhc', '2016-11-07 00:00:00', 'zhvjvnvnv', '76040065', '2016-11-07 07:05:45', '-7.2887231', '112.8093219'),
('K160716001', 'DOC11', 3, 'Dasar', '2016-11-14 00:00:00', 'Sasata', '78040126', '2016-11-14 05:35:59', '-7.288723', '112.809326'),
('K160923004', 'DOC11', 1, 'Dasar', '2016-11-20 00:00:00', 'Sasaran tempat', '78040126', '2016-11-20 14:40:12', '-7.278881', '112.80919'),
('K160624001', 'DOC11', 1, 'Ghgj', '2016-11-21 00:00:00', 'Hh', '78040126', '2016-11-21 02:07:07', '-7.288716', '112.8093'),
('K160627001', 'DOC11', 1, 'Dasar', '2016-11-21 00:00:00', 'Sasaran tempat', '78040126', '2016-11-21 14:04:39', '-7.288762', '112.809296');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_observasi_detail`
--

CREATE TABLE `tb_ba_observasi_detail` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC03',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `INFORMASI` varchar(1000) NOT NULL,
  `KELENGKAPAN` varchar(2000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_observasi_detail`
--

INSERT INTO `tb_ba_observasi_detail` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `INFORMASI`, `KELENGKAPAN`, `tgl_masuk_data`) VALUES
('K160716001', 'DOC11', 1, 1, 'informasi tempat', 'kelengkapan', '2016-11-07 06:55:16'),
('K160716001', 'DOC11', 2, 1, 'hkbgn nkvb', 'tjhv uigh', '2016-11-07 06:59:16'),
('K160622001', 'DOC11', 1, 1, 'bcbvbchxgch', 'dhchj', '2016-11-07 07:05:45'),
('K160716001', 'DOC11', 3, 1, 'Vb', 'Vnbv', '2016-11-14 05:35:59'),
('K160923004', 'DOC11', 1, 1, 'Surabaya', 'Tidak tahu', '2016-11-20 14:40:12'),
('K160923004', 'DOC11', 1, 2, 'Sidoarjo', 'Gagasan', '2016-11-20 14:40:12'),
('K160624001', 'DOC11', 1, 1, 'Sgc', 'Fjc', '2016-11-21 02:07:07'),
('K160624001', 'DOC11', 1, 2, 'Gcnn', 'Hjck', '2016-11-21 02:07:07'),
('K160627001', 'DOC11', 1, 1, 'Informasi tempat', 'Gagasan', '2016-11-21 14:04:39');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_observasi_gambar`
--

CREATE TABLE `tb_ba_observasi_gambar` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `GAMBAR` varchar(200) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_observasi_gambar`
--

INSERT INTO `tb_ba_observasi_gambar` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `GAMBAR`, `tgl_masuk_data`) VALUES
('K160622001', 'DOC11', 1, 1, 'DOC11_2016-11-07_1_K160622001_1.jpeg', '2016-11-07 07:05:45'),
('K160716001', 'DOC11', 1, 1, 'DOC11_2016-11-07_1_K160716001_1.jpeg', '2016-11-07 06:55:16'),
('K160716001', 'DOC11', 2, 1, 'DOC11_2016-11-07_2_K160716001_1.jpeg', '2016-11-07 06:59:16'),
('K160923004', 'DOC11', 1, 1, 'DOC11_2016-11-20_1_K160923004_1.jpeg', '2016-11-20 14:40:12'),
('K160923004', 'DOC11', 1, 2, 'DOC11_2016-11-20_1_K160923004_2.jpeg', '2016-11-20 14:40:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_pembuntutan`
--

CREATE TABLE `tb_ba_pembuntutan` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC05',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `DASAR` varchar(200) NOT NULL,
  `TGL_BA` datetime NOT NULL,
  `NAMA_LENGKAP` varchar(100) NOT NULL,
  `ALAMAT` varchar(200) DEFAULT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_pembuntutan`
--

INSERT INTO `tb_ba_pembuntutan` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `DASAR`, `TGL_BA`, `NAMA_LENGKAP`, `ALAMAT`, `NRP`, `tgl_masuk_data`, `LATITUDE`, `LONGITUDE`) VALUES
('K160716001', 'DOC13', 1, 'dashfgv', '2016-11-07 00:00:00', 'agung dari Thaybah', 'alamat Pak Lurah', '76040065', '2016-11-07 15:25:42', '-7.2887256', '112.8093242'),
('K160923001', 'DOC13', 1, 'dasar pembuntutan', '2016-11-07 00:00:00', 'agung Abinya', 'tuban', '76040065', '2016-11-07 15:29:32', '-7.2887229', '112.8093285'),
('K160923002', 'DOC13', 1, 'dasar imana', '2016-11-07 00:00:00', 'agung Abinya', 'tuban', '76040065', '2016-11-07 15:32:57', '-7.2830263', '112.8060558'),
('K160719001', 'DOC13', 1, 'dasar', '2016-11-07 00:00:00', 'nama sasaran', 'alamat sasaran', '76040065', '2016-11-07 15:39:18', '-7.2883976', '112.810156'),
('K160627001', 'DOC13', 1, 'Dasar', '2016-11-14 00:00:00', 'Sasata', 'Alamay', '78040126', '2016-11-14 05:36:44', '-7.288762', '112.809296');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_pembuntutan_detail`
--

CREATE TABLE `tb_ba_pembuntutan_detail` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC05',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_POSISI` int(2) NOT NULL,
  `POSISI` varchar(1000) NOT NULL,
  `INFORMASI` varchar(2000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_pembuntutan_detail`
--

INSERT INTO `tb_ba_pembuntutan_detail` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_POSISI`, `POSISI`, `INFORMASI`, `tgl_masuk_data`) VALUES
('K160716001', 'DOC13', 1, 1, 'posisi 1', 'informasi lebih baik', '2016-11-07 15:25:42'),
('K160923001', 'DOC13', 1, 1, 'jalananraya', 'Kurang tau', '2016-11-07 15:29:32'),
('K160923002', 'DOC13', 1, 1, 'posisi', 'informasi yg didapat', '2016-11-07 15:32:57'),
('K160719001', 'DOC13', 1, 1, 'posisi', 'banyak sekali', '2016-11-07 15:39:18'),
('K160627001', 'DOC13', 1, 1, 'Cn bn', 'Dhvb', '2016-11-14 05:36:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_pembuntutan_gambar`
--

CREATE TABLE `tb_ba_pembuntutan_gambar` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `GAMBAR` varchar(200) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_pembuntutan_gambar`
--

INSERT INTO `tb_ba_pembuntutan_gambar` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `GAMBAR`, `tgl_masuk_data`) VALUES
('K160719001', 'DOC13', 1, 1, 'DOC13_2016-11-07_1_K160719001_1.jpeg', '2016-11-07 15:39:18'),
('K160923001', 'DOC13', 1, 1, 'DOC13_2016-11-07_1_K160923001_1.jpeg', '2016-11-07 15:29:32'),
('K160923002', 'DOC13', 1, 1, 'DOC13_2016-11-07_1_K160923002_1.jpeg', '2016-11-07 15:32:57');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_penyamaran`
--

CREATE TABLE `tb_ba_penyamaran` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC06',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `DASAR` varchar(200) NOT NULL,
  `TGL_BA` datetime NOT NULL,
  `SASARAN_TEMPAT` varchar(500) NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_penyamaran_detail`
--

CREATE TABLE `tb_ba_penyamaran_detail` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC06',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `IDENTITAS` varchar(1000) NOT NULL,
  `INFORMASI` varchar(2000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_undercover`
--

CREATE TABLE `tb_ba_undercover` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC03',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `DASAR` varchar(200) NOT NULL,
  `TGL_BA` datetime NOT NULL,
  `SASARAN_TEMPAT` varchar(500) NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_undercover`
--

INSERT INTO `tb_ba_undercover` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `DASAR`, `TGL_BA`, `SASARAN_TEMPAT`, `NRP`, `tgl_masuk_data`, `LATITUDE`, `LONGITUDE`) VALUES
('K160624001', 'DOC14', 1, 'Dadar', '2016-11-21 00:00:00', 'Sasaran tempat', '78040126', '2016-11-21 14:10:09', '-7.288719', '112.8093'),
('K160716001', 'DOC14', 1, 'Dasar', '2016-11-21 00:00:00', 'Sasaran tempat', '78040126', '2016-11-21 14:11:01', '-7.288762', '112.809296'),
('K160910001', 'DOC14', 1, 'Dasar', '2016-11-21 00:00:00', 'Sasaran', '78040126', '2016-11-21 14:15:46', '-7.288716', '112.8093');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_undercover_detail`
--

CREATE TABLE `tb_ba_undercover_detail` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL DEFAULT 'DOC03',
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `IDENTITAS` varchar(1000) NOT NULL,
  `INFORMASI` varchar(2000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_undercover_detail`
--

INSERT INTO `tb_ba_undercover_detail` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `IDENTITAS`, `INFORMASI`, `tgl_masuk_data`) VALUES
('K160624001', 'DOC14', 1, 1, 'Identitad', 'Identitad', '2016-11-21 14:10:09'),
('K160716001', 'DOC14', 1, 1, 'Identitas', 'Identitas', '2016-11-21 14:11:01'),
('K160910001', 'DOC14', 1, 1, 'Identitas', 'Identitas', '2016-11-21 14:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_ba_undercover_gambar`
--

CREATE TABLE `tb_ba_undercover_gambar` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `NO_URUT` int(2) NOT NULL,
  `GAMBAR` varchar(200) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_ba_undercover_gambar`
--

INSERT INTO `tb_ba_undercover_gambar` (`ID_KASUS`, `ID_DOC_OPSNAL`, `BERKAS_KE`, `NO_URUT`, `GAMBAR`, `tgl_masuk_data`) VALUES
('K160910001', 'DOC14', 1, 1, 'DOC14_2016-11-21_1_K160910001_1.jpeg', '2016-11-21 14:15:46');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bobot_kasus`
--

CREATE TABLE `tb_bobot_kasus` (
  `ID_BOBOT` varchar(5) NOT NULL,
  `NAMA_BOBOT` varchar(50) NOT NULL,
  `DURASI` int(3) NOT NULL DEFAULT '0',
  `DUPLIKASI_POINT` decimal(3,2) NOT NULL DEFAULT '0.00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_bobot_kasus`
--

INSERT INTO `tb_bobot_kasus` (`ID_BOBOT`, `NAMA_BOBOT`, `DURASI`, `DUPLIKASI_POINT`) VALUES
('BOB01', 'MUDAH', 30, '1.00'),
('BOB02', 'SEDANG', 60, '1.50'),
('BOB03', 'SULIT', 90, '3.00'),
('BOB04', 'SANGAT SULIT', 120, '4.00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bobot_kasus_kanit`
--

CREATE TABLE `tb_bobot_kasus_kanit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_PERUBAHAN` date NOT NULL,
  `ID_BOBOT_ASAL` varchar(5) NOT NULL,
  `DURASI_ASAL` int(5) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `DURASI` int(5) NOT NULL,
  `PATH` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bobot_kasus_kasat`
--

CREATE TABLE `tb_bobot_kasus_kasat` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_PERUBAHAN` date NOT NULL,
  `ID_BOBOT_ASAL` varchar(5) NOT NULL,
  `DURASI_ASAL` int(5) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `DURASI` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_bobot_kasus_spkt`
--

CREATE TABLE `tb_bobot_kasus_spkt` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_PERUBAHAN` date NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `DURASI` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_catatan_kriminal`
--

CREATE TABLE `tb_catatan_kriminal` (
  `ID_KASUS` varchar(10) NOT NULL DEFAULT '0',
  `ID_PELAKU` varchar(10) NOT NULL DEFAULT '0',
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_catatan_kriminal`
--

INSERT INTO `tb_catatan_kriminal` (`ID_KASUS`, `ID_PELAKU`, `ID_DOC_BERKAS`, `BERKAS_KE`, `tgl_masuk_data`) VALUES
('K160622001', 'P160622001', NULL, NULL, '2016-06-22 06:30:37'),
('K160624001', 'P160624001', NULL, NULL, '2016-06-24 01:41:03'),
('K160627001', 'P160627001', NULL, NULL, '2016-06-27 14:31:39'),
('K160716001', 'P160627001', NULL, NULL, '2016-07-16 05:26:00'),
('K160716001', 'P160624001', NULL, NULL, '2016-07-16 05:26:55'),
('K160716002', 'P160624001', NULL, NULL, '2016-07-16 05:28:48'),
('K160719001', 'P160622001', NULL, NULL, '2016-07-19 04:04:47'),
('K160910001', 'P160624001', NULL, NULL, '2016-09-10 08:52:00'),
('K160910002', 'P160627001', NULL, NULL, '2016-09-10 09:15:35'),
('K160910003', 'P160627001', NULL, NULL, '2016-09-10 09:18:47'),
('K160910004', 'P160622001', NULL, NULL, '2016-09-10 09:29:10'),
('K160922001', 'P160624001', NULL, NULL, '2016-09-22 07:55:43'),
('K160922002', 'P160622001', NULL, NULL, '2016-09-22 08:02:47'),
('K160922003', 'P160624001', NULL, NULL, '2016-09-22 08:10:10'),
('K160923001', 'P160624001', NULL, NULL, '2016-09-23 03:30:43'),
('K160923002', 'P160627001', NULL, NULL, '2016-09-23 03:33:33'),
('K160923003', 'P160627001', NULL, NULL, '2016-09-23 03:35:23'),
('K160923004', 'P160622001', NULL, NULL, '2016-09-23 03:40:58');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_dir`
--

CREATE TABLE `tb_direktif_dir` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NAMA_SUBDIT` varchar(25) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_dir`
--

INSERT INTO `tb_direktif_dir` (`ID_KASUS`, `TGL_DIREKTIF`, `NAMA_SUBDIT`, `ID_DIREKTIF`, `DIREKTIF`, `tgl_masuk_data`) VALUES
('K160622001', '2016-06-22', 'SUBDIT IV', 'K16062200101', 'Berikan SP2HP kepada Pelapor', '2016-06-22 09:14:18'),
('K160622001', '2016-06-22', 'SUBDIT IV', 'K16062200102', 'Jika cukup bukti, sidik', '2016-06-22 09:14:18'),
('K160622001', '2016-06-22', 'SUBDIT IV', 'K16062200103', 'Cek Keaslian barang bukti', '2016-06-22 09:14:18'),
('K160624001', '2016-06-24', 'SUBDIT IV', 'K16062400101', 'Lidik dan sidik profesional, proseduran dan porposional', '2016-06-24 02:01:50'),
('K160624001', '2016-06-24', 'SUBDIT IV', 'K16062400102', 'Berikan SP2HP kepada Pelapor', '2016-06-24 02:01:50'),
('K160624001', '2016-06-24', 'SUBDIT IV', 'K16062400103', 'Cek TKP dan buat BA mendatangi TKP', '2016-06-24 02:01:50'),
('K160624001', '2016-06-24', 'SUBDIT IV', 'K16062400104', 'TOLONG SEGERA MINTA PETUNJUK WADIR', '2016-06-24 02:01:50'),
('K160626001', '2016-06-26', 'SUBDIT IV', 'K16062600101', 'Cek TKP dan buat BA mendatangi TKP', '2016-06-26 08:53:26'),
('K160626001', '2016-06-26', 'SUBDIT IV', 'K16062600102', 'Cek keabsahan dokumen', '2016-06-26 08:53:26'),
('K160719001', '2016-07-22', 'SUBDIT I', 'K16071900101', 'Jika cukup bukti, sidik', '2016-07-22 06:26:28'),
('K160719001', '2016-07-22', 'SUBDIT I', 'K16071900102', 'tunjuk penyidik dan penyidik pembantu', '2016-07-22 06:26:28'),
('K160719001', '2016-07-22', 'SUBDIT I', 'K16071900103', 'Cari Saksi-saksi', '2016-07-22 06:26:28'),
('K160719001', '2016-07-22', 'SUBDIT I', 'K16071900104', 'cek TKP', '2016-07-22 06:26:28'),
('K160716002', '2016-07-22', 'SUBDIT IV', 'K16071600201', 'Berikan SP2HP kepada Pelapor', '2016-07-22 06:27:10'),
('K160716002', '2016-07-22', 'SUBDIT IV', 'K16071600202', 'perdalam hubungan hukum kedua belah pihak', '2016-07-22 06:27:10'),
('K160716002', '2016-07-22', 'SUBDIT IV', 'K16071600203', 'periksa saksi-saksi', '2016-07-22 06:27:10'),
('K160922001', '2016-09-23', 'SUBDIT I', 'K16092200101', 'Berikan SP2HP kepada Pelapor', '2016-09-23 07:20:20'),
('K160922001', '2016-09-23', 'SUBDIT I', 'K16092200102', 'Koordinasi dengan BPN', '2016-09-23 07:20:20'),
('K160922001', '2016-09-23', 'SUBDIT I', 'K16092200103', 'Jika cukup bukti, sidik', '2016-09-23 07:20:20'),
('K160922001', '2016-09-23', 'SUBDIT I', 'K16092200104', 'Ambil saksi-saksi di TKP', '2016-09-23 07:20:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_dir_template`
--

CREATE TABLE `tb_direktif_dir_template` (
  `ID_DIREKTIF_TEMPLATE` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_dir_template`
--

INSERT INTO `tb_direktif_dir_template` (`ID_DIREKTIF_TEMPLATE`, `DIREKTIF`, `NAMA_SATWIL`, `NAMA_SATKER`) VALUES
('K12121700601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121700602', 'Berikan SP2HP kepada Pelapor', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121700603', 'Cek TKP dan buat BA mendatangi TKP', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121700604', 'Cek hasil VER', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121700606', 'Cek keabsahan dokumen', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121701508', 'perdalam hubungan hukum kedua belah pihak', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121701509', 'blokir STNK dan BPKB', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13010900214', 'koordinasi dengan pihak Bank', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13061700218', 'Jika cukup bukti, sidik', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13080200122', 'Koordinasi dengan BPN', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13080500423', 'Koordinasi dengan ahli', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13092302125', 'Cari Saksi-saksi', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13092803026', 'Cek Bukti Penyerahan Uang', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13101900329', 'tunjuk penyidik dan penyidik pembantu', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13101900330', 'lakukan visum', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K13102402232', 'Cek Sertifikat Fidusia', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K16072200601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K11607220060', 'Berikan SP2HP kepada Pelapor', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K11607220150', 'perdalam hubungan hukum kedua belah pihak', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072200214', 'koordinasi dengan pihak Bank', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072200218', 'Jika cukup bukti, sidik', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072200122', 'Koordinasi dengan BPN', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072200423', 'Koordinasi dengan ahli', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072202125', 'Cari Saksi-saksi', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072203026', 'Cek Bukti Penyerahan Uang', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072200329', 'tunjuk penyidik dan penyidik pembantu', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072200330', 'lakukan visum', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072202232', 'Cek Sertifikat Fidusia', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K16072100601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100602', 'Berikan SP2HP kepada Pelapor', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100603', 'Cek TKP dan buat BA mendatangi TKP', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100604', 'Cek hasil VER', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100606', 'Cek keabsahan dokumen', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072101508', 'perdalam hubungan hukum kedua belah pihak', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072101509', 'blokir STNK dan BPKB', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100214', 'koordinasi dengan pihak Bank', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100218', 'Jika cukup bukti, sidik', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100122', 'Koordinasi dengan BPN', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100423', 'Koordinasi dengan ahli', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072102125', 'Cari Saksi-saksi', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072103026', 'Cek Bukti Penyerahan Uang', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100329', 'tunjuk penyidik dan penyidik pembantu', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072100330', 'lakukan visum', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072102232', 'Cek Sertifikat Fidusia', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K16072000601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000602', 'Berikan SP2HP kepada Pelapor', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000603', 'Cek TKP dan buat BA mendatangi TKP', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000604', 'Cek hasil VER', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000606', 'Cek keabsahan dokumen', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072001508', 'perdalam hubungan hukum kedua belah pihak', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072001509', 'blokir STNK dan BPKB', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000214', 'koordinasi dengan pihak Bank', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000218', 'Jika cukup bukti, sidik', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000122', 'Koordinasi dengan BPN', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000423', 'Koordinasi dengan ahli', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072002125', 'Cari Saksi-saksi', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072003026', 'Cek Bukti Penyerahan Uang', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000329', 'tunjuk penyidik dan penyidik pembantu', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072000330', 'lakukan visum', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K16072002232', 'Cek Sertifikat Fidusia', 'POLDA SULAWESI SELATAN', 'DITLANTAS');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kanit`
--

CREATE TABLE `tb_direktif_kanit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_not_baru` int(1) DEFAULT '0',
  `is_not_baru_baca` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kanit`
--

INSERT INTO `tb_direktif_kanit` (`ID_KASUS`, `TGL_DIREKTIF`, `NRP`, `ID_DIREKTIF`, `DIREKTIF`, `tgl_masuk_data`, `is_not_baru`, `is_not_baru_baca`) VALUES
('K160622001', '2016-06-22', '78110968', 'K16062200101', 'Lidik/sidik, riksa saksi, laporkan', '2016-06-22 09:48:20', 0, 0),
('K160622001', '2016-06-22', '78110968', 'K16062200102', 'Laporkan perkembangan penanganan perkara', '2016-06-22 09:48:20', 0, 0),
('K160622001', '2016-06-22', '78110968', 'K16062200103', 'Segera buat spint lidik', '2016-06-22 09:48:20', 0, 0),
('K160624001', '2016-06-24', '78110968', 'K16062400101', 'Kumpulkan bukti, laporkan perkembangan', '2016-06-24 02:11:13', 0, 0),
('K160624001', '2016-06-24', '78110968', 'K16062400102', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', '2016-06-24 02:11:13', 0, 0),
('K160626001', '2016-06-26', '84070472', 'K16062600101', 'Lidik/sidik, riksa saksi, laporkan', '2016-06-26 09:20:58', 0, 0),
('K160626001', '2016-06-26', '84070472', 'K16062600102', 'Kirim SP2HP kepada pelapor', '2016-06-26 09:20:58', 0, 0),
('K160716002', '2016-07-22', '75040680', 'K16071600201', 'Lidik/sidik, riksa saksi, laporkan', '2016-07-22 11:23:12', 0, 0),
('K160716002', '2016-07-22', '75040680', 'K16071600202', 'Laporkan perkembangan penanganan perkara', '2016-07-22 11:23:12', 0, 0),
('K160716002', '2016-07-22', '75040680', 'K16071600203', 'segera temukan terlapor', '2016-07-22 11:23:12', 0, 0),
('K160719001', '2016-07-22', '85041343', 'K16071900101', 'Kirim SP2HP kepada pelapor', '2016-07-22 11:24:10', 0, 0),
('K160719001', '2016-07-22', '85041343', 'K16071900102', 'Kumpulkan bukti, laporkan perkembangan', '2016-07-22 11:24:10', 0, 0),
('K160719001', '2016-07-22', '85041343', 'K16071900103', 'cari oknum yang terlibat', '2016-07-22 11:24:10', 0, 0),
('K160922001', '2016-09-27', '84061339', 'K16092200101', 'Kirim SP2HP kepada pelapor', '2016-09-27 12:22:12', 0, 0),
('K160922001', '2016-09-27', '84061339', 'K16092200102', 'Kumpulkan bukti, laporkan perkembangan', '2016-09-27 12:22:12', 0, 0),
('K160922001', '2016-09-27', '84061339', 'K16092200103', 'lengkapi mindik', '2016-09-27 12:22:12', 0, 0),
('K160923003', '2016-09-27', '83121159', 'K16092300301', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', '2016-09-27 12:38:37', 0, 0),
('K160923004', '2016-09-27', '83010472', 'K16092300402', 'Kumpulkan bukti, laporkan perkembangan', '2016-09-27 12:36:25', 0, 0),
('K160922003', '2016-09-27', '85100981', 'K16092200301', 'Kirim SP2HP kepada pelapor', '2016-09-27 12:34:59', 0, 0),
('K160922003', '2016-09-27', '85100981', 'K16092200302', 'Laporkan perkembangan penanganan perkara', '2016-09-27 12:34:59', 0, 0),
('K160923004', '2016-09-27', '83010472', 'K16092300401', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', '2016-09-27 12:36:25', 0, 0),
('K160923003', '2016-09-27', '83121159', 'K16092300302', 'Kumpulkan bukti, laporkan perkembangan', '2016-09-27 12:38:37', 0, 0),
('K160923002', '2016-09-27', 'SUBNIT III', 'K16092300201', 'Kirim SP2HP kepada pelapor', '2016-09-27 14:29:18', 0, 0),
('K160923002', '2016-09-27', 'SUBNIT III', 'K16092300202', 'Laporkan perkembangan penanganan perkara', '2016-09-27 14:29:18', 0, 0),
('K160923002', '2016-09-27', 'SUBNIT III', 'K16092300203', 'riksa saksi-saksi', '2016-09-27 14:29:18', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kanit_template`
--

CREATE TABLE `tb_direktif_kanit_template` (
  `ID_DIREKTIF_TEMPLATE` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL,
  `NAMA_POLSEK` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kanit_template`
--

INSERT INTO `tb_direktif_kanit_template` (`ID_DIREKTIF_TEMPLATE`, `DIREKTIF`, `NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_POLSEK`) VALUES
('K12121201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL),
('K12121201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL),
('K13032500408', 'Kirim SP2HP kepada pelapor', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL),
('K13032500409', 'Laporkan perkembangan penanganan perkara', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL),
('K12122901010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL),
('K16072201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL),
('K16072201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL),
('K16072200408', 'Kirim SP2HP kepada pelapor', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL),
('K16072200409', 'Laporkan perkembangan penanganan perkara', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL),
('K16072201010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL),
('K16072101901', 'Lidik/sidik, riksa saksi, laporkan', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL),
('K16072101202', 'Kumpulkan bukti, laporkan perkembangan', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL),
('K16072100408', 'Kirim SP2HP kepada pelapor', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL),
('K16072100409', 'Laporkan perkembangan penanganan perkara', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL),
('K16072101010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL),
('K16072001901', 'Lidik/sidik, riksa saksi, laporkan', 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL),
('K16072001202', 'Kumpulkan bukti, laporkan perkembangan', 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL),
('K16072000408', 'Kirim SP2HP kepada pelapor', 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL),
('K16072000409', 'Laporkan perkembangan penanganan perkara', 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL),
('K16072001010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL),
('K16082201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL),
('K16082201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL),
('K16082200408', 'Kirim SP2HP kepada pelapor', 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL),
('K16082200409', 'Laporkan perkembangan penanganan perkara', 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL),
('K16082201010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL),
('K16082101901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL),
('K16082101202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL),
('K16082100408', 'Kirim SP2HP kepada pelapor', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL),
('K16082100409', 'Laporkan perkembangan penanganan perkara', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL),
('K16082101010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL),
('K16082001901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL),
('K16082001202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL),
('K16082000408', 'Kirim SP2HP kepada pelapor', 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL),
('K16082000409', 'Laporkan perkembangan penanganan perkara', 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL),
('K16082001010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL),
('K16092201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES GOWA', 'SATRESKRIM', NULL),
('K16092201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES GOWA', 'SATRESKRIM', NULL),
('K16092200409', 'Kirim SP2HP kepada pelapor', 'POLRES GOWA', 'SATRESKRIM', NULL),
('K16092201010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES GOWA', 'SATRESKRIM', NULL),
('K16092101901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES GOWA', 'SATRESNARKOBA', NULL),
('K16092101202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES GOWA', 'SATRESNARKOBA', NULL),
('K16092100409', 'Kirim SP2HP kepada pelapor', 'POLRES GOWA', 'SATRESNARKOBA', NULL),
('K16092101010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES GOWA', 'SATRESNARKOBA', NULL),
('K16092001901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES GOWA', 'SATLANTAS', NULL),
('K16092001202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES GOWA', 'SATLANTAS', NULL),
('K16092000409', 'Kirim SP2HP kepada pelapor', 'POLRES GOWA', 'SATLANTAS', NULL),
('K16092001010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES GOWA', 'SATLANTAS', NULL),
('K16012201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES MAROS', 'SATRESKRIM', NULL),
('K16012201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES MAROS', 'SATRESKRIM', NULL),
('K16012200401', 'Kirim SP2HP kepada pelapor', 'POLRES MAROS', 'SATRESKRIM', NULL),
('K16012201010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES MAROS', 'SATRESKRIM', NULL),
('K16012101901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES MAROS', 'SATRESNARKOBA', NULL),
('K16012101202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES MAROS', 'SATRESNARKOBA', NULL),
('K16012100401', 'Kirim SP2HP kepada pelapor', 'POLRES MAROS', 'SATRESNARKOBA', NULL),
('K16012101010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES MAROS', 'SATRESNARKOBA', NULL),
('K16012001901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES MAROS', 'SATLANTAS', NULL),
('K16012001202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES MAROS', 'SATLANTAS', NULL),
('K16012000401', 'Kirim SP2HP kepada pelapor', 'POLRES MAROS', 'SATLANTAS', NULL),
('K16012001010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES MAROS', 'SATLANTAS', NULL),
('K16012200501', 'Laporkan perkembangan penanganan perkara', 'POLRES MAROS', 'SATRESKRIM', NULL),
('K16012100501', 'Laporkan perkembangan penanganan perkara', 'POLRES MAROS', 'SATRESNARKOBA', NULL),
('K16012201001', 'Laporkan perkembangan penanganan perkara', 'POLRES MAROS', 'SATLANTAS', NULL),
('K16022201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES KP3', 'SATRESKRIM', NULL),
('K16022201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES KP3', 'SATRESKRIM', NULL),
('K16022200401', 'Kirim SP2HP kepada pelapor', 'POLRES KP3', 'SATRESKRIM', NULL),
('K16022200501', 'Laporkan perkembangan penanganan perkara', 'POLRES KP3', 'SATRESKRIM', NULL),
('K16022201010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES KP3', 'SATRESKRIM', NULL),
('K16022101901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES KP3', 'SATRESNARKOBA', NULL),
('K16022101202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES KP3', 'SATRESNARKOBA', NULL),
('K16022100401', 'Kirim SP2HP kepada pelapor', 'POLRES KP3', 'SATRESNARKOBA', NULL),
('K16022100501', 'Laporkan perkembangan penanganan perkara', 'POLRES KP3', 'SATRESNARKOBA', NULL),
('K16022101010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES KP3', 'SATRESNARKOBA', NULL),
('K16022001901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRES KP3', 'SATLANTAS', NULL),
('K16022001202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRES KP3', 'SATLANTAS', NULL),
('K16022000401', 'Kirim SP2HP kepada pelapor', 'POLRES KP3', 'SATLANTAS', NULL),
('K16022201001', 'Laporkan perkembangan penanganan perkara', 'POLRES KP3', 'SATLANTAS', NULL),
('K16022001010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRES KP3', 'SATLANTAS', NULL),
('K16032201901', 'Lidik/sidik, riksa saksi, laporkan', 'POLRESTABES MAKASSAR', 'SATRESKRIM', 'POLSEK UJUNG PANDANG'),
('K16032201202', 'Kumpulkan bukti, laporkan perkembangan', 'POLRESTABES MAKASSAR', 'SATRESKRIM', 'POLSEK UJUNG PANDANG'),
('K16032200401', 'Kirim SP2HP kepada pelapor', 'POLRESTABES MAKASSAR', 'SATRESKRIM', 'POLSEK UJUNG PANDANG'),
('K16032200501', 'Laporkan perkembangan penanganan perkara', 'POLRESTABES MAKASSAR', 'SATRESKRIM', 'POLSEK UJUNG PANDANG'),
('K16032201010', 'Dalami pemeriksaan mengenai kepemilikan akta fidusia', 'POLRESTABES MAKASSAR', 'SATRESKRIM', 'POLSEK UJUNG PANDANG'),
('K16082110409', 'Laporkan perkembangan penanganan perkara', 'POLRES GOWA', 'SATRESNARKOBA', NULL),
('K16082120409', 'Laporkan perkembangan penanganan perkara', 'POLRES GOWA', 'SATRESKRIM', NULL),
('K16082220409', 'Laporkan perkembangan penanganan perkara', 'POLRES GOWA', 'SATLANTAS', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kasat`
--

CREATE TABLE `tb_direktif_kasat` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kasat`
--

INSERT INTO `tb_direktif_kasat` (`ID_KASUS`, `TGL_DIREKTIF`, `NAMA_UNIT`, `ID_DIREKTIF`, `DIREKTIF`, `tgl_masuk_data`) VALUES
('K160923004', '2016-09-27', 'UNIT II', 'K16092300403', 'kumpulkan keterangan saksi', '2016-09-27 00:24:16'),
('K160923004', '2016-09-27', 'UNIT II', 'K16092300401', 'Lidik dan sidik profesional, proseduran dan porposional', '2016-09-27 00:24:16'),
('K160923004', '2016-09-27', 'UNIT II', 'K16092300402', 'Berikan SP2HP kepada Pelapor', '2016-09-27 00:24:16'),
('K160923002', '2016-09-27', 'UNIT III', 'K16092300201', 'Lidik dan sidik profesional, proseduran dan porposional', '2016-09-27 00:37:37'),
('K160923002', '2016-09-27', 'UNIT III', 'K16092300202', 'Berikan SP2HP kepada Pelapor', '2016-09-27 00:37:37'),
('K160923002', '2016-09-27', 'UNIT III', 'K16092300203', 'perdalam hubungan hukum kedua belah pihak', '2016-09-27 00:37:37'),
('K160923002', '2016-09-27', 'UNIT III', 'K16092300204', 'Cari Saksi-saksi', '2016-09-27 00:37:37');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kasat_template`
--

CREATE TABLE `tb_direktif_kasat_template` (
  `ID_DIREKTIF_TEMPLATE` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kasat_template`
--

INSERT INTO `tb_direktif_kasat_template` (`ID_DIREKTIF_TEMPLATE`, `DIREKTIF`, `NAMA_SATWIL`, `NAMA_SATKER`) VALUES
('K12121700601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121700602', 'Berikan SP2HP kepada Pelapor', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121700603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121700604', 'Cek hasil VER', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121700606', 'Cek keabsahan dokumen', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121701508', 'perdalam hubungan hukum kedua belah pihak', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121701509', 'blokir STNK dan BPKB', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13010100112', 'limpahkan ke polsek sesuai TKP', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13010900214', 'koordinasi dengan pihak Bank', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13061700218', 'Jika cukup bukti, sidik', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13080200122', 'Koordinasi dengan BPN', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13080500423', 'Koordinasi dengan ahli', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13092302125', 'Cari Saksi-saksi', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13092803026', 'Cek Bukti Penyerahan Uang', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13101900329', 'tunjuk penyidik dan penyidik pembantu', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13101900330', 'lakukan visum', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13102402232', 'Cek Sertifikat Fidusia', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121800601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K12121800602', 'Berikan SP2HP kepada Pelapor', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K12121800603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K12121800604', 'Cek hasil VER', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K12121801508', 'perdalam hubungan hukum kedua belah pihak', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K13061800218', 'Jika cukup bukti, sidik', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K13092302126', 'Cari Saksi-saksi', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K13092803027', 'Cek Bukti Penyerahan Uang', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K13101900331', 'tunjuk penyidik dan penyidik pembantu', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K13102402233', 'Cek Sertifikat Fidusia', 'POLRESTABES MAKASSAR', 'SATRESNARKOBA'),
('K12121900601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K12121900602', 'Berikan SP2HP kepada Pelapor', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K12121900603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K12121900604', 'Cek hasil VER', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K13061900218', 'Jika cukup bukti, sidik', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K13092302127', 'Cari Saksi-saksi', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K12122000601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES GOWA', 'SATRESKRIM'),
('K12122000602', 'Berikan SP2HP kepada Pelapor', 'POLRES GOWA', 'SATRESKRIM'),
('K12122000603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES GOWA', 'SATRESKRIM'),
('K12122000604', 'Cek hasil VER', 'POLRES GOWA', 'SATRESKRIM'),
('K12122000606', 'Cek keabsahan dokumen', 'POLRES GOWA', 'SATRESKRIM'),
('K12122001508', 'perdalam hubungan hukum kedua belah pihak', 'POLRES GOWA', 'SATRESKRIM'),
('K12122001509', 'blokir STNK dan BPKB', 'POLRES GOWA', 'SATRESKRIM'),
('K13062000218', 'Jika cukup bukti, sidik', 'POLRES GOWA', 'SATRESKRIM'),
('K13080200123', 'Koordinasi dengan BPN', 'POLRES GOWA', 'SATRESKRIM'),
('K13080500424', 'Koordinasi dengan ahli', 'POLRES GOWA', 'SATRESKRIM'),
('K13092302128', 'Cari Saksi-saksi', 'POLRES GOWA', 'SATRESKRIM'),
('K13092803028', 'Cek Bukti Penyerahan Uang', 'POLRES GOWA', 'SATRESKRIM'),
('K13101900332', 'tunjuk penyidik dan penyidik pembantu', 'POLRES GOWA', 'SATRESKRIM'),
('K13102402234', 'Cek Sertifikat Fidusia', 'POLRES GOWA', 'SATRESKRIM'),
('K12122100601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES GOWA', 'SATRESNARKOBA'),
('K12122100602', 'Berikan SP2HP kepada Pelapor', 'POLRES GOWA', 'SATRESNARKOBA'),
('K12122100603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES GOWA', 'SATRESNARKOBA'),
('K12122100604', 'Cek hasil VER', 'POLRES GOWA', 'SATRESNARKOBA'),
('K12122101508', 'perdalam hubungan hukum kedua belah pihak', 'POLRES GOWA', 'SATRESNARKOBA'),
('K13062100218', 'Jika cukup bukti, sidik', 'POLRES GOWA', 'SATRESNARKOBA'),
('K13092302129', 'Cari Saksi-saksi', 'POLRES GOWA', 'SATRESNARKOBA'),
('K13092803029', 'Cek Bukti Penyerahan Uang', 'POLRES GOWA', 'SATRESNARKOBA'),
('K13101900333', 'tunjuk penyidik dan penyidik pembantu', 'POLRES GOWA', 'SATRESNARKOBA'),
('K13102402235', 'Cek Sertifikat Fidusia', 'POLRES GOWA', 'SATRESNARKOBA'),
('K12122200601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES GOWA', 'SATLANTAS'),
('K12122200602', 'Berikan SP2HP kepada Pelapor', 'POLRES GOWA', 'SATLANTAS'),
('K12122200603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES GOWA', 'SATLANTAS'),
('K12122200604', 'Cek hasil VER', 'POLRES GOWA', 'SATLANTAS'),
('K13062200218', 'Jika cukup bukti, sidik', 'POLRES GOWA', 'SATLANTAS'),
('K13092302130', 'Cari Saksi-saksi', 'POLRES GOWA', 'SATLANTAS'),
('K13101900334', 'tunjuk penyidik dan penyidik pembantu', 'POLRES GOWA', 'SATLANTAS'),
('K12122300601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES MAROS', 'SATRESKRIM'),
('K12122300602', 'Berikan SP2HP kepada Pelapor', 'POLRES MAROS', 'SATRESKRIM'),
('K12122300603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES MAROS', 'SATRESKRIM'),
('K12122300604', 'Cek hasil VER', 'POLRES MAROS', 'SATRESKRIM'),
('K12122300606', 'Cek keabsahan dokumen', 'POLRES MAROS', 'SATRESKRIM'),
('K12122301508', 'perdalam hubungan hukum kedua belah pihak', 'POLRES MAROS', 'SATRESKRIM'),
('K12122301509', 'blokir STNK dan BPKB', 'POLRES MAROS', 'SATRESKRIM'),
('K13062300218', 'Jika cukup bukti, sidik', 'POLRES MAROS', 'SATRESKRIM'),
('K13080200124', 'Koordinasi dengan BPN', 'POLRES MAROS', 'SATRESKRIM'),
('K13080500425', 'Koordinasi dengan ahli', 'POLRES MAROS', 'SATRESKRIM'),
('K13092302131', 'Cari Saksi-saksi', 'POLRES MAROS', 'SATRESKRIM'),
('K13092803030', 'Cek Bukti Penyerahan Uang', 'POLRES MAROS', 'SATRESKRIM'),
('K13101900335', 'tunjuk penyidik dan penyidik pembantu', 'POLRES MAROS', 'SATRESKRIM'),
('K13102402236', 'Cek Sertifikat Fidusia', 'POLRES MAROS', 'SATRESKRIM'),
('K12122400601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES MAROS', 'SATRESNARKOBA'),
('K12122400602', 'Berikan SP2HP kepada Pelapor', 'POLRES MAROS', 'SATRESNARKOBA'),
('K12122400603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES MAROS', 'SATRESNARKOBA'),
('K12122400604', 'Cek hasil VER', 'POLRES MAROS', 'SATRESNARKOBA'),
('K12122401508', 'perdalam hubungan hukum kedua belah pihak', 'POLRES MAROS', 'SATRESNARKOBA'),
('K13062400218', 'Jika cukup bukti, sidik', 'POLRES MAROS', 'SATRESNARKOBA'),
('K13092302132', 'Cari Saksi-saksi', 'POLRES MAROS', 'SATRESNARKOBA'),
('K13092803031', 'Cek Bukti Penyerahan Uang', 'POLRES MAROS', 'SATRESNARKOBA'),
('K13101900336', 'tunjuk penyidik dan penyidik pembantu', 'POLRES MAROS', 'SATRESNARKOBA'),
('K13102402237', 'Cek Sertifikat Fidusia', 'POLRES MAROS', 'SATRESNARKOBA'),
('K12122500601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES MAROS', 'SATLANTAS'),
('K12122500602', 'Berikan SP2HP kepada Pelapor', 'POLRES MAROS', 'SATLANTAS'),
('K12122500603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES MAROS', 'SATLANTAS'),
('K12122500604', 'Cek hasil VER', 'POLRES MAROS', 'SATLANTAS'),
('K13062500218', 'Jika cukup bukti, sidik', 'POLRES MAROS', 'SATLANTAS'),
('K13092302133', 'Cari Saksi-saksi', 'POLRES MAROS', 'SATLANTAS'),
('K13101900337', 'tunjuk penyidik dan penyidik pembantu', 'POLRES MAROS', 'SATLANTAS'),
('K12122600601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES KP3', 'SATRESKRIM'),
('K12122600602', 'Berikan SP2HP kepada Pelapor', 'POLRES KP3', 'SATRESKRIM'),
('K12122600603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES KP3', 'SATRESKRIM'),
('K12122600604', 'Cek hasil VER', 'POLRES KP3', 'SATRESKRIM'),
('K12122600606', 'Cek keabsahan dokumen', 'POLRES KP3', 'SATRESKRIM'),
('K12122601508', 'perdalam hubungan hukum kedua belah pihak', 'POLRES KP3', 'SATRESKRIM'),
('K12122601509', 'blokir STNK dan BPKB', 'POLRES KP3', 'SATRESKRIM'),
('K13062600218', 'Jika cukup bukti, sidik', 'POLRES KP3', 'SATRESKRIM'),
('K13080200125', 'Koordinasi dengan BPN', 'POLRES KP3', 'SATRESKRIM'),
('K13080500426', 'Koordinasi dengan ahli', 'POLRES KP3', 'SATRESKRIM'),
('K13092302134', 'Cari Saksi-saksi', 'POLRES KP3', 'SATRESKRIM'),
('K13092803032', 'Cek Bukti Penyerahan Uang', 'POLRES KP3', 'SATRESKRIM'),
('K13101900338', 'tunjuk penyidik dan penyidik pembantu', 'POLRES KP3', 'SATRESKRIM'),
('K13102402238', 'Cek Sertifikat Fidusia', 'POLRES KP3', 'SATRESKRIM'),
('K12122700601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES KP3', 'SATRESNARKOBA'),
('K12122700602', 'Berikan SP2HP kepada Pelapor', 'POLRES KP3', 'SATRESNARKOBA'),
('K12122700603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES KP3', 'SATRESNARKOBA'),
('K12122700604', 'Cek hasil VER', 'POLRES KP3', 'SATRESNARKOBA'),
('K12122701508', 'perdalam hubungan hukum kedua belah pihak', 'POLRES KP3', 'SATRESNARKOBA'),
('K13062700218', 'Jika cukup bukti, sidik', 'POLRES KP3', 'SATRESNARKOBA'),
('K13092302135', 'Cari Saksi-saksi', 'POLRES KP3', 'SATRESNARKOBA'),
('K13092803033', 'Cek Bukti Penyerahan Uang', 'POLRES KP3', 'SATRESNARKOBA'),
('K13101900339', 'tunjuk penyidik dan penyidik pembantu', 'POLRES KP3', 'SATRESNARKOBA'),
('K13102402239', 'Cek Sertifikat Fidusia', 'POLRES KP3', 'SATRESNARKOBA'),
('K12121600601', 'Lidik dan sidik profesional, proseduran dan porposional', 'POLRES KP3', 'SATLANTAS'),
('K12121600602', 'Berikan SP2HP kepada Pelapor', 'POLRES KP3', 'SATLANTAS'),
('K12121600603', 'Cek TKP dan buat BA mendatangi TKP', 'POLRES KP3', 'SATLANTAS'),
('K12121600604', 'Cek hasil VER', 'POLRES KP3', 'SATLANTAS'),
('K13061600218', 'Jika cukup bukti, sidik', 'POLRES KP3', 'SATLANTAS'),
('K13092302136', 'Cari Saksi-saksi', 'POLRES KP3', 'SATLANTAS'),
('K13101900340', 'tunjuk penyidik dan penyidik pembantu', 'POLRES KP3', 'SATLANTAS'),
('K13101900323', 'tunjuk penyidik dan penyidik pembantu', 'POLRESTABES MAKASSAR', 'SATLANTAS'),
('K13010200112', 'limpahkan ke polsek sesuai TKP', 'POLRES GOWA', 'SATRESKRIM'),
('K13010200214', 'koordinasi dengan pihak Bank', 'POLRES GOWA', 'SATRESKRIM'),
('K13102900332', 'lakukan visum', 'POLRES GOWA', 'SATRESKRIM'),
('K13030100112', 'limpahkan ke polsek sesuai TKP', 'POLRES MAROS', 'SATRESKRIM'),
('K13030900214', 'koordinasi dengan pihak Bank', 'POLRES MAROS', 'SATRESKRIM'),
('K13104900333', 'lakukan visum', 'POLRES MAROS', 'SATRESKRIM'),
('K13040100112', 'limpahkan ke polsek sesuai TKP', 'POLRES KP3', 'SATRESKRIM'),
('K13040900214', 'koordinasi dengan pihak Bank', 'POLRES KP3', 'SATRESKRIM'),
('K13105900334', 'lakukan visum', 'POLRES KP3', 'SATRESKRIM');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kasubdit`
--

CREATE TABLE `tb_direktif_kasubdit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kasubdit`
--

INSERT INTO `tb_direktif_kasubdit` (`ID_KASUS`, `TGL_DIREKTIF`, `NAMA_UNIT`, `ID_DIREKTIF`, `DIREKTIF`, `tgl_masuk_data`) VALUES
('K160622001', '2016-06-22', 'UNIT II', 'K16062200101', 'Pelajari perkaranya', '2016-06-22 09:43:37'),
('K160622001', '2016-06-22', 'UNIT II', 'K16062200102', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', '2016-06-22 09:43:37'),
('K160622001', '2016-06-22', 'UNIT II', 'K16062200103', 'Perdalam Hubungan kedua belah pihak', '2016-06-22 09:43:37'),
('K160624001', '2016-06-24', 'UNIT IV', 'K16062400101', 'Datangi TKP, ambil keterangan saksi-saksi, kumpulkan dokumen terkait perkara yang dilaporkan', '2016-06-24 02:04:43'),
('K160624001', '2016-06-24', 'UNIT IV', 'K16062400102', 'Pelajari perkaranya', '2016-06-24 02:04:43'),
('K160624001', '2016-06-24', 'UNIT IV', 'K16062400103', 'Kumpulkan BB', '2016-06-24 02:04:43'),
('K160624001', '2016-06-24', 'UNIT IV', 'K16062400104', 'SEGERA KIRIM SP2HP', '2016-06-24 02:04:43'),
('K160626001', '2016-06-26', 'UNIT IV', 'K16062600101', 'Pelajari perkaranya', '2016-06-26 09:14:01'),
('K160626001', '2016-06-26', 'UNIT IV', 'K16062600102', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', '2016-06-26 09:14:01'),
('K160719001', '2016-07-22', 'UNIT II', 'K16071900101', 'Lidik dan sidik profesional', '2016-07-22 09:03:27'),
('K160719001', '2016-07-22', 'UNIT II', 'K16071900102', 'Datangi TKP, ambil keterangan saksi-saksi, kumpulkan dokumen terkait perkara yang dilaporkan', '2016-07-22 09:03:27'),
('K160719001', '2016-07-22', 'UNIT II', 'K16071900103', 'perdalam hubungan kedua pihak', '2016-07-22 09:03:27'),
('K160716002', '2016-07-22', 'UNIT I', 'K16071600201', 'Pelajari perkaranya', '2016-07-22 09:04:29'),
('K160716002', '2016-07-22', 'UNIT I', 'K16071600202', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', '2016-07-22 09:04:29'),
('K160716002', '2016-07-22', 'UNIT I', 'K16071600203', 'pantau posisi tersangka', '2016-07-22 09:04:29'),
('K160922001', '2016-09-23', 'UNIT II', 'K16092200101', 'Lidik dan sidik profesional', '2016-09-23 07:30:22'),
('K160922001', '2016-09-23', 'UNIT II', 'K16092200102', 'Pelajari perkaranya', '2016-09-23 07:30:22'),
('K160922001', '2016-09-23', 'UNIT II', 'K16092200103', 'Buat Mindik', '2016-09-23 07:30:22');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kasubdit_template`
--

CREATE TABLE `tb_direktif_kasubdit_template` (
  `ID_DIREKTIF_TEMPLATE` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kasubdit_template`
--

INSERT INTO `tb_direktif_kasubdit_template` (`ID_DIREKTIF_TEMPLATE`, `DIREKTIF`, `NAMA_SATWIL`, `NAMA_SATKER`) VALUES
('K12121301001', 'Lidik dan sidik profesional', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301003', 'Datangi TKP, ambil keterangan saksi-saksi, kumpulkan dokumen terkait perkara yang dilaporkan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301004', 'Sesuaikan giat lidik dengan batas waktu maksimal lidik yaitu 30 hari', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301005', 'Pelajari perkaranya', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301006', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301007', 'Bila tidak ditemukan pidana gelarkan, berikan SP2HP (A2) pada pelapor', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301008', 'Bila ditemukan pidana : Buat Sprin Sidik, Buat SPDP, Buat SP2HP (A3), Buat ren sidik', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301009', 'Panggil dan periksa pelapor & saksi terkait', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301010', 'Kumpulkan BB', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301012', 'Ada hambatan gelarkan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121301014', 'Laporkan hasilnya', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS'),
('K12121401001', 'Lidik dan sidik profesional', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401003', 'Datangi TKP, ambil keterangan saksi-saksi, kumpulkan dokumen terkait perkara yang dilaporkan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401004', 'Sesuaikan giat lidik dengan batas waktu maksimal lidik yaitu 30 hari', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401005', 'Pelajari perkaranya', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401006', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401007', 'Bila tidak ditemukan pidana gelarkan, berikan SP2HP (A2) pada pelapor', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401008', 'Bila ditemukan pidana : Buat Sprin Sidik, Buat SPDP, Buat SP2HP (A3), Buat ren sidik', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401009', 'Panggil dan periksa pelapor & saksi terkait', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401010', 'Kumpulkan BB', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401012', 'Ada hambatan gelarkan', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121401014', 'Laporkan hasilnya', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM'),
('K12121501001', 'Lidik dan sidik profesional', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501003', 'Datangi TKP, ambil keterangan saksi-saksi, kumpulkan dokumen terkait perkara yang dilaporkan', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501004', 'Sesuaikan giat lidik dengan batas waktu maksimal lidik yaitu 30 hari', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501005', 'Pelajari perkaranya', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501006', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501007', 'Bila tidak ditemukan pidana gelarkan, berikan SP2HP (A2) pada pelapor', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501008', 'Bila ditemukan pidana : Buat Sprin Sidik, Buat SPDP, Buat SP2HP (A3), Buat ren sidik', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501009', 'Panggil dan periksa pelapor & saksi terkait', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501010', 'Kumpulkan BB', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501012', 'Ada hambatan gelarkan', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121501014', 'Laporkan hasilnya', 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA'),
('K12121601001', 'Lidik dan sidik profesional', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601003', 'Datangi TKP, ambil keterangan saksi-saksi, kumpulkan dokumen terkait perkara yang dilaporkan', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601004', 'Sesuaikan giat lidik dengan batas waktu maksimal lidik yaitu 30 hari', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601005', 'Pelajari perkaranya', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601006', 'Lengkapi mindik lidik : Sprin tugas, Sprin lidik, Buat ren lidik, Buat LHP', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601007', 'Bila tidak ditemukan pidana gelarkan, berikan SP2HP (A2) pada pelapor', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601008', 'Bila ditemukan pidana : Buat Sprin Sidik, Buat SPDP, Buat SP2HP (A3), Buat ren sidik', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601009', 'Panggil dan periksa pelapor & saksi terkait', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601010', 'Kumpulkan BB', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601012', 'Ada hambatan gelarkan', 'POLDA SULAWESI SELATAN', 'DITLANTAS'),
('K12121601014', 'Laporkan hasilnya', 'POLDA SULAWESI SELATAN', 'DITLANTAS');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kasubnit`
--

CREATE TABLE `tb_direktif_kasubnit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kasubnit`
--

INSERT INTO `tb_direktif_kasubnit` (`ID_KASUS`, `TGL_DIREKTIF`, `NRP`, `ID_DIREKTIF`, `DIREKTIF`, `tgl_masuk_data`) VALUES
('K160923002', '2016-09-28', '65120789', 'K16092300201', 'lidik dan sidik, riksa saksi, kumpulkan bukti', '2016-09-27 18:22:31'),
('K160923002', '2016-09-28', '65120789', 'K16092300202', 'Kirim SP2HP Kepada Pelapor', '2016-09-27 18:22:31'),
('K160923002', '2016-09-28', '65120789', 'K16092300203', 'Riksa Saksi', '2016-09-27 18:22:31'),
('K160923002', '2016-09-28', '65120789', 'K16092300204', 'Ambil Keterangan Ahli', '2016-09-27 18:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `tb_direktif_kasubnit_template`
--

CREATE TABLE `tb_direktif_kasubnit_template` (
  `ID_DIREKTIF_TEMPLATE` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_direktif_kasubnit_template`
--

INSERT INTO `tb_direktif_kasubnit_template` (`ID_DIREKTIF_TEMPLATE`, `DIREKTIF`, `NAMA_SATWIL`, `NAMA_SATKER`) VALUES
('K12121201901', 'lidik/sidik, riksa saksi, laporkan Perkembangan', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12121201202', 'lidik dan sidik, riksa saksi, kumpulkan bukti', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13032600309', 'Laporkan Perkembangan Penanganan Perkara', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K13032500408', 'Dalami hubungan hukum masing - masing pihak', 'POLRESTABES MAKASSAR', 'SATRESKRIM'),
('K12122901009', 'Kirim SP2HP Kepada Pelapor', 'POLRESTABES MAKASSAR', 'SATRESKRIM');

-- --------------------------------------------------------

--
-- Table structure for table `tb_doc_berkas`
--

CREATE TABLE `tb_doc_berkas` (
  `ID_DOC_BERKAS` varchar(5) NOT NULL,
  `NAMA_DOC` varchar(300) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0',
  `PROSENTASE` int(3) NOT NULL DEFAULT '0',
  `IS_MINUS` int(1) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_doc_berkas`
--

INSERT INTO `tb_doc_berkas` (`ID_DOC_BERKAS`, `NAMA_DOC`, `POINT`, `PROSENTASE`, `IS_MINUS`, `tgl_masuk_data`) VALUES
('DOC01', 'Penelitian terhadap informasi yang masuk', 2, 0, 0, '2014-12-07 17:24:02'),
('DOC02', 'Klarifikasi atas informasi yang masuk / Pulbaket / Surat minta klarifikasi', 2, 0, 0, '2014-12-07 17:24:02'),
('DOC03', 'Laporan Informasi', 2, 3, 0, '2014-12-07 17:24:02'),
('DOC04', 'Laporan Polisi (Model A)', 2, 5, 0, '2014-12-07 17:24:02'),
('DOC05', 'SP2HP setelah menerima LP (3 hari)', 10, 5, 0, '2014-12-07 17:24:02'),
('DOC06', 'Rencana Penyelidikan', 5, 7, 0, '2014-12-07 17:24:02'),
('DOC07', 'RAB', 5, 7, 0, '2014-12-07 17:24:02'),
('DOC08', 'Sprin Tugas', 2, 10, 0, '2014-12-07 17:24:02'),
('DOC09', 'Sprin Penyelidikan', 2, 10, 0, '2014-12-07 17:24:02'),
('DOC10', 'pengolahan TKP. (BA Sket TKP/BA Potret TKP/BA Temu BB di TKP)', 20, 10, 0, '2014-12-07 17:24:02'),
('DOC11', 'pengamatan (observasi). (dibuatkan laporan pengamatan / observasi)', 10, 10, 0, '2014-12-07 17:24:02'),
('DOC12', 'BA Introgasi /wawancara (interview). (dibuatkan laporan wawancara)', 10, 10, 0, '2014-12-07 17:24:02'),
('DOC13', 'pembuntutan (surveillance). (dibuatkan laporan pembuntutan / surveillance)', 40, 10, 0, '2014-12-07 17:24:02'),
('DOC14', 'penyamaran (under cover). (dibuatkan laporan penyamaran / under cover)', 10, 10, 0, '2014-12-07 17:24:02'),
('DOC15', 'pelacakan (tracking). (dibuatkan laporan pelacakan / tracking)', 10, 10, 0, '2014-12-07 17:24:02'),
('DOC16', 'penelitian dan analisis dokumen. (dibuatkan laporan)', 10, 10, 0, '2014-12-07 17:24:02'),
('DOC17', 'Bahan gelar / slide / paparan', 10, 10, 0, '2014-12-07 17:24:02'),
('DOC18', 'Undangan gelar perkara', 2, 10, 0, '2014-12-07 17:24:02'),
('DOC19', 'Laporan Hasil gelar Perkara', 10, 13, 0, '2014-12-07 17:24:02'),
('DOC20', 'SP2HP henti penyelidikan', 5, 13, 0, '2014-12-07 17:24:02'),
('DOC21', 'Laporan Hasil Penyelidikan', 10, 15, 0, '2014-12-07 17:24:02'),
('DOC22', 'Gelar perkara tahap awal (tidak naik sidik/tambahan waktu lidik/naik sidik)', 10, 20, 0, '2014-12-07 17:24:02'),
('DOC23', 'SP2HP setelah gelar tahap awal (naik sidik)', 10, 20, 0, '2014-12-07 17:24:02'),
('DOC24', 'Rencana Penyidikan', 5, 25, 0, '2014-12-07 17:24:02'),
('DOC25', 'RAB Penyidikan', 5, 25, 0, '2014-12-07 17:24:02'),
('DOC26', 'Surat Perintah Tugas untuk melakukan Penyidikan', 2, 30, 0, '2014-12-07 17:24:02'),
('DOC27', 'Surat Perintah Penyidikan', 2, 30, 0, '2014-12-07 17:24:02'),
('DOC28', 'SPDP / SPDP Lanjutan', 2, 30, 0, '2014-12-07 17:24:02'),
('DOC29', 'Surat Panggilan', 2, 30, 0, '2014-12-07 17:24:02'),
('DOC30', 'Pemeriksaan / BAP Saksi / BAP Tambahan / BAP Lanjutan', 20, 30, 0, '2014-12-07 17:24:02'),
('DOC31', 'Pemeriksaan / BAP Ahli / BAP Tambahan / BAP Lanjutan', 40, 30, 0, '2014-12-07 17:24:02'),
('DOC32', 'Pemeriksaan / BAP Tersangka / BAP Tambahan / BAP Lanjutan', 25, 30, 0, '2014-12-07 17:24:02'),
('DOC33', 'BA Konfrontir', 20, 30, 0, '2014-12-07 17:24:02'),
('DOC35', 'BA Rekonstruksi', 40, 40, 0, '2014-12-07 17:24:02'),
('DOC36', 'BA lain-lain', 2, 40, 0, '2014-12-07 17:24:02'),
('DOC38', 'BA Police Lineatau memberikan tanda peringatan', 10, 40, 0, '2014-12-07 17:24:02'),
('DOC39', 'Surat permintaan Visum Et Repertum', 2, 50, 0, '2014-12-07 17:24:02'),
('DOC40', 'Surat Uji Laboratorium', 5, 50, 0, '2014-12-07 17:24:02'),
('DOC41', 'Menerima Dumas/Surat dari satuan atas/Kompolnas/fungsi pengawas dll)', 50, 50, 1, '2014-12-07 17:24:02'),
('DOC42', 'Menjawab Dumas/Surat dari satuan atas/Kompolnas/fungsi pengawas dll)', 25, 50, 0, '2014-12-07 17:24:02'),
('DOC43', 'Surat ke instansi lain', 2, 60, 0, '2014-12-07 17:24:02'),
('DOC44', 'Surat untuk melakukan Audit / Perhitungan kerugian', 5, 60, 0, '2014-12-07 17:24:02'),
('DOC45', 'Gelar Perkara (Peningkatan status / penahanan / penghentian) ', 20, 60, 0, '2014-12-07 17:24:02'),
('DOC46', 'Slide Gelar / undangan / notulen / laporan hasil gelar', 10, 60, 0, '2014-12-07 17:24:02'),
('DOC47', 'SP2HP (Perkembangan sidik / penghentian dsb)', 5, 60, 0, '2014-12-07 17:24:02'),
('DOC48', 'Nota Dinas Hasil gelar', 5, 60, 0, '2014-12-07 17:24:02'),
('DOC49', 'Laporan Kemajuan', 10, 60, 0, '2014-12-07 17:24:02'),
('DOC50', 'Surat Perintah Penggeledahan', 2, 65, 0, '2014-12-07 17:24:02'),
('DOC52', 'Berita Acara Penggeledahan', 5, 65, 0, '2014-12-07 17:24:02'),
('DOC53', 'permohonan ijin penggeledahan kepada Pengadilan', 5, 65, 0, '2014-12-07 17:24:02'),
('DOC54', 'Surat Perintah Penyitaan', 2, 70, 0, '2014-12-07 17:24:02'),
('DOC56', 'BA penyitaan / STP / BA Penyisihan / BA Label / BA Lelang / BA Musnah / BA Titip', 10, 70, 0, '2014-12-07 17:24:02'),
('DOC57', 'permohonan penetapan sita / Ijin Sita / ijin khusus kepada Pengadilan', 5, 70, 0, '2014-12-07 17:24:02'),
('DOC58', 'Sprin penitipan BB / BA Penitipan ', 2, 70, 0, '2014-12-07 17:24:02'),
('DOC59', 'Surat Perintah Penangkapan', 5, 80, 0, '2014-12-07 17:24:02'),
('DOC61', 'Berita Acara Penangkapan', 40, 80, 0, '2014-12-07 17:24:02'),
('DOC62', 'Surat Perintah Penahanan', 5, 80, 0, '2014-12-07 17:24:02'),
('DOC64', 'Berita Acara Penahanan', 45, 85, 0, '2014-12-07 17:24:02'),
('DOC65', 'perpanjangan penahanan (Penyidik/JPU/Pengadilan) / BA Perpanjangan', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC66', 'Sprin Pembantaran / BA Pembantaran', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC67', 'Pencabutan Pembantaran', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC68', 'Sprin Pengalihan Jenis Tahanan / BA Pengalihan Jenis Tahanan', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC69', 'Sprin Penangguhan Penahanan / BA Penangguhan', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC70', 'Sprin Pengeluaran tahanan / BA Pengeluaran Tahanan', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC71', 'Surat Perintah Membawa', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC72', 'Surat Perintah Membawa', 2, 85, 0, '2014-12-07 17:24:02'),
('DOC73', 'Berita Acara Membawa', 40, 85, 0, '2014-12-07 17:24:02'),
('DOC74', 'Gelar perkara tahap tengah / akhir', 20, 90, 0, '2014-12-07 17:24:02'),
('DOC75', 'DPO / DPB', 2, 90, 0, '2014-12-07 17:24:02'),
('DOC76', 'Pencabutan DPO / DPB', 2, 90, 0, '2014-12-07 17:24:02'),
('DOC77', 'Pengambilan foto dan sidik jari', 10, 90, 0, '2014-12-07 17:24:02'),
('DOC78', 'Resume', 50, 90, 0, '2014-12-07 17:24:02'),
('DOC79', 'Menyusun / membuat Berkas Perkara ', 30, 90, 0, '2014-12-07 17:24:02'),
('DOC80', 'Surat Pengantar Pengiriman Berkas Perkara', 2, 90, 0, '2014-12-07 17:24:02'),
('DOC82', 'Menerima P-19', 30, 90, 1, '2014-12-07 17:24:02'),
('DOC83', 'Memenuhi P-19', 15, 90, 0, '2014-12-07 17:24:02'),
('DOC84', 'Menerima P-20', 20, 90, 1, '2014-12-07 17:24:02'),
('DOC85', 'Menerima P-21A', 20, 90, 1, '2014-12-07 17:24:02'),
('DOC86', 'Menerima P-21', 50, 100, 0, '2014-12-07 17:24:02'),
('DOC87', 'Pengiriman TSK dan BB / Tahap 2', 20, 100, 0, '2014-12-07 17:24:02'),
('DOC88', 'SP3', 30, 100, 0, '2014-12-07 17:24:02'),
('DOC89', 'Penetapan Penghentian Penyidikan', 2, 100, 0, '2014-12-07 17:24:02'),
('DOC91', 'Menerima P-22', 10, 100, 1, '2014-12-07 17:24:02'),
('DOC92', 'surat pelimpahan berkas perkara ke penyidik lain (instansi lain)', 50, 100, 0, '2014-12-07 17:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_doc_iden`
--

CREATE TABLE `tb_doc_iden` (
  `ID_DOC_IDEN` varchar(5) NOT NULL,
  `NAMA_DOC` varchar(50) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_doc_iden`
--

INSERT INTO `tb_doc_iden` (`ID_DOC_IDEN`, `NAMA_DOC`, `POINT`, `tgl_masuk_data`) VALUES
('DOC79', 'BA Pemeriksaan TKP', 15, '2012-08-05 17:00:00'),
('DOC80', 'Sketsa TKP', 25, '2012-08-05 17:00:00'),
('DOC81', 'BA Penemuan Barang Bukti di TKP', 10, '2013-12-03 07:30:39'),
('DOC82', 'BA Pengambilan Darah di TKP', 25, '2012-08-05 17:00:00'),
('DOC83', 'BA Pengambilan Sperma di TKP', 25, '2012-08-05 17:00:00'),
('DOC84', 'BA Pengambilan Jejak di TKP', 25, '2012-08-05 17:00:00'),
('DOC85', 'BA Pemotretan di TKP', 25, '2013-12-03 07:31:01'),
('DOC96', 'BA Olah TKP', 50, '2013-12-03 07:31:18'),
('DOC97', 'BA Pengangkatan Sidik Jari', 50, '2013-12-03 07:31:35'),
('DOC98', 'BA Pembandingan Sidik Jari', 50, '2012-08-05 17:00:00'),
('DO100', 'BA Rekam Video', 25, '2013-12-03 07:31:18'),
('DO101', 'BA Bongkar Mayat', 25, '2013-12-03 07:31:35'),
('DO102', 'BA Bedah Mayat', 25, '2012-08-05 17:00:00'),
('DO105', 'Sidik Jari Tersangka', 25, '2013-09-30 08:46:42');

-- --------------------------------------------------------

--
-- Table structure for table `tb_doc_opsnal`
--

CREATE TABLE `tb_doc_opsnal` (
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `NAMA_DOC` varchar(50) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_doc_opsnal`
--

INSERT INTO `tb_doc_opsnal` (`ID_DOC_OPSNAL`, `NAMA_DOC`, `POINT`) VALUES
('DOC01', 'Laporan Informasi', 50),
('DOC10', 'SPRIN Penyelidikan', 2),
('DOC03', 'BA Observasi', 5),
('DOC04', 'BA Interogasi', 5),
('DOC05', 'BA Pembuntutan', 5),
('DOC06', 'BA Penyamaran', 5),
('DOC07', 'BA Pelacakan', 10),
('DOC08', 'BA Lit  Analisis Dokumen', 15),
('DOC61', 'BA Penangkapan', 50),
('DOC52', 'BA Penggeledahan', 10),
('DOC11', 'SPRIN Tugas', 2),
('DOC12', 'BA Sita', 2),
('DOC13', 'Lap Hasil Penyelidikan', 6),
('DOC14', 'BA Membawa', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tb_doc_pam`
--

CREATE TABLE `tb_doc_pam` (
  `ID_DOC_PAM` varchar(5) NOT NULL,
  `NAMA_DOC` varchar(50) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_doc_sp2hp`
--

CREATE TABLE `tb_doc_sp2hp` (
  `ID_DOC_SP2HP` varchar(5) NOT NULL,
  `NAMA_DOC_SP2HP` varchar(50) NOT NULL,
  `PROSENTASE` int(3) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_doc_sp2hp`
--

INSERT INTO `tb_doc_sp2hp` (`ID_DOC_SP2HP`, `NAMA_DOC_SP2HP`, `PROSENTASE`, `tgl_masuk_data`) VALUES
('SP01', 'SP2HP LIDIK', 5, '2012-08-05 17:00:00'),
('SP02', 'SP2HP HENTI LIDIK', 100, '2012-12-17 04:42:53'),
('SP03', 'SP2HP KAT SIDIK', 15, '2012-08-05 17:00:00'),
('SP04', 'SP2HP PUL BUKTI', 15, '2012-08-05 17:00:00'),
('SP05', 'SP2HP RIKSA TSK', 40, '2012-08-05 17:00:00'),
('SP06', 'SP2HP KIRIM BP', 15, '2012-08-05 17:00:00'),
('SP07', 'SP2HP SELRA', 100, '2012-12-17 04:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenis_bb`
--

CREATE TABLE `tb_jenis_bb` (
  `ID_JENIS_BB` varchar(4) NOT NULL,
  `NAMA_JENIS_BB` varchar(50) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_jenis_bb`
--

INSERT INTO `tb_jenis_bb` (`ID_JENIS_BB`, `NAMA_JENIS_BB`, `tgl_masuk_data`) VALUES
('BB01', 'KENDARAAN RODA EMPAT', '2012-08-05 17:00:00'),
('BB02', 'KENDARAAN RODA DUA', '2012-08-05 17:00:00'),
('BB03', 'HANDPHONE', '2012-08-05 17:00:00'),
('BB04', 'SERTIFIKAT', '2012-08-05 17:00:00'),
('BB05', 'BUKTI SURAT', '2012-08-05 17:00:00'),
('BB06', 'BARANG BUKTI LAINNYA', '2012-08-05 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus`
--

CREATE TABLE `tb_kasus` (
  `ID_KASUS` varchar(10) NOT NULL,
  `NAMA_KASUS` varchar(100) NOT NULL,
  `DESKRIPSI_KASUS` varchar(2000) NOT NULL,
  `TGL_KEJADIAN` datetime NOT NULL,
  `NO_LP` varchar(50) DEFAULT NULL,
  `ID_PELAPOR` varchar(10) NOT NULL,
  `NRP` varchar(10) DEFAULT NULL,
  `ID_PASAL` varchar(5) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ID_BOBOT` varchar(5) NOT NULL DEFAULT 'BOB01',
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL,
  `NAMA_POLSEK` varchar(25) DEFAULT NULL,
  `NAMA_SUBDIT` varchar(25) DEFAULT NULL,
  `NAMA_UNIT` varchar(25) DEFAULT NULL,
  `NAMA_SUBNIT` varchar(25) DEFAULT NULL,
  `ID_TIM_OPSNAL` varchar(5) DEFAULT NULL,
  `IS_PERHATIAN_PUBLIK` int(1) DEFAULT '0',
  `NO_LI` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus`
--

INSERT INTO `tb_kasus` (`ID_KASUS`, `NAMA_KASUS`, `DESKRIPSI_KASUS`, `TGL_KEJADIAN`, `NO_LP`, `ID_PELAPOR`, `NRP`, `ID_PASAL`, `tgl_masuk_data`, `ID_BOBOT`, `NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_POLSEK`, `NAMA_SUBDIT`, `NAMA_UNIT`, `NAMA_SUBNIT`, `ID_TIM_OPSNAL`, `IS_PERHATIAN_PUBLIK`, `NO_LI`) VALUES
('K160622001', 'Pencemaran Nama Baik', 'Terjadi Tindak Pidana pencemaran nama baik', '2016-06-22 14:16:44', 'LP / 201 / VI / 2016 / SPKT', 'L160622001', '78110968', 'PS005', '2016-06-22 09:48:20', 'BOB02', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL, NULL, 1, NULL),
('K160624001', 'PENIPUAN DAN ATAU PENGGELAPAN', 'junaedi memint uang buat investasi sampai tenggat waktu kabur', '2016-06-24 09:34:21', 'LP/8/VI/2016/SPKT/BALI', 'L160624001', '78110968', 'PS004', '2016-06-24 02:11:13', 'BOB03', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL, NULL, 1, NULL),
('K160627001', 'PENYEROBOTAN HAK ATAS TANAH', 'terjadi tindak pidana', '2016-06-27 22:29:39', 'LP / 13 / VI / 2016 / SPKT', 'L160627001', '84020445', 'PS079', '2016-06-27 14:31:39', 'BOB01', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL, '', 0, ''),
('K160716001', 'DITEMUKAN BERMAIN JUDI (JENIS SABUNG AYAM)', 'terjadi tindak pidana judi sambung ayam', '2016-07-16 13:11:17', 'LP / 19 / VI / 2016 / SPKT / BALI', 'L160624001', '67110078', 'PS043', '2016-07-21 18:00:46', 'BOB01', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL, '', 0, ''),
('K160716002', 'MEMBAWA LARI PEREMPUAN', 'terjadi tindak pidana membawa lari perempuan', '2016-07-16 13:27:28', 'LP / 07 / VI I/ 2016 / SPKT / BALI', 'L160624001', '75040680', 'PS058', '2016-07-22 11:23:12', 'BOB02', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL, NULL, 1, NULL),
('K160719001', '170 PENGEROYOKAN', 'terjadi tindak pidana pengeroyokan', '2016-07-19 12:03:02', 'LP / 17 / VII / 2016 / SPKT / BALI', 'L160622001', '85041343', 'PS067', '2016-07-22 11:24:10', 'BOB03', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT II', NULL, NULL, 1, NULL),
('K160910001', 'Penambangan Tanpa Ijin Usaha', 'Terjadi Tidak Pidana Penambangan Tanpa Ijin Usaha Oleh Terlapor', '2016-09-07 16:49:46', 'LP / 13 / VI / 2016 / SPKT / BALI', 'L160622001', '67110078', 'PS089', '2016-09-10 08:52:26', 'BOB01', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', '', 'SUBDIT IV', 'UNIT II', NULL, '', 1, ''),
('K160910002', 'PERAMPASAN SEPEDA MOTOR', 'terjadi tindak Pidana PERAMPASAN SEPEDA MOTOR', '2016-09-07 17:13:32', 'LP / 87 / X / 2016 / SPKT / MAKASSAR', 'L160627001', '59051036', 'PS072', '2016-09-10 09:15:35', 'BOB01', 'POLRESTABES MAKASSAR', 'SATRESKRIM', '', 'SUBDIT I', 'UNIT VII', 'SUBNIT II', '', 0, ''),
('K160910003', '170 PENGEROYOKAN DAN BAWA SAJAM', 'terjadi tindak Pidana 170 PENGEROYOKAN DAN BAWA SAJAM', '2016-09-06 17:16:29', 'LP / 34 / X / 2016 / SPKT / GOWA', 'L160622001', '76070904', 'PS094', '2016-09-10 09:20:50', 'BOB01', 'POLRES GOWA', 'SATRESKRIM', '', 'SUBDIT I', 'UNIT II', 'SUBNIT I', '', 1, ''),
('K160910004', 'KEKERASAN ADALAM RUMAH TANGGA', 'terjadi tindak pidana KEKERASAN ADALAM RUMAH TANGGA', '2016-09-04 17:27:45', 'LP / 10 / X / 2016 / POLSEK UJUNG PANDANG', 'L160624001', '87030164', 'PS096', '2016-09-10 09:29:10', 'BOB01', 'POLRESTABES MAKASSAR', '', 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I', '', 0, ''),
('K160922001', 'PEMERASAN DAN PENGANCAMAN', 'terjadi tindak pidana PEMERASAN DAN PENGANCAMAN', '2016-09-21 15:00:00', 'LP / 45 / IX / 2016 / SPKT / SULSEL', 'L160624001', '84061339', 'PS005', '2016-09-27 12:22:12', 'BOB02', 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', '', 'SUBDIT I', 'UNIT II', 'SUBNIT I', NULL, 1, NULL),
('K160922002', 'PELECEHAN SEKSUAL DIBAWAH UMUR', 'terjadi tindak pidana PELECEHAN SEKSUAL DIBAWAH UMUR', '2016-09-22 07:00:00', 'LP / 37 / IX / 453 / 2016 / SATRESKRIM', 'L160627001', '', 'SP107', '2016-09-22 08:02:47', 'BOB01', 'POLRESTABES MAKASSAR', 'SATRESKRIM', '', 'SUBDIT I', NULL, NULL, NULL, 0, NULL),
('K160922003', 'PERAMPASAN SEPEDA MOTOR', 'terjadi tindk pidana PERAMPASAN SEPEDA MOTOR', '2016-09-22 16:08:22', 'LP / 11 / IX / 2016 / POLSEK UJUNG PANDAANG', 'L160622001', '85100981', 'PS073', '2016-09-27 12:34:59', 'BOB02', 'POLRESTABES MAKASSAR', 'UNIT RESKRIM', 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I', NULL, 0, NULL),
('K160922004', 'PERAMPASAN SEPEDA MOTOR', 'terjadi tindak pidana PERAMPASAN SEPEDA MOTOR', '2016-09-22 13:11:25', 'LP / 21 / IX / 453 / 2016 / SPKT GOWA', 'L160627001', '', 'PS073', '2016-09-22 08:12:45', 'BOB01', 'POLRES GOWA', 'SATRESKRIM', '', 'SUBDIT I', NULL, NULL, NULL, 0, NULL),
('K160923001', 'DUGAAN PENYALAHGUNAAN DANA ANGGARAN', 'terjadi tindak pidana DUGAAN PENYALAHGUNAAN DANA ANGGARAN', '2016-09-23 11:29:00', 'LP / 13 / IX/ 453 / 2016 / SPKT SULSEL', 'L160624001', '', 'PS011', '2016-09-23 03:30:43', 'BOB01', 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', '', '', '', '', NULL, 0, NULL),
('K160923002', 'MEMBAWA LARI SESEORANG DENGAN MELAWAN HUKUM (PENCULIKAN)', 'terjadi tindak pidana MEMBAWA LARI SESEORANG DENGAN MELAWAN HUKUM (PENCULIKAN)', '2016-09-23 03:32:12', 'LP / 110 / VI / 2016 / SPKT / BALI', 'L160622001', '65120789', 'PS058', '2016-09-27 18:22:31', 'BOB02', 'POLRESTABES MAKASSAR', 'SATRESKRIM', '', 'SUBDIT I', 'UNIT III', 'SUBNIT III', NULL, 1, NULL),
('K160923003', 'PEMBAKARAN GROBAK TEMPAT JUALAN', 'terjadi tindak pidana PEMBAKARAN GROBAK TEMPAT JUALAN', '2016-09-23 13:34:12', 'LP / 145 / VI / 2016 / SPKT / POLSEK', 'L160627001', '83121159', 'SP108', '2016-09-27 12:38:37', 'BOB02', 'POLRESTABES MAKASSAR', 'UNIT RESKRIM', 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I', NULL, 0, NULL),
('K160923004', 'MEMBUAT PERASAAN TDK ENAK DAN DISERTAI DGN PENGANCAMAN', 'MEMBUAT PERASAAN TDK ENAK DAN DISERTAI DGN PENGANCAMAN', '2016-09-23 16:39:54', 'LP / 130 / 15/ 453 / 2016 / GOWA', 'L160624001', '83010472', 'PS047', '2016-09-27 12:36:25', 'BOB02', 'POLRES GOWA', 'SATRESKRIM', '', 'SUBDIT I', 'UNIT II', 'SUBNIT I', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb`
--

CREATE TABLE `tb_kasus_bb` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB06',
  `ID_BB` varchar(5) NOT NULL,
  `NAMA_BB` varchar(500) NOT NULL,
  `JUMLAH_BB` varchar(50) NOT NULL,
  `PENGGUNAAN` varchar(200) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `KOTA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `NO_DPM` varchar(4) DEFAULT NULL,
  `TGL_DISITA` datetime DEFAULT NULL,
  `KETERANGAN` varchar(3000) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `IS_DPB` int(1) DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_bb`
--

INSERT INTO `tb_kasus_bb` (`ID_KASUS`, `ID_JENIS_BB`, `ID_BB`, `NAMA_BB`, `JUMLAH_BB`, `PENGGUNAAN`, `NAMA_PEMILIK_BB`, `ALAMAT_PEMILIK_BB`, `KOTA_PEMILIK_BB`, `HP_PEMILIK_BB`, `TELP_PEMILIK_BB`, `STATUS_PEMILIK_BB`, `NO_DPM`, `TGL_DISITA`, `KETERANGAN`, `ID_DOC_BERKAS`, `BERKAS_KE`, `IS_DPB`, `tgl_masuk_data`) VALUES
('K160622001', 'BB06', 'BB001', 'Capture FB Terlapor', '2 halaman', 'Dipakai untuk mencemarkan nama baik pelapor', 'Terlapor', 'Denpasar', 'Denpasar', '', '-', 'TERS', '-', '2016-07-15 00:00:00', '2016-06-22 00:00:00', NULL, NULL, 0, '2016-07-29 01:25:37'),
('K160719001', 'BB06', 'BB001', 'Senjata Tajam', '5 buah', 'dipakai oleh pelaku', '', '', '', '', '', 'SAKSI', '', '2016-07-26 00:00:00', '', NULL, NULL, 0, '2016-07-29 01:13:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb_hp`
--

CREATE TABLE `tb_kasus_bb_hp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB03',
  `ID_BB` varchar(5) NOT NULL,
  `NOSIM` varchar(50) DEFAULT NULL,
  `NOIMEI` varchar(50) DEFAULT NULL,
  `NOIMSI` varchar(50) DEFAULT NULL,
  `MERK` varchar(100) DEFAULT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `WARNA` varchar(50) DEFAULT NULL,
  `POSISI_TERAKHIR` varchar(200) DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `IS_DPB` int(1) DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_bb_hp`
--

INSERT INTO `tb_kasus_bb_hp` (`ID_KASUS`, `ID_JENIS_BB`, `ID_BB`, `NOSIM`, `NOIMEI`, `NOIMSI`, `MERK`, `TIPE`, `WARNA`, `POSISI_TERAKHIR`, `NO_DPM`, `NAMA_PEMILIK_BB`, `ALAMAT_PEMILIK_BB`, `HP_PEMILIK_BB`, `TELP_PEMILIK_BB`, `STATUS_PEMILIK_BB`, `ID_DOC_BERKAS`, `BERKAS_KE`, `IS_DPB`, `tgl_masuk_data`) VALUES
('K160624001', 'BB03', 'BB001', 'perwe', '43234', '', 'Samsung', 'android V3', 'Putih', '', '', '', '', '', '', 'SAKSI', NULL, NULL, 0, '2016-06-24 02:19:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb_r2`
--

CREATE TABLE `tb_kasus_bb_r2` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB02',
  `ID_BB` varchar(5) NOT NULL,
  `NOPOL` varchar(50) DEFAULT NULL,
  `NOKA` varchar(50) DEFAULT NULL,
  `NOSIN` varchar(50) DEFAULT NULL,
  `NOBPKB` varchar(50) DEFAULT NULL,
  `MERK` varchar(100) DEFAULT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `JENIS` varchar(50) DEFAULT NULL,
  `MODEL` varchar(50) DEFAULT NULL,
  `WARNA` varchar(50) DEFAULT NULL,
  `TAHUN` varchar(4) DEFAULT NULL,
  `SILINDER` varchar(50) DEFAULT NULL,
  `NO_FIDUSIA` varchar(50) DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `IS_DPB` int(1) DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb_r4`
--

CREATE TABLE `tb_kasus_bb_r4` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB01',
  `ID_BB` varchar(5) NOT NULL,
  `NOPOL` varchar(50) DEFAULT NULL,
  `NOKA` varchar(50) DEFAULT NULL,
  `NOSIN` varchar(50) DEFAULT NULL,
  `NOBPKB` varchar(50) DEFAULT NULL,
  `MERK` varchar(100) DEFAULT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `JENIS` varchar(50) DEFAULT NULL,
  `MODEL` varchar(50) DEFAULT NULL,
  `WARNA` varchar(50) DEFAULT NULL,
  `TAHUN` varchar(4) DEFAULT NULL,
  `SILINDER` varchar(50) DEFAULT NULL,
  `NO_FIDUSIA` varchar(50) DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `IS_DPB` int(1) DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb_sertifikat`
--

CREATE TABLE `tb_kasus_bb_sertifikat` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB04',
  `ID_BB` varchar(5) NOT NULL,
  `NOSERTIFIKAT` varchar(50) DEFAULT NULL,
  `KELURAHAN` varchar(100) DEFAULT NULL,
  `NAMA_PEMILIK` varchar(50) DEFAULT NULL,
  `TGL_PENERBITAN` date DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `IS_DPB` int(1) DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb_sertifikat_peralihan`
--

CREATE TABLE `tb_kasus_bb_sertifikat_peralihan` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB04',
  `ID_BB` varchar(5) NOT NULL,
  `TGL_PERALIHAN` date NOT NULL DEFAULT '0000-00-00',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_bb_surat`
--

CREATE TABLE `tb_kasus_bb_surat` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB05',
  `ID_BB` varchar(5) NOT NULL,
  `NAMA_DOKUMEN` varchar(50) DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `TGL_PENERBITAN` date DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `IS_DPB` int(1) DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_berkas`
--

CREATE TABLE `tb_kasus_doc_berkas` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_BERKAS` varchar(5) NOT NULL,
  `NRP` varchar(12) NOT NULL DEFAULT '',
  `POINT` int(3) NOT NULL DEFAULT '0',
  `TGL_UPLOAD` datetime NOT NULL,
  `PATH` varchar(255) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOBOT_POINT` int(3) NOT NULL DEFAULT '0',
  `PROSENTASE` int(3) NOT NULL DEFAULT '0',
  `IS_MINUS` int(1) DEFAULT '0',
  `AUTHOR` varchar(12) NOT NULL DEFAULT '',
  `KETERANGAN_BAP` varchar(50) DEFAULT NULL,
  `is_preview_kasubdit` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_doc_berkas`
--

INSERT INTO `tb_kasus_doc_berkas` (`ID_KASUS`, `ID_DOC_BERKAS`, `NRP`, `POINT`, `TGL_UPLOAD`, `PATH`, `BERKAS_KE`, `tgl_masuk_data`, `BOBOT_POINT`, `PROSENTASE`, `IS_MINUS`, `AUTHOR`, `KETERANGAN_BAP`, `is_preview_kasubdit`) VALUES
('K160622001', 'DOC04', '78110968', 2, '2016-06-22 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_DOC04_1.pdf', 1, '2016-06-22 14:48:13', 3, 5, 0, '78110968', '', 1),
('K160622001', 'DOC09', '78110968', 2, '2016-06-24 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_DOC09_1.pdf', 1, '2016-06-23 04:09:39', 3, 5, 0, '78110968', '', 1),
('K160624001', 'DOC09', '78110968', 2, '2016-06-24 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160624001_DOC09_1.pdf', 1, '2016-06-24 02:31:13', 6, 10, 0, '78110968', '', 1),
('K160624001', 'DOC30', '78110968', 20, '2016-06-24 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160624001_DOC30_1.pdf', 1, '2016-06-24 02:37:37', 60, 20, 0, '74120011', 'undefined', 1),
('K160627001', 'DOC09', '84020445', 2, '2016-06-27 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160627001_DOC09_1.pdf', 1, '2016-06-27 14:49:44', 2, 10, 0, '84020445', '', 1),
('K160627001', 'DOC08', '84020445', 2, '2016-06-28 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160627001_DOC08_1.pdf', 1, '2016-06-27 14:50:11', 2, 0, 0, '84020445', '', 1),
('K160627001', 'DOC61', '84020445', 40, '2016-06-30 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160627001_DOC61_1.pdf', 1, '2016-06-27 15:01:15', 40, 70, 0, '73110093', '', 1),
('K160627001', 'DOC61', '84020445', 40, '2016-06-30 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160627001_DOC61_2.pdf', 2, '2016-06-27 15:01:15', 40, 0, 0, '76110488', '', 1),
('K160627001', 'DOC61', '84020445', 40, '2016-06-30 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160627001_DOC61_3.pdf', 3, '2016-06-27 15:01:15', 40, 0, 0, '83030868', '', 1),
('K160719001', 'DOC12', '85041343', 10, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160719001_DOC12_1.pdf', 1, '2016-07-23 05:13:10', 30, 10, 0, '85041343', '', 1),
('K160719001', 'DOC12', '85041343', 10, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160719001_DOC12_2.pdf', 2, '2016-07-23 05:13:10', 30, 0, 0, '89060465', '', 1),
('K160719001', 'DOC12', '85041343', 10, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160719001_DOC12_3.pdf', 3, '2016-07-23 05:13:10', 30, 0, 0, '91030140', '', 1),
('K160719001', 'DOC08', '85041343', 2, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160719001_DOC08_1.pdf', 1, '2016-07-23 05:14:04', 6, 0, 0, '85041343', '', 1),
('K160622001', 'DOC12', '78110968', 10, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_DOC12_1.pdf', 1, '2016-07-23 05:14:50', 15, 0, 0, '69090591', '', 1),
('K160622001', 'DOC12', '78110968', 10, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_DOC12_2.pdf', 2, '2016-07-23 05:14:50', 15, 0, 0, '78110968', '', 1),
('K160622001', 'DOC12', '78110968', 10, '2016-07-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_DOC12_3.pdf', 3, '2016-07-23 05:14:50', 15, 0, 0, '86090609', '', 1),
('K160622001', 'DOC35', '78110968', 40, '2016-07-24 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_DOC35_1.pdf', 1, '2016-07-23 06:07:10', 60, 30, 0, '83100112', '', 1),
('K160719001', 'DOC12', '85041343', 10, '2016-07-24 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160719001_DOC12_4.pdf', 4, '2016-07-23 06:10:27', 30, 0, 0, '79051113', '', 1),
('K160910001', 'DOC08', '67110078', 2, '2016-09-10 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910001_DOC08_1.pdf', 1, '2016-09-10 14:23:55', 2, 10, 0, '67110078', '', 1),
('K160910001', 'DOC08', '67110078', 2, '2016-09-10 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910001_DOC08_2.pdf', 2, '2016-09-10 14:23:55', 2, 0, 0, '76070400', '', 1),
('K160910001', 'DOC08', '67110078', 2, '2016-09-10 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910001_DOC08_3.pdf', 3, '2016-09-10 14:23:55', 2, 0, 0, '92090401', '', 1),
('K160910002', 'DOC04', '59051036', 2, '2016-09-11 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910002_DOC04_1.pdf', 1, '2016-09-11 04:19:52', 2, 5, 0, '59051036', '', 1),
('K160910002', 'DOC08', '59051036', 2, '2016-09-12 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910002_DOC08_1.pdf', 1, '2016-09-11 04:20:35', 2, 5, 0, '64030759', '', 1),
('K160910002', 'DOC08', '59051036', 2, '2016-09-12 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910002_DOC08_2.pdf', 2, '2016-09-11 04:20:35', 2, 0, 0, '73110188', '', 1),
('K160910002', 'DOC08', '59051036', 2, '2016-09-12 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910002_DOC08_3.pdf', 3, '2016-09-11 04:20:35', 2, 0, 0, '91020021', '', 1),
('K160910004', 'DOC08', '87030164', 2, '2016-09-08 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_DOC08_1.pdf', 1, '2016-09-11 04:25:37', 2, 10, 0, '85031692', '', 1),
('K160910004', 'DOC08', '87030164', 2, '2016-09-08 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_DOC08_2.pdf', 2, '2016-09-11 04:25:37', 2, 0, 0, '86030610', '', 1),
('K160910004', 'DOC08', '87030164', 2, '2016-09-08 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_DOC08_3.pdf', 3, '2016-09-11 04:25:37', 2, 0, 0, '87030164', '', 1),
('K160910004', 'DOC09', '87030164', 2, '2016-09-09 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_DOC09_1.pdf', 1, '2016-09-11 04:25:59', 2, 0, 0, '85031692', '', 1),
('K160910004', 'DOC09', '87030164', 2, '2016-09-09 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_DOC09_2.pdf', 2, '2016-09-11 04:25:59', 2, 0, 0, '86030610', '', 1),
('K160910004', 'DOC09', '87030164', 2, '2016-09-09 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_DOC09_3.pdf', 3, '2016-09-11 04:25:59', 2, 0, 0, '87030164', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_berkas_koreksi`
--

CREATE TABLE `tb_kasus_doc_berkas_koreksi` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_BERKAS` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `ID_KOREKSI` varchar(10) NOT NULL DEFAULT '',
  `KOREKSI` varchar(300) DEFAULT NULL,
  `TGL_KOREKSI` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_berkas_non_acc`
--

CREATE TABLE `tb_kasus_doc_berkas_non_acc` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_BERKAS` varchar(5) NOT NULL,
  `NRP` varchar(12) NOT NULL DEFAULT '',
  `POINT` int(3) NOT NULL DEFAULT '0',
  `TGL_UPLOAD` datetime NOT NULL,
  `PATH` varchar(255) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `BOBOT_POINT` int(3) NOT NULL DEFAULT '0',
  `PROSENTASE` int(3) NOT NULL DEFAULT '0',
  `AUTHOR` varchar(12) DEFAULT NULL,
  `KETERANGAN_BAP` varchar(50) DEFAULT NULL,
  `is_preview_kasubdit` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_berkas_target`
--

CREATE TABLE `tb_kasus_doc_berkas_target` (
  `ID_KASUS` varchar(10) NOT NULL,
  `PAKE_BERKAS` int(1) NOT NULL DEFAULT '0',
  `ID_DOC_BERKAS` varchar(5) NOT NULL DEFAULT '',
  `TGL_MULAI` datetime NOT NULL,
  `TGL_TARGET` datetime NOT NULL,
  `NRP_KASUBDIT` varchar(12) NOT NULL DEFAULT '',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `KETERANGAN_BAP` varchar(50) DEFAULT NULL,
  `BERKAS_KE` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_doc_berkas_target`
--

INSERT INTO `tb_kasus_doc_berkas_target` (`ID_KASUS`, `PAKE_BERKAS`, `ID_DOC_BERKAS`, `TGL_MULAI`, `TGL_TARGET`, `NRP_KASUBDIT`, `tgl_masuk_data`, `KETERANGAN_BAP`, `BERKAS_KE`) VALUES
('K160622001', 0, 'DOC17', '2016-08-01 00:00:00', '2016-08-12 00:00:00', 'KASBDIT402', '2016-08-10 03:35:42', '', 1),
('K160622001', 0, 'DOC35', '2016-08-10 00:00:00', '2016-08-10 00:00:00', 'KASBDIT402', '2016-08-10 04:19:24', '', 1),
('K160910001', 0, 'DOC30', '2016-09-12 00:00:00', '2016-09-15 00:00:00', 'KSD4SLSL01', '2016-09-11 07:17:03', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_berkas_target1`
--

CREATE TABLE `tb_kasus_doc_berkas_target1` (
  `ID_KASUS` varchar(10) NOT NULL,
  `PAKE_BERKAS` int(1) NOT NULL DEFAULT '0',
  `ID_DOC_BERKAS` varchar(5) NOT NULL DEFAULT '',
  `TGL_MULAI` datetime NOT NULL,
  `TGL_TARGET` datetime NOT NULL,
  `NRP_KASUBDIT` varchar(12) NOT NULL DEFAULT '',
  `tgl_masuk_data` timestamp NULL DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_iden`
--

CREATE TABLE `tb_kasus_doc_iden` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_IDEN` varchar(5) NOT NULL,
  `NRP` varchar(12) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0',
  `TGL_UPLOAD` datetime NOT NULL,
  `PATH` varchar(255) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `BOBOT_POINT` int(3) NOT NULL DEFAULT '0',
  `is_preview_kasubdit` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_opsnal`
--

CREATE TABLE `tb_kasus_doc_opsnal` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC` varchar(5) NOT NULL,
  `NRP` varchar(12) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0',
  `TGL_UPLOAD` datetime NOT NULL,
  `PATH` varchar(255) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `BOBOT_POINT` int(3) NOT NULL DEFAULT '0',
  `AUTHOR` varchar(12) DEFAULT NULL,
  `KETERANGAN_BAP` varchar(50) DEFAULT NULL,
  `is_preview_kasubdit` int(1) DEFAULT '0',
  `is_dok_berkas` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_sp2hp`
--

CREATE TABLE `tb_kasus_doc_sp2hp` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_SP2HP` varchar(5) NOT NULL,
  `NRP` varchar(12) NOT NULL DEFAULT '',
  `PROSENTASE` int(3) NOT NULL,
  `TGL_UPLOAD` datetime NOT NULL,
  `PATH` varchar(255) DEFAULT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_preview_kasubdit` int(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_doc_sp2hp`
--

INSERT INTO `tb_kasus_doc_sp2hp` (`ID_KASUS`, `ID_DOC_SP2HP`, `NRP`, `PROSENTASE`, `TGL_UPLOAD`, `PATH`, `BERKAS_KE`, `tgl_masuk_data`, `is_preview_kasubdit`) VALUES
('K160622001', 'SP01', '78110968', 5, '2016-06-23 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_SP01_1.pdf', 1, '2016-06-23 04:12:09', 1),
('K160622001', 'SP03', '78110968', 15, '2016-06-25 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160622001_SP03_1.pdf', 1, '2016-06-23 04:12:13', 1),
('K160627001', 'SP01', '84020445', 5, '2016-06-29 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160627001_SP01_1.pdf', 1, '2016-08-08 08:35:15', 1),
('K160910001', 'SP01', '67110078', 5, '2016-09-10 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910001_SP01_1.pdf', 1, '2016-09-10 14:13:08', 0),
('K160910001', 'SP03', '67110078', 15, '2016-09-12 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910001_SP03_1.pdf', 1, '2016-09-10 14:13:31', 0),
('K160910002', 'SP03', '59051036', 15, '2016-09-11 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910002_SP03_1.pdf', 1, '2016-09-11 04:18:35', 0),
('K160910002', 'SP03', '59051036', 0, '2016-09-13 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910002_SP03_2.pdf', 2, '2016-09-11 04:19:00', 0),
('K160910004', 'SP01', '87030164', 5, '2016-09-08 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_SP01_1.pdf', 1, '2016-09-11 04:24:23', 0),
('K160910004', 'SP03', '87030164', 15, '2016-09-10 00:00:00', 'upload/pdf/realisasi_bp_sprin/K160910004_SP03_1.pdf', 1, '2016-09-11 04:24:49', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_doc_sp2hp_target`
--

CREATE TABLE `tb_kasus_doc_sp2hp_target` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `ID_DOC_SP2HP` varchar(5) NOT NULL DEFAULT '',
  `TGL_TARGET` datetime NOT NULL,
  `PROSENTASE` int(3) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_doc_sp2hp_target`
--

INSERT INTO `tb_kasus_doc_sp2hp_target` (`ID_KASUS`, `ID_BOBOT`, `ID_DOC_SP2HP`, `TGL_TARGET`, `PROSENTASE`, `tgl_masuk_data`) VALUES
('K160622001', 'BOB02', 'SP01', '2016-06-25 16:48:20', 5, '2016-06-22 09:48:20'),
('K160622001', 'BOB02', 'SP02', '2016-07-06 16:48:20', 100, '2016-06-22 09:48:20'),
('K160622001', 'BOB02', 'SP03', '2016-07-06 16:48:20', 15, '2016-06-22 09:48:20'),
('K160622001', 'BOB02', 'SP04', '2016-07-22 16:48:20', 15, '2016-06-22 09:48:20'),
('K160622001', 'BOB02', 'SP05', '2016-08-11 16:48:20', 40, '2016-06-22 09:48:20'),
('K160622001', 'BOB02', 'SP06', '2016-08-15 16:48:20', 15, '2016-06-22 09:48:20'),
('K160622001', 'BOB02', 'SP07', '2016-08-21 16:48:20', 10, '2016-06-22 09:48:20'),
('K160624001', 'BOB03', 'SP01', '2016-06-27 09:11:13', 5, '2016-06-24 02:11:13'),
('K160624001', 'BOB03', 'SP02', '2016-07-15 09:11:13', 100, '2016-06-24 02:11:13'),
('K160624001', 'BOB03', 'SP03', '2016-07-15 09:11:13', 15, '2016-06-24 02:11:13'),
('K160624001', 'BOB03', 'SP04', '2016-08-08 09:11:13', 15, '2016-06-24 02:11:13'),
('K160624001', 'BOB03', 'SP05', '2016-09-07 09:11:13', 40, '2016-06-24 02:11:13'),
('K160624001', 'BOB03', 'SP06', '2016-09-13 09:11:13', 15, '2016-06-24 02:11:13'),
('K160624001', 'BOB03', 'SP07', '2016-09-22 09:11:13', 10, '2016-06-24 02:11:13'),
('K160626001', 'BOB02', 'SP01', '2016-06-29 16:20:58', 5, '2016-06-26 09:20:58'),
('K160626001', 'BOB02', 'SP02', '2016-07-10 16:20:58', 100, '2016-06-26 09:20:58'),
('K160626001', 'BOB02', 'SP03', '2016-07-10 16:20:58', 15, '2016-06-26 09:20:58'),
('K160626001', 'BOB02', 'SP04', '2016-07-26 16:20:58', 15, '2016-06-26 09:20:58'),
('K160626001', 'BOB02', 'SP05', '2016-08-15 16:20:58', 40, '2016-06-26 09:20:58'),
('K160626001', 'BOB02', 'SP06', '2016-08-19 16:20:58', 15, '2016-06-26 09:20:58'),
('K160626001', 'BOB02', 'SP07', '2016-08-25 16:20:58', 10, '2016-06-26 09:20:58'),
('K160716002', 'BOB02', 'SP01', '2016-07-25 18:23:12', 5, '2016-07-22 11:23:12'),
('K160716002', 'BOB02', 'SP02', '2016-08-05 18:23:12', 100, '2016-07-22 11:23:12'),
('K160716002', 'BOB02', 'SP03', '2016-08-05 18:23:12', 15, '2016-07-22 11:23:12'),
('K160716002', 'BOB02', 'SP04', '2016-08-21 18:23:12', 15, '2016-07-22 11:23:12'),
('K160716002', 'BOB02', 'SP05', '2016-09-10 18:23:12', 40, '2016-07-22 11:23:12'),
('K160716002', 'BOB02', 'SP06', '2016-09-14 18:23:12', 15, '2016-07-22 11:23:12'),
('K160716002', 'BOB02', 'SP07', '2016-09-20 18:23:12', 10, '2016-07-22 11:23:12'),
('K160719001', 'BOB03', 'SP01', '2016-07-25 18:24:10', 5, '2016-07-22 11:24:10'),
('K160719001', 'BOB03', 'SP02', '2016-08-12 18:24:10', 100, '2016-07-22 11:24:10'),
('K160719001', 'BOB03', 'SP03', '2016-08-12 18:24:10', 15, '2016-07-22 11:24:10'),
('K160719001', 'BOB03', 'SP04', '2016-09-05 18:24:10', 15, '2016-07-22 11:24:10'),
('K160719001', 'BOB03', 'SP05', '2016-10-05 18:24:10', 40, '2016-07-22 11:24:10'),
('K160719001', 'BOB03', 'SP06', '2016-10-11 18:24:10', 15, '2016-07-22 11:24:10'),
('K160719001', 'BOB03', 'SP07', '2016-10-20 18:24:10', 10, '2016-07-22 11:24:10'),
('K160922001', 'BOB02', 'SP01', '2016-09-30 19:22:12', 5, '2016-09-27 12:22:12'),
('K160922001', 'BOB02', 'SP02', '2016-10-11 19:22:12', 100, '2016-09-27 12:22:12'),
('K160922001', 'BOB02', 'SP03', '2016-10-11 19:22:12', 15, '2016-09-27 12:22:12'),
('K160922001', 'BOB02', 'SP04', '2016-10-27 19:22:12', 15, '2016-09-27 12:22:12'),
('K160922001', 'BOB02', 'SP05', '2016-11-16 19:22:12', 40, '2016-09-27 12:22:12'),
('K160922001', 'BOB02', 'SP06', '2016-11-20 19:22:12', 15, '2016-09-27 12:22:12'),
('K160922001', 'BOB02', 'SP07', '2016-11-26 19:22:12', 10, '2016-09-27 12:22:12'),
('K160922003', 'BOB02', 'SP07', '2016-11-26 19:34:59', 10, '2016-09-27 12:34:59'),
('K160922003', 'BOB02', 'SP06', '2016-11-20 19:34:59', 15, '2016-09-27 12:34:59'),
('K160922003', 'BOB02', 'SP05', '2016-11-16 19:34:59', 40, '2016-09-27 12:34:59'),
('K160922003', 'BOB02', 'SP04', '2016-10-27 19:34:59', 15, '2016-09-27 12:34:59'),
('K160922003', 'BOB02', 'SP03', '2016-10-11 19:34:59', 15, '2016-09-27 12:34:59'),
('K160922003', 'BOB02', 'SP02', '2016-10-11 19:34:59', 100, '2016-09-27 12:34:59'),
('K160922003', 'BOB02', 'SP01', '2016-09-30 19:34:59', 5, '2016-09-27 12:34:59'),
('K160923004', 'BOB02', 'SP01', '2016-09-30 19:36:25', 5, '2016-09-27 12:36:25'),
('K160923004', 'BOB02', 'SP02', '2016-10-11 19:36:25', 100, '2016-09-27 12:36:25'),
('K160923004', 'BOB02', 'SP03', '2016-10-11 19:36:25', 15, '2016-09-27 12:36:25'),
('K160923004', 'BOB02', 'SP04', '2016-10-27 19:36:25', 15, '2016-09-27 12:36:25'),
('K160923004', 'BOB02', 'SP05', '2016-11-16 19:36:25', 40, '2016-09-27 12:36:25'),
('K160923004', 'BOB02', 'SP06', '2016-11-20 19:36:25', 15, '2016-09-27 12:36:25'),
('K160923004', 'BOB02', 'SP07', '2016-11-26 19:36:25', 10, '2016-09-27 12:36:25'),
('K160923003', 'BOB02', 'SP01', '2016-09-30 19:38:37', 5, '2016-09-27 12:38:37'),
('K160923003', 'BOB02', 'SP02', '2016-10-11 19:38:37', 100, '2016-09-27 12:38:37'),
('K160923003', 'BOB02', 'SP03', '2016-10-11 19:38:37', 15, '2016-09-27 12:38:37'),
('K160923003', 'BOB02', 'SP04', '2016-10-27 19:38:37', 15, '2016-09-27 12:38:37'),
('K160923003', 'BOB02', 'SP05', '2016-11-16 19:38:37', 40, '2016-09-27 12:38:37'),
('K160923003', 'BOB02', 'SP06', '2016-11-20 19:38:37', 15, '2016-09-27 12:38:37'),
('K160923003', 'BOB02', 'SP07', '2016-11-26 19:38:37', 10, '2016-09-27 12:38:37'),
('K160923002', 'BOB02', 'SP01', '2016-10-01 01:22:31', 5, '2016-09-27 18:22:31'),
('K160923002', 'BOB02', 'SP02', '2016-10-12 01:22:31', 100, '2016-09-27 18:22:31'),
('K160923002', 'BOB02', 'SP03', '2016-10-12 01:22:31', 15, '2016-09-27 18:22:31'),
('K160923002', 'BOB02', 'SP04', '2016-10-28 01:22:31', 15, '2016-09-27 18:22:31'),
('K160923002', 'BOB02', 'SP05', '2016-11-17 01:22:31', 40, '2016-09-27 18:22:31'),
('K160923002', 'BOB02', 'SP06', '2016-11-21 01:22:31', 15, '2016-09-27 18:22:31'),
('K160923002', 'BOB02', 'SP07', '2016-11-27 01:22:31', 10, '2016-09-27 18:22:31');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_hambatan`
--

CREATE TABLE `tb_kasus_hambatan` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_HAMBATAN` varchar(5) NOT NULL,
  `TAHAP_LIDIK_SIDIK` varchar(25) NOT NULL DEFAULT 'PENYELIDIKAN',
  `HAMBATAN` varchar(3000) NOT NULL,
  `SOLUSI_PENYELESAIAN` varchar(3000) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_hambatan`
--

INSERT INTO `tb_kasus_hambatan` (`ID_KASUS`, `ID_HAMBATAN`, `TAHAP_LIDIK_SIDIK`, `HAMBATAN`, `SOLUSI_PENYELESAIAN`, `tgl_masuk_data`) VALUES
('K160622001', 'HB001', 'PENYELIDIKAN', 'Terlapor melarikan diri', '', '2016-06-22 06:30:37'),
('K160624001', 'HB001', 'PENYELIDIKAN', 'barang bukti belum ditemukan', '', '2016-06-24 02:24:25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_kerugian`
--

CREATE TABLE `tb_kasus_kerugian` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_KERUGIAN` varchar(5) NOT NULL,
  `KERUGIAN` varchar(1000) NOT NULL,
  `JUMLAH_BARANG` varchar(500) NOT NULL,
  `PEMILIK` varchar(500) NOT NULL,
  `KETERANGAN` varchar(3000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_kerugian`
--

INSERT INTO `tb_kasus_kerugian` (`ID_KASUS`, `ID_KERUGIAN`, `KERUGIAN`, `JUMLAH_BARANG`, `PEMILIK`, `KETERANGAN`, `tgl_masuk_data`) VALUES
('K160622001', 'RG001', 'Uang Tunai', '10.000.000', 'pelapor', '', '2016-06-22 06:30:37'),
('K160624001', 'RG001', 'UANG', '10.000.000,-', 'PII', '', '2016-06-24 01:41:03'),
('K160624001', 'RG002', 'LAPTOP', '1', 'PII', 'MERK ASUS', '2016-06-24 01:41:03'),
('K160910001', 'RG001', 'Batu Pasir', '2 truk kontaainer', 'Haji salam', '', '2016-09-10 08:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_korban`
--

CREATE TABLE `tb_kasus_korban` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_KORBAN` varchar(5) NOT NULL,
  `NAMA_KORBAN` varchar(50) NOT NULL,
  `ALAMAT_KORBAN` varchar(300) NOT NULL,
  `KONDISI_KORBAN` varchar(200) NOT NULL,
  `USIA_KORBAN` varchar(50) NOT NULL,
  `CIRI_KORBAN` varchar(200) NOT NULL,
  `KETERANGAN` varchar(3000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_korban`
--

INSERT INTO `tb_kasus_korban` (`ID_KASUS`, `ID_KORBAN`, `NAMA_KORBAN`, `ALAMAT_KORBAN`, `KONDISI_KORBAN`, `USIA_KORBAN`, `CIRI_KORBAN`, `KETERANGAN`, `tgl_masuk_data`) VALUES
('K160622001', 'KB001', 'Pelapor', 'Denpasar', '-', '', '', '', '2016-06-22 06:30:37'),
('K160624001', 'KB001', 'PII', 'SD PELAPOR', '', '', '', '', '2016-06-24 01:41:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_opsnal`
--

CREATE TABLE `tb_kasus_opsnal` (
  `ID_KASUS` varchar(10) NOT NULL,
  `NRP` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_opsnal`
--

INSERT INTO `tb_kasus_opsnal` (`ID_KASUS`, `NRP`) VALUES
('K160622001', 'null'),
('K160624001', 'null'),
('K160626001', 'null'),
('K160716002', 'null'),
('K160719001', 'null'),
('K160923002', 'undefined');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_ren_lidik_realisasi`
--

CREATE TABLE `tb_kasus_ren_lidik_realisasi` (
  `id_kasus` varchar(10) NOT NULL,
  `ren_lidik` varchar(100) NOT NULL,
  `deskripsi` varchar(3000) DEFAULT NULL,
  `sasaran` varchar(3000) DEFAULT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `durasi_hari` int(3) DEFAULT NULL,
  `lokasi_kegiatan` varchar(200) DEFAULT NULL,
  `rab` int(12) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `is_verifikasi` int(1) NOT NULL DEFAULT '0',
  `author` varchar(8) DEFAULT NULL,
  `is_lihat_verifikasi` int(1) NOT NULL DEFAULT '0',
  `is_markup` int(1) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_ren_lidik_realisasi`
--

INSERT INTO `tb_kasus_ren_lidik_realisasi` (`id_kasus`, `ren_lidik`, `deskripsi`, `sasaran`, `tgl_pelaksanaan`, `durasi_hari`, `lokasi_kegiatan`, `rab`, `path`, `is_verifikasi`, `author`, `is_lihat_verifikasi`, `is_markup`, `tgl_masuk_data`) VALUES
('K160622001', 'Wawancara', 'Mencatat keterangan saksi pada blanko interogasi\nMelakukan pengambilan keterangan saksi', 'Pelapor', '2016-06-24', 1, 'Polda Bali', 100000, NULL, 1, '78110968', 0, 0, '2016-06-23 06:57:58'),
('K160622001', 'Penelitian dan Analisis Dokumen', 'Mengumpulkan bukti surat dari para saksi\nMemeriksa keaslian/keabsahan bukti surat', 'Pelapor', '2016-06-25', 1, 'Polda Bali', 180000, NULL, 1, '78110968', 0, 0, '2016-06-23 06:58:16'),
('K160624001', 'Pengolaahan TKP', 'Mengamankan status quo TKP\nMencari dan mengamnkan barang bukti di TKP', 'TKP', '2016-06-25', 1, 'TKP', 200000, NULL, 1, '78110968', 0, 0, '2016-06-24 02:27:07'),
('K160627001', 'Pengamatan', 'Melakukan pengamatan\nMencatat identitas saksi-saksi', 'Saksi-saksi', '2016-06-01', 1, '-', 500000, NULL, 1, '84020445', 0, 0, '2016-06-27 14:45:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_ren_lidik_realisasi_ubah`
--

CREATE TABLE `tb_kasus_ren_lidik_realisasi_ubah` (
  `id_kasus` varchar(10) NOT NULL,
  `ren_lidik` varchar(100) NOT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `tgl_perubahan` date NOT NULL,
  `rab_awal` int(12) NOT NULL,
  `rab` int(12) NOT NULL,
  `koreksi_rab` varchar(200) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_ren_lidik_realisasi_ubah`
--

INSERT INTO `tb_kasus_ren_lidik_realisasi_ubah` (`id_kasus`, `ren_lidik`, `tgl_pelaksanaan`, `tgl_perubahan`, `rab_awal`, `rab`, `koreksi_rab`, `tgl_masuk_data`) VALUES
('K160622001', 'Penelitian dan Analisis Dokumen', '2016-06-25', '2016-06-23', 150000, 180000, 'biaya terlalu kecil', '2016-06-23 06:58:16'),
('K160624001', 'Pengolaahan TKP', '2016-06-25', '2016-06-24', 300000, 200000, 'pengajuan terlalu besar', '2016-06-24 02:27:07');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_ren_lidik_target`
--

CREATE TABLE `tb_kasus_ren_lidik_target` (
  `id_kasus` varchar(10) NOT NULL,
  `ren_lidik` varchar(100) NOT NULL,
  `deskripsi` varchar(3000) DEFAULT NULL,
  `sasaran` varchar(3000) DEFAULT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `durasi_hari` int(3) DEFAULT NULL,
  `lokasi_kegiatan` varchar(200) DEFAULT NULL,
  `rab` int(12) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_ren_sidik_realisasi`
--

CREATE TABLE `tb_kasus_ren_sidik_realisasi` (
  `id_kasus` varchar(10) NOT NULL,
  `ren_sidik` varchar(100) NOT NULL,
  `deskripsi` varchar(3000) DEFAULT NULL,
  `sasaran` varchar(3000) DEFAULT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `durasi_hari` int(3) DEFAULT NULL,
  `lokasi_kegiatan` varchar(200) DEFAULT NULL,
  `rab` int(12) NOT NULL,
  `path` varchar(255) DEFAULT NULL,
  `is_verifikasi` int(1) NOT NULL DEFAULT '0',
  `author` varchar(8) DEFAULT NULL,
  `is_lihat_verifikasi` int(1) NOT NULL DEFAULT '0',
  `is_markup` int(1) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_ren_sidik_realisasi`
--

INSERT INTO `tb_kasus_ren_sidik_realisasi` (`id_kasus`, `ren_sidik`, `deskripsi`, `sasaran`, `tgl_pelaksanaan`, `durasi_hari`, `lokasi_kegiatan`, `rab`, `path`, `is_verifikasi`, `author`, `is_lihat_verifikasi`, `is_markup`, `tgl_masuk_data`) VALUES
('K160627001', 'Melengkapi Mindik', 'Membuat Sprin Sidik\nMembuat Rencana Sidik\nMembuat SP2HP', 'Terlapor dan Saksi-saksi', '2016-06-27', 3, '-', 700000, NULL, 1, '84020445', 0, 0, '2016-06-27 14:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_ren_sidik_realisasi_ubah`
--

CREATE TABLE `tb_kasus_ren_sidik_realisasi_ubah` (
  `id_kasus` varchar(10) NOT NULL,
  `ren_sidik` varchar(100) NOT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `tgl_perubahan` date NOT NULL,
  `rab_awal` int(12) NOT NULL,
  `rab` int(12) NOT NULL,
  `koreksi_rab` varchar(200) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_ren_sidik_realisasi_ubah`
--

INSERT INTO `tb_kasus_ren_sidik_realisasi_ubah` (`id_kasus`, `ren_sidik`, `tgl_pelaksanaan`, `tgl_perubahan`, `rab_awal`, `rab`, `koreksi_rab`, `tgl_masuk_data`) VALUES
('K160627001', 'Melengkapi Mindik', '2016-06-27', '2016-06-27', 800000, 700000, 'Anggaran terlalu besar', '2016-06-27 14:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_ren_sidik_target`
--

CREATE TABLE `tb_kasus_ren_sidik_target` (
  `id_kasus` varchar(10) NOT NULL,
  `ren_sidik` varchar(100) NOT NULL,
  `deskripsi` varchar(3000) DEFAULT NULL,
  `sasaran` varchar(3000) DEFAULT NULL,
  `tgl_pelaksanaan` date NOT NULL,
  `durasi_hari` int(3) DEFAULT NULL,
  `lokasi_kegiatan` varchar(200) DEFAULT NULL,
  `rab` int(12) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_saksi`
--

CREATE TABLE `tb_kasus_saksi` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_SAKSI` varchar(5) NOT NULL,
  `NAMA_SAKSI` varchar(50) NOT NULL,
  `ALAMAT_SAKSI` varchar(200) DEFAULT NULL,
  `KOTA` varchar(50) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `PEKERJAAN` varchar(50) DEFAULT NULL,
  `HP_SAKSI` varchar(50) DEFAULT NULL,
  `TELP_SAKSI` varchar(12) DEFAULT NULL,
  `KETERANGAN_SAKSI` varchar(3000) DEFAULT NULL,
  `ID_DOC_BERKAS` varchar(5) DEFAULT NULL,
  `BERKAS_KE` int(2) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_saksi`
--

INSERT INTO `tb_kasus_saksi` (`ID_KASUS`, `ID_SAKSI`, `NAMA_SAKSI`, `ALAMAT_SAKSI`, `KOTA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `PEKERJAAN`, `HP_SAKSI`, `TELP_SAKSI`, `KETERANGAN_SAKSI`, `ID_DOC_BERKAS`, `BERKAS_KE`, `tgl_masuk_data`) VALUES
('K160622001', 'SS001', 'Andi Nuril', 'Denpasar', 'Denpasar', '', '2016-06-22', '', '', '', '', NULL, NULL, '2016-06-22 06:30:37'),
('K160622001', 'SS002', 'Rudi Sujarwo', 'Jembrana', 'Jembrana', '', '2016-06-22', '', '', '', '', NULL, NULL, '2016-06-22 06:30:37'),
('K160624001', 'SS001', 'ALI', 'TUKAD BADUNG', 'DENPASAR', '', '2016-06-24', '', '', '', '', NULL, NULL, '2016-06-24 01:41:03'),
('K160624001', 'SS002', 'Wayan', 'denpasar', '', '', '2016-06-24', 'swasta', '', '', '', 'DOC30', 1, '2016-06-24 02:33:16');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_status`
--

CREATE TABLE `tb_kasus_status` (
  `ID_KASUS` varchar(10) NOT NULL,
  `STATUS` varchar(50) NOT NULL,
  `TGL_STATUS` datetime NOT NULL,
  `KETERANGAN` varchar(3000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kasus_tindak`
--

CREATE TABLE `tb_kasus_tindak` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_TINDAK` varchar(5) NOT NULL,
  `RENCANA_TINDAK` varchar(3000) NOT NULL,
  `DURASI` varchar(50) NOT NULL,
  `IS_TERLAKSANA` int(1) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kasus_tindak`
--

INSERT INTO `tb_kasus_tindak` (`ID_KASUS`, `ID_TINDAK`, `RENCANA_TINDAK`, `DURASI`, `IS_TERLAKSANA`, `tgl_masuk_data`) VALUES
('K160624001', 'TD001', 'mendatangi TKP', '2', 1, '2016-06-24 02:23:50');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kejadian_menonjol`
--

CREATE TABLE `tb_kejadian_menonjol` (
  `ID_KEJADIAN` varchar(10) NOT NULL,
  `NAMA_KEJADIAN` varchar(200) NOT NULL,
  `TGL_KEJADIAN` datetime NOT NULL,
  `LOKASI` varchar(100) NOT NULL,
  `PELAKU` varchar(200) NOT NULL,
  `KERUGIAN` varchar(200) NOT NULL,
  `KORBAN` varchar(200) NOT NULL,
  `SAKSI` varchar(200) NOT NULL,
  `KETERANGAN` varchar(300) NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `LATITUDE` varchar(20) NOT NULL,
  `LONGITUDE` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kejadian_menonjol`
--

INSERT INTO `tb_kejadian_menonjol` (`ID_KEJADIAN`, `NAMA_KEJADIAN`, `TGL_KEJADIAN`, `LOKASI`, `PELAKU`, `KERUGIAN`, `KORBAN`, `SAKSI`, `KETERANGAN`, `NRP`, `tgl_masuk_data`, `LATITUDE`, `LONGITUDE`) VALUES
('K161122001', 'Kejadian Awal', '2016-11-22 00:00:00', '', '', '', '', '', '', '', '2016-11-22 15:20:59', '', ''),
('K161123001', 'Kejadian yang kedua hari ke dua', '2016-11-22 00:00:00', '', '', '', '', '', '', '', '2016-11-22 15:23:07', '', ''),
('K161124001', 'Kejadian di hari ketiga', '2016-11-24 00:00:00', '', '', '', '', '', '', '', '2016-11-22 15:36:00', '', ''),
('K161123003', 'Nama Kejadian Tes', '2016-11-23 01:51:51', 'Jalan Keputih Timur 34 Surabaya', '', '', '', '', '', '78040126', '2016-11-23 00:51:51', '', ''),
('K161123002', 'Nama Kejadian Tes', '2016-11-23 01:50:59', 'Jalan Keputih Timur 34 Surabaya', '', '', '', '', '', '78040126', '2016-11-23 00:50:59', '', ''),
('K161123004', 'Nama Kejadian Tes', '2016-11-23 01:53:21', 'Jalan Keputih Timur 34 Surabaya', '', '', '', '', '', '78040126', '2016-11-23 00:53:21', '', ''),
('K161123005', 'Nama Kejadian Tes32', '2016-11-23 01:53:50', 'Jalan Keputih Timur 34 Surabaya', '', '', '', '', '', '78040126', '2016-11-23 00:53:50', '', ''),
('K161123006', 'Nama Kejadian', '2016-11-23 16:27:41', 'Alamat', 'Nama Pelakau', 'Jumlah', 'Nama korban', 'Saksi', 'Keterangan', '78040126', '2016-11-23 15:27:41', '-7.28872', '112.80932'),
('K161123007', 'Nama kajian', '2016-11-23 16:34:08', 'Alamat', '', '', '', '', '', '78040126', '2016-11-23 15:34:08', '-7.288718', '112.809326'),
('K161123008', 'Nama kejadian', '2016-11-23 16:36:57', 'Alamat', 'Ada pelaku', '', '', '', '', '78040126', '2016-11-23 15:36:57', '-7.288762', '112.809296'),
('K161123009', 'Kejadiaj mmvaru', '2016-11-23 16:42:37', 'Alamasysbs', 'Nsnsknd', '', '', '', '', '78040126', '2016-11-23 15:42:37', '-7.28872', '112.809326'),
('K161123010', 'Namaks j', '2016-11-23 16:44:35', 'Djdndndkd', 'Djdndnd', '', '', '', '', '78040126', '2016-11-23 15:44:35', '-7.280625', '112.80785'),
('K161124002', 'Nama kejadian', '2016-11-24 06:21:18', 'Jnns', 'Jdns', '', '', '', '', '78040126', '2016-11-24 05:21:18', '-7.289885', '112.81111'),
('K161124003', 'tudy', '2016-11-24 06:27:06', 'Ruydy', '', '', '', '', '', '78040126', '2016-11-24 05:27:06', '-7.288721', '112.809326'),
('K161124004', 'B vx', '2016-11-24 06:28:51', 'Xxhc', '', '', '', '', '', '78040126', '2016-11-24 05:28:51', '-7.288721', '112.809326'),
('K161124005', 'B vx', '2016-11-24 06:29:58', 'Xxhc', '', '', '', '', '', '78040126', '2016-11-24 05:29:58', '-7.288721', '112.809326'),
('K161124006', 'Hxgxvx', '2016-11-24 06:30:25', 'Xxg', '', '', '', '', '', '78040126', '2016-11-24 05:30:25', '-7.288718', '112.809326'),
('K161124007', 'Hig', '2016-11-24 06:33:00', 'Ggc', '', '', '', '', '', '78040126', '2016-11-24 05:33:00', '-7.28872', '112.80932'),
('K161124008', 'Gxvfhf', '2016-11-24 06:35:24', 'Gdgh', '', '', '', '', '', '78040126', '2016-11-24 05:35:24', '-7.288718', '112.80931'),
('K161124009', 'Fggd', '2016-11-24 06:37:44', 'Dgxg', '', '', '', '', '', '78040126', '2016-11-24 05:37:44', '-7.28872', '112.80932'),
('K161124010', 'Fggd', '2016-11-24 06:38:04', 'Dgxg', '', '', '', '', '', '78040126', '2016-11-24 05:38:04', '-7.28872', '112.80932'),
('K161124011', 'Hm n', '2016-11-24 06:39:22', 'hxn', '', '', '', '', '', '78040126', '2016-11-24 05:39:22', '-7.288719', '112.80932'),
('K161124012', 'Hm n', '2016-11-24 06:41:44', 'hxnufgdgdgx', '', '', '', '', '', '78040126', '2016-11-24 05:41:44', '-7.288762', '112.809296'),
('K161124013', 'Vhcvx', '2016-11-24 06:47:24', 'Cb b', '', '', '', '', '', '78040126', '2016-11-24 05:47:24', '-7.288762', '112.809296'),
('K161124014', 'Xgxvchc', '2016-11-24 06:50:58', 'Dxx', '', '', '', '', '', '78040126', '2016-11-24 05:50:58', '-7.288719', '112.809326'),
('K161124015', 'Cxf', '2016-11-24 07:17:33', 'Fddf', '', '', '', '', '', '78040126', '2016-11-24 06:17:33', '-7.288762', '112.809296'),
('K161124016', 'Tdvl', '2016-11-24 07:39:18', 'Hfhf', '', '', '', '', '', '78040126', '2016-11-24 06:39:18', '-7.288718', '112.80931'),
('K161124017', 'Tdvl', '2016-11-24 07:39:36', 'Hfhf', 'D', 'Tijf', 'Hffch', '', 'Oke uff', '78040126', '2016-11-24 06:39:36', '-7.288719', '112.809326'),
('K161124018', 'Ncbcc', '2016-11-24 07:51:57', 'Mbnvx', 'Hcxbl', '', '', '', '', '78040126', '2016-11-24 06:51:57', '-7.288718', '112.80932'),
('K161124019', 'Hfh', '2016-11-24 08:01:13', 'Fh', '', '', '', '', '', '78040126', '2016-11-24 07:01:13', '-7.288718', '112.809326'),
('K161124020', 'Gjv', '2016-11-24 08:09:02', 'Fgg', 'Aku', '', '', '', '', '78040126', '2016-11-24 07:09:02', '-7.28872', '112.809326'),
('K161124021', 'Jbdb', '2016-11-24 08:11:23', 'Akncb', '', '', '', '', '', '78040126', '2016-11-24 07:11:23', '-7.288717', '112.809326'),
('K161125001', 'Kejafian baru', '2016-11-25 10:49:24', 'Alamat kdjbd', 'Dn d', 'Xjxnd', 'Xjs d', 'Xndns', 'Dnd d', '78040126', '2016-11-25 09:49:25', '-7.288762', '112.809296'),
('K161125002', 'Fauzan', '2016-11-25 11:18:03', 'Alamat', 'Hxbz', 'Bzjzn', 'Bzjnz', 'Xbbx', '', '78040126', '2016-11-25 10:18:03', '-7.288762', '112.809296'),
('K161125003', 'Nama Kejadian', '2016-11-25 11:23:50', 'Keputih tegal Timur', '', '', '', '', '', '78040126', '2016-11-25 10:23:50', '-7.288762', '112.809296'),
('K161125004', 'Nama Kejadian', '2016-11-25 11:24:29', 'Keputih tegal Timur', '', '', '', '', '', '78040126', '2016-11-25 10:24:29', '-7.288762', '112.809296'),
('K161125005', 'Nama Kejadian', '2016-11-25 11:26:09', 'Keputih tegal Timur', '', '', '', '', '', '78040126', '2016-11-25 10:26:09', '-7.288762', '112.809296'),
('K161125006', 'Nama Kejadian', '2016-11-25 11:27:41', 'Keputih tegal Timur', '', '', '', '', '', '78040126', '2016-11-25 10:27:41', '-7.288762', '112.809296'),
('K161125007', 'Nama Kejadian', '2016-11-25 11:28:12', 'Keputih tegal Timur', '', '', '', '', '', '78040126', '2016-11-25 10:28:12', '-7.288762', '112.809296'),
('K161125008', 'Nama Kejadian', '2016-11-25 11:29:21', 'Keputih tegal Timur', '', '', '', '', '', '78040126', '2016-11-25 10:29:21', '-7.288762', '112.809296'),
('K161125009', 'Chxhifjcj', '2016-11-25 11:36:33', 'Hxjfjfjfj', 'Fhcb', 'Vn n', 'J jc', 'H j', 'Jcucig', '78040126', '2016-11-25 10:36:33', '-7.288762', '112.809296'),
('K161125010', 'Jshdjsu', '2016-11-25 12:44:29', 'Hsjsnbs', 'Sjdn', '', '', '', '', '78040126', '2016-11-25 11:44:29', '-7.288762', '112.809296'),
('K161125011', 'Jehdvcf dhc dgx', '2016-11-25 12:51:21', 'Cbxvh dhc', 'Chdgg dgg', 'Gjf', 'Fbbh', 'Dhdg', 'Hdvc', '78040126', '2016-11-25 11:51:21', '-7.288762', '112.809296');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelompok`
--

CREATE TABLE `tb_kelompok` (
  `ID_KELOMPOK` varchar(10) NOT NULL,
  `NAMA_KELOMPOK` varchar(50) NOT NULL,
  `ID_SPESIALISASI` varchar(10) NOT NULL,
  `LOKASI_OPERASI` varchar(255) NOT NULL,
  `PIMPINAN` varchar(50) NOT NULL,
  `KETERANGAN` varchar(3000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelompok`
--

INSERT INTO `tb_kelompok` (`ID_KELOMPOK`, `NAMA_KELOMPOK`, `ID_SPESIALISASI`, `LOKASI_OPERASI`, `PIMPINAN`, `KETERANGAN`, `tgl_masuk_data`) VALUES
('KP002', 'Kelompok Gank Kapak', 'SP002', 'Surabaya dan sekitarnya', 'BEJO BIN SUPRI', 'Spesialis pencopetan dan gendam dalam angkot', '2012-12-08 01:51:14');

-- --------------------------------------------------------

--
-- Table structure for table `tb_kronologi_kejadian`
--

CREATE TABLE `tb_kronologi_kejadian` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_KEJADIAN` varchar(10) NOT NULL,
  `TGL_KEJADIAN` datetime DEFAULT NULL,
  `LOKASI_KEJADIAN` varchar(3000) NOT NULL,
  `KEJADIAN` varchar(10000) NOT NULL,
  `KETERANGAN` varchar(10000) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kabupaten` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kronologi_kejadian`
--

INSERT INTO `tb_kronologi_kejadian` (`ID_KASUS`, `ID_KEJADIAN`, `TGL_KEJADIAN`, `LOKASI_KEJADIAN`, `KEJADIAN`, `KETERANGAN`, `tgl_masuk_data`, `kecamatan`, `kabupaten`) VALUES
('K160622001', 'KJ001', '2016-06-21 14:20:01', 'di Facebook terlapor', 'Pada hari Selasa 21 Juni 2016 puku 15.30 Terjadi tindak pidana pencemaran nama balik dilakukan oleh pelapor kepada pelapor', '', '2016-06-22 06:30:37', NULL, NULL),
('K160624001', 'KJ001', '2016-04-07 09:37:32', 'DENPASAR', 'PADA HARI KAMIS 23 JUNI 2016 SDR JUNAEDI DATANG MENAWARKAN PAKET INVESTASI DLL...', '', '2016-06-24 01:41:03', NULL, NULL),
('K160627001', 'KJ001', '2016-06-27 22:31:12', 'rumah kakek pelapor', 'terjadi tindak pidana penyerobotan hak atas tanah', '', '2016-06-27 14:31:39', NULL, NULL),
('K160716001', 'KJ001', '2016-07-15 13:21:53', 'rumah pelapor', 'terjadi tindak pidana membawa lari perempuan', '', '2016-07-16 05:26:00', NULL, NULL),
('K160716002', 'KJ001', '2016-07-15 13:28:34', 'rumah pelapor', 'terjadi tindak pidana membawa lari perempuan', '', '2016-07-16 05:28:48', NULL, NULL),
('K160719001', 'KJ001', '2016-07-18 12:04:24', 'di perempatan jalan', 'terjadi tindak pidana pengeroyokan', '', '2016-07-19 04:04:47', NULL, NULL),
('K160910001', 'KJ001', '2016-09-03 16:51:04', 'PT Muara Angke', 'Terjadi Penambangan Tanpa Ijin Usaha dilakukan oleh terlapor', '', '2016-09-10 08:52:00', NULL, NULL),
('K160910002', 'KJ001', '2016-09-03 22:15:00', 'Jl Raya Arteri', 'Terjadi tinda pidana PERAMPASAN SEPEDA MOTOR pada hari kamis malam', '', '2016-09-10 09:15:35', NULL, NULL),
('K160910003', 'KJ001', '2016-09-05 23:12:02', 'desa suwayuwo naggal selatan', 'Terjadi Tindak Pidana 170 PENGEROYOKAN DAN BAWA SAJAM oleh beberapa orang tak dikenal', '', '2016-09-10 09:18:47', NULL, NULL),
('K160910004', 'KJ001', '2016-09-01 17:28:54', 'rumah mertua', 'terjadi tindak pidana KEKERASAN ADALAM RUMAH TANGGA  dilakukan oleh suami', '', '2016-09-10 09:29:10', NULL, NULL),
('K160922001', 'KJ001', '2016-09-18 15:54:58', 'rumah pelapor', 'pada hari sabtu jam 17.00 waktu setempat di rumah pelapor terjadi tindak pidana PEMERASAN DAN PENGANCAMAN', '', '2016-09-22 07:55:43', NULL, NULL),
('K160922002', 'KJ001', '2016-09-19 16:02:31', 'jalan raya sumatra', 'terjadi tindak pidana PELECEHAN SEKSUAL DIBAWAH UMUR', '', '2016-09-22 08:02:47', NULL, NULL),
('K160922003', 'KJ001', '2016-09-22 14:09:56', 'Jl Raya Kutai timur', 'terjadi tindk pidana PERAMPASAN SEPEDA MOTOR', '', '2016-09-22 08:10:10', NULL, NULL),
('K160922004', 'KJ001', '2016-09-22 12:12:29', 'Jalan Rajawai', 'terjadi tindak pidana PERAMPASAN SEPEDA MOTOR', '', '2016-09-22 08:12:45', NULL, NULL),
('K160923001', 'KJ001', '2016-09-10 11:30:31', 'dinas tata kota', 'terjadi tindak pidana DUGAAN PENYALAHGUNAAN DANA ANGGARAN', '', '2016-09-23 03:30:43', NULL, NULL),
('K160923002', 'KJ001', '2016-09-20 11:33:08', 'tempat kos pelapor', 'terjadi tindak pidana MEMBAWA LARI SESEORANG DENGAN MELAWAN HUKUM (PENCULIKAN)', '', '2016-09-23 03:33:33', NULL, NULL),
('K160923003', 'KJ001', '2016-09-22 11:35:10', 'pasar klenteng', 'terjadi tindak pidana PEMBAKARAN GROBAK TEMPAT JUALAN', '', '2016-09-23 03:35:23', NULL, NULL),
('K160923004', 'KJ001', '2016-09-19 11:40:40', 'di warung pak tarno', 'MEMBUAT PERASAAN TDK ENAK DAN DISERTAI DGN PENGANCAMAN', '', '2016-09-23 03:40:58', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_ren_lidik`
--

CREATE TABLE `tb_master_ren_lidik` (
  `ren_lidik` varchar(100) NOT NULL,
  `no_urut` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_master_ren_lidik`
--

INSERT INTO `tb_master_ren_lidik` (`ren_lidik`, `no_urut`) VALUES
('Pengolaahan TKP', 1),
('Pengamatan', 2),
('Wawancara', 3),
('Pembuntutan', 4),
('Penyamaran', 5),
('Pelacakan', 6),
('Penelitian dan Analisis Dokumen', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_ren_lidik_deskripsi`
--

CREATE TABLE `tb_master_ren_lidik_deskripsi` (
  `ren_lidik` varchar(100) NOT NULL,
  `no_urut` int(1) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_master_ren_lidik_deskripsi`
--

INSERT INTO `tb_master_ren_lidik_deskripsi` (`ren_lidik`, `no_urut`, `deskripsi`) VALUES
('Pengolahan TKP', 1, 'Mengamankan status quo TKP'),
('Pengolahan TKP', 1, 'Membuat Sketsa TKP'),
('Pengolahan TKP', 1, 'Mencari dan mengamnkan barang bukti di TKP'),
('Pengamatan', 2, 'Mendatangi TKP'),
('Pengamatan', 2, 'Melakukan pengamatan'),
('Pengamatan', 2, 'Mencatat identitas saksi-saksi'),
('Pengamatan', 2, 'Mendapatkan dokumen terkait peristiwa pidana'),
('Wawancara', 3, 'Mencatat keterangan saksi pada blanko interogasi'),
('Wawancara', 3, 'Membuat dan mengirimkan surat undangan konfirmasi'),
('Wawancara', 3, 'Melakukan pengambilan keterangan saksi'),
('Wawancara', 3, 'Memintakan dokumen pendukung'),
('Pembuntutan', 4, 'Pemantauan dari jarak jauh'),
('Pembuntutan', 4, 'Pemantauan dari jarak dekat'),
('Pembuntutan', 4, 'Pengambilan dokumentasi'),
('Penyamaran', 5, 'Mengubah identitas'),
('Penyamaran', 5, 'Menggunakan pakaian yang samar'),
('Penyamaran', 5, 'Mengubah penampilan fisik'),
('Pelacakan', 6, 'Cek Posisi'),
('Pelacakan', 6, 'Call Data Record'),
('Pelacakan', 6, 'Pendataan posisi'),
('Pelacakan', 6, 'Pemetaan jaringan'),
('Pelacakan', 6, 'Pengarsipan'),
('Penelitian dan Analisis Dokumen', 7, 'Mengumpulkan bukti surat dari para saksi'),
('Penelitian dan Analisis Dokumen', 7, 'Memeriksa keaslian/keabsahan bukti surat'),
('Penelitian dan Analisis Dokumen', 7, 'Melakukan uji kepemilikan atas obyek bidang tanah'),
('Penelitian dan Analisis Dokumen', 7, 'Membuat Timeline berdasarkan tanggal surat'),
('Penelitian dan Analisis Dokumen', 7, 'Melakukan kroscek dengan pejabat yang berwenang'),
('Pelacakan', 8, 'Membuat Sprin Tugas'),
('Melengkapi Mindik', 8, 'Membuat Sprin Lidik'),
('Melengkapi Mindik', 8, 'Membuat Ren Lidik'),
('Melengkapi Mindik', 8, 'Membuat SP2HP'),
('Melengkapi Mindik', 8, 'Membuat Surat undangan / konfirmasi'),
('Gelar Perkara', 9, 'Melakukan gelar perkara awal'),
('Gelar Perkara', 9, 'Menyiapkan Laporan Hasil Penyelidikan (LHP)'),
('Gelar Perkara', 9, 'Mengisi Daftar Hadir'),
('Gelar Perkara', 9, 'Mencatat notulensi'),
('Gelar Perkara', 9, 'Membuat Laporan Hasil Gelar Perkara'),
('Gelar Perkara', 9, 'Membuat SP2HP');

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_ren_sidik`
--

CREATE TABLE `tb_master_ren_sidik` (
  `ren_sidik` varchar(100) NOT NULL,
  `no_urut` int(1) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_master_ren_sidik`
--

INSERT INTO `tb_master_ren_sidik` (`ren_sidik`, `no_urut`) VALUES
('Melengkapi Mindik', 1),
('Pemeriksaan Saksi', 2),
('Pemeriksaan Ahli', 3),
('Gelar Perkara', 4),
('Penggeledahan Badan / Rumah', 5),
('Penyitaan', 6),
('Pemeriksaan Tersangka', 7),
('Pemberkasan', 8);

-- --------------------------------------------------------

--
-- Table structure for table `tb_master_ren_sidik_deskripsi`
--

CREATE TABLE `tb_master_ren_sidik_deskripsi` (
  `ren_sidik` varchar(100) NOT NULL,
  `no_urut` int(1) DEFAULT NULL,
  `deskripsi` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_master_ren_sidik_deskripsi`
--

INSERT INTO `tb_master_ren_sidik_deskripsi` (`ren_sidik`, `no_urut`, `deskripsi`) VALUES
('Melengkapi Mindik', 1, 'Membuat Sprin Sidik'),
('Melengkapi Mindik', 1, 'Membuat SPDP'),
('Melengkapi Mindik', 1, 'Membuat Rencana Sidik'),
('Melengkapi Mindik', 1, 'Membuat SP2HP'),
('Melengkapi Mindik', 1, 'Membuat Surat Panggilan'),
('Pemeriksaan Saksi', 2, 'Melakukan pemeriksaan'),
('Pemeriksaan Saksi', 2, 'Menguji tingkat kejujuran saksi'),
('Pemeriksaan Ahli', 3, '-	Melakukan pemeriksaan'),
('Gelar Perkara', 4, 'Melakukan gelar perkara tengah / akhir'),
('Gelar Perkara', 4, 'Mengisi Daftar Hadir'),
('Gelar Perkara', 4, 'Mencatat notulensi'),
('Gelar Perkara', 4, 'Membuat Laporan Hasil Gelar Perkara'),
('Gelar Perkara', 4, 'Membuat SP2HP'),
('Penggeledahan Badan / Rumah', 5, 'Mengajukan Ijin Khusus Penggeledahan'),
('Penggeledahan Badan / Rumah', 5, 'Mengajukan Penetapan Ijin Penggeledahan'),
('Penggeledahan Badan / Rumah', 5, 'Membuat sprin geledah'),
('Penggeledahan Badan / Rumah', 5, 'Menunjukkan surat tugas dan sprin geledah'),
('Penggeledahan Badan / Rumah', 5, 'Menggeledah badan / pakaian / rumah'),
('Penggeledahan Badan / Rumah', 5, 'Melibatkan polwan pada penggeledahan terhadap wanita'),
('Penggeledahan Badan / Rumah', 5, 'Menyiapkan saksi penggeledahan'),
('Penggeledahan Badan / Rumah', 5, 'Membuat BA Penggeledahan'),
('Penggeledahan Badan / Rumah', 5, 'Memberikan salinan BA Penggeledahan kepada saksi/tersangka yang digeledah'),
('Penyitaan', 6, 'Membuat sprin sita'),
('Penyitaan', 6, 'Membuat BA sita'),
('Penyitaan', 6, 'Melakukan pembungkusan'),
('Penyitaan', 6, 'Melakukan penyegelan'),
('Penyitaan', 6, 'Melakukan pelabelan'),
('Penyitaan', 6, 'Mengajukan uji laboratories'),
('Penyitaan', 6, 'Membuat SP2HP'),
('Pemeriksaan Tersangka', 7, 'Melakukan pemeriksaan'),
('Pemeriksaan Tersangka', 7, 'Melakukan rekonstruksi'),
('Pemeriksaan Tersangka', 7, 'Melakukan konfrontasi'),
('Pemeriksaan Tersangka', 7, 'Menguji tingkat kejujuran tersangka'),
('Pemeriksaan Tersangka', 7, 'Membuat SP2HP'),
('Pemberkasan', 8, 'Membuat resume'),
('Pemberkasan', 8, 'Membuat Daftar-daftar'),
('Pemberkasan', 8, 'Melampirkan bukti surat'),
('Pemberkasan', 8, 'Memeriksa kelengkapan berkas perkara');

-- --------------------------------------------------------

--
-- Table structure for table `tb_nama_kasus`
--

CREATE TABLE `tb_nama_kasus` (
  `nama_kasus` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_nama_kasus`
--

INSERT INTO `tb_nama_kasus` (`nama_kasus`) VALUES
('170 PENGEROYOKAN'),
('170 PENGEROYOKAN DAN BAWA SAJAM'),
('BAWA SAJAM'),
('CURAS'),
('DI DEPAN UMUM BERSAMA-SAMA MELAKUKAN KEKERASAN TERHADAP BARANG'),
('DITEMUKAN BERMAIN JUDI (JENIS SABUNG AYAM)'),
('DITEMUKAN MEILIKI, MEMBAWA DAN MENGUASAI SAJAM JENIS BADIK'),
('DITEMUKAN MEMBAWA SAJAM JENIS BADIK'),
('DITEMUKAN MEMILIKI, MEMBAWA DAN MENGUASAI SAJAM JENIS BUSUR LENGKAP DENGAN KETAPEL'),
('DUGAAN PENYALAHGUNAAN DANA ANGGARAN'),
('glggggj'),
('KDRT'),
('KEKERASAN ADALAM RUMAH TANGGA'),
('KEKERASAN DALAM RUMAH TANGGA'),
('KEKERASAN TERHADAP ANAK'),
('KEKERASAN TERHADAP ANAK DIBAWAH UMUR'),
('MELAKUKAN PERKAWINAN TERDAPAT PENGHALANG YANG SAH'),
('MEMBAWA LARI ANAK DIBAWAH UMUR'),
('MEMBAWA LARI ANAK PEREMPUAN DI BAWAH UMUR'),
('MEMBAWA LARI PEREMPUAN'),
('MEMBAWA LARI SESEORANG DENGAN MELAWAN HUKUM (PENCULIKAN)'),
('MEMBAWA SAJAM JENIS BADIK'),
('MEMBERI KETERANGAN PALSU DI DEPAN PERSIDANGAN'),
('MEMBERI KETERANGAN PALSU DIATAS SUMPAH'),
('MEMBUAT PERASAAN TDK ENAK DAN DISERTAI DGN PENGANCAMAN'),
('MEMBUAT PERASAAN TIDAK MENYENANGKAN'),
('MENTERLANTARKAN ANAK'),
('PELECEHAN SEKSUAL DIBAWAH UMUR'),
('PEMALSUAN'),
('Pemalsuan surat'),
('PEMALSUAN SURAT DAN PENGGELAPAN HAK BARANG TAK BERGERAK'),
('PEMBAKARAN GROBAK TEMPAT JUALAN'),
('PEMBANGUNAN PERUMAHAN TIDAK SESUAI DENGAN KRITERIA, SPESIFIKASI, SARANA DAN PRASARANA SERTA MEMANFAATKAN RUANG TANPA IJIN'),
('PEMBUNUHAN'),
('PEMERASAN'),
('PEMERASAN DAN PENGANCAMAN'),
('PEMERKOSAAN ANAK DI BAWAH UMUR'),
('Penambangan Tanpa Ijin Usaha'),
('Pencemaran Nama Baik'),
('PENCURIAN'),
('PENCURIAN DALAM KELUARGA'),
('Pencurian dan atau Penggelapan'),
('PENCURIAN DENGAN KEKERASAN (CURAS)'),
('PENCURIAN DENGAN KEKERASAN (JAMBRET)'),
('Pencurian dengan Pemberatan'),
('PENCURIAN DISERTAI DENGAN KEKERASAN'),
('PENCURIAN KENDARAAN BERMOTOR (CURANMOR)'),
('PENCURIAN KENDARAAN BERMOTOR (CURANMOR) RODA DUA'),
('PENCURIAN KENDARAAN BERMOTOR (CURANMOR) RODA EMPAT'),
('Pencurian Kendaraan Bermotor Roda Dua'),
('PENCURIAN KENDARAAN R4'),
('PENCURIAN SEPEDA MOTOR'),
('PENCURIAN SEPEDA MOTOR (CURANMOR)'),
('PENCUUAN KENDARAAN BERMOTOR (CURANMOR)'),
('PENELANTARAN DALAM KELUARGA'),
('PENELANTARAN DALAM RUMAH TANGGA'),
('PENELANTARAN ISTRI'),
('PENELANTARAN ISTRI DAN ANAK'),
('PENEMUAN SENJATA API RAKITAN (TIDAK AKTIF)'),
('PENGANCAMAN'),
('PENGANCAMAN DAN PENGRUSAKAN'),
('PENGANCAMAN KEKERASAN TERHADAP ANAK'),
('PENGANCAMAN LEWAT MEDIA SOSIAL (FACEBOOK)'),
('PENGANCAMAN TERHADAP ORANG'),
('PENGANIAYAAN'),
('PENGANIAYAAN RINGAN'),
('PENGANIAYAAN YANG DILAKUKAN SECARA BERSAMA-SAMA'),
('PENGAWASAN TERHADAP PROSES PEMBEBASAN LAHAN'),
('PENGEROYOKAN'),
('Penggelapan'),
('PENGGELAPAN BARANG TIDAK BERGERAK'),
('PENGGELAPAN BERAS MISKIN'),
('PENGGELAPAN MOBIL'),
('PENGGELAPAN SEPEDA MOTOR'),
('PENGGELAPAN SURAT'),
('PENGHINAAN'),
('PENGHINAAN ATAU PENCEMARAN NAMA BAIK'),
('PENGHINAAN DAN PERBUATAN TIDAK MENYENANGKAN'),
('PENGRUSAKAN'),
('Pengrusakan dan pencurian'),
('PENIPUAN'),
('PENIPUAN DAN ATAU PENGGELAPAN'),
('PENYALAH GUNAAN ANGGARAN ALOKASI DANA DESA TAHUN 2015'),
('PENYALAHGUNAAN DAN PENGANKUTAN BBM BERSUBSIDI'),
('PENYEROBOTAN'),
('PENYEROBOTAN HAK ATAS TANAH'),
('PENYEROBOTAN HAK ATAS TANAH DAN PENGRUSAKAN'),
('PERAMPASAN DISERTAI PENGANIAYAAN TERHADAP ANAK DIBAWAH UMUR'),
('PERAMPASAN KENDARAAN RODA EMPAT (MOBIL)'),
('PERAMPASAN SEPEDA MOTOR'),
('PERAMPASAN SPEDA MOTOR'),
('PERBUATAN CABUL TERHADAP ANAK DIBAWAH UMUR'),
('PERBUATAN JUDI'),
('Perbuatan memaksa orang lain dengan kekerasan atau dengan ancaman kekerasan'),
('Perbuatan Tidak Menyenangkan'),
('Perbuatan Tidak Menyenangkan disertai pengerusakan'),
('PERCOBAAN PEMBUNUHAN MELALUI MEDIA ONLINE (FACEBOOK)'),
('PERCOBAAN PENCURIAN'),
('Perebutan Hak Waris'),
('PERSETUBUHAN ANAK DIBAWAH UMUR'),
('PERSETUBUHAN DENGAN KEKERASAN'),
('PERSETUBUHAN TERHADAP ANAK DIBAWAH UMUR'),
('PORNOGRAFI'),
('SECARA BERSAMA PENYEROTAN HAK ATAS TANAH'),
('SECARA BERSAMA SAMA MELAKUKAN KEKERASAN TERHADAP BARANG (PENGRUSAKAN)'),
('SECARA BERSAMA SAMA MELAKUKAN KEKERASAN TERHADAP ORANG'),
('STATUS HAK KEPEMILIKAN LAHAN'),
('TANPA HAK MEMILIKI DAN MENGUASAI TANAH MILIK ORANG LAIN'),
('TERTANGKAP TANGAN MEMBAWA, MEMILIKI DAN MENGUASAI SENJATA TAJAM'),
('TIDAK MENJAGA, MENGAMANKAN KEUTUHAN KOTAK SUARA PILKADA'),
('TINDAK PIDANA PEMALSUAN');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pam`
--

CREATE TABLE `tb_pam` (
  `ID_PAM` varchar(10) NOT NULL,
  `LOKASI_PAM` varchar(50) DEFAULT NULL,
  `ALAMAT_PAM` varchar(50) DEFAULT NULL,
  `TGL_PAM` datetime NOT NULL,
  `JAM_MULAI_PAM` time NOT NULL DEFAULT '00:00:00',
  `JAM_SELESAI_PAM` time DEFAULT NULL,
  `JUMLAH_HARI` int(3) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pangkat`
--

CREATE TABLE `tb_pangkat` (
  `PANGKAT` varchar(50) NOT NULL,
  `id_pangkat` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pangkat`
--

INSERT INTO `tb_pangkat` (`PANGKAT`, `id_pangkat`) VALUES
('Brigadir Polisi Dua', '01'),
('Brigadir Polisi Satu', '02'),
('Brigadir Polisi', '03'),
('Brigadir Polisi Kepala', '04'),
('Ajun Inspektur Polisi Dua', '05'),
('Ajun Inspektur Polisi Satu', '06'),
('Inspektur Polisi Dua', '07'),
('Inspektur Polisi Satu', '08'),
('Ajun Komisaris Polisi', '09'),
('Komisaris Polisi', '10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pasal_kasus`
--

CREATE TABLE `tb_pasal_kasus` (
  `ID_PASAL` varchar(5) NOT NULL,
  `NAMA_PASAL` varchar(100) NOT NULL,
  `TENTANG` varchar(300) DEFAULT NULL,
  `SANKSI` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pasal_kasus`
--

INSERT INTO `tb_pasal_kasus` (`ID_PASAL`, `NAMA_PASAL`, `TENTANG`, `SANKSI`) VALUES
('PS005', '368 KUHP DAN 369 KUHP', 'PEMERASAN DAN PENGANCAMAN', '9 THN DAN ATAU 4 THN'),
('PS003', '378 KUHP', 'PENIPUAN', '5 THN'),
('PS004', '378 KUHP dan atau 372 KUHP', 'PENIPUAN DAN ATAU PENGGELAPAN', '4 THN'),
('PS006', '368 KUHP', 'PEMERASAN', '4 (EMPAT) TAHUN'),
('PS007', '104 KUHP', 'KEJAHATAN TERHADAP PRESIDEN DAN WAKIL PRESIDEN', '25 THN'),
('PS008', '106 KUHP', 'KEJAHATAN TERHADAP KEUTUHAN NEGARA', '25 THN DAN ATAU SEUMUR HIDUP'),
('PS009', '107 KUHP', 'PENGGULINGAN PEMERINTAHAN', '15 THN'),
('PS010', '108 KUHP', 'PEMBERONTAKAN TERHADAP PEMERINTAH', '20 THN'),
('PS011', '110 KUHP', 'PEMUFAKATAN JAHAT TERHADAP PEMERINTAH DAN KETATANEGARAAN', '15 THN'),
('PS012', '111 KUHP', 'PEMUFAKATAN JAHAT BERSAMA NEGARA ASING TERHADAP NEGARA DAN PEMERINTAH', '20 THN'),
('PS013', 'PASAL 112', 'MEMBOCORKAN SURAT-SURAT RAHASIA NEGARA', '7 THN'),
('PS014', '113 KUHP', 'MEMBERITAHUKAN RAHASI PERTAHANAN NEGARA', '4 THN'),
('PS015', '114 KUHP', 'KESALAHAN ATAU KEALPAAN YG MENYEBABKAN BOCORNYA RAHASIA NEGARA', '1 THN 6 BLN'),
('PS016', '372 KUHP', 'PENGGELAPAN', '4 THN'),
('PS017', '242 KUHP', 'MEMBERIKAN KETERANGAN ATAU SUMPAH PALSU', '9 THN'),
('PS018', '244 KUHP', 'MENIRU DAN ATAU MEMALSUKAN MATA UANG', '15 THN'),
('PS019', '245 KUHP', 'MENGEDARKAN MATA UANG PALSU', '15 THN'),
('PS020', '253 KUHP', 'PEMALSUAN MATERAI', '7 THN'),
('PS021', '254 KUHP', 'MENIRU DAN MEMALSUKAN MEREK ATAU CAP NEGARA', '6 THN'),
('PS022', '263 KUHP', 'MENIRU ATAU MEMBUAT SURAT PALSU', '6 THN'),
('PS023', '264 KUHP', 'PEMALSUAN SURAT', '8 THN'),
('PS024', '266 KUHP', 'MEMBERIKAN KETERANGAN PALSU DALAM KEABSAHAN SUATU SURAT PALSU', '7 THN'),
('PS025', '277 KUHP', 'MENGGELAPKAN ASAL USUL PERKAWINAN', '6 THN'),
('PS026', '279 KUHP', 'MELAKUKAN PERKAWINAN YG TERDAPAT PENGHALANG YG SAH', '5 THN'),
('PS027', '280 KUHP', 'MENGADAKAN PERKAWINAN DAN SENGAJA TDK MEMBERITAHUKAN KEPADA PENGHALANG YG SAH', '5 THN'),
('PS028', '281 KUHP', 'MELANGGAR KESUSILAAN', '2 THN 8 BLN'),
('PS029', '282 KUHP', 'MEMPERTUNJUKKAN TULISAN, GAMBAR ATAU BENDA YG ISINYA MELANGGAR KESUSILAAN', '1 THN 6 BLN'),
('PS030', '283 KUHP', 'MENYEBARKAN GAMBAR, TULISAN DAN ATAU BENDA YG MELANGGAR KESUSILAAN', '9 BLN'),
('PS031', '284 KUHP', 'MELAKUKAN PERSETUBUHAN (OVERSPEL) TERHADAP ORG DAN DIKETAHUINYA ORG TERSEBUT TELAH KAWIN', '9 BLN'),
('PS032', '285 KUHP', 'MELAKUKAN PERSETUBUHAN DENGAN ANCAMAN KEKERASAN (PEMERKOSAAN)', '12 THN'),
('PS033', '286 KUHP', 'MELAKUKAN PERSETUBUHAN KPD WANITA YG DLM KEADAAN PINGSAN ATAU TDK BERDAYA', '9 THN'),
('PS034', '287 KUHP', 'MELAKUKAN PERSETUBUHAN DGN WANITA YG UMURNYA BELUM CUKUP UNTUK DIKAWIN', '9 THN'),
('PS035', '288 KUHP', 'MELAKUKAN PERSETUBUHAN DGN WANITA YG UMURNYA BELUM CUKUP UNTUK DIKAWIN DGN MENGAKIBATKAN LUKA RINGAN, BERAT DAN ATAU MATI', '4 THN, 8 THN DAN ATAU 12 THN'),
('PS036', '289 KUHP', 'PEMAKSAAN DAN MELAKUKAN PERBUATAN CABUL', '9 THN'),
('PS037', '290 KUHP', 'MELAKUKAN PERBUATAN CABUL DENGAN SEORANG YG TDK BERDAYA DAN ATAU BELUM CUKUP UMUR UNTUK DIKAWIN', '7 THN'),
('PS038', '291 KUHP', 'MELAKUKAN PERKOSAAN YG MENGAKIBATKAN LUKA BERAT DAN ATAU KEMATIAN', '12 THN DAN ATAU 15 THN'),
('PS039', '293 KUHP', 'MEMBERI ATAU MENJANJIKAN UANG ATAU BARANG DALAM PERBUATAN CABUL', '5 THN'),
('PS040', '294 KUHP', 'MELAKUKAN PERBUATAN CABUL TERHADAP ANAK YG DIBAWAH PENGAWASANNYA', '7 THN'),
('PS041', '295 KUHP', 'MEMUDAHKAN PERBUATAN CABUL TERHADAP ANAK YG DIBAWAH DLM PENGAWASANNYA', '5 THN'),
('PS042', '296 KUHP', 'MEMUDAHKAN DAN ATAU MENYEBABKAN PERBUATAN CABUL OLEH ORANG LAIN DGN ORANG LAIN SBG PENCAHARIAN ATAU KEBIASAAN', '1 THN 4 BLN'),
('PS043', '303 KUHP', 'MELAKUKAN PERJUDIAN', '10 THN'),
('PS044', '310 KUHP', 'PENGHINAAN ATAU PENCEMARAN NAMA BAIK', '9 BLN'),
('PS045', '322 KUHP', 'MEMBUKA RAHASIA JABATAN ATAU PENCARIAN', '9 BLN'),
('PS046', '324 KUHP', 'MELAKUKAN PERNIAGAAN BUDAK', '12 THN'),
('PS047', '335 KUHP', 'MEMBUAT PERASAAN TIDAK MENYENANGKAN', '-'),
('PS048', '338 KUHP', 'PEMBUNUHAN', '15 THN'),
('PS049', '339 KUHP', 'PEMBUNUHAN DISERTAI PENCURIAN', '20 THN'),
('PS050', '340 KUHP', 'PEMBUNUHAN BERENCANA', '20 THN DAN ATAU SEUMUR HIDUP'),
('PS051', '341 DAN 342 KUHP', 'DENGAN SENGAJA MENGHILANGKAN NYAWA ANAK YG BARU LAHIR', '7 THN'),
('PS052', '406 KUHP', 'PENGRUSAKAN BARANG ATAU BENDA', '2 THN 8 BLN'),
('PS053', '408 KUHP', 'PENGRUSAKAN FASILITAS UMUM', '4 THN'),
('PS054', '212 KUHP', 'MELAKUKAN PERLAWANAN TERHADAP PETUGAS ATAU PEJABAT NEGARA YG MELAKSANAKAN TUGAS', '1 THN 4 BLN'),
('PS055', '211 KUHP', 'MELAKUKAN ANCAMAN KEKERASAN TERHADAP PETUGAS DALAM MELAKSANAKAN TUGAS', '4 THN'),
('PS056', '300 KUHP', 'MENJUAL DAN ATAU MEMBERIKAN MINUMAN MEMABUKKAN', '1 BLN'),
('PS057', '330 KUHP', 'MENARIK SESEORANG YG BELUM CUKUP UMUR DARI PENGUASAAN ORANG YG BERWENANG UNTUK ITU', '7 THN'),
('PS058', '332 KUHP', 'MEMBAWA LARI PEREMPUAN YG BELUM DEWASA TANPA PERSETUJUAN ORANG TUA ATAU WALI', '7 THN DAN ATAU 9 THN'),
('PS059', '333 KUHP', 'PERAMPASAN HAK ATAU KEMERDEKAAN SESEORANG', '8 THN'),
('PS060', '336 KUHP', 'MENGANCAM DGN KEKERASAN TERHADAP ORANG', '2 THN 8 BLN'),
('PS061', '344 KUHP', 'PEMBUNUHAN ATAS PERMINTAAN SENDIRI', '12 THN'),
('PS062', '345 KUHP', 'MEMBANTU DALAM MELAKUKAN PERBUATAN BUNUH DIRI', '4 THN'),
('PS063', '346 KUHP', 'MENGGUGURKAN KANDUNGAN', '4 THN'),
('PS064', '349 KUHP', 'DOKTER, BIDAN, JURU OBAT YG MEMBANTU MENGGUGURKAN KANDUNGAN (ABORSI)', '12 THN'),
('PS065', '351 KUHP', 'PENGANIAYAAN', '2 THN 8 BLN'),
('PS066', '352 KUHP', 'PENGANIAYAAN RINGAN', '5 BLN'),
('PS067', '353 KUHP', 'PENGANIAYAAN YG DIRENCANAKAN', '4 THN'),
('PS068', '354 KUHP', 'PENGANIAYAAN BERAT', '8 DAN ATAU 10 THN'),
('PS069', '355 KUHP', 'PENGANIAYAAN BERAT YG DIRENCANAKAN TERLEBIH DAHULU', '12 THN DAN ATAU 15 THN'),
('PS070', '359 KUHP', 'KARENA KEALPAAN (KESALAHANNYA) MENYEBABKAN ORG LAIN MATI', '1 THN'),
('PS071', '362 KUHP', 'PENCURIAN BIASA', '5 THN'),
('PS072', '363 KUHP', 'PENCURIAN DGN PEMBERATAN (CURANMOR R2  R4, MALAM HARI, DLL)', '5 THN'),
('PS073', '365 KUHP', 'PENCURIAN DGN KEKERASAN', '12 THN'),
('PS092', '27 AYAT 3 UU RI NO. 11 TAHUN 2008', 'KEJAHATAN PENCEMARAN NAMA BAIK MELALUI ITE', 'MAKSIMAL 6 (ENAM) TAHUN'),
('PS075', '369 KUHP', 'PENGANCAMAN', '4 THN'),
('PS076', '480 KUHP', 'PENADAHAN BARANG HASIL KEJAHATAN', '4 THN'),
('PS077', '374 KUHP', 'PENGGELAPAN KARENA PEKERJAANNYA', '5 THN'),
('PS078', '385 KUHP', 'MENJUAL, MENUKARKAN ATAU MEMBEBANI CREDIETVERBAND HAK ATAS TANAH', '4 THN'),
('PS079', '167 KUHP', 'PENYEROBOTAN HAK ATAS TANAH', '9 (Sembilan) bulan penjara dan denda paling banyak 4.500 (empat ribu lima ratus rupiah)'),
('PS080', '27 UU NO. 11 THN 2008 TTG ITE', 'INFORMASI DAN TRANSAKSI ELEKTRONIK', '-'),
('PS081', '53 UU NO. 22 THN 2011 TTG MIGAS', 'PENYALAHGUNAAN MIGAS BERSUBSIDI', '-'),
('PS096', '49 UURI Nomor 23 tahun 2004', 'PENGHAPUSAN KEKERASAN DALAM RUMAH TANGGA', '3 TAHUN'),
('PS083', '45 Ayat 3 Jo. UU No.11 Tahun 2008', 'INFORMASI DAN TRANSAKSI ELEKTRONIK', '-'),
('PS084', '23 Tahun 2002 Tentang Perlindungan Anak', 'KEKERASAN TERHADAP ANAK DIBAWA UMUR', '-'),
('PS085', '151 AYAT 1 UU NO. 1 THN 2011', 'TENTANG PERUMAHAN DAN KAWASAN PEMUKIMAN', '-'),
('PS086', '154 UU NO. 1 THN 2011', 'PEMBANGUNAN PERUMAHAN TANPA MENYELESAIKAN STATUS TANAH', '-'),
('PS087', '96 DAN 93 UU NO. 23 THN 2006', 'ADMINISTRASI KEPENDUDUKAN', '-'),
('PS088', '70 AYAT 1 UU NO. 26 THN 2007', 'PEMANFAATAN TATA RUANG PEMUKIMAN TDK SESUAI DGN IZIN PEMANFAATAN RUANG DARI PEJABAT YG BERWENANG', '-'),
('PS089', '158 UU NO. 4 THN 2009', 'PERTAMBANGAN MINERAL DAN BATU BARA', '-'),
('PS090', '27 AYAT 1 UU NO. 11 THN 2008', 'KEJAHATAN ATAU PELANGGARAN KESUSILAAN MELALUI ITE', '-'),
('PS091', '29 UU NO. 44 THN 2008', 'KEJAHATAN PORNOGRAFI', '-'),
('PS093', '81 (2) UU RI NO. 35 THN 2014 TTG PERUBAHAN ATAS UU RI NO. 23 THN 2002 TTG PERLINDUNGAN ANAK', 'MELAKUKAN PERSETUBUHAN TERHADAP ANAK', 'PALING SINGKAT 5 (LIMA) TAHUN, PALING LAMA 15 (LIMA BELAS) TAHUN PENJARA DAN DENDA PALING BANYAK 5 (LIMA) MILIYAR'),
('PS094', '170 KUHP', 'KEKERASAN TERHADAP ORANG ATAU BARANG SECARA BERSAMA', '5 TAHUN'),
('PS095', '49 UU RI NO. 23 TAHUN 2004 TTG PENELANTARAN KELUARGA', 'MENELANTARKAN ISTRI', '-'),
('PS097', 'UU No.23 TAHUN 2004 TENTANG PERLINDUNGAN ANAK', 'KEKERASAN TERAHADAP ANAK UMUR', '-'),
('SP103', '167 ayat (1) dan 406 KUHP', 'PENYEROBOTAN HAK ATAS TANAH DAN PENGRUSAKAN', 'Paling Lama 9 (sembilan) bulan dan 2 (dua) tahun 8 (delapan) bulan'),
('SP101', '80 UU NO. 35 THN 2014', 'TENTANG PERUBAHAN UU NO. 23 THN 2002 (PERLINDUNGAN ANAK)', '-'),
('PS099', '406 KUHP', 'PENGRUSAKAN', '-'),
('SP100', '193 UU NOMOR 8 THN 2015', 'PEMILIHAN GUBERNUR, WALIKOTA DAN BUPATI', ''),
('SP102', 'DENGAN SENGAJA MEMBAWA, MENYIMPAN, MENGUASAI DAN MEMPERGUNAKAN SENJATA, SESUATU SENJATA PEMUKUL, SEN', 'UU DARURAT NO. 12 THN 1951', '-'),
('SP104', '328 KUHP', 'PENCULIKAN (melarikan orang dari tempat kediamannya ke tempat tinggal sementara dengan maksud melawan hak)', '12 (dua belas) tahun penjara'),
('SP105', '382 KUHP', 'MEMBAWA LARI SESEORANG DGN MELAWAN HUKUM', '7 THN'),
('SP108', '187 ayat (1) KUHP', 'DENGAN SENGAJA MENIMBULKAN KEBAKARAN, LEDAKAN ATAU BANJIR', '12 (DUA BELAS) TAHUN'),
('SP106', 'UU NO. 20 THN 2001', 'PEMBERANTASAN TINDAK PIDANA KORUPSI', '-'),
('SP107', '82 UU No. 35 Tahun 2014', 'PERLINDUNGAN ANAK', '-'),
('SP109', '54 KUHPidana', 'MENCOBA MELAKUKAN PELANGGARAN TINDAK PIDANA', '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelaku`
--

CREATE TABLE `tb_pelaku` (
  `ID_PELAKU` varchar(10) NOT NULL DEFAULT '',
  `JENIS_IDENTITAS` varchar(10) DEFAULT NULL,
  `NO_IDENTITAS` varchar(20) DEFAULT NULL,
  `NAMA_LENGKAP` varchar(50) NOT NULL,
  `NAMA_PANGGILAN` varchar(50) NOT NULL,
  `NAMA_PADANAN` varchar(50) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(20) DEFAULT NULL,
  `TGL_LAHIR` date DEFAULT NULL,
  `JENIS_KELAMIN` varchar(1) DEFAULT NULL,
  `GOLONGAN_DARAH` varchar(2) DEFAULT NULL,
  `ALAMAT_KTP` varchar(75) DEFAULT NULL,
  `ALAMAT_DOMISILI` varchar(75) DEFAULT NULL,
  `WARGA_NEGARA` varchar(30) DEFAULT NULL,
  `TELP_ID_PELAKU` varchar(12) DEFAULT NULL,
  `STATUS_PERKAWINAN` varchar(30) DEFAULT NULL,
  `PENDIDIKAN_AKHIR` varchar(30) DEFAULT NULL,
  `WARNA_KULIT` varchar(30) DEFAULT NULL,
  `TINGGI_BADAN` varchar(3) DEFAULT NULL,
  `BERAT_BADAN` varchar(3) DEFAULT NULL,
  `JENIS_RAMBUT` varchar(30) DEFAULT NULL,
  `WARNA_RAMBUT` varchar(30) DEFAULT NULL,
  `MODEL_RAMBUT` varchar(30) DEFAULT NULL,
  `BERKACAMATA` int(1) DEFAULT NULL,
  `CIRI_KHUSUS` varchar(200) DEFAULT NULL,
  `STATUS` varchar(50) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelaku`
--

INSERT INTO `tb_pelaku` (`ID_PELAKU`, `JENIS_IDENTITAS`, `NO_IDENTITAS`, `NAMA_LENGKAP`, `NAMA_PANGGILAN`, `NAMA_PADANAN`, `TEMPAT_LAHIR`, `TGL_LAHIR`, `JENIS_KELAMIN`, `GOLONGAN_DARAH`, `ALAMAT_KTP`, `ALAMAT_DOMISILI`, `WARGA_NEGARA`, `TELP_ID_PELAKU`, `STATUS_PERKAWINAN`, `PENDIDIKAN_AKHIR`, `WARNA_KULIT`, `TINGGI_BADAN`, `BERAT_BADAN`, `JENIS_RAMBUT`, `WARNA_RAMBUT`, `MODEL_RAMBUT`, `BERKACAMATA`, `CIRI_KHUSUS`, `STATUS`, `tgl_masuk_data`) VALUES
('P160622001', 'KTP', '12437458548444', 'JAKA ALDILA', 'JAKA', '-', 'DENPASAR', '1990-02-01', 'L', 'A', 'JL GATOT SUBROTO DENPASAR', 'JL KEROBOKAN DENPASAR', 'INDONESIA', '081222233345', 'BELUM KAWIN', 'SMU/SMK', 'COKELAT/SAWO MATANG', '145', '120', 'LURUS', 'HITAM', 'PENDEK', 1, 'GEMUK', 'TERLAPOR', '2016-06-22 06:13:59'),
('P160624001', 'KTP', '-', 'JUNAEDI', 'JUNED', 'MAT SOLAR', '-', '1901-01-01', 'L', 'O', '', '', '', '-', 'KAWIN', 'TIDAK ADA', 'PUTIH', '', '', 'LURUS', '', 'GUNDUL', 0, '', 'TERLAPOR', '2016-06-24 01:33:26'),
('P160627001', 'KTP', '', 'JONI ESMANTO', 'JONI', '', '', '1901-01-01', 'P', 'O', '', '', '', '', 'KAWIN', 'TIDAK ADA', 'PUTIH', '', '', 'LURUS', '', 'GUNDUL', 0, '', 'TERLAPOR', '2016-06-27 14:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelaku_foto`
--

CREATE TABLE `tb_pelaku_foto` (
  `ID_PELAKU` varchar(10) NOT NULL,
  `IMG_MID` varchar(255) DEFAULT NULL,
  `IMG_KA` varchar(255) DEFAULT NULL,
  `IMG_KI` varchar(255) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelaku_foto`
--

INSERT INTO `tb_pelaku_foto` (`ID_PELAKU`, `IMG_MID`, `IMG_KA`, `IMG_KI`, `tgl_masuk_data`) VALUES
('P160622001', 'upload/foto_pelaku/ukuran_asli/P160622001.jpg', 'upload/foto_pelaku/P160622001_thumb.jpg', 'upload/foto_pelaku/P160622001_thumb.jpg', '2016-06-22 06:13:59'),
('P160624001', 'images/avatar.png', 'images/avatar.png', NULL, '2016-06-24 01:33:26'),
('P160627001', 'images/avatar.png', 'images/avatar.png', NULL, '2016-06-27 14:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelaku_kelompok`
--

CREATE TABLE `tb_pelaku_kelompok` (
  `ID_KELOMPOK` varchar(10) NOT NULL,
  `ID_PELAKU` varchar(10) NOT NULL,
  `TGL_GABUNG` datetime DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelaku_kelompok`
--

INSERT INTO `tb_pelaku_kelompok` (`ID_KELOMPOK`, `ID_PELAKU`, `TGL_GABUNG`, `tgl_masuk_data`) VALUES
('KP002', 'P160622001', '0000-00-00 00:00:00', '2016-06-22 06:13:59'),
('KP002', 'P160624001', '0000-00-00 00:00:00', '2016-06-24 01:33:26'),
('KP002', 'P160627001', '0000-00-00 00:00:00', '2016-06-27 14:14:26');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelaku_spesialisasi`
--

CREATE TABLE `tb_pelaku_spesialisasi` (
  `ID_SPESIALISASI` varchar(5) NOT NULL,
  `ID_PELAKU` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelapor`
--

CREATE TABLE `tb_pelapor` (
  `ID_PELAPOR` varchar(10) NOT NULL,
  `JENIS_IDENTITAS` varchar(10) DEFAULT NULL,
  `NO_IDENTITAS` varchar(20) DEFAULT NULL,
  `NAMA_PELAPOR` varchar(50) NOT NULL,
  `ALAMAT_PELAPOR` varchar(200) NOT NULL,
  `KOTA` varchar(50) NOT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `PEKERJAAN` varchar(50) DEFAULT NULL,
  `HP_PELAPOR` varchar(50) DEFAULT NULL,
  `TELP_PELAPOR` varchar(12) DEFAULT NULL,
  `KETERANGAN` varchar(200) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelapor`
--

INSERT INTO `tb_pelapor` (`ID_PELAPOR`, `JENIS_IDENTITAS`, `NO_IDENTITAS`, `NAMA_PELAPOR`, `ALAMAT_PELAPOR`, `KOTA`, `TEMPAT_LAHIR`, `TANGGAL_LAHIR`, `PEKERJAAN`, `HP_PELAPOR`, `TELP_PELAPOR`, `KETERANGAN`, `tgl_masuk_data`) VALUES
('L160622001', 'KTP', '25823860283602', 'ADE RIZAL', 'DENPASAR', 'DENPASAR', 'DENPASAR', '1982-06-22', 'SWASTA', '1234567', '1234567', '', '2016-06-22 06:08:28'),
('L160624001', 'KTP', '1234567890', 'PII', 'JLN WR SUPRATMAN NO5', 'DENPSAR', 'DENPASAR', '1997-08-10', 'SWASTA', '081234567', '-', '', '2016-06-24 01:30:37'),
('L160627001', 'KTP', '-', 'ALDILA JOKO', 'JEMBER UNITED', 'JEMBER', 'JEMBER', '1982-06-27', 'SWASTA', '', '', '', '2016-06-27 14:12:38');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelapor_pernyataan`
--

CREATE TABLE `tb_pelapor_pernyataan` (
  `ID_PELAPOR` varchar(10) NOT NULL,
  `KOTA_PERNYATAAN` varchar(50) NOT NULL,
  `TGL_PERNYATAAN` date NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `IS_PERNAH_DILAPORKAN` int(1) NOT NULL DEFAULT '0',
  `KET_PERNAH_DILAPORKAN` varchar(1000) DEFAULT NULL,
  `IS_PERNAH_DIHENTIKAN` int(1) NOT NULL DEFAULT '0',
  `KET_PERNAH_DIHENTIKAN` varchar(1000) DEFAULT NULL,
  `IS_SALING_MELAPORKAN` int(1) NOT NULL DEFAULT '0',
  `KET_SALING_MELAPORKAN` varchar(1000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pelapor_pernyataan`
--

INSERT INTO `tb_pelapor_pernyataan` (`ID_PELAPOR`, `KOTA_PERNYATAAN`, `TGL_PERNYATAAN`, `ID_KASUS`, `IS_PERNAH_DILAPORKAN`, `KET_PERNAH_DILAPORKAN`, `IS_PERNAH_DIHENTIKAN`, `KET_PERNAH_DIHENTIKAN`, `IS_SALING_MELAPORKAN`, `KET_SALING_MELAPORKAN`) VALUES
('L150827001', 'Jember', '2015-08-26', 'K150827001', 1, 'kira-kira satu bulan yang lalu', 1, 'karena beluma ada cukup bukti', 1, 'belum ada yang melaporkan'),
('L150827001', 'Jember', '2015-08-26', 'K150827002', 0, '', 0, '', 0, ''),
('L150909002', 'Jember', '2015-09-10', 'K150909002', 0, '', 1, 'sudah dilaporkan 2013', 0, ''),
('L150729002', 'Gowa', '2015-09-20', 'K150729002', 0, '', 0, '', 0, ''),
('L160622001', 'Denpasar', '2016-06-22', 'K160622001', 0, '', 0, '', 0, ''),
('L160624001', 'Denpasar', '2016-06-24', 'K160624001', 0, '', 0, '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penghargaan_penyidik`
--

CREATE TABLE `tb_penghargaan_penyidik` (
  `NRP` varchar(10) NOT NULL,
  `TGL_PENGHARGAAN` date NOT NULL,
  `PEMBERI_PENGHARGAAN` varchar(50) NOT NULL,
  `NAMA_PENGHARGAAN` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penghargaan_penyidik`
--

INSERT INTO `tb_penghargaan_penyidik` (`NRP`, `TGL_PENGHARGAAN`, `PEMBERI_PENGHARGAAN`, `NAMA_PENGHARGAAN`) VALUES
('12312312', '2014-08-04', 'KAPOLRES', 'PENYIDIK TERBAIK 2013'),
('12312312', '2013-10-01', 'KAPOLRES', 'ANGGOTA POLISI 2013');

-- --------------------------------------------------------

--
-- Table structure for table `tb_penyidik`
--

CREATE TABLE `tb_penyidik` (
  `NRP` varchar(10) NOT NULL,
  `NAMA_PENYIDIK` varchar(50) NOT NULL,
  `PANGKAT_PENYIDIK` varchar(25) NOT NULL,
  `HP_PENYIDIK` varchar(50) NOT NULL,
  `TELP_PENYIDIK` varchar(12) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IS_OPSNAL` int(1) NOT NULL DEFAULT '0',
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL,
  `NAMA_POLSEK` varchar(25) DEFAULT NULL,
  `NAMA_SUBDIT` varchar(25) DEFAULT 'SUBDIT I',
  `NAMA_UNIT` varchar(25) DEFAULT 'UNIT I',
  `NAMA_SUBNIT` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penyidik`
--

INSERT INTO `tb_penyidik` (`NRP`, `NAMA_PENYIDIK`, `PANGKAT_PENYIDIK`, `HP_PENYIDIK`, `TELP_PENYIDIK`, `tgl_masuk_data`, `IS_OPSNAL`, `NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_POLSEK`, `NAMA_SUBDIT`, `NAMA_UNIT`, `NAMA_SUBNIT`) VALUES
('64110458', 'Amrin Selian, S.H.', 'aiptu', '0818528326', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('79080410', 'I Gusti Agus', 'bripka', '081332414190', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('88110786', 'Novi Cahyaningtyas', 'bripda', '0856303461', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('67080153', 'AGUS SOEMARDI', 'AIPTU', '---------', '-----------', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT II'),
('76040038', 'Iwan Hari P, S.H.', 'IPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('79100115', 'Suhartono, S.Sos', 'IPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('65070379', 'Julian K Warokka', 'IPDA', '123455', '123222', '2013-01-27 01:59:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('59050196', 'Pardi', 'AIPTU', '123', '123', '2013-09-30 02:37:12', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('80050017', 'Syaiful Arif', 'BRIPKA', '123', '123', '2013-09-30 02:39:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('67060198', 'Warno, S.H.', 'BRIPKA', '123', '123', '2013-09-30 02:50:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('91100020', 'Levina Magdalena M', 'BRIPDA', '12312345', '123123456', '2013-08-27 02:53:03', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('76060645', 'Cucun Hariyanto, S.H.', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('70120078', 'Hendro Irgiyanto', 'AIPTU', '123', '123', '2012-12-11 07:07:37', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('66080689', 'Jupri', 'Aipda', '1234567', '1234567', '2013-04-01 07:02:44', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('76110669', 'Santi Widyawati', 'BRIPKA', '123564', '1235656', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('77040571', 'Bambang Prasiyo A', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('79030451', 'Indra Gunawan, S.H.', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('86120660', 'Tri Farida C, S.H.', 'BRIPTU', '03171796789', '03171796789', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87050561', 'Suci Rachmawati', 'BRIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('90100054', 'Julia Ajeng P.', 'BRIPDA', '123123', '1231234', '2013-05-31 02:59:41', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('75040159', 'Nanang Fendi DS, S.H.', 'IPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('75010397', 'Subiyantana, S.H.', 'IPTU', '--------', '--------', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('59051036', 'H. Fathol. K', 'AIPTU', '12312345', '123789456', '2013-11-12 05:06:23', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('62090682', 'Tri Hartono', 'AIPTU', '111111', '12322222', '2013-01-02 07:37:02', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT I'),
('71120167', 'Edy Sucipto', 'AIPDA', '123123', '1234567', '2014-02-13 03:49:19', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('90040136', 'Nanda Putro P', 'BRIPDA', '1231234', '123789', '2013-11-12 02:29:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('75120917', 'Djarot Wibowo', 'BRIPKA', '123123456', '123123', '2014-02-06 04:36:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('82020427', 'Purwo Widodo, S.H.', 'BRIGPOL', '12345789', '4567999', '2013-11-12 02:29:53', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('88040775', 'Dedy Kartika Duane', 'BRIPTU', '123456', '5643157', '2014-02-06 04:37:07', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('82090897', 'Priyo Sasmito', 'BRIGPOL', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('83061471', 'BUDI SANTOSO, SH', 'IPTU', '123456767', '12345467896', '2013-09-12 07:32:41', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('66090508', 'Supeno, S.H.', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT I'),
('69120475', 'Daniel Soedjatmiko', 'AIPTU', '123456', '123789', '2013-09-28 02:48:34', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT I'),
('74030404', 'Heri Setiono, S.H.', 'AIPDA', '123123', '123123', '2013-09-28 02:47:05', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT I'),
('76050189', 'Andi Kurnia R', 'AIPDA', '123123', '123789', '2013-09-28 02:47:39', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT II'),
('89090544', 'Rahma Kartika Putri', 'BRIPDA', '085649520789', '000000', '2013-09-24 02:32:44', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('68090013', 'Aris Santoso', 'AIPTU', '123', '123', '2012-12-10 06:27:20', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT I'),
('63110325', 'SUDAMIRAN', 'kompol', '', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'Wakasatreskrim', NULL),
('73070337', 'Arsyad, S.H.', 'AIPDA', '123456', '123789', '2013-09-28 02:49:41', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT I'),
('72070660', 'Nanang Iswanto, S.H.', 'AIPDA', '---------', '-----------', '2013-09-28 02:48:02', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT II'),
('69050093', 'Edy Perwanto, S.Sos', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT II'),
('64060853', 'Tri Harjanto', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('79070028', 'Iskak Purwanto ', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('60080619', 'Sugeng Yulianto, S.H.', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('82120401', 'Tony Hariyanto, S.H.', 'BRIGPOL', '1231234', '123123456', '2013-08-27 10:10:20', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('61030726', 'Drs. Untung Waloyo', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT II'),
('78110509', 'Dedik Irwanto', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT II'),
('62110478', 'Dwi Purwanto, S.H.', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT II'),
('91080008', 'Wissang Willatiko', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT II'),
('86021547', 'Henry Eko Irawan', 'IPTU', '081390259920', '000000', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('65040292', 'Sri Moliyani', 'AIPTU', '085855686857', '03134348541', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('70090252', 'Moch. Shokib,SH.', 'AIPDA', '081331436667', '031 70724272', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('74120501', 'Ainur Yadik, S.H.', 'BRIPKA', '087852948100', '03170775419', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('81090580', 'Vian Eko a P, S.H.', 'BRIGPOL', '081230831981', '03170841326', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('61040421', 'Drs. Hariyanto', 'AIPTU', '03170074334', '08123278846', '2013-09-24 08:06:47', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('73110188', 'A. Made Suniasih, S.H.', 'AIPDA', '081331343105', '03177000298', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('74110214', 'I Gede Kaniaka, S.H.', 'BRIPKA', '081332778181', '03170278181', '2013-09-24 08:06:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('82110216', 'Eko Nurfianto, S.H.', 'BRIGPOL', '03170057663', '081252344474', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('62110544', 'Muljoto', 'AIPTU', '081231131776', '03177310777', '2013-09-24 02:31:10', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('65120789', 'DP. Ardita, S.H.', 'AIPTU', '085851070606', '000000', '2013-09-24 02:31:29', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('75120336', 'Nur Khamdilah, S.H.', 'BRIPKA', '08121751185', '03170109672', '2013-09-24 02:31:58', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('78080797', 'Agus Budi Utomo', 'BRIGPOL', '081332352714', '03171610551', '2013-09-24 02:32:19', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('86031894', 'Ricky F,A.Md,IK', 'IPTU', '082143218888', '28148466 / 2', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('69060213', 'Santa Yulia S', 'AIPTU', '085231351859 Pin BB  276B2A2F', '031-71919177', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('82050180', 'SUHARIANTO', 'BRIPKA', '1234567', '12345678', '2013-08-21 03:23:59', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('73060468', 'Suwoto', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('78010182', 'Catur Ari P, S.H.', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('77060253', 'Daryanto, S.H.', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('75110500', 'Ruth Yeni Q, S.Sos', 'IPDA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('66120146', 'Budi Prastowo', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT II'),
('69050291', 'M Rochib', 'AIPTU', '1234567', '1234567', '2012-12-27 07:29:34', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT II'),
('71020147', 'Gogot Purwanto, S.H.', 'AIPDA', '1234567', '1234567', '2012-12-27 07:30:06', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT II'),
('79050752', 'Yuli Muji Lestari', 'BRIPKA', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('82080179', 'I Ketut Rapi, S.H.', 'BRIGPOL', '1234567', '1234567', '2012-12-27 07:32:01', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT II'),
('86050431', 'Andini Putri', 'BRIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('77010393', 'Yudi Astriono, S.H.', 'BRIPKA', '1234567', '1234567', '2012-12-27 07:31:32', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT II'),
('65020372', 'Wiyono', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT II'),
('86010613', 'Vicky Orbeta', 'BRIPTU', '1234567', '1234567', '2012-12-27 07:33:47', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('91090068', 'Endah Dwi Nasyia', 'BRIPDA', '1234567', '1234567', '2012-12-27 07:34:41', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('91020021', 'Fanani Rahmiyah A', 'BRIPDA', '1234567', '1234567', '2012-12-27 07:34:18', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('85050205', 'Priyantini Wahyu, S.H.', 'BRIGPOL', '1234567', '1234567', '2012-12-27 07:33:16', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('84041981', 'M. Yunus Saputra, S.H.', 'AKP', '123', '123', '2012-12-09 11:54:59', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', NULL),
('63110304', 'Agung Pribadi, S.H.', 'AKP', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', NULL),
('64120291', 'Sugiana', 'AIPTU', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT II'),
('63070786', 'Isbari, S.H.', 'AKP', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('79101234', 'Tri Okta Hendri Yanto, SIK', 'AKP', '081245012002', '000000', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('78111155', 'Roman S.E., S.H.,SIK', 'AKP', '081288792002', '28185555', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('65070578', 'suratmi, S.H.', 'AKP', '123', '123', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', NULL),
('74010595', 'Farman SH', 'AKBP', '081398328988', '0313578246', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'KASATRESKRIM', NULL),
('80070183', 'Yayuk Indarwati', 'BRIPKA', '123645', '3465879', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('63050475', 'Sutrisno', 'AIPTU', '123654', '456789', '2013-09-30 02:54:22', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('85090708', 'Andhyka Anatylova', 'BRIPTU', '7353719', '3684466', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('60120702', 'HADI SUHARJONO SH', 'AIPTU', '1246123', '123456', '2014-02-13 03:50:00', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('87021330', 'Rulian Syauri, S.H.', 'IPTU', '', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('73080531', 'Agus Widodo, S.H.', 'IPTU', '', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('80120559', 'Deddie S I, SH', 'Brigadir', '123456', '123456', '2012-12-21 08:48:27', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('85010412', 'Imam Sulindro', 'Brigadir', '081230875000', '85010412', '2013-09-30 02:43:00', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('78010167', 'IRWAN NUGRAHA, SH', 'IPDA', '1235469', '213124659', '2014-02-06 04:29:04', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('73110230', 'L. TRI WULANDARI, SH', 'AIPDA', '1234567', '123456789', '2013-09-14 05:16:50', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('87060025', 'RAGIL RISMANTO', 'BRIGPOL', '----------', '------------', '2014-02-13 04:18:34', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('65020451', 'SUTAMAJI', 'AIPTU', '1246779', '12456779', '2013-08-22 08:40:12', 4, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('86101927', 'I MADE PRAMESETIA', 'IPTU', '1234156798', '12456798', '2013-09-24 02:25:36', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('83081651', 'ARDI PURBOYO, SH.', 'IPTU', '081225018168', '---------', '2013-06-24 08:35:34', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('71010242', 'JUMENO WARSITO', 'AIPDA', '000000000000000', '000000000000', '2012-12-09 12:21:03', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('78040126', 'DWI SANTOSO', 'BRIPKA', '12346498', '12341649', '2013-09-24 09:18:14', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('81010055', 'EKO PRASETYO', 'BRIPKA', '000000000', '0000000000', '2012-12-06 12:03:06', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('58050989', 'BENNY NDOEN', 'AIPTU', '000000', '000000', '2012-12-10 06:04:53', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('86101367', 'SUHERMANTO', 'BRIPTU', '000000', '000000', '2012-12-10 06:00:30', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('63040468', 'KISWARI', 'AIPTU', '000000', '000000', '2012-12-10 06:05:34', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('73020335', 'SUGENG HARIYANTO', 'BRIPTU', '000000', '000000', '2012-12-10 06:06:19', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('84120625', 'ARIS PUJIANTO, SH', 'BRIGPOL', '000000', '000000', '2012-12-10 06:07:35', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('74100272', 'DEDY SULISTYO', 'AIPDA', '000000', '000000', '2012-12-10 06:08:23', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('76020457', 'SUTRISNO', 'BRIPKA', '000000', '000000', '2012-12-10 06:09:12', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('83090410', 'MOCH. MUSTOFAH', 'BRIGPOL', '000000', '000000', '2012-12-10 06:13:04', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('78120003', 'NANAK CATUR W', 'BRIPKA', '000000', '000000', '2012-12-10 06:13:45', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('78110150', 'ANDANG PURWANTO', 'BRIPKA', '000000', '000000', '2012-12-10 06:14:32', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('83040537', 'ANDRI KURNIAWAN', 'BRIGPOL', '000000', '000000', '2012-12-10 06:15:06', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT II'),
('64070658', 'ABDUL KADIR', 'AIPTU', '000000', '000000', '2012-12-10 06:16:06', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('80080810', 'DANANG GEGER S', 'BRIGPOL', '000000', '000000', '2013-07-27 07:46:09', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('81031298', 'AMINULLOH, SH,MH', 'BRIGPOL', '000000', '000000', '2012-12-10 06:17:35', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('85041591', 'SUKRON MAKMUN', 'BRIPTU', '000000', '000000', '2012-12-10 06:18:14', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('69011091', 'M. KASUM, SH', 'AIPTU', '000000', '000000', '2012-12-10 06:18:48', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('83010287', 'EKO WAHYU SAPUTRO', 'BRIPTU', '000000', '000000', '2012-12-10 06:20:25', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('75040173', 'DIAN HARI MANGGALA', 'AIPDA', '000000', '000000', '2012-12-10 06:30:13', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('87050533', 'M. FIRDAUS F', 'BRIPTU', '000000', '000000', '2012-12-10 06:32:47', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('89020556', 'ZAFAR NASRULLOH', 'BRIPDA', '1234567', '1234567', '2013-02-08 04:11:24', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('83050336', 'PRIMADA KURNIAWAN', 'BRIGPOL', '000000', '000000', '2014-02-06 04:35:56', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('64050169', 'MOCHTAR PADANG', 'AIPTU', '000000', '000000', '2012-12-10 06:44:04', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('85110372', 'HENDRA RISKA. SY', 'BRIPTU', '000000', '000000', '2012-12-10 06:45:53', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('87060910', 'JUNANTO SAPUTRO', 'BRIPTU', '000000', '000000', '2012-12-10 06:46:47', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('80070822', 'ANTON YUNIARSO, SH', 'BRIGPOL', '000000', '000000', '2012-12-10 06:49:28', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('71030239', 'R. GATOT SUDJOKO, SH', 'AIPTU', '000000', '000000', '2013-09-24 08:07:34', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('81040048', 'ADI PURNOMO', 'BRIPKA', '000000', '000000', '2012-12-10 06:53:02', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('83100569', 'DODIK SETIYONO, SH', 'BRIGPOL', '000000', '000000', '2013-09-24 08:07:06', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('91080061', 'RIZAL YUDISTIRA', 'BRIPDA', '000000', '000000', '2012-12-10 07:00:07', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('84020142', 'FANDY ARDIANTO', 'BRIGPOL', '000000', '000000', '2013-09-24 02:34:33', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('83080530', 'BAGUS DANY RACHMAT', 'BRIGPOL', '000000', '000000', '2013-09-24 02:34:03', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('86050133', 'FIRMANSYAH', 'BRIPTU', '000000', '000000', '2013-09-24 02:34:55', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('80060833', 'PRIYANTO UTOMO, SH', 'BRIGPOL', '000000', '000000', '2012-12-10 07:17:55', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('80070812', 'KOKO SUJARWANTO', 'BRIGPOL', '000000', '000000', '2012-12-10 07:33:04', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('79091136', 'JOKO TRISNO', 'BRIGPOL', '000000', '000000', '2012-12-10 07:34:11', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('84020729', 'RUBY POERNAWAN', 'BRIGPOL', '000000', '000000', '2012-12-10 07:35:15', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('73060266', 'DAGUK LASETIO. B, SH', 'AIPDA', '000000', '000000', '2012-12-10 07:36:08', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('84010528', 'ADNAN BUYUNG N.', 'BRIGPOL', '000000', '000000', '2012-12-10 07:37:01', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('81100716', 'DWI TONO AMBORO', 'BRIGPOL', '000000', '000000', '2012-12-10 07:37:33', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('79010265', 'MOCH. SODIQIN', 'BRIPTU', '000000', '000000', '2012-12-10 07:38:24', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('90050361', 'FAROUK ASHADI HAITI', 'IPDA', '124466669', '78994564123', '2013-11-09 05:46:25', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('84040418', 'DONNY WIDARUTAMA', 'BRIPTU', '000000', '000000', '2012-12-10 07:41:19', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('86031545', 'MARTINUS SIMANJUNTAK', 'BRIPTU', '000000', '000000', '2012-12-10 07:41:53', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('88041098', 'Parikhesit', 'IPTU', '00000000000', '00000000000', '2012-12-12 06:51:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('74100259', 'I.K. WIJAYA ADINATA, SH', 'AIPDA', '213456798', '1245678', '2013-08-28 03:37:38', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('78120239', 'AKHMAD ZULKARNAIN', 'BRIPKA', '000000', '(031) 704401', '2013-01-02 07:30:03', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT II'),
('75090758', 'Rohmawati Lailah, SH', 'iptu', '081233951273', '12345678', '2013-09-12 07:40:43', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT II'),
('5607008', 'I Nyoman Radjin, SH', 'iptu', '03170767686', '1234567', '2013-01-07 07:55:26', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('64030759', 'ABDUL HADI', 'AIPTU', '031-71300444', '1234567', '2013-01-07 08:06:52', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('63040959', 'GATOT SUHARTOKO', 'AIPTU', '08155000099', '1234567', '2013-08-28 03:35:58', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('59050108', 'SUDIRMAN', 'AIPTU', '0818333329', '1234567', '2013-01-07 08:15:50', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('57120398', 'DARNA', 'AIPTU', '08123254703', '123456', '2013-01-07 08:20:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('58070372', 'SURYADI', 'AIPTU', '0818370684', '1234567', '2013-01-07 08:24:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('58060406', 'SOEMARSONO', 'AIPTU', '58060406', '1234567', '2013-01-07 08:35:44', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('74050566', 'KUSWINARTO', 'AIPDA', '08123155586', '1234567', '2013-01-07 08:42:37', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('73100237', 'EKO SUBROTO', 'AIPDA', '081938886363', '1234567', '2013-01-07 08:45:49', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('82110495', 'AMNANDYAH', 'BRIGPOL', '82110495', '1234567', '2013-01-07 08:50:55', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('82051555', 'AGUNG PRASETYO', 'BRIGPOL', '031-71906073', '1234567', '2013-11-18 03:20:25', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('55100208', 'I NYOMAN SUWENA', 'AIPTU', '55100208', '1234567', '2013-01-07 09:12:59', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('77080450', 'ARIS RISAWAN', 'BRIPKA', '031-70484934', '1234567', '2013-01-08 03:27:52', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('82040944', 'ROCHMAN', 'BRIGPOL', '031-70434593', '1234567', '2013-01-08 03:32:36', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('77080326', 'ALI ZUHDI', 'BRIPKA', '031-70389393', '1234567', '2013-01-08 03:38:17', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('82031087', 'BAMBANG SUKARSONO', 'BRIGPOL', '081252349292', '081252349282', '2013-01-08 03:59:23', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('81110395', 'Fajar Sri Ardian', 'BRIGPOL', '031-83439477', '1234567', '2013-01-08 04:14:01', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('85111319', 'Yuga Wahyu Dinata', 'BRIPTU', '031-81216264', '1234567', '2013-01-08 04:30:38', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('61050630', 'Jamidin Simbolon', 'AIPTU', '08123109361', '1234567', '2013-01-08 04:34:34', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('73100116', 'Puji Hardianto', 'AIPDA', '031-71081107', '1234567', '2013-01-08 04:47:16', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('75010800', 'BUDI HERNOMO', 'BRIPKA', '031-72175959', '1234567', '2013-01-08 04:58:01', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('77120254', 'Widiyanto', 'BRIPKA', '031-71187677', '1234567', '2013-01-08 05:00:49', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('81050206', 'IKA DIAN ARISTANTO', 'BRIPKA', '08123292958', '1234567', '2013-01-08 05:15:24', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('80040944', 'HARIANTO', 'BRIGPOL', '80040944', '031-70434593', '2013-01-08 05:18:03', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('84040083', 'JENI TRIANTO', 'BRIGPOL', '031-72112229', '1234567', '2013-01-08 05:20:44', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IDEN', 'SUBNIT I'),
('74020080', 'SUSANTO HARI SANDI', 'AIPTU', '1234556', '123456', '2013-11-12 02:26:00', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('75030060', 'AGUS SANYOTO', 'AIPDA', '1234567', '123456', '2013-11-12 02:21:53', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('72020299', 'DIDIK SUPRIANTO', 'AIPDA', '1234567', '123456', '2013-11-12 02:22:43', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('76020594', 'EDY SUPRAYITNO', 'AIPDA', '1234567', '1234567', '2014-02-13 04:03:22', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('81120229', 'ISMAIL MARSUKI', 'BRIPKA', '1234567', '1234567', '2013-11-12 02:24:10', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('82030115', 'RATNO PUJO', 'BRIPKA', '1234567', '1234567', '2013-11-12 02:25:46', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('85121486', 'DEDY KURNIANTO', 'BRIPTU', '124567', '124567', '2013-11-12 02:22:14', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT II'),
('73110356', 'NUR CAHYONO', 'AIPDA', '1234567', '1234567', '2014-02-06 04:37:47', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('75110869', 'ABDUL ROHIM', 'BRIPKA', '1234567', '------------', '2014-02-06 04:34:56', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('82020558', 'HAFID FIRMANSYAH', 'BRIGPOL', '1234567', '1234567', '2013-02-08 04:10:56', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('75090906', 'AMIRUDDIN', 'BRIPKA', '08123223687', '70476418', '2013-02-12 03:17:58', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('74120251', 'TUGIMIN', 'AIPDA', '1234567', '1234567', '2014-02-06 04:34:03', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('83070755', 'AGUNG DWI SAPUTRA', 'BRIGPOL', '1234567', '1234567', '2014-02-06 04:35:39', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('84120733', 'HENDRO SETIAWAN', 'BRIGPOL', '1234567', '1234567', '2013-05-07 03:35:16', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('83070268', 'RIYANTO', 'BRIGPOL', '1234567', '1234567', '2013-05-07 03:34:56', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT I'),
('77090204', 'BIANTO', 'BRIPKA', '1234567', '1234567', '2014-02-06 04:34:28', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III'),
('76120449', 'NUR ROCHIM', 'BRIPKA', '1234567', '12342567', '2013-09-12 06:22:46', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('72060502', 'BENI PRAMONO', 'BRIPKA', '123456', '123456', '2013-02-14 06:20:26', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('82051240', 'ARIF EFENDI, SH', 'BRIGPOL', '12346597', '123464797', '2013-09-30 04:45:54', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('83010265', 'ARI WIDODO', 'BRIGPOL', '085733597776', '1234567', '2013-02-14 08:49:14', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('81050205', 'SOLEHPUDIN', 'BRIGPOL', '123456', '124563', '2013-02-21 07:59:02', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('71020165', 'WAHYU ZAD', 'AIPTU', '123456', '123456', '2013-09-24 02:33:20', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('76080168', 'MUHAMAD AFENDI', 'AIPDA', '-----------', '---------', '2013-08-22 08:39:59', 4, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('72080361', 'AGUS TRIARTO', 'AIPDA', '------', '----------', '2013-08-05 08:29:15', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT III'),
('72110077', 'GUNAWAN', 'AIPTU', '12345679', '12346579', '2013-09-20 02:42:27', 1, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT III'),
('74030349', 'SOEKRIS TRIHARTONO', 'ITPU', '12345679', '12346989', '2013-09-12 08:01:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'URBIN OPS', 'SUBNIT I'),
('83080829', 'PANGGAH. S,SH.', 'BRIGPOL', '12465799', '12341567', '2013-09-14 04:53:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('kdirslsl04', 'DIRLANTAS SULSEL', 'KOMBESPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'DIRLANTAS', 'DIRLANTAS', NULL),
('ku11slsl04', 'KANIT I SUBDIT I LANTAS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ksd2slsl01', 'KASUBDIT II KRIMUM SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'KASUBDIT', NULL),
('ksd3slsl01', 'KASUBDIT III KRIMUM SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'KASUBDIT', NULL),
('ksd4slsl01', 'KASUBDIT IV KRIMUM SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'KASUBDIT', NULL),
('ku23slsl01', 'KANIT II SUBDIT III KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT II', NULL),
('ku42slsl01', 'KANIT IV SUBDIT II KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('62080740', 'I NYOMAN MADRA', 'AIPTU', '87960394982', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT II', NULL),
('75040680', 'I NYOMAN  SUITRA, SH', 'AIPDA', '81805550091', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('80020448', 'I PUTU SUARNATA', 'BRIGADIR', '82340847348', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('77100260', 'I KETUT DARWIN, SH ', 'BRIPKA', '81916744411', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('85020364', 'SI PT NGR KUSUMAYADI,SH', 'BRIGADIR', '85333004444', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT III', NULL),
('66120023', 'I MADE WIDIA,SH,MH', 'KOMPOL', '81338309699', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('65090572', 'I KETUT KARI,SH', 'IPDA', '81338608851', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('70121078', 'I MADE MURDA', 'AIPTU', '81805428556', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('76110488', 'GEDE SUDIADNYA,SH', 'AIPDA', '81803702017', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('75010587', 'JANSEN A. PANJAITAN, SIK, MH', 'AKBP', '81281861996', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', NULL, NULL),
('65120041', 'I KT SOMA ADNYANA, SH.,MH', 'KOMPOL', '81236526555', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('70070352', 'SI KT ARYA PINATIH,SH.,MH', 'IPDA', '81236318686', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('64020189', 'SI PT DHARMA PUTRA,SH,MH', 'KOMPOL', '82146279717', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT III', NULL),
('79060022', 'I GST NGR MADE SUKA DWITA, SH', 'AIPDA', '82340223239', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT III', NULL),
('79080112', 'I PUTU DARMAWAN', 'BRIPKA', '81236474639', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT III', NULL),
('86020658', 'I NYOMAN ADI YURIASMANA,SH', 'BRIGADIR', '85341874999', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT II', NULL),
('80050940', 'I MADE ASTAMA YASA,SH', 'BRIPKA', '81353392037', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT II', NULL),
('76070401', 'I KETUT REDI GUNANTARA,SH', 'AIPDA', '85738223139', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT II', NULL),
('63070134', 'DEWA GEDE RAI SUJAYA', 'AKP', '81916238880', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT II', NULL),
('85030240', 'I PUTU YUDI PRASETYA', 'BRIGADI', '82237088859', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT I', NULL),
('84090364', 'I NYOMAN SURIANA, SH', 'BRIGADIR', '85237230367', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT I', NULL),
('70030329', 'I NYOMAN SARKA,SH', 'IPDA', '8123653083', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT I', NULL),
('61010571', 'I GUSTI KETUT ATNA', 'KOMPOL', '85792189208', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('92090401', 'REZA HAFIDZ DWI S., SIK', 'IPDA', '82341454545', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT I', NULL),
('63040691', 'I KETUT BANDRIA', 'AKP', '81999910181', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT I', NULL),
('66020248', 'IDA PUTU WEDANA JATI,SH.,MH', 'KOMPOL', '81337068000', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', NULL, NULL),
('86100724', 'GD EDI KURNIAWAN', 'BRIGADIR', '82345864146', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('76020672', 'MUHAMAD ALIANTO, SH', 'BRIGADIR', '81236166689', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('69090591', 'I GUSTI NGURAH PARWITA', 'AIPTU', '89618990989', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('71050450', 'NANDANG IRWANTO, SH', 'KOMPOL', '8123769702', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('77070395', 'I MADE JULI ARDANA', 'AIPDA', '81337555666', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT III', NULL),
('83100112', 'I MADE SENA, SH', 'BRIPKA', '85237995677', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT III', NULL),
('86090609', 'I GEDE CHRISNA KUSUMA ANGGARA,SH', 'BRIGADIR', '81943703545', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('63090413', 'Drs. I WAYAN SINARYASA', 'KOMPOL', '8123987105', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT III', NULL),
('76070400', 'AGUS NYM DARMAYASA,SH', 'AIPDA', '81338615002', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT III', NULL),
('77080084', 'I MADE SETENA', 'AIPDA', '87860103007', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('73110093', 'I GEDE ARI SURYAWAN, SH', 'AIPTU', '81999846882', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('65120165', 'I MADE ARIAWAN P, SH', 'AKP', '81936087566', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('62100145', 'I MADE WARDANA,SH', 'KOMPOL', '82145930007', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('87040477', 'I WAYAN DANU ARYADIPA,SH', 'BRIGADIR', '85238394000', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku33slsl01', 'KANIT III SUBDIT III KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT III', NULL),
('76010879', 'ENCEP SYAMSUL HAYAT,SH', 'KOMPOL', '82144900372', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('75030718', 'I KETUT SUDARSANA', 'BRIGADIR', '81337228155', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('59020677', 'I GUSTI PUTU SUKADANA', 'AKP', '81337342123', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('81111211', 'I WY. SONDRA BUDIANA, SE', 'BRIPKA', '81805551139', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku31slsl03', 'KANIT III SUBDIT I NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku41slsl03', 'KANIT IV SUBDIT I NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku12slsl03', 'KANIT I SUBDIT II NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT II', 'UNIT I', NULL),
('ku22slsl03', 'KANIT II SUBDIT II NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT II', 'UNIT II', NULL),
('ku32slsl03', 'KANIT III SUBDIT II NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT II', 'UNIT III', NULL),
('asd1slsl03', 'ADMIN SUBDIT I NARKOBA SULSEL', 'ADMIN SUBDIT I', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT I', 'SUBDIT I', NULL),
('ku44slsl01', 'KANIT IV SUBDIT IV KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('ku24slsl01', 'KANIT II SUBDIT IV KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('77071241', 'I GN. AGUNG ADE PANJI ANOM, SIK', 'AKBP', '82394847999', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', NULL, NULL),
('wdirslsl03', 'WADIRRESNARKOBA SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'WADIRRESNARKOBA', 'WADIRRESNARKOBA', NULL),
('67110078', 'I GST PT NGR SUARTA,SH', 'IPDA ', '81353383900', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('64120192', 'I WAYAN BALIK', 'AIPTU', '81338749566', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('78110968', 'I DEWA GEDE BUDIASA', 'BRIPKA', '81239524390', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('84020445', 'DW. GD. SUARSA KUSUMA,SH ', 'BRIGADIR', '81239636444', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('86070237', 'PRANA MANUABA,SH', 'BRIGADIR', '81236333475', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('63030697', 'I B. KETUT MANTRA, SH', 'KOMPOL', '8123610242', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('76040065', 'NI MADE SURYANI,SH', 'AIPTU', '81237676900', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('71110296', 'I NYOMAN SUMARJANA', 'AIPDA', '8214408344', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('81110719', 'I NYOMAN SUBARIANA, SH', 'BRIPKA', '81239613091', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('64120119', 'SUBIRAN, SH', 'KOMPOL', '81239938111', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('74120011', 'PUTU SUNARCAYA, SH, MM', 'AKP', '85340796857', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('76110675', 'JOKO PITONO', 'BRIPKA', '82144190944', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('81110615', 'ACHMAD MUAFA, SH', 'BRIPKA', '81805579719', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('84070472', 'JOKO WIJAYANTO', 'BRIGADIR', '81337095050', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('87030324', 'I DW PT WIRADI PUTRA', 'BRIGADIR', '87761187244', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('67080017', 'I MADE WITAYA, SH', 'KOMPOL', '81916701972', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', NULL, NULL),
('61040298', 'I GST NGR GUNADI', 'KOMPOL', '85792598422', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT I', NULL),
('66090386', 'I KETUT WINATA,SH', 'IPDA', '81338663073', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT I', NULL),
('79040162', 'I MADE SEMAREJAYA,SH', 'BRIPKA ', '0361 9190494', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT I', NULL),
('63030737', 'I GST NGR PURNAWA,SH', 'KOMPOL', '82144923333', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT II', NULL),
('74100256', 'MADE ARTANA, SH', 'AIPTU', '81236737873', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT II', NULL),
('75040552', 'ERVAN NUGROHO,SH', 'BRIPKA ', '8337605505', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT II', NULL),
('64080422', 'I KETUT SUHARTO GIRI,SH, MH', 'KOMPOL', '0361 7469686', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT III', NULL),
('71040329', 'I NYOMAN BUDAYASA,', 'AIPTU ', '81337674491', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT III', NULL),
('59100813', 'SLAMET ACHWAN', 'KOMPOL', '8123877007', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('76060263', 'ZULFI ANZHOR KHOLIK,SH', 'IPDA', '81338737625', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('81070222', 'SUHERMAN PRAYUDI,S.S', 'BRIPKA ', '81999069040', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL);
INSERT INTO `tb_penyidik` (`NRP`, `NAMA_PENYIDIK`, `PANGKAT_PENYIDIK`, `HP_PENYIDIK`, `TELP_PENYIDIK`, `tgl_masuk_data`, `IS_OPSNAL`, `NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_POLSEK`, `NAMA_SUBDIT`, `NAMA_UNIT`, `NAMA_SUBNIT`) VALUES
('81120105', 'I WAYAN ROBYARTHA', 'BRIPKA ', '8179779436', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('83030868', 'I KADEK MUSTIKA YASA', 'BRIPKA ', '81936031000', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('87030284', 'I MD DWI ARITANAYA, SH', 'BRIPKA ', '81933100199', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('87060274', 'I MD ANDIKA DWI UTAMA,SH', 'BRIGADIR ', '817560300', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('78110587', 'I MADE ADI ARYANA', 'BRIPKA ', '81338215152', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('asd4slsl03', 'ADMIN SUBDIT IV NARKOBA SULSEL', 'ADMIN SUBDIT IV', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT IV', 'SUBDIT IV', NULL),
('76120087', 'I dw  Artha nugraha', 'AIPTU', '1234567', '1234567', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('sagowa01', 'Admin Reskrim Gowa', '-', '-', '-', '2016-08-25 14:40:32', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, NULL, NULL, NULL),
('wdirslsl04', 'WADIRLANTAS SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'WADIRLANTAS', 'WADIRLANTAS', NULL),
('ksd4slsl03', 'KASUBDIT IV NARKOBA SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT IV', 'KASUBDIT', NULL),
('kdirslsl03', 'DIRRESNARKOBA SULSEL', 'KOMBESPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'DIRRESNARKOBA', 'DIRRESNARKOBA', NULL),
('asd2slsl03', 'ADMIN SUBDIT II NARKOBA SULSEL', 'ADMIN SUBDIT II', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT II', 'SUBDIT II', NULL),
('asd3slsl03', 'ADMIN SUBDIT III NARKOBA SULSEL', 'ADMIN SUBDIT III', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT III', 'SUBDIT III', NULL),
('wpolslsl', 'WAKAPOLDA SULSEL', 'BRIGJENPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', NULL, NULL, 'WAKAPOLDA', 'WAKAPOLDA', NULL),
('kpolslsl', 'KAPOLDA SULSEL', 'IRJENPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', NULL, NULL, 'KAPOLDA', 'KAPOLDA', NULL),
('ku11slsl01', 'KANIT I SUBDIT I KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('asd3slsl01', 'ADMIN SUBDIT III KRIMUM SULSEL', 'ADMIN SUBDIT III', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'SUBDIT III', NULL),
('asd2slsl01', 'ADMIN SUBDIT II KRIMUM SULSEL', 'ADMIN SUBDIT II', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'SUBDIT II', NULL),
('asd1slsl01', 'ADMIN SUBDIT I KRIMUM SULSEL', 'ADMIN SUBDIT I', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'SUBDIT I', NULL),
('asd4slsl02', 'ADMIN SUBDIT IV KRIMSUS SULSEL', 'ADMIN SUBDIT IV', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'SUBDIT IV', NULL),
('ku41slsl02', 'KANIT IV SUBDIT I KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku42slsl02', 'KANIT IV SUBDIT II KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('ku43slsl02', 'KANIT IV SUBDIT III KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('ku44slsl02', 'KANIT IV SUBDIT IV KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('saslsl01', 'ADMIN KRIMUM SULSEL', '-', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, NULL, NULL, NULL),
('saslsl02', 'ADMIN KRIMSUS SULSEL', '-', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, NULL, NULL, NULL),
('saslsl03', 'ADMIN NARKOBA SULSEL', '-', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRENARKOBA', NULL, NULL, NULL, NULL),
('saslsl04', 'ADMIN LANTAS SULSEL', '-', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, NULL, NULL, NULL),
('asd3slsl02', 'ADMIN SUBDIT III KRIMSUS SULSEL', 'ADMIN SUBDIT III', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'SUBDIT III', NULL),
('asd2slsl02', 'ADMIN SUBDIT II KRIMSUS SULSEL', 'ADMIN SUBDIT II', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'SUBDIT II', NULL),
('asd1slsl02', 'ADMIN SUBDIT I KRIMSUS SULSEL', 'ADMIN SUBDIT I', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'SUBDIT I', NULL),
('ku34slsl02', 'KANIT III SUBDIT IV KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('ku24slsl02', 'KANIT II SUBDIT IV KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('ku14slsl02', 'KANIT I SUBDIT IV KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('ku33slsl02', 'KANIT III SUBDIT III KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT III', NULL),
('ku23slsl02', 'KANIT II SUBDIT III KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT II', NULL),
('aspkslsl', 'Admin SPKT SULSEL', 'admin', '', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', NULL, NULL, NULL, NULL, NULL),
('kspkslsl', 'Kepala SPKT SULSEL', 'AKBP', '', NULL, '2012-12-02 14:30:42', 0, 'POLDA SULAWESI SELATAN', NULL, NULL, NULL, NULL, NULL),
('wdirslsl02', 'WADIRRESKRIMSUS SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'WADIRRESKRIMSUS', 'WADIRRESKRIMSUS', NULL),
('kdirslsl02', 'DIRRESKRIMSUS SULSEL', 'KOMBESPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'DIRRESKRIMSUS', 'DIRRESKRIMSUS', NULL),
('ku13slsl02', 'KANIT I SUBDIT III KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'UNIT I', NULL),
('ku32slsl02', 'KANIT III SUBDIT II KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT III', NULL),
('ku22slsl02', 'KANIT II SUBDIT II KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT II', NULL),
('ku12slsl02', 'KANIT I SUBDIT II KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'UNIT I', NULL),
('ku31slsl02', 'KANIT III SUBDIT I KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku21slsl02', 'KANIT II SUBDIT I KRIMSUS', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku11slsl02', 'KANIT I SUBDIT I KRIMSUS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('88095643', 'Irman Suyanto', 'BRIGADIR', '123456789', '123456789', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT II', NULL),
('64080833', 'S U I N A H', 'AKP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', '', NULL),
('67120289', 'LESMAN KATILI, SH', 'AKP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT III', NULL),
('72120650', 'AR. WISNU CHANDRA,A.MD, SH', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', NULL, NULL),
('74050737', 'EKO YUDYANTO, S.Si', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', NULL, NULL),
('78030353', 'ISMET ISHAK', 'BRIPKA', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('79051113', 'AHMAT JUFRI', 'BRIPKA', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('81020791', 'FEBRIYAN POTALE, SH', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('81090609', 'NGATASI SURBAKTI', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT I', NULL),
('82030981', 'RUDIANTO SIMBALA, SH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('82051578', 'SUKARNA PAIRI, SH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT I', NULL),
('82100362', 'MAMAN DATAU', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 1, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('84061339', 'MARWANDI SOLEMAN', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('84120269', 'PRANTI NATALIA OLII, SH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('85041174', 'DEDI PANEO', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 1, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('85041343', 'ARIEF RAHMAN LASANUDIN', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('85051301', 'MELKI DAI', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('85080702', 'ANES ISIMA, SH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT II', NULL),
('85091077', 'MOHAMAD TAUPIQ L. DARISE', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 1, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('86081496', 'WISNAWATI U. OTAYA', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('86121164', 'DENIS ABDJUL', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT III', NULL),
('87041629', 'DEDDY ANDRIYANTO', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 1, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('87070519', 'JULIUS CRISTOVEL TAMBAYONG', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT II', NULL),
('87110328', 'ARISTIA GANI', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('87110389', 'IRAH, SH', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('88080795', 'HADI SYAHPUTRA', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT II', NULL),
('89030050', 'HASAN RAMON BAKARI', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 1, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('89030395', 'MUH. AGUNG WH', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('89040432', 'VICTOR HILIWILO', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 1, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('89060706', 'DEVI ROEROE', 'BRIPDA', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT III', NULL),
('89090189', 'IKHSAN NUSI', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT I', NULL),
('89120520', 'PUTRA ADIANTHO', 'BRIPDA', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT III', NULL),
('91070088', 'ZUHLIANDA HULIMA', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('57110105', 'BURHANUDIN ADIPU', 'AKP', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('59040830', 'MARYAM DUE', 'IPTU', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('62020611', 'SIAN WOLOKS', 'IPDA', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('75050718', 'ZULMAN ABDUL MUIS, SH', 'IPDA', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('77100614', 'PARLIN SUKIR', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('82110679', 'FERRON BAIKU, SH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('83041321', 'SOLIHIN JAYA M. NUR, SH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('84090171', 'DIAN DWI S SOEKARDJO', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('89060465', 'INDRA PRIYATNO SOING', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('90060224', 'YUNIARTI N AKUBA', 'BRIPDA', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('91030140', 'ISWAN ABD LATIF', 'BRIPDA', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'RENMIN', NULL),
('85110758', 'RONAL U. RAHIM', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'BINOPS', NULL),
('87030855', 'DANNY ANUGRAH MUSA', 'BRIPTU', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'BINOPS', NULL),
('69040058', 'SUDARSIH, SH', 'AKP', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'BINOPS', NULL),
('77010544', 'MUHAMMAD RIFAI, SH, SIK', 'AKBP', '-', '-', '2016-08-25 14:40:32', 4, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'BINOPS', 'BINOPS', NULL),
('74070155', 'IRWAN SULEMAN', 'AIPTU', '-', '-', '2016-08-25 14:40:32', 2, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'IDEN', 'UNIT I', NULL),
('80100383', 'HANDONO CHRISBUDIHARTO,S.SOS', 'BRIPKA', '-', '-', '2016-08-25 14:40:32', 2, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'IDEN', 'UNIT I', NULL),
('83050470', 'MEIYANTOW BAWUOH', 'BRIGADIR', '-', '-', '2016-08-25 14:40:32', 2, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'IDEN', 'UNIT I', NULL),
('66070308', 'PIETMON TAMALAWE,SH', 'AKP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'WASSIDIK', 'UNIT I', NULL),
('67050428', 'GAGAS NUGRAHA,SH,SIK,M.M', 'KOMBESPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'DIRRESKRIMUM', 'DIRRESKRIMUM', NULL),
('70020384', 'IWAN EKA PUTRA,SIK', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'WADIRRESKRIMUM', 'WADIRRESKRIMUM', NULL),
('asd1slsl04', 'ADMIN SUBDIT I LANTAS SULSEL', 'ADMIN SUBDIT I', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'SUBDIT I', 'SUBDIT I', NULL),
('ku13slsl01', 'KANIT I SUBDIT III KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT I', NULL),
('ku43slsl01', 'KANIT IV SUBDIT III KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('ku21slsl03', 'KANIT II SUBDIT I NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku24slsl03', 'KANIT II SUBDIT IV NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT IV', 'UNIT II', NULL),
('ksd1slsl01', 'KASUBDIT I KRIMUM SULSEL', 'AKBP', '-', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'KASUBDIT', NULL),
('ku41slsl04', 'KANIT IV SUBDIT I LANTAS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku31slsl04', 'KANIT III SUBDIT I LANTAS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku21slsl04', 'KANIT II SUBDIT I LANTAS SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ksd2slsl03', 'KASUBDIT II NARKOBA SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT II', 'KASUBDIT', NULL),
('ku44slsl03', 'KANIT IV SUBDIT IV NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT IV', 'UNIT IV', NULL),
('ku34slsl03', 'KANIT III SUBDIT IV NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('ku14slsl03', 'KANIT I SUBDIT IV NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('ku43slsl03', 'KANIT IV SUBDIT III NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT III', 'UNIT IV', NULL),
('ku33slsl03', 'KANIT III SUBDIT III NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT III', 'UNIT III', NULL),
('ku23slsl03', 'KANIT II SUBDIT III NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT III', 'UNIT II', NULL),
('ku42slsl03', 'KANIT IV SUBDIT II NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT II', 'UNIT IV', NULL),
('ksd4slsl02', 'KASUBDIT IV KRIMSUS SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT IV', 'KASUBDIT', NULL),
('ku21slsl01', 'KANIT II SUBDIT I KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku31slsl01', 'KANIT III SUBDIT I KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ksd1slsl03', 'KASUBDIT I NARKOBA SULSEL', 'AKBP', '-', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT I', 'KASUBDIT', NULL),
('ku13slsl03', 'KANIT I SUBDIT III NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT III', 'UNIT I', NULL),
('ku11slsl03', 'KANIT I SUBDIT I NARKOBA SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ksd3slsl02', 'KASUBDIT III KRIMSUS SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT III', 'KASUBDIT', NULL),
('ksd2slsl02', 'KASUBDIT II KRIMSUS SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT II', 'KASUBDIT', NULL),
('ksd1slsl02', 'KASUBDIT I KRIMSUS SULSEL', 'AKBP', '-', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', NULL, 'SUBDIT I', 'KASUBDIT', NULL),
('ksd3slsl03', 'KASUBDIT III NARKOBA SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESNARKOBA', NULL, 'SUBDIT III', 'KASUBDIT', NULL),
('wdirslsl01', 'WADIRRESKRIMUM SULSEL', 'AKBP', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'WADIRRESKRIMUM', 'WADIRRESKRIMUM', NULL),
('ku41slsl01', 'KANIT IV SUBDIT I KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku12slsl01', 'KANIT I SUBDIT II KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT I', NULL),
('ku22slsl01', 'KANIT II SUBDIT II KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT II', NULL),
('ku32slsl01', 'KANIT III SUBDIT II KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT II', 'UNIT III', NULL),
('kdirslsl01', 'DIRRESKRIMUM SULSEL', 'KOMBESPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'DIRRESKRIMUM', 'DIRRESKRIMUM', NULL),
('ku14slsl01', 'KANIT I SUBDIT IV KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT I', NULL),
('74040474', 'SYAHRIR,SH', 'IPTU', '123456', '123456', '2015-11-24 03:49:19', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('79010026', 'HASMAWATI, SH', 'AIPDA', '123456', '123456', '2016-03-08 02:49:05', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('80110921', 'REAGAN DULIH ALIMIN. S.Sos', 'BRIPKA', '123456', '123456', '2015-11-24 03:52:43', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('88060145', 'NUR RESKI. A.YUSUF, SH', 'BRIGADIR', '08114458686', '1234567', '2015-11-27 06:40:57', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('88090454', 'NUR CAHYADI AM, S.Psi', 'BRIPTU', '123456', '123456', '2015-12-16 10:30:27', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('63020241', 'P MALELAK SH MH', 'IPDA', '123456', '123456', '2015-11-24 04:11:04', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('78050736', 'SYARIFUDDIN', 'BRIPKA', '123456', '123456', '2015-11-24 04:12:54', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('79080436', 'FITRIADI JAMALUDDIN, SH', 'BRIPKA', '123456', '123456', '2015-11-24 04:14:33', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('79040345', 'SUPARMAN', 'BRIPKA', '123456', '123456', '2015-12-17 09:35:14', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('80040784', 'FAISAL', 'BRIPKA', '123456', '123456', '2015-12-18 08:59:52', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('80070890', 'ANWAR', 'BRIPKA', '123456', '123456', '2015-12-17 09:36:24', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('82090432', 'YUSRAN JAFSUR', 'BRIPKA', '123456', '123456', '2015-11-24 04:19:18', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('81011118', 'SAKIR', 'BRIGADIR', '123456', '123456', '2015-12-17 09:36:57', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('84090992', 'ANDIKA AMIR', 'BRIGADIR', '123456', '123456', '2015-11-24 03:38:49', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('86020011', 'ANDI FAISAL M', 'BRIGADIR', '123456', '123456', '2015-12-17 09:38:17', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85040958', 'TAMRIN, SH', 'BRIGADIR', '123456', '123456', '2015-12-17 09:39:12', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85031408', 'SYAMSURIJAL', 'BRIGADIR', '123456', '123456', '2015-12-17 09:40:51', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('76050952', 'MUH. RAMLI', 'BRIGADIR', '123456', '123456', '2015-12-16 09:27:46', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87080448', 'H. A. MUH AKHSAN', 'BRIGADIR', '123456', '123456', '2015-12-17 09:42:53', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('80090229', 'SAINAR', 'BRIGADIR', '123456', '123456', '2015-12-17 09:42:08', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('78030939', 'HAERUDDIN', 'BRIGADIR', '123456', '123456', '2015-11-24 06:53:07', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('83010880', 'MUHAMMAD YUNUS', 'BRIGADIR', '123456', '123456', '2015-12-17 09:37:41', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85071133', 'GEDE SUPRIYADI, S.Psi', 'BRIGADIR', '123456', '123456', '2016-02-02 08:44:19', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('89020394', 'SUAIB IDRIS', 'BRIPTU', '123456', '123456', '2015-12-17 09:43:27', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('89010465', 'NIHMAL YUDING KAHAR', 'BRIPTU', '085299666632', '085299666632', '2016-02-02 04:17:57', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('77040661', 'ILHAM', 'BRIPKA', '123456', '123456', '2015-12-17 09:34:16', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('83121248', 'ZULKIFLI SUKBA', 'BRIGADIR', '123456', '123456', '2016-02-22 03:02:20', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('70080004', 'SURAHMAN', 'IPDA', '123456', '123456', '2015-11-24 07:26:22', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('72020396', 'ABDUL HAMID', 'AIPTU', '081342493559', '081342493559', '2015-11-24 03:15:10', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('79041116', 'MUH AMIR, SH', 'BRIPKA', '123456', '123456', '2015-12-18 08:12:47', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('83010472', 'ASDAR. AR', 'BRIPKA', '123456', '123456', '2015-11-24 07:27:53', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('79070658', 'ACHMAD', 'BRIGADIR', '082396425575', '082396425575', '2015-11-24 03:18:09', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('76070904', 'M. YUSUF HS', 'BRIGADIR', '123456', '123456', '2015-11-24 07:28:39', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('86110955', 'SYAMSU DHUHA SH', 'BRIGADIR', '123456', '123456', '2015-11-24 07:33:27', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'SUBNIT I'),
('90040431', 'DONNA BRIADI, S.I.K.', 'IPDA', '082221222013', '082221222013', '2015-11-24 07:36:05', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('79050846', 'AMIRUDDIN', 'BRIPKA', '085255195547', '085255195547', '2015-11-24 03:34:05', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('79040208', 'MUH. AKBAR, SH', 'BRIPKA', '082310744722', '082310744722', '2015-11-24 07:39:27', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('82041138', 'HENDRA WIJAYA', 'BRIPKA', '123456', '123456', '2015-12-22 06:43:40', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('83050211', 'AWALUDDIN NUR, S.Sos', 'BRIPKA', '123456', '123456', '2015-11-24 07:41:45', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('80040340', 'ARMAN', 'BRIGADIR', '123456', '123456', '2015-12-29 02:57:22', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('83080176', 'RAHMAN', 'BRIGADIR', '-', '-', '2016-02-22 02:58:18', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('87110170', 'EKO SULISTYO, S.Psi', 'BRIGADIR', '082189555082', '082189555082', '2015-11-24 07:43:38', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('89120051', 'MUH. SATRIAH Y, SH', 'BRIPTU', '081241971784', '081241971784', '2015-11-24 07:45:01', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('86101817', 'KAHARUDDIN', 'BRIPTU', '123456', '123456', '2015-11-24 07:46:02', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('77030610', 'SARCE. HS, SH', 'IPDA', '123456', '123456', '2015-12-11 09:22:19', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('79061199', 'SUHARDI RABASING, SH', 'BRIPKA', '123456', '123456', '2015-12-11 09:22:41', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('79101069', 'SAHRIR', 'BRIPKA', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('81041132', 'HASRUDDIN, S.Psi., MH', 'BRIPKA', '123456', '123456', '2015-12-11 09:21:25', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('86071695', 'ISWAN HIDAYAT', 'BRIGADIR', '123456', '123456', '2015-12-11 09:21:51', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('86101629', 'DANI ARDIN, SH', 'BRIGADIR', '123456', '123456', '2015-12-11 09:20:17', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('87081538', 'AGUS', 'BRIPTU', '123456', '123456', '2015-12-11 09:19:41', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'SUBNIT I'),
('69090210', 'H. A. RIDWAN', 'AIPTU', '085399014883', '085399014883', '2016-03-08 02:49:20', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('84031192', 'TAMSIL IRAWAN. S', 'BRIGADIR', '082184220307', '082184220307', '2016-02-04 03:11:26', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85041071', 'ANSAR.G', 'BRIGADIR', '123456', '123456', '2015-12-11 09:16:52', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('82051602', 'ST. SURIYATI. R, S.Ksi', 'BRIGADIR', '123456', '123456', '2015-12-11 09:17:24', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('86051971', 'PUJI LESTARI. H', 'BRIGADIR', '081398683459', '081398683459', '2015-12-11 09:17:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('95010161', 'SITTI HAJAR RAHMAT', 'BRIPDA', '082193509708', '082193509708', '2015-12-11 09:18:23', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('94080562', 'AMANDA MAULIYA SARI', 'BRIPDA', '081356987125', '081356987125', '2015-12-11 09:13:56', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('88030819', 'BELLA WIDYA BOMANTARA', 'BRIPTU', '085322225533', '085322225533', '2015-12-17 09:45:56', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT_IDEN', 'SUBNIT I'),
('65090634', 'DEKY RUDY RONNY WORANG', 'AIPTU', '123456', '123456', '2015-11-24 08:01:42', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT_IDEN', 'SUBNIT I'),
('82111097', 'ABD. HALIM. H', 'BRIGADIR', '123456', '123456', '2015-11-24 07:24:00', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85090281', 'SYAMSUL RIZAL. B', 'BRIGADIR', '123456', '123456', '2015-11-24 08:04:16', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT_IDEN', 'SUBNIT I'),
('84081297', 'SUTRISNO ADIGUNAWAN', 'BRIGADIR', '123456', '123456', '2015-11-24 08:05:18', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT_IDEN', 'SUBNIT I'),
('asd4slsl01', 'ADMIN SUBDIT IV KRIMUM SULSEL', 'ADMIN SUBDIT IV', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'SUBDIT IV', NULL),
('83101371', 'HARYANI ARSYAD', 'BRIGADIR', '081264624225', '082165522255', '2015-11-25 07:27:56', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('87031538', 'SIDIK SAKTI INDRA WASPADA', 'BRIPTU', '123456', '123456', '2015-12-01 08:47:43', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'RENMIN', 'SUBNIT I'),
('79081245', 'BURHANUDDIN', 'BRIPKA', '-', '-', '2016-02-02 08:26:07', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('93081012', 'SITTY HARDIYANTI FAHRUDDIN', 'BRIPDA', '123456', '123456', '2015-12-18 09:18:16', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('94110858', 'YUYUN IDRAWATI', 'BRIPDA', '123456', '123456', '2015-12-18 09:19:46', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('96050054', 'DEWI HARTINA', 'BRIPDA', '123456', '123456', '2015-12-18 09:20:50', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('88051001', 'ZULFITRAH WAHID', 'BRIPTU', '123456', '123456', '2016-01-26 03:09:21', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('77120931', 'SULHAN. S', 'BRIGADIR', '081280647355', '081280647355', '2016-03-04 10:40:18', 1, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('89040508', 'YUSRIL MALLESEANG', 'BRIPTU', '085298722572', '085298722572', '2016-03-07 02:10:28', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('82110157', 'MUHAMMAD RAIS', 'BRIPKA', '085242300621', '085242300621', '2016-02-02 08:48:11', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('80100874', 'DIRHANTO', 'BRIPKA', '-', '-', '2016-02-02 08:41:03', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('95020688', 'ANDI ORISHA ZHATIVA', 'BRIPDA', '085394447743', '085394447743', '2016-03-07 05:48:52', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('94120760', 'CITRA SARI RAMADHANI', 'BRIPDA', '081242886536', '081242886536', '2016-03-07 05:52:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('95020773', 'FEBRY RAMADHANI BAKRI', 'BRIPDA', '085299293312', '085299293312', '2016-03-07 05:55:39', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('95040413', 'RISNAWATI', 'BRIPDA', '085146222295', '085146222295', '2016-03-07 05:57:13', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('ksd1slsl04', 'KASUBDIT I LANTAS SULSEL', 'AKBP', '-', NULL, '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITLANTAS', NULL, 'SUBDIT I', 'KASUBDIT', NULL),
('ku34slsl01', 'KANIT III SUBDIT IV KRIMUM SULSEL', 'KOMPOL', '-', '-', '2016-08-25 14:40:32', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMUM', NULL, 'SUBDIT IV', 'UNIT III', NULL),
('kpolgowa', 'Kapolres Gowa', 'AKBP', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', NULL, NULL, 'SUBDIT I', '-', NULL),
('wpolgowa', 'Wakapolres Gowa', 'KOMPOL', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', NULL, NULL, 'SUBDIT I', '-', NULL),
('ksatgowa01', 'Kasatreskrim Gowa', 'AKP', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', '-', NULL),
('wkbogowa01', 'KBO Reskrim Gowa', 'IPTU', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', '-', NULL),
('ku10gowa01', 'Kanit I Reskrim Gowa', 'IPDA', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku20gowa01', 'Kanit II Reskrim Gowa', 'IPDA', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku30gowa01', 'Kanit III Reskrim Gowa', 'IPDA', '', NULL, '2012-12-02 14:56:20', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku40gowa01', 'Kanit IV Reskrim Gowa', 'IPDA', '', NULL, '2012-12-02 14:30:42', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku50gowa01', 'Kanit V Reskrim Gowa', 'AIPTU', '', NULL, '2015-12-11 09:23:55', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('aspkgowa', 'Admin SPKT Gowa', 'admin', '', NULL, '2015-11-24 03:23:35', 0, 'POLRES GOWA', NULL, NULL, NULL, NULL, NULL),
('kspkgowa', 'Kepala SPKT Gowa', 'admin', '', NULL, '2015-11-24 03:23:35', 0, 'POLRES GOWA', NULL, NULL, NULL, NULL, NULL),
('au10gowa01', 'admin UNIT I RESKRIM GOWA', 'admin UNIT I', '', NULL, '2012-12-06 09:53:00', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('au20gowa01', 'admin UNIT II RESKRIM GOWA', 'admin UNIT II', '', NULL, '2012-12-06 09:53:00', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au30gowa01', 'admin UNIT III RESKRIM GOWA', 'admin UNIT III', '', NULL, '2012-12-06 09:53:00', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('au40gowa01', 'admin UNIT IV RESKRIM GOWA', 'admin UNIT IV', '', NULL, '2012-12-06 09:53:00', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au50gowa01', 'admin UNIT V RESKRIM GOWA', 'admin UNIT V', '', NULL, '2012-12-06 09:53:00', 0, 'POLRES GOWA', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', NULL),
('samksr01', 'ADMIN RESKRIM MAKASSAR', '-', '-', '-', '2016-08-25 14:40:32', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, NULL, NULL, NULL),
('au20mksr01', 'admin UNIT II RESKRIM MAKASSAR', 'admin UNIT II', '', NULL, '2012-12-06 09:53:15', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au50mksr01', 'admin UNIT V RESKRIM MAKASSAR', 'admin UNIT V', '', NULL, '2012-12-06 09:52:51', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', NULL),
('au30mksr01', 'admin UNIT III RESKRIM MAKASSAR', 'admin UNIT III', '081230044296', '000000000', '2013-01-29 02:46:35', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('ks35mksr01', 'kasubnit III UNIT V RESKRIM MAKASSAR', 'iptu', '1257956', '-123456', '2013-09-24 08:15:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT III'),
('au40mksr01', 'admin UNIT IV RESKRIM MAKASSAR', 'admin UNIT IV', '', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au10mksr01', 'admin UNIT I RESKRIM MAKASSAR', 'admin UNIT I', '', NULL, '2012-12-06 09:53:00', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ks34mksr01', 'kasubnit  III UNIT IV RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'Subnit III'),
('kpolmksr', 'kapolrestabes MAKASSAR', 'kombespol', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', '-', '-'),
('wpolmksr', 'wakapolrestabes MAKASSAR', 'AKBP', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', '-', '-'),
('ksatmksr01', 'kasatreskrim MAKASSAR', 'akbp', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', '-', '-'),
('wsatmksr01', 'wakasatreskrim MAKASSAR', 'kompol', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', '-', '-'),
('ku10mksr01', 'kanit I RESKRIM MAKASSAR', 'akp', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', '-'),
('au10mksr03', 'admin UNIT I RESNARKOBA MAKASSAR', 'admin UNIT I', '', NULL, '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku20mksr01', 'kanit II RESKRIM MAKASSAR', 'akp', '-', NULL, '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', '-'),
('ku30mksr01', 'kanit III RESKRIM MAKASSAR', 'akp', '-', '-', '2012-12-03 05:56:20', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', '-'),
('kU40mksr01', 'kanit IV RESKRIM MAKASSAR', 'akp', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', '-'),
('kU50mksr01', 'kanit V RESKRIM MAKASSAR', 'akp', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', '-'),
('ks11mksr01', 'kasubnit I UNIT I RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'Subnit I'),
('ks21mksr01', 'kasubnit II UNIT I RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'Subnit II'),
('ks31mksr01', 'kasubnit III UNIT I RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', 'Subnit III'),
('ks12mksr03', 'kasubnit I UNIT II RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', 'Subnit I'),
('ks11mksr03', 'kasubnit I UNIT I RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', 'Subnit I'),
('au20mksr03', 'admin UNIT II RESNARKOBA MAKASSAR', 'admin UNIT II', '', NULL, '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au30mksr03', 'admin UNIT III RESNARKOBA MAKASSAR', 'admin UNIT III', '081230044296', '000000000', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('au40mksr03', 'admin UNIT IV RESNARKOBA MAKASSAR', 'admin UNIT IV', '', NULL, '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ks12mksr01', 'kasubnit I UNIT II RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'Subnit I'),
('ks22mksr01', 'kasubnit II UNIT II RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'Subnit II'),
('ks32mksr01', 'kasubnit  III UNIT II RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', 'Subnit III'),
('ks13mksr01', 'kasubnit I UNIT III RESKRIM MAKASSAR', 'iptu', '12314646', '12464979', '2013-09-24 08:25:13', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('ks23mksr01', 'kasubnit II UNIT III RESKRIM MAKASSAR', 'iptu', '124699', '124154649', '2013-09-24 08:24:41', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('ks14mksr01', 'kasubnit I UNIT IV RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'Subnit I'),
('ks24mksr01', 'kasubnit II UNIT IV RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', 'Subnit II'),
('ks15mksr01', 'kasubnit I UNIT V RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'Subnit I'),
('ks25mksr01', 'kasubnit II UNIT V RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'Subnit II'),
('ks33mksr01', 'kasubnit III UNIT III RESKRIM MAKASSAR', 'iptu', '1257956', '-123456', '2013-09-24 08:15:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('aspkmksr', 'Admin SPKT MAKASSAR', 'Aiptu', '123456', '123456', '2012-11-14 05:49:40', 2, 'POLRESTABES MAKASSAR', NULL, NULL, NULL, NULL, NULL),
('kspkmksr', 'Kepala SPKT MAKASSAR', 'IPTU', '', NULL, '2012-12-02 14:30:42', 0, 'POLRESTABES MAKASSAR', NULL, NULL, NULL, NULL, NULL),
('84041545', 'SUTRISNO DIDIPU', 'IPTU', '-', '-', '2014-04-10 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('66030433', 'DRS.HERSADWI RUSDIYONO,SH', 'BRIGADIR', '-', '-', '2014-03-12 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('68010373', 'FITRIZAL SILA, SH', 'BRIGADIR', '-', '-', '2014-03-13 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('64120336', 'ALI TAHALELE, SH', 'AIPTU', '-', '-', '2014-03-15 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('76061086', 'AMNER B. PURBA, S.SOS', 'BRIGADIR', '-', '-', '2014-03-16 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('68080316', 'HUSIN HASAN', 'AIPTU', '-', '-', '2014-03-18 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('61010087', 'SULEMAN MOHA', 'BRIPTU', '-', '-', '2014-03-19 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('84091819', 'RONNY BURUNGUDJU', 'BRIPTU', '-', '-', '2014-03-20 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('77050679', 'HARISNO PAKAJA, SE', 'IPDA', '-', '-', '2014-03-21 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('75010064', 'SUYONO PULUHULAWA, SE', 'AIPTU', '-', '-', '2014-03-22 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('81040205', 'ERYO MOHAMAD, SH', 'IPTU', '-', '-', '2014-03-24 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('84040380', 'RONAL ALI, SH, MH', 'BRIGADIR', '-', '-', '2014-03-25 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85100981', 'RAHMAT PONGOLIU, SH', 'BRIGADIR', '-', '-', '2014-03-27 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87030164', 'MARTOSONO SABIHI', 'AIPTU', '-', '-', '2014-03-28 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('86111101', 'RAHMAN SUPU', 'BRIGADIR', '-', '-', '2014-03-29 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('83121159', 'EBIT BLONGKOD', 'AIPTU', '-', '-', '2014-03-30 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('86030610', 'MANAF USMAN', 'BRIPTU', '-', '-', '2014-03-31 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85031692', 'ARFIAN DETUAGE, SH', 'BRIPTU', '-', '-', '2014-04-01 16:32:27', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87090301', 'RONAWATY UMAR', 'IPTU', '-', '-', '2014-04-02 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('84110646', 'IRWANSYAH M. DALI', 'AIPTU', '-', '-', '2014-04-06 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('82100343', 'SILVANA DIANI, SH', 'BRIGADIR', '-', '-', '2014-04-12 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85091798', 'RISWAN', 'BRIGADIR', '-', '-', '2014-04-13 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('88050189', 'RAMLI UTIARAHMAN', 'AIPTU', '-', '-', '2014-04-14 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87100775', 'ILHAM BAHARUDDIN', 'BRIPTU', '-', '-', '2014-04-15 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('86051918', 'FITRIYANTO SUTOMO', 'BRIPTU', '-', '-', '2014-04-16 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('86051692', 'KASRUDDIN', 'IPTU', '-', '-', '2014-04-17 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('88090726', 'EDI SURYANTO', 'BRIGADIR', '-', '-', '2014-04-18 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87051706', 'HENDRA J. HASAN', 'AIPTU', '-', '-', '2014-04-18 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('89050011', 'MOHAMAD RIZAL YUSUF', 'AIPTU', '-', '-', '2014-04-18 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('75100924', 'FARNO TUWENO, SH', 'BRIPTU', '-', '-', '2014-04-18 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('85010877', 'YUSRIN ABAS', 'BRIGADIR', '-', '-', '2014-04-07 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('87090769', 'MOH. ZULFIKRI DATUKRAMAT', 'BRIPTU', '-', '-', '2014-04-18 16:32:27', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('kpmksrrpcn', 'KAPOLSEK RAPPOCINI', 'KOMPOL', '-', '-', '2016-08-30 03:55:41', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', NULL, NULL, NULL),
('ksmksrrpcn', 'KEPALA SPKT POLSEK RAPPOCINI', 'IPDA', '-', '-', '2016-08-30 03:55:41', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', NULL, NULL, NULL),
('kumksrrpcn', 'KANIT RESKRIM POLSEK RAPPOCINI', 'IPTU', '-', '-', '2016-08-30 03:55:41', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', NULL, NULL, NULL),
('samksrrpcn', 'ADMIN RESKRIM POLSEK RAPPOCINI', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:41', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', NULL, NULL, NULL);
INSERT INTO `tb_penyidik` (`NRP`, `NAMA_PENYIDIK`, `PANGKAT_PENYIDIK`, `HP_PENYIDIK`, `TELP_PENYIDIK`, `tgl_masuk_data`, `IS_OPSNAL`, `NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_POLSEK`, `NAMA_SUBDIT`, `NAMA_UNIT`, `NAMA_SUBNIT`) VALUES
('asmksrmmjg', 'ADMIN SPKT POLSEK MAMAJANG', 'ADMIN', '-', NULL, '2016-08-30 03:55:19', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAMAJANG', NULL, NULL, NULL),
('kpmksrmmjg', 'KAPOLSEK MAMAJANG', 'KOMPOL', '-', '-', '2016-08-30 03:55:19', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAMAJANG', NULL, NULL, NULL),
('ksmksrmmjg', 'KEPALA SPKT POLSEK MAMAJANG', 'IPDA', '-', '-', '2016-08-30 03:55:19', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAMAJANG', NULL, NULL, NULL),
('kumksrmmjg', 'KANIT RESKRIM POLSEK MAMAJANG', 'IPTU', '-', '-', '2016-08-30 03:55:19', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAMAJANG', NULL, NULL, NULL),
('ksmksrbrkn', 'KEPALA SPKT POLSEK BIRINGKANAYA', 'IPDA', '-', '-', '2016-08-30 03:55:07', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BIRINGKANAYA', NULL, NULL, NULL),
('kumksrbrkn', 'KANIT RESKRIM POLSEK BIRINGKANAYA', 'IPTU', '-', '-', '2016-08-30 03:55:07', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BIRINGKANAYA', NULL, NULL, NULL),
('samksrbrkn', 'ADMIN RESKRIM POLSEK BIRINGKANAYA', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:07', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BIRINGKANAYA', NULL, NULL, NULL),
('asmksrbtla', 'ADMIN SPKT POLSEK BONTOALA', 'ADMIN', '-', NULL, '2016-08-30 03:55:13', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BONTOALA', NULL, NULL, NULL),
('kpmksrbtla', 'KAPOLSEK BONTOALA', 'KOMPOL', '-', '-', '2016-08-30 03:55:13', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BONTOALA', NULL, NULL, NULL),
('ksmksrbtla', 'KEPALA SPKT POLSEK BONTOALA', 'IPDA', '-', '-', '2016-08-30 03:55:13', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BONTOALA', NULL, NULL, NULL),
('kumksrbtla', 'KANIT RESKRIM POLSEK BONTOALA', 'IPTU', '-', '-', '2016-08-30 03:55:13', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BONTOALA', NULL, NULL, NULL),
('samksrbtla', 'ADMIN RESKRIM POLSEK BONTOALA', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:13', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BONTOALA', NULL, NULL, NULL),
('astria8916', 'galau bingung rabi 101016', 'admin', '-', '-', '2016-07-31 17:00:00', 0, 'POLDA SULAWESI SELATAN', NULL, NULL, 'SUBDIT I', 'UNIT I', NULL),
('au40kptg01', 'admin UNIT IV RESKRIM KP3', 'admin UNIT IV', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au40kptg03', 'admin UNIT IV RESNARKOBA KP3', 'admin UNIT IV', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au30kptg03', 'admin UNIT III RESNARKOBA KP3', 'admin UNIT III', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('au30kptg01', 'admin UNIT III RESKRIM KP3', 'admin UNIT III', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('au20kptg03', 'admin UNIT II RESNARKOBA KP3', 'admin UNIT II', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au20kptg01', 'admin UNIT II RESKRIM KP3', 'admin UNIT II', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au10kptg04', 'admin UNIT I LANTAS KP3', 'admin UNIT I', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('au10kptg03', 'admin UNIT I RESNARKOBA KP3', 'admin UNIT I', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku50kptg01', 'Kanit V Reskrim KP3', 'AIPTU', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('au10kptg01', 'admin UNIT I RESKRIM KP3', 'admin UNIT I', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku40kptg03', 'Kanit IV RESNARKOBA KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku30kptg01', 'Kanit III Reskrim KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku30kptg03', 'Kanit III RESNARKOBA KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku40kptg01', 'Kanit IV Reskrim KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku10kptg04', 'Kanit I LANTAS KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku20kptg01', 'Kanit II Reskrim KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku20kptg03', 'Kanit II RESNARKOBA KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku10kptg03', 'Kanit I RESNARKOBA KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku10kptg01', 'Kanit I Reskrim KP3', 'IPDA', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('wkbokptg03', 'KBO RESNARKOBA KP3', 'IPTU', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', NULL),
('wkbokptg04', 'KBO LANTAS KP3', 'IPTU', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATLANTAS', NULL, 'SUBDIT I', '-', NULL),
('wkbokptg01', 'KBO Reskrim KP3', 'IPTU', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', '-', NULL),
('ksatkptg04', 'Kasatlantas KP3', 'AKP', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATLANTAS', NULL, 'SUBDIT I', '-', NULL),
('ksatkptg01', 'Kasatreskrim KP3', 'AKP', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', '-', NULL),
('ksatkptg03', 'Kasatresnarkoba KP3', 'AKP', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', NULL),
('kpolkptg', 'Kapolres KP3', 'AKBP', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', NULL, NULL, 'SUBDIT I', '-', NULL),
('wpolkptg', 'Wakapolres KP3', 'KOMPOL', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', NULL, NULL, 'SUBDIT I', '-', NULL),
('sakptg01', 'Admin Reskrim KP3', '-', '-', '-', '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, NULL, NULL, NULL),
('sakptg03', 'Admin RESNARKOBA KP3', '-', '-', '-', '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESNARKOBA', NULL, NULL, NULL, NULL),
('sakptg04', 'Admin LANTAS KP3', '-', '-', '-', '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATLANTAS', NULL, NULL, NULL, NULL),
('kspkmros', 'Kepala SPKT MAROS', 'admin', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', NULL, NULL, NULL, NULL, NULL),
('aspkmros', 'Admin SPKT MAROS', 'admin', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', NULL, NULL, NULL, NULL, NULL),
('au50mros01', 'admin UNIT V RESKRIM MAROS', 'admin UNIT V', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', NULL),
('au40mros03', 'admin UNIT IV RESNARKOBA MAROS', 'admin UNIT IV', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au40mros01', 'admin UNIT IV RESKRIM MAROS', 'admin UNIT IV', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au30mros03', 'admin UNIT III RESNARKOBA MAROS', 'admin UNIT III', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('au30mros01', 'admin UNIT III RESKRIM MAROS', 'admin UNIT III', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('au10mros04', 'admin UNIT I LANTAS MAROS', 'admin UNIT I', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('au20mros01', 'admin UNIT II RESKRIM MAROS', 'admin UNIT II', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au20mros03', 'admin UNIT II RESNARKOBA MAROS', 'admin UNIT II', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au10mros03', 'admin UNIT I RESNARKOBA MAROS', 'admin UNIT I', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('au10mros01', 'admin UNIT I RESKRIM MAROS', 'admin UNIT I', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku50mros01', 'Kanit V Reskrim MAROS', 'AIPTU', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('ku40mros03', 'Kanit IV RESNARKOBA MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku40mros01', 'Kanit IV Reskrim MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku30mros03', 'Kanit III RESNARKOBA MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku30mros01', 'Kanit III Reskrim MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku20mros03', 'Kanit II RESNARKOBA MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku20mros01', 'Kanit II Reskrim MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku10mros01', 'Kanit I Reskrim MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku10mros03', 'Kanit I RESNARKOBA MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ku10mros04', 'Kanit I LANTAS MAROS', 'IPDA', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ksatmros04', 'Kasatlantas MAROS', 'AKP', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATLANTAS', NULL, 'SUBDIT I', '-', NULL),
('wkbomros01', 'KBO Reskrim MAROS', 'IPTU', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', '-', NULL),
('wkbomros03', 'KBO RESNARKOBA MAROS', 'IPTU', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', NULL),
('wkbomros04', 'KBO LANTAS MAROS', 'IPTU', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATLANTAS', NULL, 'SUBDIT I', '-', NULL),
('samros04', 'Admin LANTAS MAROS', '-', '-', '-', '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATLANTAS', NULL, NULL, NULL, NULL),
('kpolmros', 'Kapolres MAROS', 'AKBP', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', NULL, NULL, 'SUBDIT I', '-', NULL),
('wpolmros', 'Wakapolres MAROS', 'KOMPOL', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', NULL, NULL, 'SUBDIT I', '-', NULL),
('ksatmros01', 'Kasatreskrim MAROS', 'AKP', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, 'SUBDIT I', '-', NULL),
('ksatmros03', 'Kasatresnarkoba MAROS', 'AKP', '', NULL, '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', NULL),
('samros01', 'Admin Reskrim MAROS', '-', '-', '-', '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESKRIM', NULL, NULL, NULL, NULL),
('samros03', 'Admin RESNARKOBA MAROS', '-', '-', '-', '2016-08-29 13:12:52', 0, 'POLRES MAROS', 'SATRESNARKOBA', NULL, NULL, NULL, NULL),
('wkbogowa04', 'KBO LANTAS Gowa', 'IPTU', '', NULL, '2016-08-29 12:45:40', 0, 'POLRES GOWA', 'SATLANTAS', NULL, 'SUBDIT I', '-', NULL),
('sagowa04', 'Admin LANTAS Gowa', '-', '-', '-', '2016-08-29 12:45:40', 0, 'POLRES GOWA', 'SATLANTAS', NULL, NULL, NULL, NULL),
('ku10gowa04', 'Kanit I LANTAS Gowa', 'IPDA', '', NULL, '2016-08-29 12:45:40', 0, 'POLRES GOWA', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ksatgowa04', 'Kasatlantas Gowa', 'AKP', '', NULL, '2016-08-29 12:45:40', 0, 'POLRES GOWA', 'SATLANTAS', NULL, 'SUBDIT I', '-', NULL),
('au10gowa04', 'admin UNIT I LANTAS GOWA', 'admin UNIT I', '', NULL, '2016-08-29 12:45:40', 0, 'POLRES GOWA', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('wkbogowa03', 'KBO RESNARKOBA Gowa', 'IPTU', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', NULL),
('sagowa03', 'Admin RESNARKOBA Gowa', '-', '-', '-', '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, NULL, NULL, NULL),
('ku40gowa03', 'Kanit IV RESNARKOBA Gowa', 'IPDA', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('au40gowa03', 'admin UNIT IV RESNARKOBA GOWA', 'admin UNIT IV', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', NULL),
('ku30gowa03', 'Kanit III RESNARKOBA Gowa', 'IPDA', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('ku20gowa03', 'Kanit II RESNARKOBA Gowa', 'IPDA', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('ku10gowa03', 'Kanit I RESNARKOBA Gowa', 'IPDA', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ksatgowa03', 'Kasatresnarkoba Gowa', 'AKP', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', NULL),
('au30gowa03', 'admin UNIT III RESNARKOBA GOWA', 'admin UNIT III', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', NULL),
('au20gowa03', 'admin UNIT II RESNARKOBA GOWA', 'admin UNIT II', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', NULL),
('au10gowa03', 'admin UNIT I RESNARKOBA GOWA', 'admin UNIT I', '', NULL, '2016-08-29 12:44:41', 0, 'POLRES GOWA', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', NULL),
('ksatmksr04', 'kasatlantas MAKASSAR', 'akbp', '-', '-', '2016-08-29 09:13:51', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', '-', '-'),
('ksatmksr03', 'kasatresnarkoba MAKASSAR', 'akbp', '-', '-', '2016-08-29 09:13:18', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', '-'),
('ku10mksr04', 'kanit I LANTAS MAKASSAR', 'akp', '-', '-', '2016-08-29 09:02:13', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', '-'),
('ku10mksr03', 'kanit I RESNARKOBA MAKASSAR', 'akp', '-', '-', '2016-08-29 09:02:13', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', '-'),
('wsatmksr04', 'wakasatLANTAS MAKASSAR', 'kompol', '-', '-', '2016-08-29 08:55:24', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', '-', '-'),
('samksr04', 'ADMIN LANTAS MAKASSAR', '-', '-', '-', '2016-08-29 08:55:24', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, NULL, NULL, NULL),
('ks31mksr04', 'kasubnit III UNIT I LANTAS MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:55:24', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', 'Subnit III'),
('ks21mksr04', 'kasubnit II UNIT I LANTAS MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:55:24', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', 'Subnit II'),
('ks11mksr04', 'kasubnit I UNIT I LANTAS MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:55:24', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', 'Subnit I'),
('au10mksr04', 'admin UNIT I LANTAS MAKASSAR', 'admin UNIT I', '', NULL, '2016-08-29 08:55:23', 0, 'POLRESTABES MAKASSAR', 'SATLANTAS', NULL, 'SUBDIT I', 'UNIT I', NULL),
('wsatmksr03', 'wakasatRESNARKOBA MAKASSAR', 'kompol', '-', '-', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', '-', '-'),
('samksr03', 'ADMIN RESNARKOBA MAKASSAR', '-', '-', '-', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, NULL, NULL, NULL),
('kU40mksr03', 'kanit IV RESNARKOBA MAKASSAR', 'akp', '-', '-', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', '-'),
('ku30mksr03', 'kanit III RESNARKOBA MAKASSAR', 'akp', '-', '-', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', '-'),
('ku20mksr03', 'kanit II RESNARKOBA MAKASSAR', 'akp', '-', NULL, '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', '-'),
('ks34mksr03', 'kasubnit  III UNIT IV RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', 'Subnit III'),
('ks33mksr03', 'kasubnit III UNIT III RESNARKOBA MAKASSAR', 'iptu', '1257956', '-123456', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT III'),
('ks31mksr03', 'kasubnit III UNIT I RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', 'Subnit III'),
('ks32mksr03', 'kasubnit  III UNIT II RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:06', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', 'Subnit III'),
('ks24mksr03', 'kasubnit II UNIT IV RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', 'Subnit II'),
('ks23mksr03', 'kasubnit II UNIT III RESNARKOBA MAKASSAR', 'iptu', '124699', '124154649', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT II'),
('ks14mksr03', 'kasubnit I UNIT IV RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT IV', 'Subnit I'),
('ks22mksr03', 'kasubnit II UNIT II RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT II', 'Subnit II'),
('ks13mksr03', 'kasubnit I UNIT III RESNARKOBA MAKASSAR', 'iptu', '12314646', '12464979', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('ks21mksr03', 'kasubnit II UNIT I RESNARKOBA MAKASSAR', 'iptu', '-', '-', '2016-08-29 08:53:05', 0, 'POLRESTABES MAKASSAR', 'SATRESNARKOBA', NULL, 'SUBDIT I', 'UNIT I', 'Subnit II'),
('au50kptg01', 'admin UNIT V RESKRIM KP3', 'admin UNIT V', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT V', NULL),
('aspkkptg', 'Admin SPKT KP3', 'admin', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', NULL, NULL, NULL, NULL, NULL),
('kspkkptg', 'Kepala SPKT KP3', 'admin', '', NULL, '2016-08-29 13:13:08', 0, 'POLRES KP3', NULL, NULL, NULL, NULL, NULL),
('kugowaprgi', 'KANIT RESKRIM POLSEK PARIGI', 'IPTU', '-', '-', '2016-08-30 03:58:11', 0, 'POLRES GOWA', NULL, 'POLSEK PARIGI', NULL, NULL, NULL),
('sagowaprgi', 'ADMIN RESKRIM POLSEK PARIGI', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:11', 0, 'POLRES GOWA', NULL, 'POLSEK PARIGI', NULL, NULL, NULL),
('asgowaptls', 'ADMIN SPKT POLSEK PATTALLASSANG', 'ADMIN', '-', NULL, '2016-08-30 03:58:20', 0, 'POLRES GOWA', NULL, 'POLSEK PATTALLASSANG', NULL, NULL, NULL),
('asgowabtnp', 'ADMIN SPKT POLSEK BONTONOMPO', 'ADMIN', '-', NULL, '2016-08-30 03:57:34', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO', NULL, NULL, NULL),
('kpgowabtnp', 'KAPOLSEK BONTONOMPO', 'KOMPOL', '-', '-', '2016-08-30 03:57:34', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO', NULL, NULL, NULL),
('ksgowabtnp', 'KEPALA SPKT POLSEK BONTONOMPO', 'IPDA', '-', '-', '2016-08-30 03:57:34', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO', NULL, NULL, NULL),
('kugowabtnp', 'KANIT RESKRIM POLSEK BONTONOMPO', 'IPTU', '-', '-', '2016-08-30 03:57:34', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO', NULL, NULL, NULL),
('sagowabtnp', 'ADMIN RESKRIM POLSEK BONTONOMPO', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:34', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO', NULL, NULL, NULL),
('asgowabtsl', 'ADMIN SPKT POLSEK BONTONOMPO SELATAN', 'ADMIN', '-', NULL, '2016-08-30 03:57:40', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO SELATAN', NULL, NULL, NULL),
('kpgowabtsl', 'KAPOLSEK BONTONOMPO SELATAN', 'KOMPOL', '-', '-', '2016-08-30 03:57:40', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO SELATAN', NULL, NULL, NULL),
('ksgowabtsl', 'KEPALA SPKT POLSEK BONTONOMPO SELATAN', 'IPDA', '-', '-', '2016-08-30 03:57:40', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO SELATAN', NULL, NULL, NULL),
('kugowabtsl', 'KANIT RESKRIM POLSEK BONTONOMPO SELATAN', 'IPTU', '-', '-', '2016-08-30 03:57:40', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO SELATAN', NULL, NULL, NULL),
('sagowabtsl', 'ADMIN RESKRIM POLSEK BONTONOMPO SELATAN', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:40', 0, 'POLRES GOWA', NULL, 'POLSEK BONTONOMPO SELATAN', NULL, NULL, NULL),
('asgowabngy', 'ADMIN SPKT POLSEK BUNGAYA', 'ADMIN', '-', NULL, '2016-08-30 03:57:45', 0, 'POLRES GOWA', NULL, 'POLSEK BUNGAYA', NULL, NULL, NULL),
('kpgowabngy', 'KAPOLSEK BUNGAYA', 'KOMPOL', '-', '-', '2016-08-30 03:57:45', 0, 'POLRES GOWA', NULL, 'POLSEK BUNGAYA', NULL, NULL, NULL),
('ksgowabngy', 'KEPALA SPKT POLSEK BUNGAYA', 'IPDA', '-', '-', '2016-08-30 03:57:45', 0, 'POLRES GOWA', NULL, 'POLSEK BUNGAYA', NULL, NULL, NULL),
('kugowabngy', 'KANIT RESKRIM POLSEK BUNGAYA', 'IPTU', '-', '-', '2016-08-30 03:57:45', 0, 'POLRES GOWA', NULL, 'POLSEK BUNGAYA', NULL, NULL, NULL),
('sagowabngy', 'ADMIN RESKRIM POLSEK BUNGAYA', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:45', 0, 'POLRES GOWA', NULL, 'POLSEK BUNGAYA', NULL, NULL, NULL),
('asgowamnju', 'ADMIN SPKT POLSEK MANUJU', 'ADMIN', '-', NULL, '2016-08-30 03:57:52', 0, 'POLRES GOWA', NULL, 'POLSEK MANUJU', NULL, NULL, NULL),
('kpgowamnju', 'KAPOLSEK MANUJU', 'KOMPOL', '-', '-', '2016-08-30 03:57:52', 0, 'POLRES GOWA', NULL, 'POLSEK MANUJU', NULL, NULL, NULL),
('ksgowamnju', 'KEPALA SPKT POLSEK MANUJU', 'IPDA', '-', '-', '2016-08-30 03:57:52', 0, 'POLRES GOWA', NULL, 'POLSEK MANUJU', NULL, NULL, NULL),
('kugowamnju', 'KANIT RESKRIM POLSEK MANUJU', 'IPTU', '-', '-', '2016-08-30 03:57:52', 0, 'POLRES GOWA', NULL, 'POLSEK MANUJU', NULL, NULL, NULL),
('sagowamnju', 'ADMIN RESKRIM POLSEK MANUJU', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:52', 0, 'POLRES GOWA', NULL, 'POLSEK MANUJU', NULL, NULL, NULL),
('asgowaplng', 'ADMIN SPKT POLSEK PALLANGGA', 'ADMIN', '-', NULL, '2016-08-30 03:58:00', 0, 'POLRES GOWA', NULL, 'POLSEK PALLANGGA', NULL, NULL, NULL),
('kpgowaplng', 'KAPOLSEK PALLANGGA', 'KOMPOL', '-', '-', '2016-08-30 03:58:00', 0, 'POLRES GOWA', NULL, 'POLSEK PALLANGGA', NULL, NULL, NULL),
('ksgowaplng', 'KEPALA SPKT POLSEK PALLANGGA', 'IPDA', '-', '-', '2016-08-30 03:58:00', 0, 'POLRES GOWA', NULL, 'POLSEK PALLANGGA', NULL, NULL, NULL),
('kugowaplng', 'KANIT RESKRIM POLSEK PALLANGGA', 'IPTU', '-', '-', '2016-08-30 03:58:00', 0, 'POLRES GOWA', NULL, 'POLSEK PALLANGGA', NULL, NULL, NULL),
('sagowaplng', 'ADMIN RESKRIM POLSEK PALLANGGA', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:00', 0, 'POLRES GOWA', NULL, 'POLSEK PALLANGGA', NULL, NULL, NULL),
('asgowaprlo', 'ADMIN SPKT POLSEK PARANGLOE', 'ADMIN', '-', NULL, '2016-08-30 03:58:04', 0, 'POLRES GOWA', NULL, 'POLSEK PARANGLOE', NULL, NULL, NULL),
('kpgowaprlo', 'KAPOLSEK PARANGLOE', 'KOMPOL', '-', '-', '2016-08-30 03:58:04', 0, 'POLRES GOWA', NULL, 'POLSEK PARANGLOE', NULL, NULL, NULL),
('ksgowaprlo', 'KEPALA SPKT POLSEK PARANGLOE', 'IPDA', '-', '-', '2016-08-30 03:58:04', 0, 'POLRES GOWA', NULL, 'POLSEK PARANGLOE', NULL, NULL, NULL),
('kugowaprlo', 'KANIT RESKRIM POLSEK PARANGLOE', 'IPTU', '-', '-', '2016-08-30 03:58:04', 0, 'POLRES GOWA', NULL, 'POLSEK PARANGLOE', NULL, NULL, NULL),
('sagowaprlo', 'ADMIN RESKRIM POLSEK PARANGLOE', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:04', 0, 'POLRES GOWA', NULL, 'POLSEK PARANGLOE', NULL, NULL, NULL),
('asgowaprgi', 'ADMIN SPKT POLSEK PARIGI', 'ADMIN', '-', NULL, '2016-08-30 03:58:11', 0, 'POLRES GOWA', NULL, 'POLSEK PARIGI', NULL, NULL, NULL),
('ksgowabtmr', 'KEPALA SPKT POLSEK BONTOMARANNU', 'IPDA', '-', '-', '2016-08-30 03:57:28', 0, 'POLRES GOWA', NULL, 'POLSEK BONTOMARANNU', NULL, NULL, NULL),
('kugowabtmr', 'KANIT RESKRIM POLSEK BONTOMARANNU', 'IPTU', '-', '-', '2016-08-30 03:57:28', 0, 'POLRES GOWA', NULL, 'POLSEK BONTOMARANNU', NULL, NULL, NULL),
('sagowabtmr', 'ADMIN RESKRIM POLSEK BONTOMARANNU', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:28', 0, 'POLRES GOWA', NULL, 'POLSEK BONTOMARANNU', NULL, NULL, NULL),
('kugowabrbl', 'KANIT RESKRIM POLSEK BIRINGBULU', 'IPTU', '-', '-', '2016-08-30 03:57:23', 0, 'POLRES GOWA', NULL, 'POLSEK BIRINGBULU', NULL, NULL, NULL),
('sagowabrbl', 'ADMIN RESKRIM POLSEK BIRINGBULU', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:23', 0, 'POLRES GOWA', NULL, 'POLSEK BIRINGBULU', NULL, NULL, NULL),
('asgowabtmr', 'ADMIN SPKT POLSEK BONTOMARANNU', 'ADMIN', '-', NULL, '2016-08-30 03:57:28', 0, 'POLRES GOWA', NULL, 'POLSEK BONTOMARANNU', NULL, NULL, NULL),
('kpgowabtmr', 'KAPOLSEK BONTOMARANNU', 'KOMPOL', '-', '-', '2016-08-30 03:57:28', 0, 'POLRES GOWA', NULL, 'POLSEK BONTOMARANNU', NULL, NULL, NULL),
('asgowabrbg', 'ADMIN SPKT POLSEK BAROMBONG', 'ADMIN', '-', NULL, '2016-08-30 03:57:17', 0, 'POLRES GOWA', NULL, 'POLSEK BAROMBONG', NULL, NULL, NULL),
('kpgowabrbg', 'KAPOLSEK BAROMBONG', 'KOMPOL', '-', '-', '2016-08-30 03:57:17', 0, 'POLRES GOWA', NULL, 'POLSEK BAROMBONG', NULL, NULL, NULL),
('ksgowabrbg', 'KEPALA SPKT POLSEK BAROMBONG', 'IPDA', '-', '-', '2016-08-30 03:57:17', 0, 'POLRES GOWA', NULL, 'POLSEK BAROMBONG', NULL, NULL, NULL),
('kugowabrbg', 'KANIT RESKRIM POLSEK BAROMBONG', 'IPTU', '-', '-', '2016-08-30 03:57:17', 0, 'POLRES GOWA', NULL, 'POLSEK BAROMBONG', NULL, NULL, NULL),
('sagowabrbg', 'ADMIN RESKRIM POLSEK BAROMBONG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:17', 0, 'POLRES GOWA', NULL, 'POLSEK BAROMBONG', NULL, NULL, NULL),
('asgowabrbl', 'ADMIN SPKT POLSEK BIRINGBULU', 'ADMIN', '-', NULL, '2016-08-30 03:57:23', 0, 'POLRES GOWA', NULL, 'POLSEK BIRINGBULU', NULL, NULL, NULL),
('kpgowabrbl', 'KAPOLSEK BIRINGBULU', 'KOMPOL', '-', '-', '2016-08-30 03:57:23', 0, 'POLRES GOWA', NULL, 'POLSEK BIRINGBULU', NULL, NULL, NULL),
('ksgowabrbl', 'KEPALA SPKT POLSEK BIRINGBULU', 'IPDA', '-', '-', '2016-08-30 03:57:23', 0, 'POLRES GOWA', NULL, 'POLSEK BIRINGBULU', NULL, NULL, NULL),
('asgowabjng', 'ADMIN SPKT POLSEK BAJENG', 'ADMIN', '-', NULL, '2016-08-30 03:57:11', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', NULL, NULL, NULL),
('kpgowabjng', 'KAPOLSEK BAJENG', 'KOMPOL', '-', '-', '2016-08-30 03:57:11', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', NULL, NULL, NULL),
('ksgowabjng', 'KEPALA SPKT POLSEK BAJENG', 'IPDA', '-', '-', '2016-08-30 03:57:11', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', NULL, NULL, NULL),
('kugowabjng', 'KANIT RESKRIM POLSEK BAJENG', 'IPTU', '-', '-', '2016-08-30 03:57:11', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', NULL, NULL, NULL),
('sagowabjng', 'ADMIN RESKRIM POLSEK BAJENG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:57:11', 0, 'POLRES GOWA', NULL, 'POLSEK BAJENG', NULL, NULL, NULL),
('kumksrujtn', 'KANIT RESKRIM POLSEK UJUNG TANAH', 'IPTU', '-', '-', '2016-08-30 03:56:15', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG TANAH', NULL, NULL, NULL),
('samksrujtn', 'ADMIN RESKRIM POLSEK UJUNG TANAH', 'SUPER ADMIN', '-', '-', '2016-08-30 03:56:15', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG TANAH', NULL, NULL, NULL),
('asmksrwajo', 'ADMIN SPKT POLSEK WAJO', 'ADMIN', '-', NULL, '2016-08-30 03:56:22', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK WAJO', NULL, NULL, NULL),
('ksgowaprgi', 'KEPALA SPKT POLSEK PARIGI', 'IPDA', '-', '-', '2016-08-30 03:58:11', 0, 'POLRES GOWA', NULL, 'POLSEK PARIGI', NULL, NULL, NULL),
('kpmksrwajo', 'KAPOLSEK WAJO', 'KOMPOL', '-', '-', '2016-08-30 03:56:22', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK WAJO', NULL, NULL, NULL),
('ksmksrwajo', 'KEPALA SPKT POLSEK WAJO', 'IPDA', '-', '-', '2016-08-30 03:56:22', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK WAJO', NULL, NULL, NULL),
('kumksrwajo', 'KANIT RESKRIM POLSEK WAJO', 'IPTU', '-', '-', '2016-08-30 03:56:22', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK WAJO', NULL, NULL, NULL),
('samksrwajo', 'ADMIN RESKRIM POLSEK WAJO', 'SUPER ADMIN', '-', '-', '2016-08-30 03:56:22', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK WAJO', NULL, NULL, NULL),
('samksrujpd', 'ADMIN RESKRIM POLSEK UJUNG PANDANG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:56:06', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', NULL, NULL, NULL),
('asmksrujtn', 'ADMIN SPKT POLSEK UJUNG TANAH', 'ADMIN', '-', NULL, '2016-08-30 03:56:15', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG TANAH', NULL, NULL, NULL),
('kpmksrujtn', 'KAPOLSEK UJUNG TANAH', 'KOMPOL', '-', '-', '2016-08-30 03:56:15', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG TANAH', NULL, NULL, NULL),
('ksmksrujtn', 'KEPALA SPKT POLSEK UJUNG TANAH', 'IPDA', '-', '-', '2016-08-30 03:56:15', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG TANAH', NULL, NULL, NULL),
('kpmksrujpd', 'KAPOLSEK UJUNG PANDANG', 'KOMPOL', '-', '-', '2016-08-30 03:56:06', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', NULL, NULL, NULL),
('ksmksrujpd', 'KEPALA SPKT POLSEK UJUNG PANDANG', 'IPDA', '-', '-', '2016-08-30 03:56:06', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', NULL, NULL, NULL),
('kumksrujpd', 'KANIT RESKRIM POLSEK UJUNG PANDANG', 'IPTU', '-', '-', '2016-08-30 03:56:06', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', NULL, NULL, NULL),
('kpgowaprgi', 'KAPOLSEK PARIGI', 'KOMPOL', '-', '-', '2016-08-30 03:58:11', 0, 'POLRES GOWA', NULL, 'POLSEK PARIGI', NULL, NULL, NULL),
('kpmksrbrkn', 'KAPOLSEK BIRINGKANAYA', 'KOMPOL', '-', '-', '2016-08-30 03:55:07', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BIRINGKANAYA', NULL, NULL, NULL),
('asmksrbrkn', 'ADMIN SPKT POLSEK BIRINGKANAYA', 'ADMIN', '-', NULL, '2016-08-30 03:55:07', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK BIRINGKANAYA', NULL, NULL, NULL),
('samksrmksr', 'ADMIN RESKRIM POLSEK MAKASSAR', 'SUPER ADMIN', '-', '-', '2016-08-30 03:53:51', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAKASSAR', NULL, NULL, NULL),
('asmksrmksr', 'ADMIN SPKT POLSEK MAKASSAR', 'ADMIN', '-', NULL, '2016-08-30 03:53:51', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAKASSAR', NULL, NULL, NULL),
('kpmksrmksr', 'KAPOLSEK MAKASSAR', 'KOMPOL', '-', '-', '2016-08-30 03:53:51', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAKASSAR', NULL, NULL, NULL),
('ksmksrmksr', 'KEPALA SPKT POLSEK MAKASSAR', 'IPDA', '-', '-', '2016-08-30 03:53:51', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAKASSAR', NULL, NULL, NULL),
('kumksrmksr', 'KANIT RESKRIM POLSEK MAKASSAR', 'IPTU', '-', '-', '2016-08-30 03:53:51', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAKASSAR', NULL, NULL, NULL),
('ksmksrtmlt', 'KEPALA SPKT POLSEK TAMALATE', 'IPDA', '-', '-', '2016-08-30 03:55:59', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALATE', NULL, NULL, NULL),
('kumksrtmlt', 'KANIT RESKRIM POLSEK TAMALATE', 'IPTU', '-', '-', '2016-08-30 03:55:59', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALATE', NULL, NULL, NULL),
('samksrtmlt', 'ADMIN RESKRIM POLSEK TAMALATE', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:59', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALATE', NULL, NULL, NULL),
('asmksrujpd', 'ADMIN SPKT POLSEK UJUNG PANDANG', 'ADMIN', '-', NULL, '2016-08-30 03:56:06', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK UJUNG PANDANG', NULL, NULL, NULL),
('kumksrtmlr', 'KANIT RESKRIM POLSEK TAMALANREA', 'IPTU', '-', '-', '2016-08-30 03:55:52', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALANREA', NULL, NULL, NULL),
('samksrtmlr', 'ADMIN RESKRIM POLSEK TAMALANREA', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:52', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALANREA', NULL, NULL, NULL),
('asmksrtmlt', 'ADMIN SPKT POLSEK TAMALATE', 'ADMIN', '-', NULL, '2016-08-30 03:55:59', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALATE', NULL, NULL, NULL),
('kpmksrtmlt', 'KAPOLSEK TAMALATE', 'KOMPOL', '-', '-', '2016-08-30 03:55:59', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALATE', NULL, NULL, NULL),
('samksrtllo', 'ADMIN RESKRIM POLSEK TALLO', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:47', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TALLO', NULL, NULL, NULL),
('asmksrtmlr', 'ADMIN SPKT POLSEK TAMALANREA', 'ADMIN', '-', NULL, '2016-08-30 03:55:52', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALANREA', NULL, NULL, NULL),
('kpmksrtmlr', 'KAPOLSEK TAMALANREA', 'KOMPOL', '-', '-', '2016-08-30 03:55:52', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALANREA', NULL, NULL, NULL),
('ksmksrtmlr', 'KEPALA SPKT POLSEK TAMALANREA', 'IPDA', '-', '-', '2016-08-30 03:55:52', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TAMALANREA', NULL, NULL, NULL),
('asmksrtllo', 'ADMIN SPKT POLSEK TALLO', 'ADMIN', '-', NULL, '2016-08-30 03:55:47', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TALLO', NULL, NULL, NULL),
('kpmksrtllo', 'KAPOLSEK TALLO', 'KOMPOL', '-', '-', '2016-08-30 03:55:47', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TALLO', NULL, NULL, NULL),
('ksmksrtllo', 'KEPALA SPKT POLSEK TALLO', 'IPDA', '-', '-', '2016-08-30 03:55:47', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TALLO', NULL, NULL, NULL),
('kumksrtllo', 'KANIT RESKRIM POLSEK TALLO', 'IPTU', '-', '-', '2016-08-30 03:55:47', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK TALLO', NULL, NULL, NULL),
('ksmksrpnkk', 'KEPALA SPKT POLSEK PANAKUKKANG', 'IPDA', '-', '-', '2016-08-30 03:55:35', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK PANAKUKKANG', NULL, NULL, NULL),
('kumksrpnkk', 'KANIT RESKRIM POLSEK PANAKUKKANG', 'IPTU', '-', '-', '2016-08-30 03:55:35', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK PANAKUKKANG', NULL, NULL, NULL),
('samksrpnkk', 'ADMIN RESKRIM POLSEK PANAKUKKANG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:35', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK PANAKUKKANG', NULL, NULL, NULL),
('asmksrrpcn', 'ADMIN SPKT POLSEK RAPPOCINI', 'ADMIN', '-', NULL, '2016-08-30 03:55:41', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK RAPPOCINI', NULL, NULL, NULL),
('kumksrmrso', 'KANIT RESKRIM POLSEK MARISO', 'IPTU', '-', '-', '2016-08-30 03:55:30', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MARISO', NULL, NULL, NULL),
('samksrmrso', 'ADMIN RESKRIM POLSEK MARISO', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:30', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MARISO', NULL, NULL, NULL),
('asmksrpnkk', 'ADMIN SPKT POLSEK PANAKUKKANG', 'ADMIN', '-', NULL, '2016-08-30 03:55:35', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK PANAKUKKANG', NULL, NULL, NULL),
('kpmksrpnkk', 'KAPOLSEK PANAKUKKANG', 'KOMPOL', '-', '-', '2016-08-30 03:55:35', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK PANAKUKKANG', NULL, NULL, NULL),
('samksrmngl', 'ADMIN RESKRIM POLSEK MANGGALA', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:24', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MANGGALA', NULL, NULL, NULL),
('asmksrmrso', 'ADMIN SPKT POLSEK MARISO', 'ADMIN', '-', NULL, '2016-08-30 03:55:30', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MARISO', NULL, NULL, NULL),
('kpmksrmrso', 'KAPOLSEK MARISO', 'KOMPOL', '-', '-', '2016-08-30 03:55:30', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MARISO', NULL, NULL, NULL),
('ksmksrmrso', 'KEPALA SPKT POLSEK MARISO', 'IPDA', '-', '-', '2016-08-30 03:55:30', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MARISO', NULL, NULL, NULL),
('kumksrmngl', 'KANIT RESKRIM POLSEK MANGGALA', 'IPTU', '-', '-', '2016-08-30 03:55:24', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MANGGALA', NULL, NULL, NULL),
('ksmksrmngl', 'KEPALA SPKT POLSEK MANGGALA', 'IPDA', '-', '-', '2016-08-30 03:55:24', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MANGGALA', NULL, NULL, NULL),
('kpmksrmngl', 'KAPOLSEK MANGGALA', 'KOMPOL', '-', '-', '2016-08-30 03:55:24', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MANGGALA', NULL, NULL, NULL),
('asmksrmngl', 'ADMIN SPKT POLSEK MANGGALA', 'ADMIN', '-', NULL, '2016-08-30 03:55:24', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MANGGALA', NULL, NULL, NULL),
('samksrmmjg', 'ADMIN RESKRIM POLSEK MAMAJANG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:55:19', 0, 'POLRESTABES MAKASSAR', NULL, 'POLSEK MAMAJANG', NULL, NULL, NULL),
('kpgowaptls', 'KAPOLSEK PATTALLASSANG', 'KOMPOL', '-', '-', '2016-08-30 03:58:20', 0, 'POLRES GOWA', NULL, 'POLSEK PATTALLASSANG', NULL, NULL, NULL),
('ksgowaptls', 'KEPALA SPKT POLSEK PATTALLASSANG', 'IPDA', '-', '-', '2016-08-30 03:58:20', 0, 'POLRES GOWA', NULL, 'POLSEK PATTALLASSANG', NULL, NULL, NULL),
('kugowaptls', 'KANIT RESKRIM POLSEK PATTALLASSANG', 'IPTU', '-', '-', '2016-08-30 03:58:20', 0, 'POLRES GOWA', NULL, 'POLSEK PATTALLASSANG', NULL, NULL, NULL),
('sagowaptls', 'ADMIN RESKRIM POLSEK PATTALLASSANG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:20', 0, 'POLRES GOWA', NULL, 'POLSEK PATTALLASSANG', NULL, NULL, NULL),
('asgowasbop', 'ADMIN SPKT POLSEK SOMBA OPU', 'ADMIN', '-', NULL, '2016-08-30 03:58:28', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', NULL, NULL, NULL),
('kpgowasbop', 'KAPOLSEK SOMBA OPU', 'KOMPOL', '-', '-', '2016-08-30 03:58:28', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', NULL, NULL, NULL),
('ksgowasbop', 'KEPALA SPKT POLSEK SOMBA OPU', 'IPDA', '-', '-', '2016-08-30 03:58:28', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', NULL, NULL, NULL),
('kugowasbop', 'KANIT RESKRIM POLSEK SOMBA OPU', 'IPTU', '-', '-', '2016-08-30 03:58:28', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', NULL, NULL, NULL),
('sagowasbop', 'ADMIN RESKRIM POLSEK SOMBA OPU', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:28', 0, 'POLRES GOWA', NULL, 'POLSEK SOMBA OPU', NULL, NULL, NULL),
('asgowatgmc', 'ADMIN SPKT POLSEK TINGGIMONCONG', 'ADMIN', '-', NULL, '2016-08-30 03:58:36', 0, 'POLRES GOWA', NULL, 'POLSEK TINGGIMONCONG', NULL, NULL, NULL),
('kpgowatgmc', 'KAPOLSEK TINGGIMONCONG', 'KOMPOL', '-', '-', '2016-08-30 03:58:36', 0, 'POLRES GOWA', NULL, 'POLSEK TINGGIMONCONG', NULL, NULL, NULL),
('ksgowatgmc', 'KEPALA SPKT POLSEK TINGGIMONCONG', 'IPDA', '-', '-', '2016-08-30 03:58:36', 0, 'POLRES GOWA', NULL, 'POLSEK TINGGIMONCONG', NULL, NULL, NULL),
('kugowatgmc', 'KANIT RESKRIM POLSEK TINGGIMONCONG', 'IPTU', '-', '-', '2016-08-30 03:58:36', 0, 'POLRES GOWA', NULL, 'POLSEK TINGGIMONCONG', NULL, NULL, NULL),
('sagowatgmc', 'ADMIN RESKRIM POLSEK TINGGIMONCONG', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:36', 0, 'POLRES GOWA', NULL, 'POLSEK TINGGIMONCONG', NULL, NULL, NULL),
('asgowatblp', 'ADMIN SPKT POLSEK TOMBOLO PAO', 'ADMIN', '-', NULL, '2016-08-30 03:58:43', 0, 'POLRES GOWA', NULL, 'POLSEK TOMBOLO PAO', NULL, NULL, NULL),
('kpgowatblp', 'KAPOLSEK TOMBOLO PAO', 'KOMPOL', '-', '-', '2016-08-30 03:58:43', 0, 'POLRES GOWA', NULL, 'POLSEK TOMBOLO PAO', NULL, NULL, NULL),
('ksgowatblp', 'KEPALA SPKT POLSEK TOMBOLO PAO', 'IPDA', '-', '-', '2016-08-30 03:58:43', 0, 'POLRES GOWA', NULL, 'POLSEK TOMBOLO PAO', NULL, NULL, NULL),
('kugowatblp', 'KANIT RESKRIM POLSEK TOMBOLO PAO', 'IPTU', '-', '-', '2016-08-30 03:58:43', 0, 'POLRES GOWA', NULL, 'POLSEK TOMBOLO PAO', NULL, NULL, NULL),
('sagowatblp', 'ADMIN RESKRIM POLSEK TOMBOLO PAO', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:43', 0, 'POLRES GOWA', NULL, 'POLSEK TOMBOLO PAO', NULL, NULL, NULL),
('asgowatpbl', 'ADMIN SPKT POLSEK TOMPOBULU', 'ADMIN', '-', NULL, '2016-08-30 03:58:50', 0, 'POLRES GOWA', NULL, 'POLSEK TOMPOBULU', NULL, NULL, NULL),
('kpgowatpbl', 'KAPOLSEK TOMPOBULU', 'KOMPOL', '-', '-', '2016-08-30 03:58:50', 0, 'POLRES GOWA', NULL, 'POLSEK TOMPOBULU', NULL, NULL, NULL),
('ksgowatpbl', 'KEPALA SPKT POLSEK TOMPOBULU', 'IPDA', '-', '-', '2016-08-30 03:58:50', 0, 'POLRES GOWA', NULL, 'POLSEK TOMPOBULU', NULL, NULL, NULL),
('kugowatpbl', 'KANIT RESKRIM POLSEK TOMPOBULU', 'IPTU', '-', '-', '2016-08-30 03:58:50', 0, 'POLRES GOWA', NULL, 'POLSEK TOMPOBULU', NULL, NULL, NULL),
('sagowatpbl', 'ADMIN RESKRIM POLSEK TOMPOBULU', 'SUPER ADMIN', '-', '-', '2016-08-30 03:58:50', 0, 'POLRES GOWA', NULL, 'POLSEK TOMPOBULU', NULL, NULL, NULL),
('55555555', 'Raden Purnomo', 'AIPTU', '', '', '2016-09-05 04:36:08', 0, 'POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', '', 'SUBDIT II', 'UNIT III', 'SUBNIT I'),
('666666', 'Raden Subeki', 'AIPTU', '', '', '2016-09-05 04:36:41', 0, 'POLRESTABES MAKASSAR', '', 'POLSEK UJUNG PANDANG', 'SUBDIT I', 'UNIT I', 'SUBNIT I'),
('77777777', 'Raden Kartolo', 'BRIGADIR', '', '', '2016-09-05 04:37:59', 0, 'POLRES GOWA', 'SATRESKRIM', '', 'SUBDIT I', 'UNIT V', 'SUBNIT I'),
('88888888', 'Raden Lauto', 'AIPTU', '', '', '2016-09-05 04:39:58', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', '', 'SUBDIT I', 'UNIT IV', 'SUBNIT III'),
('99999999', 'Raden Ginanjar', 'AIPTU', '', '', '2016-09-05 04:40:51', 1, 'POLRES GOWA', 'SATRESNARKOBA', '', 'SUBDIT I', 'UNIT III', 'SUBNIT I'),
('kU60mksr01', 'kanit VII RESKRIM MAKASSAR', 'akp', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', '-'),
('kU70mksr01', 'kanit VII RESKRIM MAKASSAR', 'akp', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', '-'),
('ks16mksr01', 'kasubnit I UNIT VI RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'Subnit I'),
('ks26mksr01', 'kasubnit II UNIT VI RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'Subnit II'),
('ks36mksr01', 'kasubnit III UNIT VI RESKRIM MAKASSAR', 'iptu', '-', '-', '2013-09-24 08:15:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VI', 'SUBNIT III'),
('ks17mksr01', 'kasubnit I UNIT VII RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'Subnit I'),
('ks27mksr01', 'kasubnit II UNIT VII RESKRIM MAKASSAR', 'iptu', '-', '-', '2012-12-03 05:30:42', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'Subnit II'),
('ks37mksr01', 'kasubnit III UNIT VII RESKRIM MAKASSAR', 'iptu', '-', '-', '2013-09-24 08:15:30', 0, 'POLRESTABES MAKASSAR', 'SATRESKRIM', NULL, 'SUBDIT I', 'UNIT VII', 'SUBNIT III');

-- --------------------------------------------------------

--
-- Table structure for table `tb_perubahan_bobot`
--

CREATE TABLE `tb_perubahan_bobot` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_BOBOT_LAMA` varchar(5) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `TGL_PERUBAHAN_BOBOT` datetime NOT NULL,
  `PATH` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_perubahan_bobot_point_lama`
--

CREATE TABLE `tb_perubahan_bobot_point_lama` (
  `ID_KASUS` varchar(10) NOT NULL,
  `NRP` varchar(12) NOT NULL,
  `ID_BOBOT_LAMA` varchar(5) NOT NULL,
  `ID_DOC_BERKAS` varchar(5) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_perubahan_bobot_point_lama_opsnal`
--

CREATE TABLE `tb_perubahan_bobot_point_lama_opsnal` (
  `ID_KASUS` varchar(10) NOT NULL,
  `NRP` varchar(12) NOT NULL,
  `ID_BOBOT_LAMA` varchar(5) NOT NULL,
  `ID_DOC_OPSNAL` varchar(5) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_polsek`
--

CREATE TABLE `tb_polsek` (
  `NAMA_POLSEK` varchar(25) NOT NULL,
  `NAMA_SATWIL` varchar(25) NOT NULL,
  `NAMA_KAPOLSEK` varchar(50) NOT NULL,
  `PANGKAT_KAPOLSEK` varchar(25) NOT NULL,
  `TELP_POLSEK` varchar(12) DEFAULT NULL,
  `ID_POLSEK` varchar(2) DEFAULT NULL,
  `KATA_KUNCI` varchar(4) DEFAULT NULL,
  `JENIS_KEL_OPSNAL` int(1) DEFAULT '1' COMMENT '1=berbentuk tim opsnal, 2=masuk kedalam satu SUBDIT/UNIT',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_polsek`
--

INSERT INTO `tb_polsek` (`NAMA_POLSEK`, `NAMA_SATWIL`, `NAMA_KAPOLSEK`, `PANGKAT_KAPOLSEK`, `TELP_POLSEK`, `ID_POLSEK`, `KATA_KUNCI`, `JENIS_KEL_OPSNAL`, `tgl_masuk_data`) VALUES
('POLSEK BAJENG', 'POLRES GOWA', '-', 'KOMPOL', '-', '01', 'bjng', 1, '2016-08-25 12:46:20'),
('POLSEK BAROMBONG', 'POLRES GOWA', '-', 'KOMPOL', '-', '02', 'brbg', 1, '2016-08-25 12:46:20'),
('POLSEK BIRINGBULU', 'POLRES GOWA', '-', 'KOMPOL', '-', '03', 'brbl', 1, '2016-08-25 12:46:20'),
('POLSEK BONTOMARANNU', 'POLRES GOWA', '-', 'KOMPOL', '-', '04', 'btmr', 1, '2016-08-25 12:46:20'),
('POLSEK BONTONOMPO', 'POLRES GOWA', '-', 'KOMPOL', '-', '05', 'btnp', 1, '2016-08-25 12:46:20'),
('POLSEK BONTONOMPO SELATAN', 'POLRES GOWA', '-', 'KOMPOL', '-', '06', 'btsl', 1, '2016-08-25 12:46:20'),
('POLSEK BUNGAYA', 'POLRES GOWA', '-', 'KOMPOL', '-', '07', 'bngy', 1, '2016-08-25 12:46:20'),
('POLSEK MANUJU', 'POLRES GOWA', '-', 'KOMPOL', '-', '08', 'mnju', 1, '2016-08-25 12:46:20'),
('POLSEK PALLANGGA', 'POLRES GOWA', '-', 'KOMPOL', '-', '09', 'plng', 1, '2016-08-25 12:46:20'),
('POLSEK PARANGLOE', 'POLRES GOWA', '-', 'KOMPOL', '-', '10', 'prlo', 1, '2016-08-25 12:46:20'),
('POLSEK PARIGI', 'POLRES GOWA', '-', 'KOMPOL', '-', '11', 'prgi', 1, '2016-08-25 12:46:20'),
('POLSEK PATTALLASSANG', 'POLRES GOWA', '-', 'KOMPOL', '-', '12', 'ptls', 1, '2016-08-25 12:46:20'),
('POLSEK SOMBA OPU', 'POLRES GOWA', '-', 'KOMPOL', '-', '13', 'sbop', 1, '2016-08-25 12:46:20'),
('POLSEK TINGGIMONCONG', 'POLRES GOWA', '-', 'KOMPOL', '-', '14', 'tgmc', 1, '2016-08-25 12:46:20'),
('POLSEK TOMBOLO PAO', 'POLRES GOWA', '-', 'KOMPOL', '-', '15', 'tblp', 1, '2016-08-25 12:46:20'),
('POLSEK TOMPOBULU', 'POLRES GOWA', '-', 'KOMPOL', '-', '16', 'tpbl', 1, '2016-08-25 12:46:20'),
('POLSEK BIRINGKANAYA', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '01', 'brkn', 1, '2016-08-25 12:46:20'),
('POLSEK BONTOALA', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '02', 'btla', 1, '2016-08-25 12:46:20'),
('POLSEK MAKASSAR', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '03', 'mksr', 1, '2016-08-25 12:46:20'),
('POLSEK MAMAJANG', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '04', 'mmjg', 1, '2016-08-25 12:46:20'),
('POLSEK MANGGALA', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '05', 'mngl', 1, '2016-08-25 12:46:20'),
('POLSEK MARISO', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '06', 'mrso', 1, '2016-08-25 12:46:20'),
('POLSEK PANAKUKKANG', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '07', 'pnkk', 1, '2016-08-25 12:46:20'),
('POLSEK RAPPOCINI', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '08', 'rpcn', 1, '2016-08-25 12:46:20'),
('POLSEK TALLO', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '09', 'tllo', 1, '2016-08-25 12:46:20'),
('POLSEK TAMALANREA', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '10', 'tmlr', 1, '2016-08-25 12:46:20'),
('POLSEK TAMALATE', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '11', 'tmlt', 1, '2016-08-25 12:46:20'),
('POLSEK UJUNG PANDANG', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '12', 'ujpd', 1, '2016-08-25 12:46:20'),
('POLSEK UJUNG TANAH', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '13', 'ujtn', 1, '2016-08-25 12:46:20'),
('POLSEK WAJO', 'POLRESTABES MAKASSAR', '-', 'KOMPOL', '-', '14', 'wajo', 1, '2016-08-25 12:46:20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_realisasi_doc_pam`
--

CREATE TABLE `tb_realisasi_doc_pam` (
  `ID_PAM` varchar(10) NOT NULL,
  `ID_DOC_PAM` varchar(5) NOT NULL,
  `NRP` varchar(12) NOT NULL,
  `POINT` int(3) NOT NULL DEFAULT '0',
  `TGL_UPLOAD` datetime NOT NULL,
  `PATH` varchar(255) NOT NULL,
  `BOBOT_POINT` int(3) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_rekomendasi_lp`
--

CREATE TABLE `tb_rekomendasi_lp` (
  `ID_PELAPOR` varchar(10) NOT NULL,
  `KOTA_REKOMENDASI` varchar(25) NOT NULL,
  `TGL_REKOMENDASI` date NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `NAMA_DELIK` int(1) DEFAULT NULL,
  `IS_WILAYAH_HUKUM` int(1) DEFAULT NULL,
  `KET_WILAYAH_HUKUM` varchar(1000) DEFAULT NULL,
  `IS_PIDANA` int(1) DEFAULT NULL,
  `KET_PIDANA` varchar(1000) DEFAULT NULL,
  `IS_PIDANA_TERPENUHI` int(1) DEFAULT NULL,
  `KET_PIDANA_TERPENUHI` varchar(1000) DEFAULT NULL,
  `IS_ALAT_BUKTI` int(1) DEFAULT NULL,
  `KET_ALAT_BUKTI` varchar(1000) DEFAULT NULL,
  `IS_ADA_SAKSI` int(1) DEFAULT NULL,
  `KET_ADA_SAKSI` varchar(1000) DEFAULT NULL,
  `IS_ADA_BB` int(1) DEFAULT NULL,
  `KET_ADA_BB` varchar(1000) DEFAULT NULL,
  `JENIS_LANJUT` int(1) DEFAULT NULL,
  `IS_LANJUT_LIDIK` int(1) DEFAULT NULL,
  `IS_LANJUT_SIDIK` int(1) DEFAULT NULL,
  `KET_JENIS_LANJUT` varchar(1000) DEFAULT NULL,
  `NRP` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_riwayat_jabatan_penyidik`
--

CREATE TABLE `tb_riwayat_jabatan_penyidik` (
  `NRP` varchar(10) NOT NULL,
  `TGL_AWAL_RIWAYAT` date NOT NULL,
  `TGL_AKHIR_RIWAYAT` date NOT NULL,
  `NAMA_JABATAN` varchar(50) NOT NULL,
  `SATUAN_KERJA` varchar(50) NOT NULL,
  `SATUAN_WILAYAH` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_riwayat_jabatan_penyidik`
--

INSERT INTO `tb_riwayat_jabatan_penyidik` (`NRP`, `TGL_AWAL_RIWAYAT`, `TGL_AKHIR_RIWAYAT`, `NAMA_JABATAN`, `SATUAN_KERJA`, `SATUAN_WILAYAH`) VALUES
('12312312', '2014-05-01', '2014-04-01', 'ANGGOTA', 'INTELKAM', 'POLRES MADIUN KAB');

-- --------------------------------------------------------

--
-- Table structure for table `tb_satker`
--

CREATE TABLE `tb_satker` (
  `NAMA_SATKER` varchar(25) NOT NULL,
  `NAMA_SATWIL` varchar(25) NOT NULL,
  `NAMA_POLSEK` varchar(25) NOT NULL DEFAULT '',
  `NAMA_KASATKER` varchar(50) DEFAULT NULL,
  `PANGKAT_KASATKER` varchar(25) DEFAULT NULL,
  `TELP_SATKER` varchar(12) DEFAULT NULL,
  `ID_SATKER` varchar(2) NOT NULL,
  `JENIS_KEL_OPSNAL` int(1) DEFAULT '1' COMMENT '1=berbentuk tim opsnal, 2=masuk kedalam satu SUBDIT/UNIT',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_satker`
--

INSERT INTO `tb_satker` (`NAMA_SATKER`, `NAMA_SATWIL`, `NAMA_POLSEK`, `NAMA_KASATKER`, `PANGKAT_KASATKER`, `TELP_SATKER`, `ID_SATKER`, `JENIS_KEL_OPSNAL`, `tgl_masuk_data`) VALUES
('DITRESKRIMSUS', 'POLDA SULAWESI SELATAN', '', '-', 'KOMBESPOL', NULL, '02', 1, '2016-07-16 13:46:29'),
('DITRESKRIMUM', 'POLDA SULAWESI SELATAN', '', '-', 'KOMBESPOL', NULL, '01', 1, '2016-07-16 13:46:29'),
('DITRESNARKOBA', 'POLDA SULAWESI SELATAN', '', '-', 'KOMBESPOL', NULL, '03', 1, '2016-07-16 13:46:29'),
('DITLANTAS', 'POLDA SULAWESI SELATAN', '', '-', 'KOMBESPOL', NULL, '04', 1, '2016-07-16 13:46:29'),
('SATRESKRIM', 'POLRESTABES MAKASSAR', '', '-', 'AKBP', NULL, '01', 1, '2016-07-16 13:46:29'),
('SATRESNARKOBA', 'POLRESTABES MAKASSAR', '', '-', 'AKBP', NULL, '03', 1, '2016-07-16 13:46:29'),
('SATLANTAS', 'POLRESTABES MAKASSAR', '', '-', 'AKBP', NULL, '04', 1, '2016-07-16 13:46:29'),
('SATRESKRIM', 'POLRES GOWA', '', '-', 'AKP', NULL, '01', 1, '2016-07-16 13:46:29'),
('SATRESNARKOBA', 'POLRES GOWA', '', '-', 'AKP', NULL, '03', 1, '2016-07-16 13:46:29'),
('SATLANTAS', 'POLRES GOWA', '', '-', 'AKP', NULL, '04', 1, '2016-07-16 13:46:29'),
('SATRESKRIM', 'POLRES KP3', '', NULL, NULL, NULL, '01', 1, '2016-08-29 13:13:08'),
('SATLANTAS', 'POLRES MAROS', '', NULL, NULL, NULL, '04', 1, '2016-08-29 13:12:52'),
('SATRESKRIM', 'POLRES MAROS', '', NULL, NULL, NULL, '01', 1, '2016-08-29 13:12:52'),
('SATRESNARKOBA', 'POLRES MAROS', '', NULL, NULL, NULL, '03', 1, '2016-08-29 13:12:52'),
('SATRESNARKOBA', 'POLRES KP3', '', NULL, NULL, NULL, '03', 1, '2016-08-29 13:13:08'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK BIRINGKANAYA', '-', 'AKBP', NULL, '01', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK BONTOALA', '-', 'AKBP', NULL, '02', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK MAKASSAR', '-', 'AKBP', NULL, '03', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK MAMAJANG', '-', 'AKBP', NULL, '04', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK MANGGALA', '-', 'AKBP', NULL, '05', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK MARISO', '-', 'AKBP', NULL, '06', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK PANAKUKKANG', '-', 'AKBP', NULL, '07', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK RAPPOCINI', '-', 'AKBP', NULL, '08', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK TALLO', '-', 'AKBP', NULL, '09', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK TAMALANREA', '-', 'AKBP', NULL, '10', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK TAMALATE', '-', 'AKBP', NULL, '11', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK UJUNG PANDANG', '-', 'AKBP', NULL, '12', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK UJUNG TANAH', '-', 'AKBP', NULL, '13', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRESTABES MAKASSAR', 'POLSEK WAJO', '-', 'AKBP', NULL, '14', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BAJENG', '-', 'AKBP', NULL, '01', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BAROMBONG', '-', 'AKBP', NULL, '02', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BIRINGBULU', '-', 'AKBP', NULL, '03', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BONTOMARANNU', '-', 'AKBP', NULL, '04', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BONTONOMPO', '-', 'AKBP', NULL, '05', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BONTONOMPO SELATAN', '-', 'AKBP', NULL, '06', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK BUNGAYA', '-', 'AKBP', NULL, '07', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK MANUJU', '-', 'AKBP', NULL, '08', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK PALLANGGA', '-', 'AKBP', NULL, '09', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK PARANGLOE', '-', 'AKBP', NULL, '10', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK PARIGI', '-', 'AKBP', NULL, '11', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK PATTALLASSANG', '-', 'AKBP', NULL, '12', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK SOMBA OPU', '-', 'AKBP', NULL, '13', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK TINGGIMONCONG', '-', 'AKBP', NULL, '14', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK TOMBOLO PAO', '-', 'AKBP', NULL, '15', 1, '2016-07-16 13:46:29'),
('UNIT RESKRIM', 'POLRES GOWA', 'POLSEK TOMPOBULU', '-', 'AKBP', NULL, '16', 1, '2016-07-16 13:46:29'),
('SATLANTAS', 'POLRES KP3', '', NULL, NULL, NULL, '04', 1, '2016-08-29 13:13:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_satwil`
--

CREATE TABLE `tb_satwil` (
  `NAMA_SATWIL` varchar(25) NOT NULL,
  `NAMA_KASATWIL` varchar(50) NOT NULL,
  `PANGKAT_KASATWIL` varchar(25) NOT NULL,
  `TELP_SATWIL` varchar(12) DEFAULT NULL,
  `ID_SATWIL` varchar(2) DEFAULT NULL,
  `KATA_KUNCI` varchar(4) DEFAULT NULL,
  `TIPE_SATWIL` int(1) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_satwil`
--

INSERT INTO `tb_satwil` (`NAMA_SATWIL`, `NAMA_KASATWIL`, `PANGKAT_KASATWIL`, `TELP_SATWIL`, `ID_SATWIL`, `KATA_KUNCI`, `TIPE_SATWIL`, `tgl_masuk_data`) VALUES
('POLDA SULAWESI SELATAN', '-', 'IRJENPOL', NULL, '01', 'slsl', 0, '2016-07-16 13:46:29'),
('POLRESTABES MAKASSAR', '-', 'KOMBESPOL', NULL, '02', 'mksr', 1, NULL),
('POLRES GOWA', '-', 'AKBP', NULL, '03', 'gowa', 2, '2016-08-25 12:30:34'),
('POLRES MAROS', '-', 'AKBP', '-', '04', 'mros', 2, '2016-08-29 13:12:52'),
('POLRES KP3', '-', 'AKBP', '-', '05', 'kptg', 2, '2016-08-29 13:13:08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sidik_jari`
--

CREATE TABLE `tb_sidik_jari` (
  `ID_PELAKU` varchar(10) NOT NULL DEFAULT '',
  `ID_JARI` varchar(10) NOT NULL DEFAULT '',
  `KAN_JEMPOL` blob,
  `KAN_TELUNJUK` blob,
  `KAN_TENGAH` blob,
  `KAN_MANIS` blob,
  `KAN_KELINGKING` blob,
  `KIR_JEMPOL` blob,
  `KIR_TELUNJUK` blob,
  `KIR_TENGAH` blob,
  `KIR_MANIS` blob,
  `KIR_KELINGKING` blob,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_spesialisasi`
--

CREATE TABLE `tb_spesialisasi` (
  `ID_SPESIALISASI` varchar(5) NOT NULL,
  `SPESIALISASI` varchar(255) NOT NULL,
  `KETERANGAN` varchar(255) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_spesialisasi`
--

INSERT INTO `tb_spesialisasi` (`ID_SPESIALISASI`, `SPESIALISASI`, `KETERANGAN`, `tgl_masuk_data`) VALUES
('SP002', 'COPET', 'MENCOPET DI TEMPAT-TEMPAT UMUM DENGAN CARA BERGEROMBOL DAN PURA-PURA JATUH, HIPNOTIS, DAN LAIN-LAIN', '2012-08-05 17:00:00'),
('SP003', 'CURANMOR', 'MENCURI SEPEDA MOTOR DI PERUMAHAN', '2012-08-05 17:00:00'),
('SP004', 'PEMBUNUHAN DISERTAI PERAMPOKAN', 'JIKA KORBAN PERAMPOKAN MELAWAN TIDAK SEGAN- SEGAN DIBUNUH', '2012-08-05 17:00:00'),
('SP005', 'Perjudian', 'Melakukan Tindak Pidana Perjudian jenis togel, bola dsb.', '2014-02-05 05:53:42'),
('SP008', 'Pencabulan', 'Mencabuli gadis dibawah umur maupun perempuan dewasa', '2014-02-05 05:56:23'),
('SP007', 'Pemerkosaan', 'Memerkosa gadis dibawah umur maupun perempuan dewasa', '2014-02-05 05:55:11'),
('SP009', 'Penggelapan', 'menggunakan uang atau benda milik orang lain tanpa sepengetahuan atau seijin pemilik uang atau benda untuk kepentingan pribadinya sendiri.', '2014-02-05 05:59:05');

-- --------------------------------------------------------

--
-- Table structure for table `tb_subdit`
--

CREATE TABLE `tb_subdit` (
  `NAMA_SATWIL` varchar(25) NOT NULL,
  `NAMA_SATKER` varchar(25) NOT NULL,
  `NAMA_SUBDIT` varchar(25) NOT NULL,
  `NAMA_KASUBDIT` varchar(50) DEFAULT NULL,
  `PANGKAT_KASUBDIT` varchar(25) DEFAULT NULL,
  `TELP_SUBDIT` varchar(12) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_ada_penyidik` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_subdit`
--

INSERT INTO `tb_subdit` (`NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_SUBDIT`, `NAMA_KASUBDIT`, `PANGKAT_KASUBDIT`, `TELP_SUBDIT`, `tgl_masuk_data`, `is_ada_penyidik`) VALUES
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT III', 'IDA PUTU WEDANA JATI,SH.,MH', 'IPDA', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT II', 'I MADE WITAYA, SH', 'IPDA', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT I', 'I GN. AGUNG ADE PANJI ANOM, SIK', 'IPDA', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'RENMIN', 'HASMAWATI, SH', 'AIPTU', '-', '2016-08-25 13:58:51', 0),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT IV', 'JANSEN A. PANJAITAN, SIK, MH', 'IPDA', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT_IDEN', 'DEKY RUDY RONNY WORANG', 'AIPTU', NULL, '2016-08-25 13:58:51', 0),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT III', 'AHMAD JUFRIANTO', 'AKBP', '1234567', '2016-08-30 13:02:58', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT II', 'RUDI WIDODO', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT I', 'HENDRIK WAJANARKO', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'RENMIN', '-', 'AKBP', '-', '2016-08-25 13:58:51', 0),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT IV', 'MOKO WIDIATMOKO', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT_IDEN', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 0),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT III', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT II', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT I', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'RENMIN', '-', 'AKBP', '-', '2016-08-25 13:58:51', 0),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT IV', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT_IDEN', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 0),
('POLDA SULAWESI SELATAN', 'DITLANTAS', 'SUBDIT I', '-', 'AKBP', NULL, '2016-08-25 13:58:51', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', '-', '-', NULL, '2015-08-02 16:56:20', 1),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', '-', '-', NULL, '2015-08-02 16:56:20', 1),
('POLRESTABES MAKASSAR', 'SATLANTAS', 'SUBDIT I', '-', '-', NULL, '2015-08-02 16:56:20', 1),
('POLRES GOWA', 'SATRESKRIM', 'SUBDIT I', '-', '-', NULL, '2015-08-02 16:56:20', 1),
('POLRES GOWA', 'SATRESNARKOBA', 'SUBDIT I', '-', '-', NULL, '2015-08-02 16:56:20', 1),
('POLRES GOWA', 'SATLANTAS', 'SUBDIT I', '-', '-', NULL, '2015-08-02 16:56:20', 1),
('POLRES MAROS', 'SATRESKRIM', 'SUBDIT I', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESNARKOBA', 'SUBDIT I', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATLANTAS', 'SUBDIT I', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES KP3', 'SATRESKRIM', 'SUBDIT I', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESNARKOBA', 'SUBDIT I', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATLANTAS', 'SUBDIT I', NULL, NULL, NULL, '2016-08-29 13:13:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_subdit_penyidik_histori`
--

CREATE TABLE `tb_subdit_penyidik_histori` (
  `TGL_PINDAH` date NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `NAMA_SUBDIT_LAMA` varchar(25) NOT NULL,
  `NAMA_UNIT_LAMA` varchar(25) NOT NULL,
  `NAMA_SUBDIT` varchar(25) NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `is_opsnal` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_subdit_penyidik_histori`
--

INSERT INTO `tb_subdit_penyidik_histori` (`TGL_PINDAH`, `NRP`, `NAMA_SUBDIT_LAMA`, `NAMA_UNIT_LAMA`, `NAMA_SUBDIT`, `NAMA_UNIT`, `is_opsnal`) VALUES
('2014-12-08', '12312312', 'PIDEK', 'UNIT I', 'PIDKOR', 'UNIT I', 0),
('2015-09-20', '76020345', 'SUBDIT II', 'UNIT I', 'SUBDIT III', 'UNIT I', 0),
('2016-06-23', '62080740', 'SUBDIT IV', 'UNIT III', 'SUBDIT II', 'UNIT II', 0),
('2016-06-23', '78110968', 'SUBDIT IV', 'UNIT I', 'SUBDIT II', 'UNIT II', 0),
('2016-06-23', '78110968', 'SUBDIT II', 'UNIT I', 'SUBDIT IV', 'UNIT II', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tb_subnit`
--

CREATE TABLE `tb_subnit` (
  `NAMA_SATWIL` varchar(25) NOT NULL,
  `NAMA_SATKER` varchar(25) NOT NULL,
  `NAMA_SUBDIT` varchar(25) NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `NAMA_SUBNIT` varchar(25) NOT NULL,
  `NAMA_KASUBNIT` varchar(50) DEFAULT NULL,
  `PANGKAT_KASUBNIT` varchar(25) DEFAULT NULL,
  `TELP_SUBNIT` varchar(12) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_subnit`
--

INSERT INTO `tb_subnit` (`NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_SUBDIT`, `NAMA_UNIT`, `NAMA_SUBNIT`, `NAMA_KASUBNIT`, `PANGKAT_KASUBNIT`, `TELP_SUBNIT`, `tgl_masuk_data`) VALUES
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATLANTAS', 'SUBDIT I', 'UNIT I', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATLANTAS', 'SUBDIT I', 'UNIT I', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATLANTAS', 'SUBDIT I', 'UNIT I', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VII', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VII', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VII', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VI', 'SUBNIT III', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VI', 'SUBNIT I', '-', '-', NULL, '2016-08-25 14:13:34'),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VI', 'SUBNIT II', '-', '-', NULL, '2016-08-25 14:13:34');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tahanan`
--

CREATE TABLE `tb_tahanan` (
  `ID_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `ID_PELAKU` varchar(10) NOT NULL DEFAULT '',
  `ID_KASUS` varchar(10) NOT NULL DEFAULT '',
  `TGL_TAHANAN` date NOT NULL,
  `NO_TAHANAN` varchar(3) NOT NULL DEFAULT '0',
  `NO_RUANG_TAHANAN` varchar(5) NOT NULL,
  `DURASI_TAHANAN` int(3) NOT NULL,
  `KETERANGAN` varchar(100) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tahanan_pemberhentian`
--

CREATE TABLE `tb_tahanan_pemberhentian` (
  `ID_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `ID_PEMBERHENTIAN_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `TGL_PEMBERHENTIAN_TAHANAN` date NOT NULL,
  `ALASAN_PEMBERHENTIAN` varchar(200) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tahanan_perpanjangan`
--

CREATE TABLE `tb_tahanan_perpanjangan` (
  `ID_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `ID_PERPANJANGAN_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `TGL_PERPANJANGAN` date NOT NULL,
  `DURASI_PERPANJANGAN_TAHANAN` int(3) NOT NULL,
  `DURASI_TOTAL_TAHANAN` int(3) NOT NULL,
  `ALASAN_PERPANJANGAN` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tahanan_perpindahan`
--

CREATE TABLE `tb_tahanan_perpindahan` (
  `ID_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `ID_PERPINDAHAN_TAHANAN` varchar(10) NOT NULL DEFAULT '',
  `TGL_PERPINDAHAN_TAHANAN` date NOT NULL,
  `TEMPAT_TAHANAN_BARU` varchar(200) NOT NULL,
  `ALASAN_PERPINDAHAN` varchar(1000) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_target_doc_sp2hp`
--

CREATE TABLE `tb_target_doc_sp2hp` (
  `ID_BOBOT` varchar(5) NOT NULL,
  `ID_DOC_SP2HP` varchar(5) NOT NULL DEFAULT '',
  `HARI_KE` int(3) NOT NULL DEFAULT '0',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_target_doc_sp2hp`
--

INSERT INTO `tb_target_doc_sp2hp` (`ID_BOBOT`, `ID_DOC_SP2HP`, `HARI_KE`, `tgl_masuk_data`) VALUES
('BOB01', 'SP01', 3, '2013-01-08 04:42:53'),
('BOB01', 'SP02', 7, '2013-01-08 04:42:53'),
('BOB01', 'SP03', 7, '2013-01-08 04:42:53'),
('BOB01', 'SP04', 15, '2013-01-08 04:42:53'),
('BOB01', 'SP05', 25, '2013-01-08 04:42:53'),
('BOB01', 'SP06', 27, '2013-01-08 04:42:53'),
('BOB01', 'SP07', 30, '2013-01-08 04:42:53'),
('BOB02', 'SP01', 3, '2013-01-08 04:42:53'),
('BOB02', 'SP02', 14, '2013-01-08 04:42:53'),
('BOB02', 'SP03', 14, '2013-01-08 04:42:53'),
('BOB02', 'SP04', 30, '2013-01-08 04:42:53'),
('BOB02', 'SP05', 50, '2013-01-08 04:42:53'),
('BOB02', 'SP06', 54, '2013-01-08 04:42:53'),
('BOB02', 'SP07', 60, '2013-01-08 04:42:53'),
('BOB03', 'SP01', 3, '2013-01-08 04:42:53'),
('BOB03', 'SP02', 21, '2013-01-08 04:42:53'),
('BOB03', 'SP03', 21, '2013-01-08 04:42:53'),
('BOB03', 'SP04', 45, '2013-01-08 04:42:53'),
('BOB03', 'SP05', 75, '2013-01-08 04:42:53'),
('BOB03', 'SP06', 81, '2013-01-08 04:42:53'),
('BOB03', 'SP07', 90, '2013-01-08 04:42:53');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tim_opsnal`
--

CREATE TABLE `tb_tim_opsnal` (
  `ID_TIM_OPSNAL` varchar(5) NOT NULL,
  `KA_TIM_OPSNAL` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tim_opsnal_anggota`
--

CREATE TABLE `tb_tim_opsnal_anggota` (
  `ID_TIM_OPSNAL` varchar(5) NOT NULL,
  `ANGGOTA_TIM_OPSNAL` varchar(10) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tingkat_kesulitan_grup`
--

CREATE TABLE `tb_tingkat_kesulitan_grup` (
  `ID_GROUP` varchar(5) NOT NULL,
  `NAMA_GROUP` varchar(50) NOT NULL,
  `KETERANGAN` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tingkat_kesulitan_grup`
--

INSERT INTO `tb_tingkat_kesulitan_grup` (`ID_GROUP`, `NAMA_GROUP`, `KETERANGAN`) VALUES
('G0001', 'SAKSI', NULL),
('G0002', 'SURAT', NULL),
('G0003', 'PETUNJUK', NULL),
('G0004', 'SAKSI AHLI', NULL),
('G0005', 'TERSANGKA', NULL),
('G0006', 'TEMPAT KEJADIAN PERKARA', NULL),
('G0007', 'BARANG BUKTI', NULL),
('G0008', 'ALAT KHUSUS/PENDUKUNG KEPOLISIAN', NULL),
('G0009', 'PERANAN  LEMBAGA LAIN', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tingkat_kesulitan_hasil`
--

CREATE TABLE `tb_tingkat_kesulitan_hasil` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_GROUP` varchar(5) NOT NULL,
  `ID_TOPIK` varchar(5) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `IS_PILIH` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tingkat_kesulitan_resume`
--

CREATE TABLE `tb_tingkat_kesulitan_resume` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `JUMLAH` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_tingkat_kesulitan_rincian`
--

CREATE TABLE `tb_tingkat_kesulitan_rincian` (
  `ID_GROUP` varchar(5) NOT NULL,
  `ID_TOPIK` varchar(5) NOT NULL,
  `ID_BOBOT` varchar(5) NOT NULL,
  `JAWABAN_TOPIK` varchar(300) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tingkat_kesulitan_rincian`
--

INSERT INTO `tb_tingkat_kesulitan_rincian` (`ID_GROUP`, `ID_TOPIK`, `ID_BOBOT`, `JAWABAN_TOPIK`) VALUES
('G0001', 'T0001', 'BOB01', 'ya'),
('G0001', 'T0001', 'BOB02', 'ya'),
('G0001', 'T0001', 'BOB03', 'tidak'),
('G0001', 'T0001', 'BOB04', 'tidak'),
('G0001', 'T0002', 'BOB01', 'satu kecamatan dengan dengan kantor penyidik'),
('G0001', 'T0002', 'BOB02', 'satu kabupaten dengan kantor penyidik'),
('G0001', 'T0002', 'BOB03', 'satu propinsi dengan kantor penyidik'),
('G0001', 'T0002', 'BOB04', 'Lokasi berada diluar negeri, alamat tidak jelas, daerah terpencil (faktor geografis)'),
('G0001', 'T0003', 'BOB01', 'ya'),
('G0001', 'T0003', 'BOB02', 'ya'),
('G0001', 'T0003', 'BOB03', 'tidak'),
('G0001', 'T0003', 'BOB04', 'tidak'),
('G0001', 'T0004', 'BOB01', 'tidak'),
('G0001', 'T0004', 'BOB02', 'tidak'),
('G0001', 'T0004', 'BOB03', 'ya'),
('G0001', 'T0004', 'BOB04', 'ya'),
('G0001', 'T0005', 'BOB01', 'tidak'),
('G0001', 'T0005', 'BOB02', 'tidak'),
('G0001', 'T0005', 'BOB03', 'ya'),
('G0001', 'T0005', 'BOB04', 'ya'),
('G0001', 'T0006', 'BOB01', 'tidak'),
('G0001', 'T0006', 'BOB02', 'tidak'),
('G0001', 'T0006', 'BOB03', 'ya'),
('G0001', 'T0006', 'BOB04', 'ya'),
('G0001', 'T0007', 'BOB01', 'tidak'),
('G0001', 'T0007', 'BOB02', 'tidak'),
('G0001', 'T0007', 'BOB03', 'tidak'),
('G0001', 'T0007', 'BOB04', 'ya'),
('G0001', 'T0008', 'BOB01', 'tidak'),
('G0001', 'T0008', 'BOB02', 'tidak'),
('G0001', 'T0008', 'BOB03', 'tidak'),
('G0001', 'T0008', 'BOB04', 'ya'),
('G0001', 'T0009', 'BOB01', 'lebih dari 2 (dua) orang'),
('G0001', 'T0009', 'BOB02', 'lebih dari 3 (tiga) orang'),
('G0001', 'T0009', 'BOB03', 'kurang dari 2 (dua) orang'),
('G0001', 'T0009', 'BOB04', 'kurang dari 2 (dua) orang'),
('G0002', 'T0001', 'BOB01', 'ada'),
('G0002', 'T0001', 'BOB02', 'ada'),
('G0002', 'T0001', 'BOB03', 'ada tapi sebagian'),
('G0002', 'T0001', 'BOB04', 'tidak ada'),
('G0002', 'T0002', 'BOB01', 'ya'),
('G0002', 'T0002', 'BOB02', 'ya'),
('G0002', 'T0002', 'BOB03', 'diperlukan ijin khusus'),
('G0002', 'T0002', 'BOB04', 'diperlukan ijin khusus'),
('G0002', 'T0003', 'BOB01', 'tidak diperlukan'),
('G0002', 'T0003', 'BOB02', 'diperlukan'),
('G0002', 'T0003', 'BOB03', 'sangat diperlukan'),
('G0002', 'T0003', 'BOB04', 'sangat diperlukan'),
('G0002', 'T0004', 'BOB01', 'tidak'),
('G0002', 'T0004', 'BOB02', 'tidak'),
('G0002', 'T0004', 'BOB03', 'tidak'),
('G0002', 'T0004', 'BOB04', 'ya'),
('G0003', 'T0001', 'BOB01', 'terdapat kesesuaian yang mengarah kepada tersangka'),
('G0003', 'T0001', 'BOB02', 'sebagian terdapat kesesuaian yang mengarah kepada tersangka'),
('G0003', 'T0001', 'BOB03', 'sebagian terdapat kesesuaian tapi belum mengarah kepada tersangka'),
('G0003', 'T0001', 'BOB04', 'tidak terdapat kesesuaian yang mengarah kepada tersangka'),
('G0004', 'T0001', 'BOB01', 'tidak diperlukan'),
('G0004', 'T0001', 'BOB02', 'tidak diperlukan'),
('G0004', 'T0001', 'BOB03', 'diperlukan'),
('G0004', 'T0001', 'BOB04', 'sangat diperlukan'),
('G0004', 'T0002', 'BOB01', 'ya'),
('G0004', 'T0002', 'BOB02', 'ya'),
('G0004', 'T0002', 'BOB03', 'tidak'),
('G0004', 'T0002', 'BOB04', 'tidak dan perlu didatangkan dari luar negri'),
('G0005', 'T0001', 'BOB01', 'ya'),
('G0005', 'T0001', 'BOB02', 'tidak'),
('G0005', 'T0001', 'BOB03', 'tidak'),
('G0005', 'T0001', 'BOB04', 'tidak'),
('G0005', 'T0002', 'BOB01', 'ya'),
('G0005', 'T0002', 'BOB02', 'tidak'),
('G0005', 'T0002', 'BOB03', 'tidak'),
('G0005', 'T0002', 'BOB04', 'tidak'),
('G0005', 'T0003', 'BOB01', 'ya'),
('G0005', 'T0003', 'BOB02', 'ya'),
('G0005', 'T0003', 'BOB03', 'tidak'),
('G0005', 'T0003', 'BOB04', 'tidak'),
('G0005', 'T0004', 'BOB01', 'tidak'),
('G0005', 'T0004', 'BOB02', 'tidak'),
('G0005', 'T0004', 'BOB03', 'ya'),
('G0005', 'T0004', 'BOB04', 'ya'),
('G0005', 'T0005', 'BOB01', 'tidak'),
('G0005', 'T0005', 'BOB02', 'tidak'),
('G0005', 'T0005', 'BOB03', 'ya'),
('G0005', 'T0005', 'BOB04', 'ya'),
('G0005', 'T0006', 'BOB01', 'tidak'),
('G0005', 'T0006', 'BOB02', 'tidak'),
('G0005', 'T0006', 'BOB03', 'tidak'),
('G0005', 'T0006', 'BOB04', 'ya'),
('G0005', 'T0007', 'BOB01', 'tidak'),
('G0005', 'T0007', 'BOB02', 'tidak'),
('G0005', 'T0007', 'BOB03', 'tidak'),
('G0005', 'T0007', 'BOB04', 'ya'),
('G0005', 'T0008', 'BOB01', 'tidak'),
('G0005', 'T0008', 'BOB02', 'tidak'),
('G0005', 'T0008', 'BOB03', 'tidak'),
('G0005', 'T0008', 'BOB04', 'ya'),
('G0005', 'T0009', 'BOB01', 'tidak'),
('G0005', 'T0009', 'BOB02', 'tidak'),
('G0005', 'T0009', 'BOB03', 'tidak'),
('G0005', 'T0009', 'BOB04', 'ya'),
('G0005', 'T0010', 'BOB01', 'tidak lebih dari 2 (dua) orang'),
('G0005', 'T0010', 'BOB02', 'tidak lebih dari 3 (tiga) orang'),
('G0005', 'T0010', 'BOB03', 'lebih dari 4 (empat) orang'),
('G0005', 'T0010', 'BOB04', 'lebih dari 4 (empat) orang'),
('G0005', 'T0011', 'BOB01', 'ya'),
('G0005', 'T0011', 'BOB02', 'ya'),
('G0005', 'T0011', 'BOB03', 'tidak'),
('G0005', 'T0011', 'BOB04', 'tidak'),
('G0005', 'T0012', 'BOB01', 'tidak'),
('G0005', 'T0012', 'BOB02', 'tidak'),
('G0005', 'T0012', 'BOB03', 'ya'),
('G0005', 'T0012', 'BOB04', 'ya'),
('G0006', 'T0001', 'BOB01', 'ya'),
('G0006', 'T0001', 'BOB02', 'ya'),
('G0006', 'T0001', 'BOB03', 'tidak'),
('G0006', 'T0001', 'BOB04', 'tidak'),
('G0006', 'T0002', 'BOB01', 'ya'),
('G0006', 'T0002', 'BOB02', 'ya'),
('G0006', 'T0002', 'BOB03', 'tidak'),
('G0006', 'T0002', 'BOB04', 'tidak'),
('G0006', 'T0003', 'BOB01', 'tidak'),
('G0006', 'T0003', 'BOB02', 'ya'),
('G0006', 'T0003', 'BOB03', 'ya'),
('G0006', 'T0003', 'BOB04', 'ya'),
('G0006', 'T0004', 'BOB01', 'tidak'),
('G0006', 'T0004', 'BOB02', 'ya'),
('G0006', 'T0004', 'BOB03', 'ya'),
('G0006', 'T0004', 'BOB04', 'ya'),
('G0006', 'T0005', 'BOB01', 'tidak'),
('G0006', 'T0005', 'BOB02', 'tidak'),
('G0006', 'T0005', 'BOB03', 'ya'),
('G0006', 'T0005', 'BOB04', 'ya'),
('G0006', 'T0006', 'BOB01', 'tidak'),
('G0006', 'T0006', 'BOB02', 'tidak'),
('G0006', 'T0006', 'BOB03', 'ya'),
('G0006', 'T0006', 'BOB04', 'ya'),
('G0006', 'T0007', 'BOB01', 'tidak'),
('G0006', 'T0007', 'BOB02', 'tidak'),
('G0006', 'T0007', 'BOB03', 'tidak'),
('G0006', 'T0007', 'BOB04', 'ya'),
('G0006', 'T0008', 'BOB01', 'tidak'),
('G0006', 'T0008', 'BOB02', 'tidak'),
('G0006', 'T0008', 'BOB03', 'tidak'),
('G0006', 'T0008', 'BOB04', 'ya'),
('G0007', 'T0001', 'BOB01', 'ya'),
('G0007', 'T0001', 'BOB02', 'ya'),
('G0007', 'T0001', 'BOB03', 'tidak'),
('G0007', 'T0001', 'BOB04', 'tidak'),
('G0007', 'T0002', 'BOB01', 'tidak'),
('G0007', 'T0002', 'BOB02', 'tidak'),
('G0007', 'T0002', 'BOB03', 'ya'),
('G0007', 'T0002', 'BOB04', 'ya'),
('G0007', 'T0003', 'BOB01', 'ya'),
('G0007', 'T0003', 'BOB02', 'ya'),
('G0007', 'T0003', 'BOB03', 'tidak'),
('G0007', 'T0003', 'BOB04', 'tidak'),
('G0007', 'T0004', 'BOB01', 'tidak'),
('G0007', 'T0004', 'BOB02', 'tidak'),
('G0007', 'T0004', 'BOB03', 'ya'),
('G0007', 'T0004', 'BOB04', 'ya'),
('G0007', 'T0005', 'BOB01', 'tidak'),
('G0007', 'T0005', 'BOB02', 'tidak'),
('G0007', 'T0005', 'BOB03', 'ya'),
('G0007', 'T0005', 'BOB04', 'ya'),
('G0007', 'T0006', 'BOB01', 'tidak'),
('G0007', 'T0006', 'BOB02', 'tidak'),
('G0007', 'T0006', 'BOB03', 'ya'),
('G0007', 'T0006', 'BOB04', 'ya'),
('G0007', 'T0007', 'BOB01', 'tidak'),
('G0007', 'T0007', 'BOB02', 'tidak'),
('G0007', 'T0007', 'BOB03', 'tidak'),
('G0007', 'T0007', 'BOB04', 'ya'),
('G0007', 'T0008', 'BOB01', 'tidak'),
('G0007', 'T0008', 'BOB02', 'tidak'),
('G0007', 'T0008', 'BOB03', 'tidak'),
('G0007', 'T0008', 'BOB04', 'ya'),
('G0007', 'T0009', 'BOB01', 'tidak'),
('G0007', 'T0009', 'BOB02', 'tidak'),
('G0007', 'T0009', 'BOB03', 'tidak'),
('G0007', 'T0009', 'BOB04', 'ya'),
('G0007', 'T0010', 'BOB01', 'tidak'),
('G0007', 'T0010', 'BOB02', 'tidak'),
('G0007', 'T0010', 'BOB03', 'tidak'),
('G0007', 'T0010', 'BOB04', 'ya'),
('G0007', 'T0011', 'BOB01', 'ya'),
('G0007', 'T0011', 'BOB02', 'ya'),
('G0007', 'T0011', 'BOB03', 'ya'),
('G0007', 'T0011', 'BOB04', 'tidak'),
('G0008', 'T0001', 'BOB01', 'tidak diperlukan'),
('G0008', 'T0001', 'BOB02', 'diperlukan'),
('G0008', 'T0001', 'BOB03', 'diperlukan'),
('G0008', 'T0001', 'BOB04', 'sangat diperlukan'),
('G0008', 'T0002', 'BOB01', 'tidak'),
('G0008', 'T0002', 'BOB02', 'tidak'),
('G0008', 'T0002', 'BOB03', 'tidak'),
('G0008', 'T0002', 'BOB04', 'ya'),
('G0008', 'T0003', 'BOB01', 'ya'),
('G0008', 'T0003', 'BOB02', 'ya'),
('G0008', 'T0003', 'BOB03', 'ya'),
('G0008', 'T0003', 'BOB04', 'tidak'),
('G0009', 'T0001', 'BOB01', 'tidak diperlukan'),
('G0009', 'T0001', 'BOB02', 'diperlukan dan mudah untuk mendapatkannya'),
('G0009', 'T0001', 'BOB03', 'diperlukan tapi sulit untuk mendapatkannya'),
('G0009', 'T0001', 'BOB04', 'mutlak diperlukan dan lebih dari satu lembaga');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tingkat_kesulitan_topik`
--

CREATE TABLE `tb_tingkat_kesulitan_topik` (
  `ID_GROUP` varchar(5) NOT NULL,
  `ID_TOPIK` varchar(5) NOT NULL,
  `NAMA_TOPIK` varchar(200) NOT NULL,
  `KETERANGAN` varchar(300) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tingkat_kesulitan_topik`
--

INSERT INTO `tb_tingkat_kesulitan_topik` (`ID_GROUP`, `ID_TOPIK`, `NAMA_TOPIK`, `KETERANGAN`) VALUES
('G0001', 'T0001', 'Apakah ada saksi yang melihat, mendengar, mengetahui secara langsung peristiwa yang terjadi ?', NULL),
('G0001', 'T0002', 'Dimana Lokasi tempat tinggal saksi ?', NULL),
('G0001', 'T0003', 'Apakah saksi adalah merupakan sumber pertama ?', NULL),
('G0001', 'T0004', 'apakah diperlukan penterjemah ?', NULL),
('G0001', 'T0005', 'Apakah pemeriksa saksi diperlukan prosedur/birokrasi khusus ?', NULL),
('G0001', 'T0006', 'Apakah Saksi berhubugan dengan lembaga lain ?', NULL),
('G0001', 'T0007', 'Apakah diperlukan pengamanan khusus terhadap saksi ?', NULL),
('G0001', 'T0008', 'Apakah saksi utama sakit-sakitan ?', NULL),
('G0001', 'T0009', 'Berapa jumlah saksi ?', NULL),
('G0002', 'T0001', 'Apakah terdapat bukti surat yang berkaitan dengan perkara ?', NULL),
('G0002', 'T0002', 'Apakah bukti surat mudah didapat ?', NULL),
('G0002', 'T0003', 'Apakah diperlukan bukti surat dalam perkara yang disidik ?', NULL),
('G0002', 'T0004', 'Apakah diperlukan pemeriksaan forensik terhadap bukti surat ?', NULL),
('G0003', 'T0001', 'Apakah Petunjuk yang ada terdapat kesesuaian antara keterangan para saksi, keterangan tersangka dan  barang bukti  yang ditemukan ?', NULL),
('G0004', 'T0001', 'Apakah diperlukan keterangan ahli ?', NULL),
('G0004', 'T0002', 'Jika  diperlukan, Apakah saksi ahli berada di wilayah hukum penyidik ?', NULL),
('G0005', 'T0001', 'Apakah tersangka tertangkap tangan atau menyerahkan diri kepada polisi ?', NULL),
('G0005', 'T0002', 'Apakah keterangan tentang tersangka mudah didapat ?', NULL),
('G0005', 'T0003', 'Apakah tersangka diketahui identitasnya ?', NULL),
('G0005', 'T0004', 'Apakah tersangka dilindungi kelompok tertentu ?', NULL),
('G0005', 'T0005', 'Apakah tersangka memiliki jabatan tertentu (dalam pemeriksaan diatur oleh perundang – undangan tertentu) ?', NULL),
('G0005', 'T0006', 'Apakah dalam pemeriksaan tersangka diperlukan penterjemah/ahli bahasa ?', NULL),
('G0005', 'T0007', 'Apakah dalam memeriksa tersangka memerlukan ijin khusus ?', NULL),
('G0005', 'T0008', 'Apakah tersangka  warga  negara asing ?', NULL),
('G0005', 'T0009', 'Apakah tersangka melarikan diri ke Luar Negeri ?', NULL),
('G0005', 'T0010', 'Berapa Jumlah Tersangka ?', NULL),
('G0005', 'T0011', 'Apakah tersangka sehat jasmani dan rohani  ?', NULL),
('G0005', 'T0012', 'Apakah tersangka merupakan bagian dari sindikat kejahatan (pelaku kejahatan terorganisis) ?', NULL),
('G0006', 'T0001', 'Apakah tempat Kejadian Perkara mudah dijangkau ?', NULL),
('G0006', 'T0002', 'Apakah tempat Kejadian Perkara masih utuh ?', NULL),
('G0006', 'T0003', 'Apakah diperlukan pengolahan Tempat Kejadian Perkara ?', NULL),
('G0006', 'T0004', 'Apakah diperlukan bantuan teknis Kepolisian dalam pengolahan Tempat Kejadian Perkara ?', NULL),
('G0006', 'T0005', 'Apakah diperlukan pengamanan khusus terhadap TKP ?', NULL),
('G0006', 'T0006', 'Apakah tempat kejadian Perkara lebih dari satu yurisdiksi wilayah hukum penyidikan ?', NULL),
('G0006', 'T0007', 'Apakah tempat Kejadian Perkara di atas Kapal Laut/ Udara  yang  berbendera Republik Indonesia dan  sedang berada di Luar Negeri ?', NULL),
('G0006', 'T0008', 'Apakah tempat Kejadian Perkara di Kedutaan Besar Indonesia yang berada di luar negeri ?', NULL),
('G0007', 'T0001', 'Apakah barang bukti mudah didapat ?', NULL),
('G0007', 'T0002', 'Apakah diperlukan pemeriksaan forensik/ahli terhadap barang bukti ?', NULL),
('G0007', 'T0003', 'Apakah barang bukti mudah diamankan ?', NULL),
('G0007', 'T0004', 'Apakah diperlukan pengamanan khusus terhadap barang bukti ?', NULL),
('G0007', 'T0005', 'Apakah diperlukan alat khusus untuk pengangkutan barang bukti ?', NULL),
('G0007', 'T0006', 'Apakah diperlukan tempat penyimpanan khusus untuk mengamankan barang bukti ?', NULL),
('G0007', 'T0007', 'Apakah barang bukti mudah rusak ?', NULL),
('G0007', 'T0008', 'Apakah untuk mengambil barang bukti diperlukan keahlian dan alat khusus ?', NULL),
('G0007', 'T0009', 'Apakah untuk menyita barang bukti diperlukan prosedur khusus ?', NULL),
('G0007', 'T0010', 'Apakah barang bukti berada di luar negari ?', NULL),
('G0007', 'T0011', 'Apakah barang bukti pada suatu tempat yang mudah dijangkau ?', NULL),
('G0008', 'T0001', 'Apakah diperlukan alat khusus untuk mendukung proses penyidikan perkara yang ditangani ?', NULL),
('G0008', 'T0002', 'Apakah peralatan yang dibutuhkan perlu di datangkan dari luar negeri ?', NULL),
('G0008', 'T0003', 'Apakah peralatan yang dibutuhkan tersedia di wilayah hukum penyidikt ?', NULL),
('G0009', 'T0001', 'Apakah diperlukan peranan lembaga lain untuk mendukung proses penyidikan perkara yang ditangani ?', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_tipe_user`
--

CREATE TABLE `tb_tipe_user` (
  `TIPE_USER` int(1) NOT NULL,
  `NAMA_TIPE_USER` varchar(50) NOT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_tipe_user`
--

INSERT INTO `tb_tipe_user` (`TIPE_USER`, `NAMA_TIPE_USER`, `tgl_masuk_data`) VALUES
(1, 'SUPER ADMIN', '2012-08-05 17:00:00'),
(2, 'KASAT/KBO', '2012-08-05 17:00:00'),
(3, 'KASUBDIT', '2012-08-05 17:00:00'),
(4, 'PENYIDIK', '2012-08-05 17:00:00'),
(5, 'PELAPOR', '2012-08-05 17:00:00'),
(6, 'KANIT', '2012-08-05 17:00:00'),
(7, 'ADMIN SUBDIT', '2012-08-05 17:00:00'),
(8, 'OPSNAL', '2012-12-06 09:18:36'),
(9, 'ADMIN SPKT', '2012-12-06 09:18:36'),
(10, 'ANGGOTA IDEN', '2012-12-11 17:00:00'),
(11, 'URBIN OPS', '2012-12-11 17:00:00'),
(12, 'KAPOLRES/KSPKT', '2015-11-18 02:24:59'),
(13, 'KABAGSUMDA', '2015-11-18 02:24:59'),
(14, 'KASIWAS/PROPAM', '2015-11-18 02:25:12');

-- --------------------------------------------------------

--
-- Table structure for table `tb_unit`
--

CREATE TABLE `tb_unit` (
  `NAMA_SATWIL` varchar(25) NOT NULL,
  `NAMA_SATKER` varchar(25) NOT NULL,
  `NAMA_SUBDIT` varchar(25) NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `NAMA_KANIT` varchar(50) DEFAULT NULL,
  `PANGKAT_KANIT` varchar(25) DEFAULT NULL,
  `TELP_UNIT` varchar(12) DEFAULT NULL,
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_ada_penyidik` int(1) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_unit`
--

INSERT INTO `tb_unit` (`NAMA_SATWIL`, `NAMA_SATKER`, `NAMA_SUBDIT`, `NAMA_UNIT`, `NAMA_KANIT`, `PANGKAT_KANIT`, `TELP_UNIT`, `tgl_masuk_data`, `is_ada_penyidik`) VALUES
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT I', 'UNIT I', 'I GUSTI KETUT ATNA', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT II', 'UNIT I', 'I GST NGR GUNADI', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT III', 'UNIT I', 'I KETUT BANDRIA', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT IV', 'UNIT I', 'I KT SOMA ADNYANA, SH.,MH', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT IV', 'UNIT IV', 'SUBIRAN, SH', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'PAURMINOPS', 'UNIT I', 'SUBIYANTO', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT I', 'UNIT II', 'I MADE WARDANA,SH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT I', 'UNIT III', 'Drs. I WAYAN SINARYASA', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT II', 'UNIT II', 'I GST NGR PURNAWA,SH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT II', 'UNIT III', 'I KETUT SUHARTO GIRI,SH, MH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT III', 'UNIT II', 'DEWA GEDE RAI SUJAYA', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT III', 'UNIT III', 'SI PT DHARMA PUTRA,SH,MH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT IV', 'UNIT II', 'ENCEP SYAMSUL HAYAT,SH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT IV', 'UNIT III', 'I B. KETUT MANTRA, SH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT I', 'UNIT IV', 'NANDANG IRWANTO, SH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT III', 'UNIT IV', 'I MADE WIDIA,SH,MH', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMSUS', 'SUBDIT II', 'UNIT IV', 'SLAMET ACHWAN', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT I', 'UNIT I', 'HADI SURYANTO', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT II', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT III', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT IV', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT IV', 'UNIT IV', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'PAURMINOPS', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT I', 'UNIT II', 'KUSUMA WARDANI', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT I', 'UNIT III', 'JONI RISMANTO', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT II', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT II', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT III', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT III', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT IV', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT IV', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT I', 'UNIT IV', 'SAEFUL ROMLI', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT III', 'UNIT IV', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESKRIMUM', 'SUBDIT II', 'UNIT IV', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT I', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT II', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT III', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT IV', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT IV', 'UNIT IV', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'PAURMINOPS', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT I', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT I', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT II', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT II', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT III', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT III', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT IV', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT IV', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT I', 'UNIT IV', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT III', 'UNIT IV', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITRESNARKOBA', 'SUBDIT II', 'UNIT IV', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITLANTAS', 'SUBDIT I', 'UNIT I', '-', 'KOMPOL', '-', '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITLANTAS', 'SUBDIT I', 'UNIT II', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITLANTAS', 'SUBDIT I', 'UNIT III', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLDA SULAWESI SELATAN', 'DITLANTAS', 'SUBDIT I', 'UNIT IV', '-', 'KOMPOL', NULL, '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', '-', 'AKP', '-', '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', '-', 'AKP', '-', '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', '-', 'IPDA', '-', '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATLANTAS', 'SUBDIT I', 'UNIT I', '-', 'AKP', '-', '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRESTABES MAKASSAR', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', '-', 'AKP', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', '-', 'IPDA', '-', '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', '-', 'IPDA', NULL, '2016-08-25 13:06:59', 1),
('POLRES GOWA', 'SATLANTAS', 'SUBDIT I', 'UNIT I', '-', 'IPDA', '-', '2016-08-25 13:06:59', 1),
('POLRES MAROS', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES MAROS', 'SATLANTAS', 'SUBDIT I', 'UNIT I', NULL, NULL, NULL, '2016-08-29 13:12:52', 1),
('POLRES KP3', 'SATRESKRIM', 'SUBDIT I', 'UNIT I', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESKRIM', 'SUBDIT I', 'UNIT II', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESKRIM', 'SUBDIT I', 'UNIT III', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESKRIM', 'SUBDIT I', 'UNIT IV', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESKRIM', 'SUBDIT I', 'UNIT V', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT I', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT II', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT III', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATRESNARKOBA', 'SUBDIT I', 'UNIT IV', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRES KP3', 'SATLANTAS', 'SUBDIT I', 'UNIT I', NULL, NULL, NULL, '2016-08-29 13:13:08', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VI', NULL, 'AKP', NULL, '2016-09-11 04:09:44', 1),
('POLRESTABES MAKASSAR', 'SATRESKRIM', 'SUBDIT I', 'UNIT VII', NULL, 'AKP', NULL, '2016-09-11 04:10:09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `ID_USER` varchar(10) NOT NULL,
  `USER_NAME` varchar(50) DEFAULT NULL,
  `PASSWORD` varchar(50) DEFAULT NULL,
  `TIPE_USER` int(1) NOT NULL,
  `FOTO` varchar(255) NOT NULL DEFAULT 'images/profile.jpg',
  `FOTO_BIG` varchar(255) NOT NULL DEFAULT 'images/profile.jpg	',
  `tgl_masuk_data` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status_login` int(11) NOT NULL DEFAULT '0',
  `chat_last_activity` datetime NOT NULL,
  `STATUS_AKTIF` int(1) DEFAULT '1',
  `user_regId` varchar(400) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`ID_USER`, `USER_NAME`, `PASSWORD`, `TIPE_USER`, `FOTO`, `FOTO_BIG`, `tgl_masuk_data`, `status_login`, `chat_last_activity`, `STATUS_AKTIF`, `user_regId`) VALUES
('65120041', 'SOMA_6512', 'SOMA_6512', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('70070352', 'ARYA_7007', 'ARYA_7007', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('62080740', 'NYOMAN_6208', 'NYOMAN_6208', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('81120105', 'WAYAN_8112', 'WAYAN_8112', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('83030868', 'KADEK_8303', 'KADEK_8303', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('87030284', 'DWI_8703', 'DWI_8703', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('87060274', 'ANDIKA_8706', 'ANDIKA_8706', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('78110587', 'MADE_7811', 'MADE_7811', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('66020248', 'IDA_6602', 'IDA_6602', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('63040691', 'KETUT_6304', 'KETUT_6304', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('92090401', 'REZA_9209', 'REZA_9209', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('70030329', 'NYOMAN_7003', 'NYOMAN_7003', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('84090364', 'NYOMAN_8409', 'NYOMAN_8409', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('ku31slsl04', 'KANIT_III_SUBDIT_I_LANTAS_SULSEL', 'KANIT_III_SUBDIT_I_LANTAS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku31slsl03', 'KANIT_III_SUBDIT_I_NARKOBA_SULSEL', 'KANIT_III_SUBDIT_I_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('81110615', 'ACHMAD_8111', 'ACHMAD_8111', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('76110675', 'JOKO_7611', 'JOKO_7611', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('74120011', 'PUTU_7412', 'PUTU_7412', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('64120119', 'SUBIRAN_6412', 'SUBIRAN_6412', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('81110719', 'NYOMAN_8111', 'NYOMAN_8111', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('71110296', 'NYOMAN_7111', 'NYOMAN_7111', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('76040065', 'MADE_7604', 'MADE_7604', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-11-10 09:13:53', 0, '2016-01-27 09:01:58', 1, ''),
('63030697', 'KETUT_6303', 'KETUT_6303', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('86070237', 'PRANA_8607', 'PRANA_8607', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('84020445', 'SUARSA_8402', 'SUARSA_8402', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('78110968', 'DEWA_7811', 'DEWA_7811', 4, 'upload/foto_user/78110968_thumb.jpg', 'upload/foto_user/ukuran_asli/78110968.jpg', '2016-09-08 15:06:55', 0, '2016-09-08 22:06:43', 1, ''),
('64120192', 'WAYAN_6412', 'WAYAN_6412', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('67110078', 'GST_6711', 'GST_6711', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-19 08:24:50', 0, '2016-09-19 15:24:40', 1, ''),
('ku43slsl02', 'KANIT_IV_SUBDIT_III_KRIMSUS_SULSEL', 'KANIT_IV_SUBDIT_III_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku43slsl01', 'KANIT_IV_SUBDIT_III_KRIMUM_SULSEL', 'KANIT_IV_SUBDIT_III_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku33slsl03', 'KANIT_III_SUBDIT_III_NARKOBA_SULSEL', 'KANIT_III_SUBDIT_III_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku33slsl02', 'KANIT_III_SUBDIT_III_KRIMSUS_SULSEL', 'KANIT_III_SUBDIT_III_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku14slsl02', 'KANIT_I_SUBDIT_IV_KRIMSUS_SULSEL', 'KANIT_I_SUBDIT_IV_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ku24slsl03', 'KANIT_II_SUBDIT_IV_NARKOBA_SULSEL', 'KANIT_II_SUBDIT_IV_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku34slsl01', 'KANIT_III_SUBDIT_IV_KRIMUM_SULSEL', 'KANIT_III_SUBDIT_IV_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku34slsl03', 'KANIT_III_SUBDIT_IV_NARKOBA_SULSEL', 'KANIT_III_SUBDIT_IV_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku44slsl03', 'KANIT_IV_SUBDIT_IV_NARKOBA_SULSEL', 'KANIT_IV_SUBDIT_IV_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:16', 0, '2014-03-12 23:32:27', 1, ''),
('ksd1slsl01', 'KASUBDIT_I_KRIMUM_SULSEL', 'KASUBDIT_I_KRIMUM_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd1slsl03', 'KASUBDIT_I_NARKOBA_SULSEL', 'KASUBDIT_I_NARKOBA_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('81111211', 'SONDRA_8111', 'SONDRA_8111', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('59020677', 'GUSTI_5902', 'GUSTI_5902', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('61010571', 'GUSTI_6101', 'GUSTI_6101', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('77071241', 'AGUNG_7707', 'AGUNG_7707', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('86090609', 'GEDE_8609', 'GEDE_8609', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('77080084', 'MADE_7708', 'MADE_7708', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('73110093', 'GEDE_7311', 'GEDE_7311', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-09 01:42:18', 0, '2016-09-09 08:42:11', 1, ''),
('65120165', 'MADE_6512', 'MADE_6512', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('62100145', 'MADE_6210', 'MADE_6210', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('87040477', 'WAYAN_8704', 'WAYAN_8704', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('81070222', 'SUHERMAN_8107', 'SUHERMAN_8107', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('76060263', 'ZULFI_7606', 'ZULFI_7606', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('59100813', 'SLAMET_5910', 'SLAMET_5910', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('71040329', 'NYOMAN_7104', 'NYOMAN_7104', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('64080422', 'KETUT_5910', 'KETUT_5910', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('75040552', 'ERVAN_7504', 'ERVAN_7504', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('74100256', 'MADE_7410', 'MADE_7410', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('63030737', 'GST_6303', 'GST_6303', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('79040162', 'MADE_7904', 'MADE_7904', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('66090386', 'KETUT_6609', 'KETUT_6609', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('61040298', 'GST_6104', 'GST_6104', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('67080017', 'MADE_6708', 'MADE_6708', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('70121078', 'MADE_7012', 'MADE_7012', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('86100724', 'EDI_8610', 'EDI_8610', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('65090572', 'KETUT_6509', 'KETUT_6509', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('76020672', 'MUHAMAD_7602', 'MUHAMAD_7602', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-06-24 03:05:37', 1, '2016-06-24 10:05:37', 1, ''),
('85020364', 'NGR_8502', 'NGR_8502', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('ku12slsl03', 'KANIT_I_SUBDIT_II_NARKOBA_SULSEL', 'KANIT_I_SUBDIT_II_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku12slsl02', 'KANIT_I_SUBDIT_II_KRIMSUS_SULSEL', 'KANIT_I_SUBDIT_II_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('75040680', 'NYOMAN_7504', 'NYOMAN_7504', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-07-22 11:25:40', 1, '2016-07-22 18:25:40', 1, ''),
('76110488', 'GEDE_7611', 'GEDE_7611', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('80020448', 'PUTU_8002', 'PUTU_8002', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('76010879', 'ENCEP_7601', 'ENCEP_7601', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('85030240', 'PUTU_8503', 'PUTU_8503', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('63070134', 'DEWA_6307', 'DEWA_6307', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-09-08 15:06:36', 0, '2016-09-08 22:06:10', 1, ''),
('76070401', 'KETUT_7607', 'KETUT_7607', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('80050940', 'MADE_8005', 'MADE_8005', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('86020658', 'NYOMAN_8602', 'NYOMAN_8602', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('64020189', 'DHARMA_6402', 'DHARMA_6402', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('79060022', 'GST_7906', 'GST_7906', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('79080112', 'PUTU_7908', 'PUTU_7908', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('69090591', 'GUSTI_6909', 'GUSTI_6909', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('71050450', 'NANDANG_7105', 'NANDANG_7105', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('83100112', 'MADE_8310', 'MADE_8310', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('77070395', 'MADE_7707', 'MADE_7707', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-15 15:01:18', 0, '2016-09-15 22:01:10', 1, ''),
('76070400', 'AGUS_7607', 'AGUS_7607', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('77100260', 'KETUT_7710', 'KETUT_7710', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('66120023', 'MADE_6612', 'MADE_6612', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('75030718', 'KETUT_7503', 'KETUT_7503', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('63090413', 'WAYAN_6309', 'WAYAN_6309', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('75010587', 'JANSEN_7501', 'JANSEN_7501', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('ksd3slsl01', 'KASUBDIT_III_KRIMUM_SULSEL', 'KASUBDIT_III_KRIMUM_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd3slsl02', 'KASUBDIT_III_KRIMSUS_SULSEL', 'KASUBDIT_III_KRIMSUS_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ku41slsl04', 'KANIT_IV_SUBDIT_I_LANTAS_SULSEL', 'KANIT_IV_SUBDIT_I_LANTAS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku41slsl02', 'KANIT_IV_SUBDIT_I_KRIMSUS_SULSEL', 'KANIT_IV_SUBDIT_I_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku41slsl01', 'KANIT_IV_SUBDIT_I_KRIMUM_SULSEL', 'KANIT_IV_SUBDIT_I_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('81010055', 'EKO_8101', 'EKO_8101', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:50:00', 0, '2012-12-14 09:49:38', 1, ''),
('12111401', 'admin_spkt', 'admin_spkt', 9, 'images/profile.jpg', 'images/profile.jpg', '2014-09-21 15:19:00', 0, '2014-09-19 02:23:54', 1, ''),
('12111415', 'opsnal_1211', 'opsnal_1211', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-11-14 05:51:59', 0, '2012-11-14 12:51:31', 1, ''),
('71010242', 'juwono_7101', 'juwono_7101', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-09 12:20:19', 0, '2012-12-09 19:18:38', 1, ''),
('80120559', 'dedd_8012', 'dedd_8012', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-04 07:54:50', 0, '2014-02-04 14:53:25', 1, ''),
('85010412', 'imam_8501', 'imam_8501', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-09 01:42:57', 0, '2016-09-09 08:42:50', 1, ''),
('80080810', 'DANANG_8008', 'DANANG_8008', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-07-29 05:31:57', 0, '2012-12-14 09:53:34', 1, ''),
('71020165', 'wahyu_7102', 'wahyu_7102', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-21 07:42:07', 0, '2013-02-21 14:42:07', 1, ''),
('88041098', 'pari_8804', '88041098', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-02-05 06:52:38', 0, '2014-02-05 13:45:28', 1, ''),
('78040126', 'DWI_7804', 'DWI_7804', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-11-25 09:47:30', 0, '2012-12-14 09:49:13', 1, 'APA91bGpmS2NGJeb6Dy7BYtEJnj9yQx-sjgPjbidjAJCW0UpdPk_EaWYoBL2tZnE469Bcm-O1GoqCIWZOiul-jRnHg2Bj2-E-LFNcTtXMfjO7mlWsNOSYM9AQpX9PstyFoKna_cqV_rc'),
('58050989', 'BENNY_5805', 'BENNY_5805', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-11-12 02:59:59', 0, '2012-12-14 09:39:00', 1, ''),
('63040468', 'KISWARI_6304', 'KISWARI_6304', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:39:32', 1, ''),
('73020335', 'SUGENG_7302', 'SUGENG_7302', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:40:05', 1, ''),
('84120625', 'ARIS_8412', 'ARIS_8412', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:40:44', 1, ''),
('74100272', 'DEDY_7410', 'DEDY_7410', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:41:41', 1, ''),
('76020457', 'SUTRISNO_7602', 'SUTRISNO_7602', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:42:04', 1, ''),
('83090410', 'MUSTOFAH_8309', 'MUSTOFAH_8309', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-01-28 11:47:24', 1, ''),
('78120003', 'NANAK_7812', 'NANAK_7812', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:43:11', 1, ''),
('78110150', 'ANDANG_7811', 'ANDANG_7811', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:44:24', 1, ''),
('83040537', 'ANDRI_8304', 'ANDRI_8304', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:43:43', 0, '2012-12-14 09:44:59', 1, ''),
('64070658', 'ABDUL_6407', 'ABDUL_6407', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 09:53:04', 1, ''),
('81031298', 'AMINULLOH_8103', 'AMINULLOH_8103', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 09:54:10', 1, ''),
('85041591', 'SUKRON_8504', 'SUKRON_8504', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 09:57:11', 1, ''),
('69011091', 'KASUM_6901', 'KASUM_6901', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 10:00:34', 1, ''),
('83010287', 'EKO_8301', 'EKO_8301', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 10:01:03', 1, ''),
('71090225', 'RISTITANTO_7109', 'RISTITANTO_7109', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-07-27 07:37:52', 0, '2012-12-14 10:01:37', 0, ''),
('75040173', 'DIAN_7504', 'DIAN_7504', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 10:02:19', 1, ''),
('87050533', 'FIRDAUS_8705', 'FIRDAUS_8705', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:02:05', 0, '2012-12-14 10:03:21', 1, ''),
('89020556', 'zafar_8902', 'zafar_8902', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-09-12 06:03:10', 0, '2013-02-08 10:26:57', 0, ''),
('83050336', 'PRIMADA_8305', 'PRIMADA_8305', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:05:27', 0, '2012-12-14 10:06:43', 1, ''),
('64050169', 'MOCHTAR_6405', 'MOCHTAR_6405', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:16:20', 0, '2012-12-14 10:15:57', 1, ''),
('85110372', 'HENDRA_8511', 'HENDRA_8511', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:16:20', 0, '2012-12-14 10:16:25', 1, ''),
('87060910', 'JUNANTO_8706', 'JUNANTO_8706', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-10-01 07:49:08', 0, '2012-12-14 10:16:52', 1, ''),
('80070822', 'ANTON_8007', 'ANTON_8007', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:16:20', 0, '2012-12-14 10:17:41', 1, ''),
('71030239', 'GATOT_7103', 'GATOT_7103', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:18:42', 0, '2012-12-14 10:18:33', 1, ''),
('81040048', 'ADI_8104', 'ADI_8104', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:18:42', 0, '2012-12-14 10:19:04', 1, ''),
('83100569', 'DODIK_8310', 'DODIK_8310', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:18:42', 0, '2012-12-14 10:19:32', 1, ''),
('91080061', 'RIZAL_9108', 'RIZAL_9108', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:18:42', 0, '2012-12-14 10:20:00', 1, ''),
('84020142', 'FANDY_8402', 'FANDY_8402', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:20:49', 0, '2012-12-14 10:21:39', 1, ''),
('83080530', 'BAGUS_8308', 'BAGUS_8308', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:20:49', 0, '2012-12-14 10:22:11', 1, ''),
('86050133', 'FIRMANSYAH_8605', 'FIRMANSYAH_8605', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:21:19', 0, '2012-12-14 10:22:41', 1, ''),
('80060833', 'PRIYANTO_8006', 'PRIYANTO_8006', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:28:45', 1, ''),
('80070812', 'KOKO_8007', 'KOKO_8007', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:29:19', 1, ''),
('79091136', 'JOKO_7909', 'JOKO_7909', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:29:50', 1, ''),
('84020729', 'RUBY_8402', 'RUBY_8402', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:30:26', 1, ''),
('73060266', 'DAGUK_7306', 'DAGUK_7306', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:31:19', 1, ''),
('84010528', 'ADNAN_8401', 'ADNAN_8401', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:32:55', 1, ''),
('81100716', 'DWI_8110', 'DWI_8110', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:33:28', 1, ''),
('79010265', 'SODIQIN_7901', 'SODIQIN_7901', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:35:01', 1, ''),
('84050644', 'DIMAS_8405', 'DIMAS_8405', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-07-27 07:43:49', 0, '2012-12-14 10:35:31', 0, ''),
('78070073', 'MARJI_7807', 'MARJI_7807', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-07-27 07:35:04', 0, '2012-12-14 10:36:01', 0, ''),
('84040418', 'DONNY_8404', 'DONNY_8404', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-02-12 11:43:25', 1, ''),
('86031545', 'MARTINUS_8603', 'MARTINUS_8603', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:35:44', 0, '2012-12-14 10:37:06', 1, ''),
('59050108', 'sudirman_5905', 'sudirman_5905', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-10-02 02:45:42', 0, '2013-10-02 09:42:40', 1, ''),
('5607008', 'nyoman_5607', 'nyoman_5607', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 10:45:10', 0, '2013-01-07 14:56:44', 1, ''),
('78120239', 'zulkarnain_7812', 'zulkarnain_7812', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-06-21 11:02:17', 1, ''),
('75090906', 'amiruddin_7509', 'amiruddin_7509', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-12 03:18:20', 0, '2013-02-12 10:18:20', 1, ''),
('83010265', 'ari_8301', 'ari_8301', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-14 08:50:04', 0, '2013-02-14 15:50:04', 1, ''),
('75090758', 'rohmawati_7509', 'rohmawati_7509', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-02-19 11:10:44', 1, ''),
('64030759', 'abdul_6403', 'abdul_6403', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 08:07:22', 0, '2013-01-07 15:07:22', 1, ''),
('63040959', 'gatot_6304', 'gatot_6304', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-08-30 08:22:52', 0, '2013-01-07 15:11:13', 1, ''),
('57120398', 'darna_5712', 'darna_5712', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 08:20:48', 0, '2013-01-07 15:20:48', 1, ''),
('58070372', 'suryadi_5807', 'suryadi_5807', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 08:24:54', 0, '2013-01-07 15:24:54', 1, ''),
('58060406', 'soemarsono_5806', 'soemarsono_5806', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 03:53:19', 0, '2013-01-08 09:46:01', 1, ''),
('74050566', 'kuswinarto_7405', 'kuswinarto_7405', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 08:42:53', 0, '2013-01-07 15:42:53', 1, ''),
('73100237', 'eko_7310', 'eko_7310', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 08:46:02', 0, '2013-01-07 15:46:02', 1, ''),
('82110495', 'amnandyah_8211', 'amnandyah_8211', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 08:51:23', 0, '2013-01-07 15:51:23', 1, ''),
('82051555', 'AGUNG URBIN', '0407', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-12-16 05:37:34', 0, '2013-12-16 12:23:31', 1, ''),
('55100208', 'i nyoman suwena_5510', 'i nyoman suwena_5510', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 09:13:29', 0, '2013-01-07 16:13:29', 1, ''),
('81110395', 'fajar_8111', 'fajar_8111', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 04:15:00', 0, '2013-01-08 11:15:00', 1, ''),
('77080450', 'aris_7708', 'aris_7708', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-03-27 06:59:02', 0, '2013-03-27 13:58:29', 1, ''),
('82040944', 'rochman_8204', 'rochman_8204', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 03:33:20', 0, '2013-01-08 10:33:20', 1, ''),
('77080326', 'ali_7708', 'ali_7708', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 03:39:07', 0, '2013-01-08 10:39:07', 1, ''),
('82031087', 'bambang_8203', 'bambang_8203', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 03:59:43', 0, '2013-01-08 10:59:43', 1, ''),
('85111319', 'yuga_8511', 'yuga_8511', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 04:31:40', 0, '2013-01-08 11:31:40', 1, ''),
('61050630', 'jamidin_6105', 'jamidin_6105', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 04:35:11', 0, '2013-01-08 11:35:11', 1, ''),
('73100116', 'puji_7310', 'puji_7310', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 04:48:28', 0, '2013-01-08 11:48:28', 1, ''),
('75010800', 'budi_7501', 'budi_7501', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 04:58:20', 0, '2013-01-08 11:58:20', 1, ''),
('77120254', 'widiyanto_7712', 'widiyanto_7712', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 05:01:43', 0, '2013-01-08 12:01:43', 1, ''),
('81050206', 'ika_8105', 'ika_8105', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 05:15:44', 0, '2013-01-08 12:15:44', 1, ''),
('80040944', 'harianto_8004', 'harianto_8004', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 05:18:58', 0, '2013-01-08 12:18:58', 1, ''),
('84040083', 'jeni_8404', 'jeni_8404', 10, 'images/profile.jpg', 'images/profile.jpg', '2013-01-08 05:21:22', 0, '2013-01-08 12:21:22', 1, ''),
('81050205', 'solehpudin_8105', 'solehpudin_8105', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-21 07:39:29', 0, '2013-02-21 14:39:29', 1, ''),
('86101927', 'i made pramesetia_8610', 'i made pramesetia_8610', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-09-24 02:25:58', 0, '2013-09-24 09:25:58', 1, ''),
('85111910', 'ida_8511', 'ida_8511', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-12 06:41:25', 0, '2013-03-04 15:49:12', 1, ''),
('77090204', 'bianto_7709', 'bianto_7709', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 04:05:03', 0, '2013-02-08 11:05:03', 1, ''),
('74020080', 'susanto_7402', 'susanto_7402', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 02:51:27', 0, '2013-02-08 09:51:27', 1, ''),
('75030060', 'agus_7503', 'agus_7503', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 02:54:31', 0, '2013-02-08 09:54:31', 1, ''),
('72020299', 'didik_7202', 'didik_7202', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 02:56:29', 0, '2013-02-08 09:56:29', 1, ''),
('76020594', 'edy_7602', 'edy_7602', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:00:43', 0, '2013-02-08 10:00:43', 1, ''),
('81120229', 'ismail_8112', 'ismail_8112', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:02:58', 0, '2013-02-08 10:02:58', 1, ''),
('82030115', 'ratno_8203', 'ratno_8203', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:06:08', 0, '2013-02-08 10:06:08', 1, ''),
('85121486', 'dedy_8512', 'dedy_8512', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:08:27', 0, '2013-02-08 10:08:27', 1, ''),
('73110356', 'nur_7311', 'nur_7311', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:11:22', 0, '2013-02-08 10:11:22', 1, ''),
('75110869', 'abdul_7511', 'abdul_7511', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:18:13', 0, '2013-02-08 10:18:13', 1, ''),
('82020558', 'hafid_8202', 'hafid_8202', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:20:44', 0, '2013-02-08 10:20:44', 1, ''),
('76120449', 'nur_7612', 'nur_7612', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-09-12 06:23:42', 0, '2013-02-08 11:07:50', 1, ''),
('74120251', 'tugimin_7412', 'tugimin_7412', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:31:32', 0, '2013-02-08 10:31:32', 1, ''),
('83070755', 'agung_8307', 'agung_8307', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:34:28', 0, '2013-02-08 10:34:28', 1, ''),
('84120733', 'hendro_8412', 'hendro_8412', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:49:10', 0, '2013-02-08 10:49:10', 1, ''),
('83070268', 'riyanto_8307', 'riyanto_8307', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-08 03:50:43', 0, '2013-02-08 10:50:43', 1, ''),
('72060502', 'beni_7206', 'beni_7206', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-02-14 06:22:51', 0, '2013-02-14 13:22:51', 1, ''),
('77070363', 'hariadi_7707', 'hariadi_7707', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-07-27 07:38:05', 0, '2013-02-14 13:23:02', 0, ''),
('76080168', 'muhamad_7608', 'muhamad_7608', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-08-22 08:39:59', 0, '2013-02-25 16:29:40', 1, ''),
('65020451', 'sutamaji_6502', 'sutamaji_6502', 11, 'images/profile.jpg', 'images/profile.jpg', '2013-08-22 08:40:12', 0, '2013-05-07 11:02:41', 1, ''),
('22223333', 'tes_2222', 'tes_2222', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-06-18 08:07:02', 0, '2013-06-15 12:23:34', 1, ''),
('82050180', 'suharianto_8205', 'suharianto_8205', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-08-21 03:24:28', 0, '2013-08-21 10:24:28', 1, ''),
('72080361', 'agus_7208', 'agus_7208', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-08-05 08:30:43', 0, '2013-08-05 15:30:43', 1, ''),
('83081651', 'ardi_8308', 'ardi_8308', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-06-24 08:44:20', 0, '2013-06-24 15:43:28', 1, ''),
('74100259', 'i.k._7410', 'i.k._7410', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-08-28 03:38:49', 0, '2013-08-28 10:38:49', 1, ''),
('83061471', 'budi_8306', 'budi_8306', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-09-12 07:33:02', 0, '2013-09-12 14:33:02', 1, ''),
('73110230', 'l._7311', 'l._7311', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2014-02-13 10:54:34', 1, ''),
('72110077', 'gunawan_7211', 'gunawan_7211', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-09-14 04:49:47', 0, '2013-09-12 15:02:01', 1, ''),
('90050361', 'farouk_9005', 'farouk_9005', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-11-12 07:30:22', 0, '2013-11-12 14:28:20', 1, ''),
('ksd2slsl03', 'KASUBDIT_II_NARKOBA_SULSEL', 'KASUBDIT_II_NARKOBA_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('82051240', 'ARIF_8205', 'ARIF_8205', 8, 'images/profile.jpg', 'images/profile.jpg', '2013-09-24 09:09:25', 0, '2012-12-14 09:50:36', 0, ''),
('86101367', 'SUHERMANTO_8610', 'SUHERMANTO_8610', 8, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 02:50:00', 0, '2012-12-14 09:51:06', 1, ''),
('73080531', 'agus_7308', 'agus_7308', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-09-14 05:04:05', 0, '2012-09-29 17:09:28', 0, ''),
('79010049', 'TEGUH SETIAWAN', '79010049', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-09-02 12:24:38', 1, ''),
('63110325', 'sudamiran_6311', 'sudamiran_6311', 2, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('86021547', 'HENRY_8602', 'HENRY_8602', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-01-21 10:21:46', 0, ''),
('86021549', 'FERY_8602', 'FERY_8602', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-10-05 10:22:25', 0, '2012-10-05 17:22:25', 1, ''),
('74030349', 'soekris_7403', 'SEAN', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2014-02-08 15:12:09', 1, ''),
('80070183', 'yayuk_8007', 'yayuk_8007', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-01-04 07:38:43', 0, '2013-01-04 14:29:36', 1, ''),
('78010167', 'irwan_7801', 'irwan_7801', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-02-06 04:33:11', 0, '2012-09-29 17:09:28', 1, ''),
('67060198', 'BRIPKA WARNO', '67060198', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-10-23 16:38:28', 1, ''),
('86120660', 'tri_8612', 'tri_8612', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-12-10 03:57:11', 0, '2012-12-10 10:39:06', 1, ''),
('80050017', 'BRIPKA ARIEF', '80050017080580', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-12-30 10:35:52', 0, '2013-12-30 17:32:45', 1, ''),
('63050475', 'sutrisno_6305', 'sutrisno_6305', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-09-30 16:35:46', 1, ''),
('79100115', 'suhartono_7910', 'suhartono_7910', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-06-18 03:47:56', 0, '2013-06-18 10:42:24', 1, ''),
('79120472', 'roni_7912', 'roni_7912', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-01-21 03:02:43', 0, '2013-01-19 09:56:09', 1, ''),
('65090120', 'AIPTU KETUT', '65090120', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-05-10 13:28:29', 0, ''),
('66080689', 'jupri_6608', 'jupri_6608', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-12-08 03:55:44', 0, '2012-12-08 10:53:51', 1, ''),
('65070379', 'JULIAN WAROKKA', 'PEJUDO', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-06-18 03:42:02', 0, '2013-06-18 10:35:56', 1, ''),
('90100054', 'JULIA AJENG P.', 'PLANKTON90', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-07-10 09:16:40', 0, '2013-07-10 16:10:59', 1, ''),
('76040038', 'iwan_7604', 'iwan_7604', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-11-25 07:02:20', 0, '2012-09-29 17:09:28', 0, 'APA91bHOwSGZnjkXsHAmlY_ATfFbJEchIe8sJ_tDf5WNQL_DYi-qTGjvzAEKGMlMNcfb8bPK4zPLnV4bKhhdjJS_PPvkXHZ0SyDnxXeyOOZDjoUuo4G7F7T3ccRC5tgJO01MwncU8Yle'),
('79030451', 'indra_7903', 'indra_7903', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-12-12 02:00:00', 0, '2012-12-12 08:42:14', 1, ''),
('70120078', 'AIPTU HENDRO', '70120078', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-05-16 09:13:51', 1, ''),
('85090708', 'andhyka_8509', 'andhyka_8509', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-06-05 05:44:40', 0, '2013-06-05 12:04:37', 1, ''),
('62050671', 'enny_6205', 'enny_6205', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-12-06 08:36:13', 0, '2012-12-06 15:36:13', 1, ''),
('61040421', 'hariyanto_6104', 'hariyanto_6104', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-01-21 14:48:31', 1, ''),
('66070583', 'suwono_6607', 'suwono_6607', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-11-19 07:49:20', 0, '2013-09-30 11:34:24', 0, ''),
('67080153', 'agus_6708', 'agus_6708', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-07-12 11:21:52', 1, ''),
('60120702', 'hadi_6012', 'hadi_6012', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-06-25 11:50:20', 1, ''),
('91090068', 'endah_9109', 'endah_9109', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-03 03:15:27', 0, '2014-02-03 10:10:48', 1, ''),
('91020021', 'fanani_9102', 'fanani_9102', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-02-14 09:29:37', 1, ''),
('86010613', 'vicky_8601', 'vicky_8601', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-03-06 05:27:25', 0, '2013-03-06 12:10:15', 1, ''),
('86050431', 'andini_8605', 'andini_8605', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-01-22 08:06:20', 0, '2014-01-22 14:40:45', 1, ''),
('85050205', 'priyantini_8505', 'priyantini_8505', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 04:16:24', 0, '2013-01-07 11:00:32', 1, ''),
('82080179', 'i ketut rapi, s.h._8208', 'i ketut rapi, s.h._8208', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-12-06 09:27:26', 0, '2012-12-06 16:09:24', 1, ''),
('79050752', 'YULI MUJI LESTARI', 'YULIVANIA', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-05-20 14:03:17', 1, ''),
('77010393', 'yudi_7701', 'yudi_7701', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-01-17 05:48:56', 0, '2014-01-17 12:47:12', 1, ''),
('77080428', 'zainul_7708', 'zainul_7708', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-07-27 07:32:46', 0, '2012-09-29 17:09:28', 0, ''),
('71020147', 'gogot_7102', 'gogot_7102', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-12-04 10:26:22', 1, ''),
('69050291', 'm rochib_6905', 'm rochib_6905', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-02-27 09:29:37', 1, ''),
('66120146', 'budi_6612', 'budi_6612', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-01-23 03:12:37', 0, '2014-01-23 10:11:59', 1, ''),
('65020372', 'wiyono_6502', 'wiyono_6502', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-12-04 10:17:52', 1, ''),
('75110500', 'RUTH YENI', 'RUTH_7511', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-01-23 03:11:48', 0, '2014-01-23 10:10:55', 1, ''),
('62090682', 'trihartono_6209', 'trihartono_6209', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-20 15:37:49', 0, '2013-02-28 16:08:00', 1, ''),
('66030539', 'suwito_6603', 'suwito_6603', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-28 02:32:38', 0, '2013-09-28 09:22:08', 1, ''),
('75010397', 'SUBIYANTANA7501', 'SUBIYANTANA7501', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-04-17 06:44:47', 0, '2013-04-17 13:41:22', 1, ''),
('90040136', 'nanda_9004', 'nanda_9004', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-24 03:57:22', 0, '2013-09-24 10:32:59', 1, ''),
('82020427', 'purwo_8202', 'purwo_8202', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-12-17 07:03:14', 0, '2013-12-17 13:52:09', 1, ''),
('75040159', 'nanang_7504', 'nanang_7504', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-12-08 04:52:38', 0, '2012-12-08 11:52:35', 1, ''),
('59051036', 'fathol_5905', 'fathol_5905', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-20 07:29:03', 0, '2016-09-20 14:28:58', 1, ''),
('71120167', 'edy_7112', 'edy_7112', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-07-02 02:56:35', 0, '2013-07-02 09:49:03', 1, ''),
('75120917', 'djarot_7512', 'djarot_7512', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-06 07:47:51', 0, '2013-09-06 14:46:43', 1, ''),
('88040775', 'dedy_8804', 'dedy_8804', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-01-28 02:34:26', 0, '2014-01-28 09:31:19', 1, ''),
('70100267', 'johson_7010', 'johson_7010', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-07-27 07:36:04', 0, '2013-06-14 12:34:39', 0, ''),
('74080085', 'edy_7408', 'edy_7408', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-09-27 16:18:45', 1, ''),
('73070337', 'ARSYAD_7307', 'ARSYAD_7307', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-03-05 09:59:30', 1, ''),
('72070660', 'nanang_7207', 'nanang_7207', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-01-09 02:26:24', 0, '2014-01-09 09:25:52', 1, ''),
('89090544', 'rahma_8909', 'rahma_8909', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-06-27 10:52:37', 0, '2013-06-27 17:46:40', 1, ''),
('74030404', 'hery_7403', 'hery_7403', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-03-01 10:38:33', 1, ''),
('81090580', 'vian_8109', 'vian_8109', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-10-01 10:39:04', 0, '2013-10-01 17:31:34', 1, ''),
('82110216', 'BRIGADIR EKO. N', 'EKO_8211', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-14 03:14:06', 0, '2013-09-14 10:02:18', 1, ''),
('78080797', 'agus_7808', 'agus_7808', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-01-13 09:02:38', 0, '2014-01-13 15:59:38', 1, ''),
('75120336', 'nur_7512', 'nur_7512', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-09-25 09:10:41', 1, ''),
('74120501', 'ainur_7412', 'ainur_7412', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-11-28 09:26:10', 1, ''),
('73110188', 'AIPDA GHEK_7311', 'AIPDA GHEK_7311', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-04-08 14:35:37', 1, ''),
('70090252', 'shokib_7009', 'shokib_7009', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-03-05 14:29:28', 1, ''),
('61030726', 'untung_6103', 'untung_6103', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-05-17 11:21:06', 1, ''),
('88110786', 'novi_8811', 'novi_8811', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-11-11 00:53:23', 0, '2014-01-20 11:29:50', 1, ''),
('64060853', 'tri_6406', 'tri_6406', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-02-11 09:32:15', 0, '2013-02-11 16:29:45', 1, ''),
('65120312', 'harun_6512', 'harun_6512', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-11-19 07:49:34', 0, '2013-05-21 15:14:12', 0, ''),
('69060213', 'santa_6906', 'santa_6906', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-08 15:36:50', 0, '2016-09-08 22:34:43', 1, ''),
('81010711', 'herry_8101', 'herry_8101', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-14 04:56:19', 0, '2013-09-06 15:19:23', 0, ''),
('60080619', 'sugeng_6008', 'sugeng_6008', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-02-11 09:29:18', 0, '2013-02-11 16:27:58', 1, ''),
('62110478', 'dwi_6211', 'dwi_6211', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-01-28 06:00:21', 0, '2013-01-28 12:59:45', 1, ''),
('79070028', 'iskak_7907', 'iskak_7907', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('76110669', 'Santi_7611', 'SHINDY', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-03-24 10:15:43', 0, '2014-03-24 17:15:37', 1, ''),
('74010595', 'FARMAN_7401', 'FARMAN_7401', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-06-07 08:48:56', 1, ''),
('69050093', 'edy_6905', 'edy_6905', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-27 11:07:11', 0, '2013-09-27 18:05:41', 1, ''),
('86031894', 'ricky_8603', 'ricky_8603', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-06-19 03:19:16', 0, '2012-10-05 14:54:34', 0, ''),
('87021330', 'MR. RULIAN', 'RULIAN', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2014-02-10 14:03:45', 1, ''),
('64110458', 'amrin_6411', 'amrin_6411', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-11-11 14:43:12', 0, '2014-01-30 13:56:16', 1, ''),
('67060079', 'hasyim_6706', 'hasyim_6706', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-01-29 10:36:23', 0, '2013-01-29 17:34:27', 1, ''),
('68020259', 'hery_6802', 'hery_6802', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-05-23 01:04:59', 0, '2013-05-23 08:04:47', 1, ''),
('72020300', 'komar_7202', 'komar_7202', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-09-23 06:59:58', 0, '2013-09-23 13:58:38', 1, ''),
('73060240', 'roni_7306', 'roni_7306', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-05-27 10:18:13', 0, '2013-05-27 17:12:43', 1, ''),
('73060468', 'suwoto_7306', 'suwoto_7306', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2014-02-10 11:48:24', 1, ''),
('77060253', 'daryanto_7706', 'daryanto_7706', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-01-29 09:02:41', 0, '2014-01-29 15:28:32', 1, ''),
('82120401', 'tony_8212', 'tony_8212', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-06-12 05:01:58', 0, '2013-06-12 12:01:48', 1, ''),
('78010182', 'catur_7801', 'catur_7801', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-04 07:09:20', 0, '2014-02-04 14:04:01', 1, ''),
('78110509', 'dedik_7811', 'dedik_7811', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-01-28 05:59:28', 0, '2013-01-28 12:58:38', 1, ''),
('79080410', 'igusti_7908', 'igusti_7908', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-11 10:11:50', 0, '2014-02-11 17:02:17', 1, ''),
('87050561', 'Suci_8705', 'ROMAN', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-12-20 14:22:11', 1, ''),
('74110214', 'I GEDE KANIAKA, SH', '818181', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2014-01-21 09:39:21', 1, ''),
('91100020', 'Levina_9110', 'Levina_9110', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-12-20 07:45:07', 1, ''),
('83080829', 'panggah._8308', 'panggah._8308', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2014-02-08 12:02:38', 1, ''),
('77040571', 'MBINK', 'BIMO', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-02-12 11:29:10', 1, ''),
('82090897', 'priyo_8209', 'priyo_8209', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-11-19 08:46:54', 1, ''),
('67070156', 'didiek_6707', 'didiek_6707', 4, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('68090013', 'aris_6809', 'aris_6809', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-03-26 11:39:07', 1, ''),
('91080008', 'WISSANG', 'MENDEM', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-04-22 06:28:53', 0, '2014-04-22 13:10:49', 1, ''),
('76050189', 'ANDI_7605', 'ADINDA191112', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-07-22 10:10:25', 1, ''),
('59050196', 'Pardi_5905', 'Pardi_5905', 4, 'images/profile.jpg', 'images/profile.jpg', '2013-03-26 03:43:29', 0, '2013-03-26 10:43:13', 1, ''),
('66090508', 'SUPENO_6609', 'SUPENO_6609', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-03-27 17:54:02', 1, ''),
('76060645', 'Cucun_7606', 'Cucun_7606', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-06-22 23:47:04', 0, '2014-05-12 01:58:57', 1, ''),
('65080070', 'Yulis_6508', 'Yulis_6508', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-01-23 09:54:52', 1, ''),
('69120475', 'DANS_MIKO', 'DANS_MIKO', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-02-01 09:32:49', 1, ''),
('64120291', 'Sugiana_6412', 'Sugiana_6412', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-07 06:46:39', 0, '2014-02-07 13:43:06', 1, ''),
('84041981', 'yunus_8404', 'yunus_8404', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-12-09 12:01:54', 0, '2012-12-09 19:00:32', 1, ''),
('78111155', 'Roman_7811', 'Roman_7811', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('63070786', 'Isbari_6307', 'Isbari_6307', 3, 'images/profile.jpg', 'images/profile.jpg', '2013-01-10 02:45:22', 0, '2013-01-10 09:33:46', 1, ''),
('79101234', 'KANITPIDTER', 'TRIOKTA', 3, 'images/profile.jpg', 'images/profile.jpg', '2013-04-03 03:34:22', 0, '2013-04-03 10:32:11', 1, ''),
('65070578', 'suratmi_6507', 'suratmi_6507', 3, 'images/profile.jpg', 'images/profile.jpg', '2014-01-24 03:12:28', 0, '2014-01-24 10:12:21', 1, ''),
('63110304', 'Agung_6311', 'Agung_6311', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('62110544', 'muljoto_6211', 'muljoto_6211', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-04-08 14:40:36', 1, ''),
('65040292', 'sri_6504', 'sri_6504', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-02-13 12:12:08', 0, '2013-01-29 09:41:43', 1, ''),
('65120789', 'ardita_6512', 'ardita_6512', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-29 04:26:51', 0, '2016-09-29 11:23:31', 1, ''),
('84070472', 'JOKO_8407', 'JOKO_8407', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('87030324', 'WIRADI_8703', 'WIRADI_8703', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 03:41:56', 0, '2016-01-27 09:01:58', 1, ''),
('ksd2slsl01', 'KASUBDIT_II_KRIMUM_SULSEL', 'KASUBDIT_II_KRIMUM_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ku44slsl02', 'KANIT_IV_SUBDIT_IV_KRIMSUS_SULSEL', 'KANIT_IV_SUBDIT_IV_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:16', 0, '2014-03-12 23:32:27', 1, ''),
('ku24slsl01', 'KANIT_II_SUBDIT_IV_KRIMUM_SULSEL', 'KANIT_II_SUBDIT_IV_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku23slsl03', 'KANIT_II_SUBDIT_III_NARKOBA_SULSEL', 'KANIT_II_SUBDIT_III_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('76120087', 'artha_7612', 'artha_7612', 4, 'images/profile.jpg', 'images/profile.jpg	', '2016-06-23 13:48:23', 0, '2016-06-23 20:48:23', 1, ''),
('saslsl03', 'ADMIN_NARKOBA_SULSEL', 'ADMIN_NARKOBA_SULSEL', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('saslsl02', 'ADMIN_KRIMSUS_SULSEL', 'ADMIN_KRIMSUS_SULSEL', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('wdirslsl04', 'WADIRLANTAS_SULSEL', 'WADIRLANTAS_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, '');
INSERT INTO `tb_user` (`ID_USER`, `USER_NAME`, `PASSWORD`, `TIPE_USER`, `FOTO`, `FOTO_BIG`, `tgl_masuk_data`, `status_login`, `chat_last_activity`, `STATUS_AKTIF`, `user_regId`) VALUES
('wdirslsl03', 'WADIRRESNARKOBA_SULSEL', 'WADIRRESNARKOBA_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('wdirslsl02', 'WADIRRESKRIMSUS_SULSEL', 'WADIRRESKRIMSUS_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('wdirslsl01', 'WADIRRESKRIMUM_SULSEL', 'WADIRRESKRIMUM_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('ksd4slsl02', 'KASUBDIT_IV_KRIMSUS_SULSEL', 'KASUBDIT_IV_KRIMSUS_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd4slsl01', 'KASUBDIT_IV_KRIMUM_SULSEL', 'KASUBDIT_IV_KRIMUM_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd3slsl03', 'KASUBDIT_III_NARKOBA_SULSEL', 'KASUBDIT_III_NARKOBA_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd2slsl02', 'KASUBDIT_II_KRIMSUS_SULSEL', 'KASUBDIT_II_KRIMSUS_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd1slsl04', 'KASUBDIT_I_LANTAS_SULSEL', 'KASUBDIT_I_LANTAS_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ksd1slsl02', 'KASUBDIT_I_KRIMSUS_SULSEL', 'KASUBDIT_I_KRIMSUS_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('kpolslsl', 'KAPOLDA_SULSEL', 'KAPOLDA_SULSEL', 12, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('ku44slsl01', 'KANIT_IV_SUBDIT_IV_KRIMUM_SULSEL', 'KANIT_IV_SUBDIT_IV_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:16', 0, '2014-03-12 23:32:27', 1, ''),
('ku34slsl02', 'KANIT_III_SUBDIT_IV_KRIMSUS_SULSEL', 'KANIT_III_SUBDIT_IV_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku24slsl02', 'KANIT_II_SUBDIT_IV_KRIMSUS_SULSEL', 'KANIT_II_SUBDIT_IV_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku14slsl03', 'KANIT_I_SUBDIT_IV_NARKOBA_SULSEL', 'KANIT_I_SUBDIT_IV_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ku14slsl01', 'KANIT_I_SUBDIT_IV_KRIMUM_SULSEL', 'KANIT_I_SUBDIT_IV_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ku43slsl03', 'KANIT_IV_SUBDIT_III_NARKOBA_SULSEL', 'KANIT_IV_SUBDIT_III_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku33slsl01', 'KANIT_III_SUBDIT_III_KRIMUM_SULSEL', 'KANIT_III_SUBDIT_III_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku23slsl02', 'KANIT_II_SUBDIT_III_KRIMSUS_SULSEL', 'KANIT_II_SUBDIT_III_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku23slsl01', 'KANIT_II_SUBDIT_III_KRIMUM_SULSEL', 'KANIT_II_SUBDIT_III_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku13slsl03', 'KANIT_I_SUBDIT_III_NARKOBA_SULSEL', 'KANIT_I_SUBDIT_III_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku12slsl01', 'KANIT_I_SUBDIT_II_KRIMUM_SULSEL', 'KANIT_I_SUBDIT_II_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku41slsl03', 'KANIT_IV_SUBDIT_I_NARKOBA_SULSEL', 'KANIT_IV_SUBDIT_I_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('aspkslsl', 'Admin_SPKT_SULSEL', 'Admin_SPKT_SULSEL', 9, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2012-09-29 17:09:28', 1, ''),
('kspkslsl', 'Kepala_SPKT_SULSEL', 'Kepala_SPKT_SULSEL', 9, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2012-09-29 17:09:28', 1, ''),
('asd1slsl01', 'ADMIN_SUBDIT_I_KRIMUM_SULSEL', 'ADMIN_SUBDIT_I_KRIMUM_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd1slsl02', 'ADMIN_SUBDIT_I_KRIMSUS_SULSEL', 'ADMIN_SUBDIT_I_KRIMSUS_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd1slsl03', 'ADMIN_SUBDIT_I_NARKOBA_SULSEL', 'ADMIN_SUBDIT_I_NARKOBA_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd1slsl04', 'ADMIN_SUBDIT_I_LANTAS_SULSEL', 'ADMIN_SUBDIT_I_LANTAS_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd2slsl01', 'ADMIN_SUBDIT_II_KRIMUM_SULSEL', 'ADMIN_SUBDIT_II_KRIMUM_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd2slsl02', 'ADMIN_SUBDIT_II_KRIMSUS_SULSEL', 'ADMIN_SUBDIT_II_KRIMSUS_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd2slsl03', 'ADMIN_SUBDIT_II_NARKOBA_SULSEL', 'ADMIN_SUBDIT_II_NARKOBA_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd3slsl01', 'ADMIN_SUBDIT_III_KRIMUM_SULSEL', 'ADMIN_SUBDIT_III_KRIMUM_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd3slsl02', 'ADMIN_SUBDIT_III_KRIMSUS_SULSEL', 'ADMIN_SUBDIT_III_KRIMSUS_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd3slsl03', 'ADMIN_SUBDIT_III_NARKOBA_SULSEL', 'ADMIN_SUBDIT_III_NARKOBA_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd4slsl01', 'ADMIN_SUBDIT_IV_KRIMUM_SULSEL', 'ADMIN_SUBDIT_IV_KRIMUM_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd4slsl02', 'ADMIN_SUBDIT_IV_KRIMSUS_SULSEL', 'ADMIN_SUBDIT_IV_KRIMSUS_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('asd4slsl03', 'ADMIN_SUBDIT_IV_NARKOBA_SULSEL', 'ADMIN_SUBDIT_IV_NARKOBA_SULSEL', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('kdirslsl01', 'DIRRESKRIMUM_SULSEL', 'DIRRESKRIMUM_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('kdirslsl02', 'DIRRESKRIMSUS_SULSEL', 'DIRRESKRIMSUS_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('kdirslsl03', 'DIRRESNARKOBA_SULSEL', 'DIRRESNARKOBA_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('kdirslsl04', 'DIRLANTAS_SULSEL', 'DIRLANTAS_SULSEL', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('ku11slsl01', 'KANIT_I_SUBDIT_I_KRIMUM_SULSEL', 'KANIT_I_SUBDIT_I_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku11slsl02', 'KANIT_I_SUBDIT_I_KRIMSUS_SULSEL', 'KANIT_I_SUBDIT_I_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku11slsl03', 'KANIT_I_SUBDIT_I_NARKOBA_SULSEL', 'KANIT_I_SUBDIT_I_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku11slsl04', 'KANIT_I_SUBDIT_I_LANTAS_SULSEL', 'KANIT_I_SUBDIT_I_LANTAS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku21slsl01', 'KANIT_II_SUBDIT_I_KRIMUM_SULSEL', 'KANIT_II_SUBDIT_I_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku21slsl02', 'KANIT_II_SUBDIT_I_KRIMSUS_SULSEL', 'KANIT_II_SUBDIT_I_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku21slsl03', 'KANIT_II_SUBDIT_I_NARKOBA_SULSEL', 'KANIT_II_SUBDIT_I_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku21slsl04', 'KANIT_II_SUBDIT_I_LANTAS_SULSEL', 'KANIT_II_SUBDIT_I_LANTAS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:47', 0, '2014-03-12 23:32:27', 1, ''),
('ku31slsl01', 'KANIT_III_SUBDIT_I_KRIMUM_SULSEL', 'KANIT_III_SUBDIT_I_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku31slsl02', 'KANIT_III_SUBDIT_I_KRIMSUS_SULSEL', 'KANIT_III_SUBDIT_I_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('88095643', 'irman_8809', 'irman_8809', 4, 'images/profile.jpg', 'images/profile.jpg	', '2016-07-18 05:57:53', 0, '2016-07-18 12:00:06', 1, ''),
('57110105', 'BURHANUDIN_5711', 'BURHANUDIN_5711', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('59040830', 'MARYAM_5904', 'MARYAM_5904', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('62020611', 'SIAN_6202', 'SIAN_6202', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('64080833', 'SUINAH_6408', 'SUINAH_6408', 3, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('66070308', 'PIETMON_6607', 'PIETMON_6607', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('67050428', 'GAGAS_6705', 'GAGAS_6705', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('67120289', 'LESMAN_6712', 'LESMAN_6712', 6, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('69040058', 'SUDARSIH_6904', 'SUDARSIH_6904', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('70020384', 'IWAN_7002', 'IWAN_7002', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('72120650', 'WISNU_7212', 'WISNU_7212', 3, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('74050737', 'EKO_7405', 'EKO_7405', 3, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('74070155', 'IRWAN_7407', 'IRWAN_7407', 10, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('75050718', 'ZULMAN_7505', 'ZULMAN_7505', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('77010544', 'MUHAMMAD_7701', 'MUHAMMAD_7701', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('77100614', 'PARLIN_7710', 'PARLIN_7710', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('78030353', 'ISMET_7803', 'ISMET_7803', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:48', 0, '2014-09-26 09:28:53', 1, ''),
('79051113', 'AHMAT_7905', 'AHMAT_7905', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('80100383', 'HANDONO_8010', 'HANDONO_8010', 10, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('81020791', 'FEBRIYAN_8102', 'FEBRIYAN_8102', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('81090609', 'NGATASI_8109', 'NGATASI_8109', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('82030981', 'RUDIANTO_8203', 'RUDIANTO_8203', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('82051578', 'SUKARNA_8205', 'SUKARNA_8205', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('82100362', 'MAMAN_8210', 'MAMAN_8210', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('82110679', 'FERRON_8211', 'FERRON_8211', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('83041321', 'SOLIHIN_8304', 'SOLIHIN_8304', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('83050470', 'MEIYANTOW_8305', 'MEIYANTOW_8305', 10, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('84061339', 'MARWANDI_8406', 'MARWANDI_8406', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-29 04:21:13', 0, '2016-09-29 11:09:57', 1, ''),
('84090171', 'DIAN_8409', 'DIAN_8409', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('84120269', 'PRANTI_8412', 'PRANTI_8412', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('85041174', 'DEDI_8504', 'DEDI_8504', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('85041343', 'ARIEF_8504', 'ARIEF_8504', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-07-22 11:26:01', 1, '2016-07-22 18:26:01', 1, ''),
('85051301', 'MELKI_8505', 'MELKI_8505', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('85080702', 'ANES_8508', 'ANES_8508', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-28 07:20:52', 0, '2014-03-12 23:32:27', 1, ''),
('85091077', 'MOHAMAD_8509', 'MOHAMAD_8509', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('85110758', 'RONAL_8511', 'RONAL_8511', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('86081496', 'WISNAWATI_8608', 'WISNAWATI_8608', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('86121164', 'DENIS_8612', 'DENIS_8612', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('87030855', 'DANNY_8703', 'DANNY_8703', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('87041629', 'DEDDY_8704', 'DEDDY_8704', 10, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('87070519', 'JULIUS_8707', 'JULIUS_8707', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('87110328', 'ARISTIA_8711', 'ARISTIA_8711', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('87110389', 'IRAH_8711', 'IRAH_8711', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('88080795', 'HADI_8808', 'HADI_8808', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('89030050', 'HASAN_8903', 'HASAN_8903', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('89030395', 'MUH_8903', 'MUH_8903', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('89040432', 'VICTOR_8904', 'VICTOR_8904', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('89060465', 'INDRA_8906', 'INDRA_8906', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('89060706', 'DEVI_8906', 'DEVI_8906', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('89090189', 'IKHSAN_8909', 'IKHSAN_8909', 8, 'images/profile.jpg', 'images/profile.jpg', '2014-09-28 07:20:04', 0, '2014-03-12 23:32:27', 1, ''),
('89120520', 'PUTRA_8912', 'PUTRA_8912', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('90060224', 'YUNIARTI_9006', 'YUNIARTI_9006', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('91030140', 'ISWAN_9103', 'ISWAN_9103', 11, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('91070088', 'ZUHLIANDA_9107', 'ZUHLIANDA_9107', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:40:49', 0, '2014-03-12 23:32:27', 1, ''),
('saslsl01', 'ADMIN_KRIMUM_SULSEL', 'ADMIN_KRIMUM_SULSEL', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('wpolslsl', 'WAKAPOLDA_SULSEL', 'WAKAPOLDA_SULSEL', 12, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('87031538', 'COP_LND', '323286_UPHY', 1, 'upload/foto_user/87031538_thumb.png', 'upload/foto_user/ukuran_asli/87031538.png', '2015-12-14 10:04:42', 0, '2015-12-14 17:04:36', 1, ''),
('83101371', 'haryani_8310', 'IRFANDI84', 1, 'upload/foto_user/83101371_thumb.jpg', 'upload/foto_user/ukuran_asli/83101371.jpg', '2016-03-08 07:19:34', 1, '2016-03-08 14:19:34', 1, ''),
('ksd4slsl03', 'KASUBDIT_IV_NARKOBA_SULSEL', 'KASUBDIT_IV_NARKOBA_SULSEL', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:43:46', 0, '2014-03-12 23:32:27', 1, ''),
('ku22slsl01', 'KANIT_II_SUBDIT_II_KRIMUM_SULSEL', 'KANIT_II_SUBDIT_II_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku22slsl02', 'KANIT_II_SUBDIT_II_KRIMSUS_SULSEL', 'KANIT_II_SUBDIT_II_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku22slsl03', 'KANIT_II_SUBDIT_II_NARKOBA_SULSEL', 'KANIT_II_SUBDIT_II_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku32slsl01', 'KANIT_III_SUBDIT_II_KRIMUM_SULSEL', 'KANIT_III_SUBDIT_II_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku32slsl02', 'KANIT_III_SUBDIT_II_KRIMSUS_SULSEL', 'KANIT_III_SUBDIT_II_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku32slsl03', 'KANIT_III_SUBDIT_II_NARKOBA_SULSEL', 'KANIT_III_SUBDIT_II_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku42slsl01', 'KANIT_IV_SUBDIT_II_KRIMUM_SULSEL', 'KANIT_IV_SUBDIT_II_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku42slsl02', 'KANIT_IV_SUBDIT_II_KRIMSUS_SULSEL', 'KANIT_IV_SUBDIT_II_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('ku42slsl03', 'KANIT_IV_SUBDIT_II_NARKOBA_SULSEL', 'KANIT_IV_SUBDIT_II_NARKOBA_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('74040474', 'SYAHRIR_7404', 'SYAHRIR_7404', 2, 'images/profile.jpg', 'images/profile.jpg', '2015-12-19 01:00:48', 1, '2015-12-19 08:00:48', 1, ''),
('79010026', 'HASMAWATI_7901', 'HASMAWATI_7901', 3, 'upload/foto_user/79010026_thumb.jpg', 'upload/foto_user/ukuran_asli/79010026.jpg', '2016-03-10 04:06:16', 1, '2016-03-10 11:06:16', 1, ''),
('80110921', 'REAGAN_8011', 'REAGAN_8011', 11, 'images/profile.jpg', 'images/profile.jpg', '2016-01-07 07:51:04', 0, '2016-01-07 14:40:42', 1, ''),
('88060145', 'NUR_8806', 'NUR_8806', 11, 'upload/foto_user/88060145_thumb.jpg', 'upload/foto_user/ukuran_asli/88060145.jpg', '2016-03-07 07:21:36', 1, '2016-03-07 14:21:36', 1, ''),
('88090454', 'COP_NUR', '88090454_NUR', 1, 'upload/foto_user/88090454_thumb.jpg', 'upload/foto_user/ukuran_asli/88090454.jpg', '2016-02-16 07:02:12', 1, '2016-02-16 14:02:12', 1, ''),
('63020241', 'MALELAK_6302', 'MALELAK_6302', 3, 'upload/foto_user/63020241_thumb.jpg', 'upload/foto_user/ukuran_asli/63020241.jpg', '2016-03-08 06:14:24', 1, '2016-03-08 13:14:24', 1, ''),
('78050736', 'SYARIFUDDIN_7805', 'SYARIFUDDIN_7805', 8, 'images/profile.jpg', 'images/profile.jpg', '2015-11-24 04:13:28', 0, '2014-11-27 17:36:23', 1, ''),
('79080436', 'ADHI_200879', 'ASO', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-02-25 05:09:57', 1, '2016-02-25 12:09:57', 1, ''),
('79040345', 'SUPARMAN_7904', 'SUPARMAN', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-02-08 15:56:02', 1, '2016-02-08 22:56:02', 1, ''),
('80040784', 'FAISAL_8004', 'FAISAL_8004', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-03-08 07:36:56', 1, '2016-03-08 14:36:56', 1, ''),
('80070890', 'ANWAR_8007', 'ANWAR_8007', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-01-28 18:58:27', 1, '2016-01-29 01:58:27', 1, ''),
('82090432', 'YUSRAN.JAFSUR.MAKKA', 'YJMAKKA', 4, 'upload/foto_user/82090432_thumb.jpg', 'upload/foto_user/ukuran_asli/82090432.jpg', '2016-03-06 22:25:23', 0, '2016-03-07 05:23:02', 1, ''),
('81011118', 'SAKIR_8101', 'MANJANG1', 8, 'upload/foto_user/81011118_thumb.jpg', 'upload/foto_user/ukuran_asli/81011118.jpg', '2016-02-25 13:07:15', 1, '2016-02-25 20:07:15', 1, ''),
('84090992', 'ANDIKA AMIR', 'ANDIKAAMIRTFC', 4, 'upload/foto_user/84090992_thumb.png', 'upload/foto_user/ukuran_asli/84090992.png', '2016-03-07 18:14:08', 1, '2016-03-08 01:14:08', 1, ''),
('86020011', 'ANDI_8602', 'MADAT_03', 8, 'upload/foto_user/86020011_thumb.jpg', 'upload/foto_user/ukuran_asli/86020011.jpg', '2016-01-11 19:31:57', 1, '2016-01-12 02:31:57', 1, ''),
('85040958', 'TAMRIN_8504', 'TAMRIN_8504', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-02-02 05:41:37', 1, '2016-02-02 12:41:37', 1, ''),
('85031408', 'SYAMSURIZAL_8503', 'SYAMSURIZAL_8503', 8, 'upload/foto_user/85031408_thumb.jpg', 'upload/foto_user/ukuran_asli/85031408.jpg', '2016-02-29 18:06:57', 1, '2016-03-01 01:06:57', 1, ''),
('76050952', 'MUH_7605', 'MUH_7605', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-01-13 08:00:22', 1, '2016-01-13 15:00:22', 1, ''),
('87080448', 'MUH_8708', 'PRUSTEQ28', 8, 'upload/foto_user/87080448_thumb.jpg', 'upload/foto_user/ukuran_asli/87080448.jpg', '2016-03-03 09:13:54', 1, '2016-03-03 16:13:54', 1, ''),
('80090229', 'SAINAR_8009', 'SAINAR_8009', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-01-28 15:05:44', 0, '2016-01-28 22:02:43', 1, ''),
('78030939', 'HAERUDDIN_2000', 'HAERUDDIN_2000', 4, 'upload/foto_user/78030939_thumb.jpg', 'upload/foto_user/ukuran_asli/78030939.jpg', '2016-03-05 04:08:06', 0, '2016-03-05 11:06:58', 1, ''),
('83010880', 'MUHAMMAD_8301', 'MUHAMMAD_8301', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-01-22 07:16:31', 0, '2016-01-22 14:09:42', 1, ''),
('85071133', 'GD.SUPRIYADI26', 'GD.SUPRIYADI26', 4, 'upload/foto_user/85071133_thumb.jpg', 'upload/foto_user/ukuran_asli/85071133.jpg', '2016-03-08 03:15:36', 1, '2016-03-08 10:15:36', 1, ''),
('89020394', 'AI_08', 'AI_08', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-02-29 16:41:32', 0, '2016-02-29 23:30:10', 1, ''),
('77120931', 'sulhan._7712', 'sulhan._7712', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-03-04 10:40:41', 1, '2016-02-17 11:40:27', 1, ''),
('77040661', 'ILHAM_7704', 'ILHAM_7704', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-01-08 16:54:32', 0, '2016-01-08 23:36:50', 1, ''),
('83121248', 'ZULKIFLI_8312', 'ZULKIFLI_8312', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-02-02 04:48:55', 0, '2016-01-29 01:34:38', 1, ''),
('70080004', 'SURAHMAN_7008', 'SURAHMAN_7008', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-03-10 01:57:29', 1, '2016-03-10 08:57:29', 1, ''),
('72020396', 'ABDUL_7202', 'ABDUL_7202', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-05 01:01:26', 1, '2016-03-05 08:01:26', 1, ''),
('79041116', 'AMIR_7904', 'AMIR_7904', 4, 'upload/foto_user/79041116_thumb.jpg', 'upload/foto_user/ukuran_asli/79041116.jpg', '2016-03-09 10:06:51', 1, '2016-03-09 17:06:51', 1, ''),
('83010472', 'ASDAR_8301', 'ASDAR22', 4, 'upload/foto_user/83010472_thumb.jpg', 'upload/foto_user/ukuran_asli/83010472.jpg', '2016-09-29 04:29:21', 0, '2016-09-29 11:27:07', 1, ''),
('79070658', 'ACHMAD_7907', 'ACHMAD_7907', 4, 'upload/foto_user/79070658_thumb.jpg', 'upload/foto_user/ukuran_asli/79070658.jpg', '2016-02-24 02:46:12', 1, '2016-02-24 09:46:12', 1, ''),
('76070904', 'YUSUF_7607', 'YUSUF_7607', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-29 03:58:23', 0, '2016-09-29 10:57:32', 1, ''),
('86110955', 'SYAMSU_8611', 'ARJUNAMO', 4, 'upload/foto_user/86110955_thumb.jpg', 'upload/foto_user/ukuran_asli/86110955.jpg', '2016-02-29 02:42:14', 1, '2016-02-29 09:42:14', 1, ''),
('90040431', 'DONNA_9004', 'BALIKPAPAN25041990', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-03-08 06:13:28', 1, '2016-03-08 13:13:28', 1, ''),
('79050846', 'AMIRUDDIN_7905', 'AMIRUDDIN_7905', 4, 'upload/foto_user/79050846_thumb.jpg', 'upload/foto_user/ukuran_asli/79050846.jpg', '2016-03-08 03:18:27', 0, '2016-03-08 10:16:01', 1, ''),
('79040208', 'AKBAR_7904', 'AKBAR_7904', 4, 'upload/foto_user/79040208_thumb.jpg', 'upload/foto_user/ukuran_asli/79040208.jpg', '2016-03-08 02:59:06', 1, '2016-03-08 09:59:06', 1, ''),
('82041138', 'HENDRA_8204', 'HENDRA_8204', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-08 03:43:51', 1, '2016-03-08 10:43:51', 1, ''),
('83050211', 'AWALUDDIN_8305', 'AWALUDDIN_8305', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-09 01:52:44', 0, '2016-09-09 08:52:38', 1, ''),
('80040340', 'ARMAN_8004', 'ARMAN_8004', 8, 'images/profile.jpg', 'images/profile.jpg', '2015-12-29 02:57:37', 0, '2015-12-29 09:55:18', 1, ''),
('83080176', 'RAHMAN_8308', 'RAHMAN_8308', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2014-11-27 17:36:23', 1, ''),
('87110170', 'EKO SULISTYO W', 'EKO_8711', 4, 'upload/foto_user/87110170_thumb.png', 'upload/foto_user/ukuran_asli/87110170.png', '2016-03-08 07:28:36', 1, '2016-03-08 14:28:36', 1, ''),
('89120051', 'SATRIAH_31', 'TURNBACKCRIME', 4, 'upload/foto_user/89120051_thumb.jpg', 'upload/foto_user/ukuran_asli/89120051.jpg', '2016-03-08 06:13:51', 1, '2016-03-08 13:13:51', 1, ''),
('86101817', 'KAHARUDDIN_8610', 'KAHARUDDIN_8610', 4, 'upload/foto_user/86101817_thumb.jpg', 'upload/foto_user/ukuran_asli/86101817.jpg', '2016-03-10 02:58:14', 1, '2016-03-10 09:58:14', 1, ''),
('77030610', 'SARCE_7703', 'SARCE_7703', 3, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2014-11-27 17:36:23', 1, ''),
('79061199', 'SUHARDI_7906', 'SUHARDI_7906', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2014-11-27 17:36:23', 1, ''),
('79101069', 'SAHRIR_7910', 'SAHRIR_7910', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2014-11-27 17:36:23', 1, ''),
('81041132', 'HASRUDDIN_8104', 'HASRUDDIN_8104', 4, 'upload/foto_user/81041132_thumb.jpg', 'upload/foto_user/ukuran_asli/81041132.jpg', '2016-02-09 04:21:55', 0, '2016-02-09 11:13:37', 1, ''),
('86071695', 'ISWAN_8607', 'ISWAN_8607', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-02-12 10:43:52', 0, '2016-02-12 17:36:02', 1, ''),
('86101629', 'DANI 29', 'SATRIANI', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-02-27 01:24:08', 1, '2016-02-27 08:24:08', 1, ''),
('87081538', 'AGUS_8708', 'AGUS_8708', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-02-02 02:19:56', 0, '2016-02-02 09:19:13', 1, ''),
('69090210', 'RIDWAN_6909', 'RIDWAN_6909', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-08 02:49:20', 0, '2016-03-07 13:14:44', 1, ''),
('84031192', 'TAMSIL_8403', 'TAMSIL25', 4, 'upload/foto_user/84031192_thumb.jpg', 'upload/foto_user/ukuran_asli/84031192.jpg', '2016-03-09 05:34:13', 0, '2016-03-09 12:33:40', 1, ''),
('85041071', 'ANSAR_8504', 'ANSAR_8504', 4, 'upload/foto_user/85041071_thumb.jpg', 'upload/foto_user/ukuran_asli/85041071.jpg', '2016-03-10 03:23:50', 1, '2016-03-10 10:23:50', 1, ''),
('82051602', 'SURIYATI_8205', 'R4TU82', 4, 'upload/foto_user/82051602_thumb.jpg', 'upload/foto_user/ukuran_asli/82051602.jpg', '2016-01-27 04:23:13', 0, '2016-01-27 11:00:33', 1, ''),
('86051971', 'PUJI_8605', 'PUJI_8605', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-08 06:01:00', 1, '2016-03-08 13:01:00', 1, ''),
('95010161', 'SITTI HAJAR R', 'SITTI_9501', 4, 'upload/foto_user/95010161_thumb.jpg', 'upload/foto_user/ukuran_asli/95010161.jpg', '2016-03-10 01:02:57', 1, '2016-03-10 08:02:57', 1, ''),
('94080562', 'AMANDA_9408', 'AMANDA_9408', 4, 'upload/foto_user/94080562_thumb.jpg', 'upload/foto_user/ukuran_asli/94080562.jpg', '2016-03-08 04:46:47', 0, '2016-03-08 11:46:40', 1, ''),
('88030819', 'ABHEL_WB', 'ABHEL WIDYA BOMANTARA', 10, 'upload/foto_user/88030819_thumb.jpg', 'upload/foto_user/ukuran_asli/88030819.jpg', '2015-12-17 09:46:37', 1, '2015-12-14 14:05:14', 1, ''),
('65090634', 'DEKY_6509', 'DEKY_6509', 10, 'images/profile.jpg', 'images/profile.jpg', '2016-02-26 08:53:25', 1, '2016-02-26 15:53:25', 1, ''),
('82111097', 'ABD HALIM ZAZG', 'ZAZG24', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-09 23:24:37', 0, '2016-03-10 06:19:11', 1, ''),
('85090281', 'SYAMSUL_8509', 'SYAMSUL_8509', 10, 'images/profile.jpg', 'images/profile.jpg', '2014-11-27 10:36:23', 0, '2014-11-27 17:36:23', 1, ''),
('84081297', 'SUTRISNO_8408', 'SUTRISNO_8408', 10, 'upload/foto_user/84081297_thumb.jpg', 'upload/foto_user/ukuran_asli/84081297.jpg', '2016-03-02 06:27:14', 1, '2016-03-02 13:27:14', 1, ''),
('ku13slsl02', 'KANIT_I_SUBDIT_III_KRIMSUS_SULSEL', 'KANIT_I_SUBDIT_III_KRIMSUS_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('95020688', 'ANDIORISHA', '95020688', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-10 03:08:26', 1, '2016-03-10 10:08:26', 1, ''),
('88051001', 'zulfitrah_8805', 'zulfitrah_8805', 8, 'images/profile.jpg', 'images/profile.jpg', '2016-01-27 16:57:34', 0, '2016-01-27 23:54:44', 1, ''),
('93081012', 'SITTY HARDIYANTI F', '01DIDADAKU', 4, 'upload/foto_user/93081012_thumb.jpg', 'upload/foto_user/ukuran_asli/93081012.jpg', '2016-03-07 05:50:02', 0, '2016-03-07 12:36:24', 1, ''),
('94110858', 'yuyun_9411', 'yuyun_9411', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-02-29 03:09:37', 1, '2016-02-29 10:09:37', 1, ''),
('96050054', 'dewi_9605', 'dewi_9605', 4, 'upload/foto_user/96050054_thumb.jpg', 'upload/foto_user/ukuran_asli/96050054.jpg', '2016-03-03 07:14:01', 0, '2016-03-03 13:19:05', 1, ''),
('89010465', 'nihmal_8901', 'nihmal_8901', 4, 'upload/foto_user/89010465_thumb.jpg', 'upload/foto_user/ukuran_asli/89010465.jpg', '2016-03-09 07:33:42', 1, '2016-03-09 14:33:42', 1, ''),
('89040508', 'yusril_8904', 'yusril_8904', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-15 15:00:15', 0, '2016-09-15 22:00:08', 1, ''),
('79081245', 'BURHANUDDIN', '3339', 8, 'upload/foto_user/79081245_thumb.jpg', 'upload/foto_user/ukuran_asli/79081245.jpg', '2016-02-22 07:23:54', 1, '2016-02-22 14:16:49', 1, ''),
('82110157', 'muhammad_8211', 'muhammad_8211', 4, 'upload/foto_user/82110157_thumb.jpg', 'upload/foto_user/ukuran_asli/82110157.jpg', '2016-03-09 00:55:13', 1, '2016-03-09 07:55:13', 1, ''),
('80100874', 'dirhanto_8010', 'dirhanto_8010', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-04 04:25:40', 0, '2016-03-04 11:25:15', 1, ''),
('94120760', 'citra_9412', '111294CITRA', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-10 04:05:25', 1, '2016-03-10 11:05:25', 1, ''),
('95020773', 'febry_9502', '07FEB95FRB', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-10 01:01:03', 1, '2016-03-10 08:01:03', 1, ''),
('95040413', 'risnawati_9504', 'risnawati_9504', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-03-10 02:07:11', 1, '2016-03-10 09:07:11', 1, ''),
('ku13slsl01', 'KANIT_I_SUBDIT_III_KRIMUM_SULSEL', 'KANIT_I_SUBDIT_III_KRIMUM_SULSEL', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 14:52:17', 0, '2014-03-12 23:32:27', 1, ''),
('saslsl04', 'ADMIN_LANTAS_SULSEL', 'ADMIN_LANTAS_SULSEL', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-07-16 13:49:20', 0, '2014-03-12 23:32:27', 1, ''),
('sagowa01', 'admin_reskrim_gowa', 'admin_reskrim_gowa', 1, 'images/profile.jpg', 'images/profile.jpg', '2015-12-17 07:16:36', 0, '2012-09-29 17:09:28', 1, ''),
('aspkgowa', 'admin_spkt_gowa', 'admin_spkt_gowa', 9, 'images/profile.jpg', 'images/profile.jpg', '2015-12-17 07:16:36', 0, '2012-09-29 17:09:28', 1, ''),
('kspkgowa', 'kepala_spkt_gowa', 'kepala_spkt_gowa', 9, 'images/profile.jpg', 'images/profile.jpg', '2015-12-17 07:16:36', 0, '2012-09-29 17:09:28', 1, ''),
('wkbogowa01', 'kbo_reskrim_gowa', 'kbo_reskrim_gowa', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-02-03 08:54:01', 0, '2012-12-20 12:49:39', 1, ''),
('kpolgowa', 'kapolres_gowa', 'kapolres_gowa', 12, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ksatgowa01', 'kasatreskrim_gowa', 'kasatreskrim_gowa', 2, 'images/profile.jpg', 'images/profile.jpg', '2015-12-01 05:49:21', 0, '2012-12-20 12:49:39', 1, ''),
('ku10gowa01', 'kanit_I_reskrim_gowa', 'kanit_I_reskrim_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:47:06', 0, '2012-12-14 10:47:06', 1, ''),
('ku20gowa01', 'kanit_II_reskrim_gowa', 'kanit_II_reskrim_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-12-13 04:02:28', 0, '2012-12-13 11:02:28', 1, ''),
('ku30gowa01', 'kanit_III_reskrim_gowa', 'kanit_III_reskrim_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2015-12-22 01:58:29', 0, '2012-09-29 17:09:28', 1, ''),
('ku40gowa01', 'kanit_IV_reskrim_gowa', 'kanit_IV_reskrim_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ku50gowa01', 'kanit_V_reskrim_gowa', 'kanit_V_reskrim_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('wpolgowa', 'wakapolres_gowa', 'wakapolres_gowa', 12, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('au10gowa01', 'admin_unit_I_reskrim_gowa', 'admin_unit_I_reskrim_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('au20gowa01', 'admin_unit_II_reskrim_gowa', 'admin_unit_II_reskrim_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('au30gowa01', 'admin_unit_III_reskrim_gowa', 'admin_unit_III_reskrim_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('au40gowa01', 'admin_unit_IV_reskrim_gowa', 'admin_unit_IV_reskrim_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('au50gowa01', 'admin_unit_V_reskrim_gowa', 'admin_unit_V_reskrim_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('samksr01', 'admin_reskrim_makassar', 'admin_reskrim_makassar', 1, 'images/profile.jpg', 'images/profile.jpg', '2015-11-27 00:46:03', 0, '2015-11-27 07:45:55', 1, ''),
('au20mksr01', 'admin_UNIT_II_RESKRIM_MAKASSAR', 'admin_UNIT_II_RESKRIM_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2013-03-20 05:36:08', 0, '2012-09-29 17:09:28', 1, ''),
('au50mksr01', 'admin_UNIT_V_RESKRIM_MAKASSAR', 'admin_UNIT_V_RESKRIM_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2013-05-30 09:03:33', 0, '2012-12-08 11:53:31', 1, ''),
('au30mksr01', 'admin_UNIT_III_RESKRIM_MAKASSAR', 'admin_UNIT_III_RESKRIM_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2014-01-15 06:34:48', 0, '2012-09-29 17:09:28', 1, ''),
('au10kptg03', 'admin_unit_I_resnarkoba_KP3', 'admin_unit_I_resnarkoba_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au40mksr01', 'admin_UNIT_IV_RESKRIM_MAKASSAR', 'admin_UNIT_IV_RESKRIM_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2013-01-07 04:48:47', 0, '2012-10-05 15:47:12', 1, ''),
('au10mksr01', 'admin_UNIT_I_RESKRIM_MAKASSAR', 'admin_UNIT_I_RESKRIM_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2013-10-16 03:00:09', 0, '2012-11-05 10:30:08', 1, ''),
('kpolmksr', 'kapolrestabes_MAKASSAR', 'kapolrestabes_MAKASSAR', 12, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('wpolmksr', 'wakapolrestabes_MAKASSAR', 'wakapolrestabes_MAKASSAR', 12, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ksatmksr01', 'kasatreskrim_MAKASSAR', 'kasatreskrim_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2012-12-20 05:49:39', 0, '2012-12-20 12:49:39', 1, ''),
('wsatmksr01', 'wakasatreskrim_MAKASSAR', 'wakasatreskrim_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ku10mksr01', 'kanit_I_RESKRIM_MAKASSAR', 'kanit_I_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:47:06', 0, '2012-12-14 10:47:06', 1, ''),
('ku70mksr01', 'kanit_VII_RESKRIM_MAKASSAR', 'kanit_VII_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ku60mksr01', 'kanit_VI_RESKRIM_MAKASSAR', 'kanit_VI_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2013-04-13 02:24:31', 0, '2012-12-20 12:15:26', 1, ''),
('ku20mksr01', 'kanit_II_RESKRIM_MAKASSAR', 'kanit_II_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-12-13 04:02:28', 0, '2012-12-13 11:02:28', 1, ''),
('kU30mksr01', 'kanit_III_RESKRIM_MAKASSAR', 'kanit_III_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-12-03 05:54:55', 0, '2012-09-29 17:09:28', 1, ''),
('kU40mksr01', 'kanit_IV_RESKRIM_MAKASSAR', 'kanit_IV_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('kU50mksr01', 'kanit_V_RESKRIM_MAKASSAR', 'kanit_V_RESKRIM_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks11mksr01', 'kasubnit_I_UNIT_I_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_I_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks21mksr01', 'kasubnit_II_UNIT_I_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_I_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-12-12 08:37:52', 0, '2012-12-12 15:37:52', 1, ''),
('ks31mksr01', 'kasubnit_III_UNIT_I_RESKRIM_MAKASSAR', 'kasubnit_III_UNIT_I_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-12-14 03:49:46', 0, '2012-12-14 10:49:46', 1, ''),
('au10kptg01', 'admin_unit_I_reskrim_KP3', 'admin_unit_I_reskrim_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ku50kptg01', 'kanit_V_reskrim_KP3', 'kanit_V_reskrim_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ku40kptg03', 'kanit_IV_resnarkoba_KP3', 'kanit_IV_resnarkoba_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ku30kptg03', 'kanit_III_resnarkoba_KP3', 'kanit_III_resnarkoba_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ku40kptg01', 'kanit_IV_reskrim_KP3', 'kanit_IV_reskrim_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ks12mksr01', 'kasubnit_I_UNIT_II_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_II_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks22mksr01', 'kasubnit_II_UNIT_II_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_II_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-09-14 05:03:12', 0, '2012-09-29 17:09:28', 0, ''),
('ks32mksr01', 'kasubnit_III_UNIT_II_RESKRIM_MAKASSAR', 'kasubnit__III_UNIT_II_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks13mksr01', 'kasubnit_I_UNIT_III_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_III_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-11-29 10:30:22', 0, '2012-09-29 17:09:28', 1, ''),
('ks23mksr01', 'kasubnit_II_UNIT_III_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_III_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2013-09-20 07:27:37', 0, '2012-09-29 17:09:28', 0, ''),
('ks14mksr01', 'kasubnit_I_UNIT_IV_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_IV_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks24mksr01', 'kasubnit_II_UNIT_IV_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_IV_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks15mksr01', 'kasubnit_I_UNIT_V_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_V_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks25mksr01', 'kasubnit_II_UNIT_V_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_V_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks33mksr01', 'kasubnit_III_UNIT_III_RESKRIM_MAKASSAR', 'kasubnit_III_UNIT_III_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('aspkmksr', 'Admin_SPKT_MAKASSAR', 'Admin_SPKT_MAKASSAR', 9, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', NULL, ''),
('kspkmksr', 'Kepala_SPKT_MAKASSAR', 'Kepala_SPKT_MAKASSAR', 9, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', NULL, ''),
('84041545', 'SUTRISNO_8404', 'SUTRISNO_8404', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('85010877', 'YUSRIN_8502', 'YUSRIN_8502', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('87090769', 'MOH_8709', 'MOH_8709', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('88090726', 'EDI_8809', 'EDI_8809', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('87051706', 'HENDRA_8705', 'HENDRA_8705', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('89050011', 'MOHAMAD_8905', 'MOHAMAD_8905', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('75100924', 'FARNO_7510', 'FARNO_7510', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('66030433', 'HERSADWI_6603', 'HERSADWI_6603', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('68010373', 'FITRIZAL_6801', 'FITRIZAL_6801', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('64120336', 'ALI_6412', 'ALI_6412', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('76061086', 'AMNER_7606', 'AMNER_7606', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('68080316', 'HUSIN_6808', 'HUSIN_6808', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('61010087', 'SULEMAN_6101', 'SULEMAN_6101', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('84091819', 'RONNY_8409', 'RONNY_8409', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('77050679', 'HARISNO_7705', 'HARISNO_7705', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('75010064', 'SUYONO_7501', 'SUYONO_7501', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('81040205', 'ERYO_8104', 'ERYO_8104', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('84040380', 'RONAL_8404', 'RONAL_8404', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('85100981', 'RAHMAT_8510', 'RAHMAT_8510', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-29 03:56:26', 0, '2016-09-29 10:56:01', 1, ''),
('87030164', 'MARTOSONO_8703', 'MARTOSONO_8703', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-20 09:59:23', 0, '2016-09-20 16:58:26', 1, ''),
('86111101', 'RAHMAN_8611', 'RAHMAN_8611', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('83121159', 'EBIT_8312', 'EBIT_8312', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-29 04:29:28', 1, '2016-09-29 11:29:28', 1, ''),
('86030610', 'MANAF_8603', 'MANAF_8603', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('85031692', 'ARFIAN_8503', 'ARFIAN_8503', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('87090301', 'RONAWATY_8709', 'RONAWATY_8709', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('84110646', 'IRWANSYAH_8411', 'IRWANSYAH_8411', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('82100343', 'SILVANA_8210', 'SILVANA_8210', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('85091798', 'RISWAN_8509', 'RISWAN_8509', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('88050189', 'RAMLI_8805', 'RAMLI_8805', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('87100775', 'ILHAM_8710', 'ILHAM_8710', 4, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('86051918', 'FITRIYANTO_8605', 'FITRIYANTO_8605', 4, 'images/profile.jpg', 'images/profile.jpg', '2016-09-15 15:00:01', 0, '2016-09-15 21:59:41', 1, ''),
('86051692', 'KASRUDDIN_8605', 'KASRUDDIN_8605', 2, 'images/profile.jpg', 'images/profile.jpg', '2014-09-26 03:47:31', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowaprlo', 'KEPALA_SPKT_POLSEK_PARANGLOE', 'KEPALA_SPKT_POLSEK_PARANGLOE', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:04', 0, '2014-03-12 23:32:27', 1, ''),
('kugowaprlo', 'KANIT_RESKRIM_POLSEK_PARANGLOE', 'KANIT_RESKRIM_POLSEK_PARANGLOE', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:04', 0, '2014-03-12 23:32:27', 1, ''),
('sagowaprlo', 'ADMIN_RESKRIM_POLSEK_PARANGLOE', 'ADMIN_RESKRIM_POLSEK_PARANGLOE', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:04', 0, '2014-03-12 23:32:27', 1, ''),
('kugowamnju', 'KANIT_RESKRIM_POLSEK_MANUJU', 'KANIT_RESKRIM_POLSEK_MANUJU', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:52', 0, '2014-03-12 23:32:27', 1, ''),
('sagowamnju', 'ADMIN_RESKRIM_POLSEK_MANUJU', 'ADMIN_RESKRIM_POLSEK_MANUJU', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:52', 0, '2014-03-12 23:32:27', 1, ''),
('asgowaplng', 'ADMIN_SPKT_POLSEK_PALLANGGA', 'ADMIN_SPKT_POLSEK_PALLANGGA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:00', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowaplng', 'KAPOLSEK_PALLANGGA', 'KAPOLSEK_PALLANGGA', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:00', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowaplng', 'KEPALA_SPKT_POLSEK_PALLANGGA', 'KEPALA_SPKT_POLSEK_PALLANGGA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:00', 0, '2014-03-12 23:32:27', 1, ''),
('sagowabngy', 'ADMIN_RESKRIM_POLSEK_BUNGAYA', 'ADMIN_RESKRIM_POLSEK_BUNGAYA', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:45', 0, '2014-03-12 23:32:27', 1, ''),
('asgowamnju', 'ADMIN_SPKT_POLSEK_MANUJU', 'ADMIN_SPKT_POLSEK_MANUJU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:52', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowamnju', 'KAPOLSEK_MANUJU', 'KAPOLSEK_MANUJU', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:52', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowamnju', 'KEPALA_SPKT_POLSEK_MANUJU', 'KEPALA_SPKT_POLSEK_MANUJU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:52', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabngy', 'ADMIN_SPKT_POLSEK_BUNGAYA', 'ADMIN_SPKT_POLSEK_BUNGAYA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:45', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabngy', 'KAPOLSEK_BUNGAYA', 'KAPOLSEK_BUNGAYA', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:45', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabngy', 'KEPALA_SPKT_POLSEK_BUNGAYA', 'KEPALA_SPKT_POLSEK_BUNGAYA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:45', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabngy', 'KANIT_RESKRIM_POLSEK_BUNGAYA', 'KANIT_RESKRIM_POLSEK_BUNGAYA', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:45', 0, '2014-03-12 23:32:27', 1, '');
INSERT INTO `tb_user` (`ID_USER`, `USER_NAME`, `PASSWORD`, `TIPE_USER`, `FOTO`, `FOTO_BIG`, `tgl_masuk_data`, `status_login`, `chat_last_activity`, `STATUS_AKTIF`, `user_regId`) VALUES
('sagowabtsl', 'ADMIN_RESKRIM_POLSEK_BONTONOMPO_SELATAN', 'ADMIN_RESKRIM_POLSEK_BONTONOMPO_SELATAN', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:40', 0, '2014-03-12 23:32:27', 1, ''),
('astria8916', 'galau_bingung_rabi_010116', 'galau_bingung_rabi_010116', 89, 'images/profile.jpg', 'images/profile.jpg	', '2016-08-28 09:18:27', 0, '2016-08-28 16:17:05', 1, ''),
('au40kptg01', 'admin_unit_IV_reskrim_KP3', 'admin_unit_IV_reskrim_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au40kptg03', 'admin_unit_IV_resnarkoba_KP3', 'admin_unit_IV_resnarkoba_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au50kptg01', 'admin_unit_V_reskrim_KP3', 'admin_unit_V_reskrim_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('aspkkptg', 'admin_spkt_KP3', 'admin_spkt_KP3', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au30kptg01', 'admin_unit_III_reskrim_KP3', 'admin_unit_III_reskrim_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au30kptg03', 'admin_unit_III_resnarkoba_KP3', 'admin_unit_III_resnarkoba_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au10kptg04', 'admin_unit_I_lantas_KP3', 'admin_unit_I_lantas_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au20kptg01', 'admin_unit_II_reskrim_KP3', 'admin_unit_II_reskrim_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('au20kptg03', 'admin_unit_II_resnarkoba_KP3', 'admin_unit_II_resnarkoba_KP3', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ku30kptg01', 'kanit_III_reskrim_KP3', 'kanit_III_reskrim_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('ku20kptg03', 'kanit_II_resnarkoba_KP3', 'kanit_II_resnarkoba_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-13 11:02:28', 1, ''),
('ku20kptg01', 'kanit_II_reskrim_KP3', 'kanit_II_reskrim_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-13 11:02:28', 1, ''),
('ku10kptg04', 'kanit_I_lantas_KP3', 'kanit_I_lantas_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-14 10:47:06', 1, ''),
('ku10kptg03', 'kanit_I_resnarkoba_KP3', 'kanit_I_resnarkoba_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-14 10:47:06', 1, ''),
('ku10kptg01', 'kanit_I_reskrim_KP3', 'kanit_I_reskrim_KP3', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-14 10:47:06', 1, ''),
('wkbokptg04', 'kbo_lantas_KP3', 'kbo_lantas_KP3', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-20 12:49:39', 1, ''),
('wkbokptg03', 'kbo_resnarkoba_KP3', 'kbo_resnarkoba_KP3', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-20 12:49:39', 1, ''),
('ksatkptg04', 'kasatlantas_KP3', 'kasatlantas_KP3', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-20 12:49:39', 1, ''),
('wkbokptg01', 'kbo_reskrim_KP3', 'kbo_reskrim_KP3', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-20 12:49:39', 1, ''),
('ksatkptg01', 'kasatreskrim_KP3', 'kasatreskrim_KP3', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-20 12:49:39', 1, ''),
('ksatkptg03', 'kasatresnarkoba_KP3', 'kasatresnarkoba_KP3', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-12-20 12:49:39', 1, ''),
('kpolkptg', 'kapolres_KP3', 'kapolres_KP3', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('wpolkptg', 'wakapolres_KP3', 'wakapolres_KP3', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('sakptg04', 'admin_lantas_KP3', 'admin_lantas_KP3', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('sakptg01', 'admin_reskrim_KP3', 'admin_reskrim_KP3', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('sakptg03', 'admin_resnarkoba_KP3', 'admin_resnarkoba_KP3', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('aspkmros', 'admin_spkt_MAROS', 'admin_spkt_MAROS', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('kspkmros', 'kepala_spkt_MAROS', 'kepala_spkt_MAROS', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au30mros03', 'admin_unit_III_resnarkoba_MAROS', 'admin_unit_III_resnarkoba_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au40mros01', 'admin_unit_IV_reskrim_MAROS', 'admin_unit_IV_reskrim_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au40mros03', 'admin_unit_IV_resnarkoba_MAROS', 'admin_unit_IV_resnarkoba_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au50mros01', 'admin_unit_V_reskrim_MAROS', 'admin_unit_V_reskrim_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au30mros01', 'admin_unit_III_reskrim_MAROS', 'admin_unit_III_reskrim_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au20mros01', 'admin_unit_II_reskrim_MAROS', 'admin_unit_II_reskrim_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au20mros03', 'admin_unit_II_resnarkoba_MAROS', 'admin_unit_II_resnarkoba_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au10mros04', 'admin_unit_I_lantas_MAROS', 'admin_unit_I_lantas_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au10mros03', 'admin_unit_I_resnarkoba_MAROS', 'admin_unit_I_resnarkoba_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('au10mros01', 'admin_unit_I_reskrim_MAROS', 'admin_unit_I_reskrim_MAROS', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku50mros01', 'kanit_V_reskrim_MAROS', 'kanit_V_reskrim_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku40mros03', 'kanit_IV_resnarkoba_MAROS', 'kanit_IV_resnarkoba_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku40mros01', 'kanit_IV_reskrim_MAROS', 'kanit_IV_reskrim_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku30mros03', 'kanit_III_resnarkoba_MAROS', 'kanit_III_resnarkoba_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku30mros01', 'kanit_III_reskrim_MAROS', 'kanit_III_reskrim_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku10mros03', 'kanit_I_resnarkoba_MAROS', 'kanit_I_resnarkoba_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-14 10:47:06', 1, ''),
('ku10mros04', 'kanit_I_lantas_MAROS', 'kanit_I_lantas_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-14 10:47:06', 1, ''),
('ku20mros01', 'kanit_II_reskrim_MAROS', 'kanit_II_reskrim_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-13 11:02:28', 1, ''),
('ku20mros03', 'kanit_II_resnarkoba_MAROS', 'kanit_II_resnarkoba_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-13 11:02:28', 1, ''),
('wkbomros04', 'kbo_lantas_MAROS', 'kbo_lantas_MAROS', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-20 12:49:39', 1, ''),
('ku10mros01', 'kanit_I_reskrim_MAROS', 'kanit_I_reskrim_MAROS', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-14 10:47:06', 1, ''),
('wkbomros03', 'kbo_resnarkoba_MAROS', 'kbo_resnarkoba_MAROS', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-20 12:49:39', 1, ''),
('ksatmros01', 'kasatreskrim_MAROS', 'kasatreskrim_MAROS', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-20 12:49:39', 1, ''),
('ksatmros03', 'kasatresnarkoba_MAROS', 'kasatresnarkoba_MAROS', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-20 12:49:39', 1, ''),
('ksatmros04', 'kasatlantas_MAROS', 'kasatlantas_MAROS', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-20 12:49:39', 1, ''),
('wkbomros01', 'kbo_reskrim_MAROS', 'kbo_reskrim_MAROS', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-12-20 12:49:39', 1, ''),
('kpolmros', 'kapolres_MAROS', 'kapolres_MAROS', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('wpolmros', 'wakapolres_MAROS', 'wakapolres_MAROS', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku40gowa03', 'kanit_IV_resnarkoba_gowa', 'kanit_IV_resnarkoba_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('sagowa03', 'admin_resnarkoba_gowa', 'admin_resnarkoba_gowa', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('wkbogowa03', 'kbo_resnarkoba_gowa', 'kbo_resnarkoba_gowa', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-12-20 12:49:39', 1, ''),
('au10gowa04', 'admin_unit_I_lantas_gowa', 'admin_unit_I_lantas_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:04:33', 0, '2012-09-29 17:09:28', 1, ''),
('ksatgowa04', 'kasatlantas_gowa', 'kasatlantas_gowa', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:04:33', 0, '2012-12-20 12:49:39', 1, ''),
('ku10gowa04', 'kanit_I_lantas_gowa', 'kanit_I_lantas_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:04:33', 0, '2012-12-14 10:47:06', 1, ''),
('sagowa04', 'admin_lantas_gowa', 'admin_lantas_gowa', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:04:33', 0, '2012-09-29 17:09:28', 1, ''),
('wkbogowa04', 'kbo_lantas_gowa', 'kbo_lantas_gowa', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:04:33', 0, '2012-12-20 12:49:39', 1, ''),
('samros01', 'admin_reskrim_MAROS', 'admin_reskrim_MAROS', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('samros03', 'admin_resnarkoba_MAROS', 'admin_resnarkoba_MAROS', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('samros04', 'admin_lantas_MAROS', 'admin_lantas_MAROS', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:12:52', 0, '2012-09-29 17:09:28', 1, ''),
('ku30gowa03', 'kanit_III_resnarkoba_gowa', 'kanit_III_resnarkoba_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('ksatgowa03', 'kasatresnarkoba_gowa', 'kasatresnarkoba_gowa', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-12-20 12:49:39', 1, ''),
('ku10gowa03', 'kanit_I_resnarkoba_gowa', 'kanit_I_resnarkoba_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-12-14 10:47:06', 1, ''),
('ku20gowa03', 'kanit_II_resnarkoba_gowa', 'kanit_II_resnarkoba_gowa', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-12-13 11:02:28', 1, ''),
('au40gowa03', 'admin_unit_IV_resnarkoba_gowa', 'admin_unit_IV_resnarkoba_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('au30gowa03', 'admin_unit_III_resnarkoba_gowa', 'admin_unit_III_resnarkoba_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('wsatmksr04', 'wakasatLANTAS_MAKASSAR', 'wakasatLANTAS_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-09-29 17:09:28', 1, ''),
('au20gowa03', 'admin_unit_II_resnarkoba_gowa', 'admin_unit_II_resnarkoba_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('au10gowa03', 'admin_unit_I_resnarkoba_gowa', 'admin_unit_I_resnarkoba_gowa', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:03:50', 0, '2012-09-29 17:09:28', 1, ''),
('samksr04', 'admin_LANTAS_makassar', 'admin_LANTAS_makassar', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2015-11-27 07:45:55', 1, ''),
('ksatmksr04', 'kasatLANTAS_MAKASSAR', 'kasatLANTAS_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-12-20 12:49:39', 1, ''),
('ku10mksr04', 'kanit_I_LANTAS_MAKASSAR', 'kanit_I_LANTAS_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-12-14 10:47:06', 1, ''),
('ks31mksr04', 'kasubnit_III_UNIT_I_LANTAS_MAKASSAR', 'kasubnit_III_UNIT_I_LANTAS_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-12-14 10:49:46', 1, ''),
('ks21mksr04', 'kasubnit_II_UNIT_I_LANTAS_MAKASSAR', 'kasubnit_II_UNIT_I_LANTAS_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-12-12 15:37:52', 1, ''),
('kU30mksr03', 'kanit_III_RESNARKOBA_MAKASSAR', 'kanit_III_RESNARKOBA_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:44', 0, '2012-09-29 17:09:28', 1, ''),
('kU40mksr03', 'kanit_IV_RESNARKOBA_MAKASSAR', 'kanit_IV_RESNARKOBA_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:44', 0, '2012-09-29 17:09:28', 1, ''),
('ks11mksr04', 'kasubnit_I_UNIT_I_LANTAS_MAKASSAR', 'kasubnit_I_UNIT_I_LANTAS_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-09-29 17:09:28', 1, ''),
('au10mksr04', 'admin_UNIT_I_LANTAS_MAKASSAR', 'admin_UNIT_I_LANTAS_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:48:12', 0, '2012-11-05 10:30:08', 1, ''),
('wsatmksr03', 'wakasatRESNARKOBA_MAKASSAR', 'wakasatRESNARKOBA_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:44', 0, '2012-09-29 17:09:28', 1, ''),
('samksr03', 'admin_RESNARKOBA_makassar', 'admin_RESNARKOBA_makassar', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:44', 0, '2015-11-27 07:45:55', 1, ''),
('ku20mksr03', 'kanit_II_RESNARKOBA_MAKASSAR', 'kanit_II_RESNARKOBA_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:44', 0, '2012-12-13 11:02:28', 1, ''),
('ku10mksr03', 'kanit_I_RESNARKOBA_MAKASSAR', 'kanit_I_RESNARKOBA_MAKASSAR', 3, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-12-14 10:47:06', 1, ''),
('ksatmksr03', 'kasatRESNARKOBA_MAKASSAR', 'kasatRESNARKOBA_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-12-20 12:49:39', 1, ''),
('ks34mksr03', 'kasubnit_III_UNIT_IV_RESNARKOBA_MAKASSAR', 'kasubnit_III_UNIT_IV_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks33mksr03', 'kasubnit_III_UNIT_III_RESNARKOBA_MAKASSAR', 'kasubnit_III_UNIT_III_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks32mksr03', 'kasubnit_III_UNIT_II_RESNARKOBA_MAKASSAR', 'kasubnit__III_UNIT_II_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks31mksr03', 'kasubnit_III_UNIT_I_RESNARKOBA_MAKASSAR', 'kasubnit_III_UNIT_I_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-12-14 10:49:46', 1, ''),
('ks24mksr03', 'kasubnit_II_UNIT_IV_RESNARKOBA_MAKASSAR', 'kasubnit_II_UNIT_IV_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks23mksr03', 'kasubnit_II_UNIT_III_RESNARKOBA_MAKASSAR', 'kasubnit_II_UNIT_III_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 0, ''),
('ks22mksr03', 'kasubnit_II_UNIT_II_RESNARKOBA_MAKASSAR', 'kasubnit_II_UNIT_II_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 0, ''),
('ks21mksr03', 'kasubnit_II_UNIT_I_RESNARKOBA_MAKASSAR', 'kasubnit_II_UNIT_I_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-12-12 15:37:52', 1, ''),
('ks13mksr03', 'kasubnit_I_UNIT_III_RESNARKOBA_MAKASSAR', 'kasubnit_I_UNIT_III_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks14mksr03', 'kasubnit_I_UNIT_IV_RESNARKOBA_MAKASSAR', 'kasubnit_I_UNIT_IV_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks12mksr03', 'kasubnit_I_UNIT_II_RESNARKOBA_MAKASSAR', 'kasubnit_I_UNIT_II_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('ks11mksr03', 'kasubnit_I_UNIT_I_RESNARKOBA_MAKASSAR', 'kasubnit_I_UNIT_I_RESNARKOBA_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('au40mksr03', 'admin_UNIT_IV_RESNARKOBA_MAKASSAR', 'admin_UNIT_IV_RESNARKOBA_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-10-05 15:47:12', 1, ''),
('au20mksr03', 'admin_UNIT_II_RESNARKOBA_MAKASSAR', 'admin_UNIT_II_RESNARKOBA_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('au30mksr03', 'admin_UNIT_III_RESNARKOBA_MAKASSAR', 'admin_UNIT_III_RESNARKOBA_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-09-29 17:09:28', 1, ''),
('au10mksr03', 'admin_UNIT_I_RESNARKOBA_MAKASSAR', 'admin_UNIT_I_RESNARKOBA_MAKASSAR', 7, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:45:43', 0, '2012-11-05 10:30:08', 1, ''),
('ks35mksr01', 'kasubnit_III_UNIT_V_RESKRIM_MAKASSAR', 'kasubnit_III_UNIT_V_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:43:03', 0, '2012-09-29 17:09:28', 1, ''),
('ks34mksr01', 'kasubnit_III_UNIT_IV_RESKRIM_MAKASSAR', 'kasubnit_III_UNIT_IV_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:43:03', 0, '2012-09-29 17:09:28', 1, ''),
('kspkkptg', 'kepala_spkt_KP3', 'kepala_spkt_KP3', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 13:13:08', 0, '2012-09-29 17:09:28', 1, ''),
('kpgowaprgi', 'KAPOLSEK_PARIGI', 'KAPOLSEK_PARIGI', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:11', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabtsl', 'KEPALA_SPKT_POLSEK_BONTONOMPO_SELATAN', 'KEPALA_SPKT_POLSEK_BONTONOMPO_SELATAN', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:40', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabtsl', 'KANIT_RESKRIM_POLSEK_BONTONOMPO_SELATAN', 'KANIT_RESKRIM_POLSEK_BONTONOMPO_SELATAN', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:40', 0, '2014-03-12 23:32:27', 1, ''),
('asgowaprlo', 'ADMIN_SPKT_POLSEK_PARANGLOE', 'ADMIN_SPKT_POLSEK_PARANGLOE', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:04', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowaprlo', 'KAPOLSEK_PARANGLOE', 'KAPOLSEK_PARANGLOE', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:04', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabtnp', 'KEPALA_SPKT_POLSEK_BONTONOMPO', 'KEPALA_SPKT_POLSEK_BONTONOMPO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:34', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabtnp', 'KANIT_RESKRIM_POLSEK_BONTONOMPO', 'KANIT_RESKRIM_POLSEK_BONTONOMPO', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:34', 0, '2014-03-12 23:32:27', 1, ''),
('sagowabtnp', 'ADMIN_RESKRIM_POLSEK_BONTONOMPO', 'ADMIN_RESKRIM_POLSEK_BONTONOMPO', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:34', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabtsl', 'ADMIN_SPKT_POLSEK_BONTONOMPO_SELATAN', 'ADMIN_SPKT_POLSEK_BONTONOMPO_SELATAN', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:40', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabtsl', 'KAPOLSEK_BONTONOMPO_SELATAN', 'KAPOLSEK_BONTONOMPO_SELATAN', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:40', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabtmr', 'KEPALA_SPKT_POLSEK_BONTOMARANNU', 'KEPALA_SPKT_POLSEK_BONTOMARANNU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:28', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabtmr', 'KANIT_RESKRIM_POLSEK_BONTOMARANNU', 'KANIT_RESKRIM_POLSEK_BONTOMARANNU', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:28', 0, '2014-03-12 23:32:27', 1, ''),
('sagowabtmr', 'ADMIN_RESKRIM_POLSEK_BONTOMARANNU', 'ADMIN_RESKRIM_POLSEK_BONTOMARANNU', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:28', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabtnp', 'ADMIN_SPKT_POLSEK_BONTONOMPO', 'ADMIN_SPKT_POLSEK_BONTONOMPO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:34', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabtnp', 'KAPOLSEK_BONTONOMPO', 'KAPOLSEK_BONTONOMPO', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:34', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabrbl', 'KANIT_RESKRIM_POLSEK_BIRINGBULU', 'KANIT_RESKRIM_POLSEK_BIRINGBULU', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:23', 0, '2014-03-12 23:32:27', 1, ''),
('sagowabrbl', 'ADMIN_RESKRIM_POLSEK_BIRINGBULU', 'ADMIN_RESKRIM_POLSEK_BIRINGBULU', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:23', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabtmr', 'ADMIN_SPKT_POLSEK_BONTOMARANNU', 'ADMIN_SPKT_POLSEK_BONTOMARANNU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:28', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabtmr', 'KAPOLSEK_BONTOMARANNU', 'KAPOLSEK_BONTOMARANNU', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:28', 0, '2014-03-12 23:32:27', 1, ''),
('sagowabrbg', 'ADMIN_RESKRIM_POLSEK_BAROMBONG', 'ADMIN_RESKRIM_POLSEK_BAROMBONG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:17', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabrbl', 'ADMIN_SPKT_POLSEK_BIRINGBULU', 'ADMIN_SPKT_POLSEK_BIRINGBULU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:23', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabrbl', 'KAPOLSEK_BIRINGBULU', 'KAPOLSEK_BIRINGBULU', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:23', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabrbl', 'KEPALA_SPKT_POLSEK_BIRINGBULU', 'KEPALA_SPKT_POLSEK_BIRINGBULU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:23', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabjng', 'KANIT_RESKRIM_POLSEK_BAJENG', 'KANIT_RESKRIM_POLSEK_BAJENG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:11', 0, '2014-03-12 23:32:27', 1, ''),
('sagowabjng', 'ADMIN_RESKRIM_POLSEK_BAJENG', 'ADMIN_RESKRIM_POLSEK_BAJENG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:11', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabrbg', 'ADMIN_SPKT_POLSEK_BAROMBONG', 'ADMIN_SPKT_POLSEK_BAROMBONG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:17', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabrbg', 'KAPOLSEK_BAROMBONG', 'KAPOLSEK_BAROMBONG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:17', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabrbg', 'KEPALA_SPKT_POLSEK_BAROMBONG', 'KEPALA_SPKT_POLSEK_BAROMBONG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:17', 0, '2014-03-12 23:32:27', 1, ''),
('kugowabrbg', 'KANIT_RESKRIM_POLSEK_BAROMBONG', 'KANIT_RESKRIM_POLSEK_BAROMBONG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:17', 0, '2014-03-12 23:32:27', 1, ''),
('samksrwajo', 'ADMIN_RESKRIM_POLSEK_WAJO', 'ADMIN_RESKRIM_POLSEK_WAJO', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:22', 0, '2014-03-12 23:32:27', 1, ''),
('asgowabjng', 'ADMIN_SPKT_POLSEK_BAJENG', 'ADMIN_SPKT_POLSEK_BAJENG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:11', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowabjng', 'KAPOLSEK_BAJENG', 'KAPOLSEK_BAJENG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:11', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowabjng', 'KEPALA_SPKT_POLSEK_BAJENG', 'KEPALA_SPKT_POLSEK_BAJENG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:57:11', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrwajo', 'ADMIN_SPKT_POLSEK_WAJO', 'ADMIN_SPKT_POLSEK_WAJO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:22', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrwajo', 'KAPOLSEK_WAJO', 'KAPOLSEK_WAJO', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:22', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrwajo', 'KEPALA_SPKT_POLSEK_WAJO', 'KEPALA_SPKT_POLSEK_WAJO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:22', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrwajo', 'KANIT_RESKRIM_POLSEK_WAJO', 'KANIT_RESKRIM_POLSEK_WAJO', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:22', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrujtn', 'KAPOLSEK_UJUNG_TANAH', 'KAPOLSEK_UJUNG_TANAH', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:15', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrujtn', 'KEPALA_SPKT_POLSEK_UJUNG_TANAH', 'KEPALA_SPKT_POLSEK_UJUNG_TANAH', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:15', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrujtn', 'KANIT_RESKRIM_POLSEK_UJUNG_TANAH', 'KANIT_RESKRIM_POLSEK_UJUNG_TANAH', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:15', 0, '2014-03-12 23:32:27', 1, ''),
('samksrujtn', 'ADMIN_RESKRIM_POLSEK_UJUNG_TANAH', 'ADMIN_RESKRIM_POLSEK_UJUNG_TANAH', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:15', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrujpd', 'KEPALA_SPKT_POLSEK_UJUNG_PANDANG', 'KEPALA_SPKT_POLSEK_UJUNG_PANDANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:06', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrujpd', 'KANIT_RESKRIM_POLSEK_UJUNG_PANDANG', 'KANIT_RESKRIM_POLSEK_UJUNG_PANDANG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:06', 0, '2014-03-12 23:32:27', 1, ''),
('samksrujpd', 'ADMIN_RESKRIM_POLSEK_UJUNG_PANDANG', 'ADMIN_RESKRIM_POLSEK_UJUNG_PANDANG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:06', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrujtn', 'ADMIN_SPKT_POLSEK_UJUNG_TANAH', 'ADMIN_SPKT_POLSEK_UJUNG_TANAH', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:15', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrtmlt', 'KANIT_RESKRIM_POLSEK_TAMALATE', 'KANIT_RESKRIM_POLSEK_TAMALATE', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:59', 0, '2014-03-12 23:32:27', 1, ''),
('samksrtmlt', 'ADMIN_RESKRIM_POLSEK_TAMALATE', 'ADMIN_RESKRIM_POLSEK_TAMALATE', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:59', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrujpd', 'ADMIN_SPKT_POLSEK_UJUNG_PANDANG', 'ADMIN_SPKT_POLSEK_UJUNG_PANDANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:06', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrujpd', 'KAPOLSEK_UJUNG_PANDANG', 'KAPOLSEK_UJUNG_PANDANG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:56:06', 0, '2014-03-12 23:32:27', 1, ''),
('samksrtmlr', 'ADMIN_RESKRIM_POLSEK_TAMALANREA', 'ADMIN_RESKRIM_POLSEK_TAMALANREA', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:52', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrtmlt', 'ADMIN_SPKT_POLSEK_TAMALATE', 'ADMIN_SPKT_POLSEK_TAMALATE', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:59', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrtmlt', 'KAPOLSEK_TAMALATE', 'KAPOLSEK_TAMALATE', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:59', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrtmlt', 'KEPALA_SPKT_POLSEK_TAMALATE', 'KEPALA_SPKT_POLSEK_TAMALATE', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:59', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrtmlr', 'ADMIN_SPKT_POLSEK_TAMALANREA', 'ADMIN_SPKT_POLSEK_TAMALANREA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:52', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrtmlr', 'KAPOLSEK_TAMALANREA', 'KAPOLSEK_TAMALANREA', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:52', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrtmlr', 'KEPALA_SPKT_POLSEK_TAMALANREA', 'KEPALA_SPKT_POLSEK_TAMALANREA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:52', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrtmlr', 'KANIT_RESKRIM_POLSEK_TAMALANREA', 'KANIT_RESKRIM_POLSEK_TAMALANREA', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:52', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrtllo', 'ADMIN_SPKT_POLSEK_TALLO', 'ADMIN_SPKT_POLSEK_TALLO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:47', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrtllo', 'KAPOLSEK_TALLO', 'KAPOLSEK_TALLO', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:47', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrtllo', 'KEPALA_SPKT_POLSEK_TALLO', 'KEPALA_SPKT_POLSEK_TALLO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:47', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrtllo', 'KANIT_RESKRIM_POLSEK_TALLO', 'KANIT_RESKRIM_POLSEK_TALLO', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:47', 0, '2014-03-12 23:32:27', 1, ''),
('samksrtllo', 'ADMIN_RESKRIM_POLSEK_TALLO', 'ADMIN_RESKRIM_POLSEK_TALLO', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:47', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrrpcn', 'KAPOLSEK_RAPPOCINI', 'KAPOLSEK_RAPPOCINI', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:41', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrrpcn', 'KEPALA_SPKT_POLSEK_RAPPOCINI', 'KEPALA_SPKT_POLSEK_RAPPOCINI', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:41', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrrpcn', 'KANIT_RESKRIM_POLSEK_RAPPOCINI', 'KANIT_RESKRIM_POLSEK_RAPPOCINI', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:41', 0, '2014-03-12 23:32:27', 1, ''),
('samksrrpcn', 'ADMIN_RESKRIM_POLSEK_RAPPOCINI', 'ADMIN_RESKRIM_POLSEK_RAPPOCINI', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:41', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrpnkk', 'KEPALA_SPKT_POLSEK_PANAKUKKANG', 'KEPALA_SPKT_POLSEK_PANAKUKKANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:35', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrpnkk', 'KANIT_RESKRIM_POLSEK_PANAKUKKANG', 'KANIT_RESKRIM_POLSEK_PANAKUKKANG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:35', 0, '2014-03-12 23:32:27', 1, ''),
('samksrpnkk', 'ADMIN_RESKRIM_POLSEK_PANAKUKKANG', 'ADMIN_RESKRIM_POLSEK_PANAKUKKANG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:35', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrrpcn', 'ADMIN_SPKT_POLSEK_RAPPOCINI', 'ADMIN_SPKT_POLSEK_RAPPOCINI', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:41', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrmrso', 'KANIT_RESKRIM_POLSEK_MARISO', 'KANIT_RESKRIM_POLSEK_MARISO', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:30', 0, '2014-03-12 23:32:27', 1, ''),
('samksrmrso', 'ADMIN_RESKRIM_POLSEK_MARISO', 'ADMIN_RESKRIM_POLSEK_MARISO', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:30', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrpnkk', 'ADMIN_SPKT_POLSEK_PANAKUKKANG', 'ADMIN_SPKT_POLSEK_PANAKUKKANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:35', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrpnkk', 'KAPOLSEK_PANAKUKKANG', 'KAPOLSEK_PANAKUKKANG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:35', 0, '2014-03-12 23:32:27', 1, ''),
('samksrmngl', 'ADMIN_RESKRIM_POLSEK_MANGGALA', 'ADMIN_RESKRIM_POLSEK_MANGGALA', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:24', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrmrso', 'ADMIN_SPKT_POLSEK_MARISO', 'ADMIN_SPKT_POLSEK_MARISO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:30', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrmrso', 'KAPOLSEK_MARISO', 'KAPOLSEK_MARISO', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:30', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrmrso', 'KEPALA_SPKT_POLSEK_MARISO', 'KEPALA_SPKT_POLSEK_MARISO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:30', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrmngl', 'ADMIN_SPKT_POLSEK_MANGGALA', 'ADMIN_SPKT_POLSEK_MANGGALA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:24', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrmngl', 'KAPOLSEK_MANGGALA', 'KAPOLSEK_MANGGALA', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:24', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrmngl', 'KEPALA_SPKT_POLSEK_MANGGALA', 'KEPALA_SPKT_POLSEK_MANGGALA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:24', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrmngl', 'KANIT_RESKRIM_POLSEK_MANGGALA', 'KANIT_RESKRIM_POLSEK_MANGGALA', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:24', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrmmjg', 'KAPOLSEK_MAMAJANG', 'KAPOLSEK_MAMAJANG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:19', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrmmjg', 'KEPALA_SPKT_POLSEK_MAMAJANG', 'KEPALA_SPKT_POLSEK_MAMAJANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:19', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrmmjg', 'KANIT_RESKRIM_POLSEK_MAMAJANG', 'KANIT_RESKRIM_POLSEK_MAMAJANG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:19', 0, '2014-03-12 23:32:27', 1, ''),
('samksrmmjg', 'ADMIN_RESKRIM_POLSEK_MAMAJANG', 'ADMIN_RESKRIM_POLSEK_MAMAJANG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:19', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrbtla', 'KEPALA_SPKT_POLSEK_BONTOALA', 'KEPALA_SPKT_POLSEK_BONTOALA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:13', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrbtla', 'KANIT_RESKRIM_POLSEK_BONTOALA', 'KANIT_RESKRIM_POLSEK_BONTOALA', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:13', 0, '2014-03-12 23:32:27', 1, ''),
('samksrbtla', 'ADMIN_RESKRIM_POLSEK_BONTOALA', 'ADMIN_RESKRIM_POLSEK_BONTOALA', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:13', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrmmjg', 'ADMIN_SPKT_POLSEK_MAMAJANG', 'ADMIN_SPKT_POLSEK_MAMAJANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:19', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrbrkn', 'KANIT_RESKRIM_POLSEK_BIRINGKANAYA', 'KANIT_RESKRIM_POLSEK_BIRINGKANAYA', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:07', 0, '2014-03-12 23:32:27', 1, ''),
('samksrbrkn', 'ADMIN_RESKRIM_POLSEK_BIRINGKANAYA', 'ADMIN_RESKRIM_POLSEK_BIRINGKANAYA', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:07', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrbtla', 'ADMIN_SPKT_POLSEK_BONTOALA', 'ADMIN_SPKT_POLSEK_BONTOALA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:13', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrbtla', 'KAPOLSEK_BONTOALA', 'KAPOLSEK_BONTOALA', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:13', 0, '2014-03-12 23:32:27', 1, ''),
('samksrmksr', 'ADMIN_RESKRIM_POLSEK_MAKASSAR', 'ADMIN_RESKRIM_POLSEK_MAKASSAR', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:53:51', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrbrkn', 'ADMIN_SPKT_POLSEK_BIRINGKANAYA', 'ADMIN_SPKT_POLSEK_BIRINGKANAYA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:07', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrbrkn', 'KAPOLSEK_BIRINGKANAYA', 'KAPOLSEK_BIRINGKANAYA', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:07', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrbrkn', 'KEPALA_SPKT_POLSEK_BIRINGKANAYA', 'KEPALA_SPKT_POLSEK_BIRINGKANAYA', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:55:07', 0, '2014-03-12 23:32:27', 1, ''),
('kumksrmksr', 'KANIT_RESKRIM_POLSEK_MAKASSAR', 'KANIT_RESKRIM_POLSEK_MAKASSAR', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:53:51', 0, '2014-03-12 23:32:27', 1, ''),
('asgowaprgi', 'ADMIN_SPKT_POLSEK_PARIGI', 'ADMIN_SPKT_POLSEK_PARIGI', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:11', 0, '2014-03-12 23:32:27', 1, ''),
('kugowaplng', 'KANIT_RESKRIM_POLSEK_PALLANGGA', 'KANIT_RESKRIM_POLSEK_PALLANGGA', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:00', 0, '2014-03-12 23:32:27', 1, ''),
('sagowaplng', 'ADMIN_RESKRIM_POLSEK_PALLANGGA', 'ADMIN_RESKRIM_POLSEK_PALLANGGA', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:00', 0, '2014-03-12 23:32:27', 1, ''),
('asmksrmksr', 'ADMIN_SPKT_POLSEK_MAKASSAR', 'ADMIN_SPKT_POLSEK_MAKASSAR', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:53:51', 0, '2014-03-12 23:32:27', 1, ''),
('kpmksrmksr', 'KAPOLSEK_MAKASSAR', 'KAPOLSEK_MAKASSAR', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:53:51', 0, '2014-03-12 23:32:27', 1, ''),
('ksmksrmksr', 'KEPALA_SPKT_POLSEK_MAKASSAR', 'KEPALA_SPKT_POLSEK_MAKASSAR', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:53:51', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowaprgi', 'KEPALA_SPKT_POLSEK_PARIGI', 'KEPALA_SPKT_POLSEK_PARIGI', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:11', 0, '2014-03-12 23:32:27', 1, ''),
('kugowaprgi', 'KANIT_RESKRIM_POLSEK_PARIGI', 'KANIT_RESKRIM_POLSEK_PARIGI', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:11', 0, '2014-03-12 23:32:27', 1, ''),
('sagowaprgi', 'ADMIN_RESKRIM_POLSEK_PARIGI', 'ADMIN_RESKRIM_POLSEK_PARIGI', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:11', 0, '2014-03-12 23:32:27', 1, ''),
('asgowaptls', 'ADMIN_SPKT_POLSEK_PATTALLASSANG', 'ADMIN_SPKT_POLSEK_PATTALLASSANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:20', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowaptls', 'KAPOLSEK_PATTALLASSANG', 'KAPOLSEK_PATTALLASSANG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:20', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowaptls', 'KEPALA_SPKT_POLSEK_PATTALLASSANG', 'KEPALA_SPKT_POLSEK_PATTALLASSANG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:20', 0, '2014-03-12 23:32:27', 1, ''),
('kugowaptls', 'KANIT_RESKRIM_POLSEK_PATTALLASSANG', 'KANIT_RESKRIM_POLSEK_PATTALLASSANG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:20', 0, '2014-03-12 23:32:27', 1, ''),
('sagowaptls', 'ADMIN_RESKRIM_POLSEK_PATTALLASSANG', 'ADMIN_RESKRIM_POLSEK_PATTALLASSANG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:20', 0, '2014-03-12 23:32:27', 1, ''),
('asgowasbop', 'ADMIN_SPKT_POLSEK_SOMBA_OPU', 'ADMIN_SPKT_POLSEK_SOMBA_OPU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:28', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowasbop', 'KAPOLSEK_SOMBA_OPU', 'KAPOLSEK_SOMBA_OPU', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:28', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowasbop', 'KEPALA_SPKT_POLSEK_SOMBA_OPU', 'KEPALA_SPKT_POLSEK_SOMBA_OPU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:28', 0, '2014-03-12 23:32:27', 1, ''),
('kugowasbop', 'KANIT_RESKRIM_POLSEK_SOMBA_OPU', 'KANIT_RESKRIM_POLSEK_SOMBA_OPU', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:28', 0, '2014-03-12 23:32:27', 1, ''),
('sagowasbop', 'ADMIN_RESKRIM_POLSEK_SOMBA_OPU', 'ADMIN_RESKRIM_POLSEK_SOMBA_OPU', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:28', 0, '2014-03-12 23:32:27', 1, ''),
('asgowatgmc', 'ADMIN_SPKT_POLSEK_TINGGIMONCONG', 'ADMIN_SPKT_POLSEK_TINGGIMONCONG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:36', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowatgmc', 'KAPOLSEK_TINGGIMONCONG', 'KAPOLSEK_TINGGIMONCONG', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:36', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowatgmc', 'KEPALA_SPKT_POLSEK_TINGGIMONCONG', 'KEPALA_SPKT_POLSEK_TINGGIMONCONG', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:36', 0, '2014-03-12 23:32:27', 1, ''),
('kugowatgmc', 'KANIT_RESKRIM_POLSEK_TINGGIMONCONG', 'KANIT_RESKRIM_POLSEK_TINGGIMONCONG', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:36', 0, '2014-03-12 23:32:27', 1, ''),
('sagowatgmc', 'ADMIN_RESKRIM_POLSEK_TINGGIMONCONG', 'ADMIN_RESKRIM_POLSEK_TINGGIMONCONG', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:36', 0, '2014-03-12 23:32:27', 1, ''),
('asgowatblp', 'ADMIN_SPKT_POLSEK_TOMBOLO_PAO', 'ADMIN_SPKT_POLSEK_TOMBOLO_PAO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:43', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowatblp', 'KAPOLSEK_TOMBOLO_PAO', 'KAPOLSEK_TOMBOLO_PAO', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:43', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowatblp', 'KEPALA_SPKT_POLSEK_TOMBOLO_PAO', 'KEPALA_SPKT_POLSEK_TOMBOLO_PAO', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:43', 0, '2014-03-12 23:32:27', 1, ''),
('kugowatblp', 'KANIT_RESKRIM_POLSEK_TOMBOLO_PAO', 'KANIT_RESKRIM_POLSEK_TOMBOLO_PAO', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:43', 0, '2014-03-12 23:32:27', 1, ''),
('sagowatblp', 'ADMIN_RESKRIM_POLSEK_TOMBOLO_PAO', 'ADMIN_RESKRIM_POLSEK_TOMBOLO_PAO', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:43', 0, '2014-03-12 23:32:27', 1, ''),
('asgowatpbl', 'ADMIN_SPKT_POLSEK_TOMPOBULU', 'ADMIN_SPKT_POLSEK_TOMPOBULU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:50', 0, '2014-03-12 23:32:27', 1, ''),
('kpgowatpbl', 'KAPOLSEK_TOMPOBULU', 'KAPOLSEK_TOMPOBULU', 12, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:50', 0, '2014-03-12 23:32:27', 1, ''),
('ksgowatpbl', 'KEPALA_SPKT_POLSEK_TOMPOBULU', 'KEPALA_SPKT_POLSEK_TOMPOBULU', 9, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:50', 0, '2014-03-12 23:32:27', 1, ''),
('kugowatpbl', 'KANIT_RESKRIM_POLSEK_TOMPOBULU', 'KANIT_RESKRIM_POLSEK_TOMPOBULU', 2, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:50', 0, '2014-03-12 23:32:27', 1, ''),
('sagowatpbl', 'ADMIN_RESKRIM_POLSEK_TOMPOBULU', 'ADMIN_RESKRIM_POLSEK_TOMPOBULU', 1, 'images/profile.jpg', 'images/profile.jpg', '2016-08-30 03:58:50', 0, '2014-03-12 23:32:27', 1, ''),
('55555555', 'raden_5555', 'raden_5555', 4, 'images/profile.jpg', 'images/profile.jpg	', '2016-09-05 04:54:02', 0, '2016-09-05 11:54:02', 1, ''),
('99999999', 'raden_9999', 'raden_9999', 8, 'images/profile.jpg', 'images/profile.jpg	', '2016-09-05 04:55:08', 0, '2016-09-05 11:55:08', 1, ''),
('ks16mksr01', 'kasubnit_I_UNIT_VI_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_VI_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks26mksr01', 'kasubnit_II_UNIT_VI_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_VI_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks36mksr01', 'kasubnit_III_UNIT_VI_RESKRIM_MAKASSAR', 'kasubnit_III_UNIT_VI_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:43:03', 0, '2012-09-29 17:09:28', 1, ''),
('ks17mksr01', 'kasubnit_I_UNIT_VII_RESKRIM_MAKASSAR', 'kasubnit_I_UNIT_VII_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks27mksr01', 'kasubnit_II_UNIT_VII_RESKRIM_MAKASSAR', 'kasubnit_II_UNIT_VII_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2012-09-29 10:10:20', 0, '2012-09-29 17:09:28', 1, ''),
('ks37mksr01', 'kasubnit_III_UNIT_VII_RESKRIM_MAKASSAR', 'kasubnit_III_UNIT_VII_RESKRIM_MAKASSAR', 6, 'images/profile.jpg', 'images/profile.jpg', '2016-08-29 09:43:03', 0, '2012-09-29 17:09:28', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `_temp_pelaksana`
--

CREATE TABLE `_temp_pelaksana` (
  `nrp` varchar(10) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `id_user` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `_temp_pelaksana`
--

INSERT INTO `_temp_pelaksana` (`nrp`, `nama`, `id_user`) VALUES
('64030759', 'ABDUL HADI', 'samksr01'),
('67110078', 'I GST PT NGR SUARTA,SH', 'saslsl02'),
('73110093', 'I GEDE ARI SURYAWAN, SH', 'admsbdt4'),
('73110188', 'A. Made Suniasih, S.H.', 'samksr01'),
('76070400', 'AGUS NYM DARMAYASA,SH', 'saslsl02'),
('76110488', 'GEDE SUDIADNYA,SH', 'admsbdt4'),
('83030868', 'I KADEK MUSTIKA YASA', 'admsbdt4'),
('83100112', 'I MADE SENA, SH', 'admsbdt402'),
('85031692', 'ARFIAN DETUAGE, SH', 'samksrujpd'),
('86030610', 'MANAF USMAN', 'samksrujpd'),
('87030164', 'MARTOSONO SABIHI', 'samksrujpd'),
('91020021', 'Fanani Rahmiyah A', 'samksr01'),
('92090401', 'REZA HAFIDZ DWI S., SIK', 'saslsl02');

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_catatan_kriminal`
--

CREATE TABLE `_temp_tb_catatan_kriminal` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_PELAKU` varchar(10) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_direktif_dir`
--

CREATE TABLE `_temp_tb_direktif_dir` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NAMA_SUBDIT` varchar(25) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `IS_FROM_TEMPLATE` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_direktif_kanit`
--

CREATE TABLE `_temp_tb_direktif_kanit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `IS_FROM_TEMPLATE` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_direktif_kasat`
--

CREATE TABLE `_temp_tb_direktif_kasat` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `IS_FROM_TEMPLATE` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_direktif_kasubdit`
--

CREATE TABLE `_temp_tb_direktif_kasubdit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NAMA_UNIT` varchar(25) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `IS_FROM_TEMPLATE` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_direktif_kasubnit`
--

CREATE TABLE `_temp_tb_direktif_kasubnit` (
  `ID_KASUS` varchar(10) NOT NULL,
  `TGL_DIREKTIF` date NOT NULL,
  `NRP` varchar(10) NOT NULL,
  `ID_DIREKTIF` varchar(12) NOT NULL,
  `DIREKTIF` varchar(1000) NOT NULL,
  `IS_FROM_TEMPLATE` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus`
--

CREATE TABLE `_temp_tb_kasus` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `NAMA_KASUS` varchar(100) NOT NULL,
  `DESKRIPSI_KASUS` varchar(20000) NOT NULL,
  `TGL_KEJADIAN` datetime NOT NULL,
  `NO_LP` varchar(50) DEFAULT NULL,
  `ID_PELAPOR` varchar(10) NOT NULL,
  `NRP` varchar(12) NOT NULL,
  `ID_PASAL` varchar(5) NOT NULL,
  `ID_TIM_OPSNAL` varchar(5) DEFAULT NULL,
  `IS_PERHATIAN_PUBLIK` int(1) DEFAULT '0',
  `NO_LI` varchar(50) DEFAULT NULL,
  `NAMA_SATWIL` varchar(25) DEFAULT NULL,
  `NAMA_SATKER` varchar(25) DEFAULT NULL,
  `NAMA_POLSEK` varchar(25) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb`
--

CREATE TABLE `_temp_tb_kasus_bb` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB06',
  `ID_BB` varchar(5) NOT NULL,
  `NAMA_BB` varchar(100) NOT NULL,
  `JUMLAH_BB` varchar(500) NOT NULL,
  `PENGGUNAAN` varchar(500) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(500) DEFAULT NULL,
  `KOTA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(500) DEFAULT NULL,
  `NO_DPM` varchar(40) DEFAULT NULL,
  `TGL_DISITA` datetime DEFAULT NULL,
  `KETERANGAN` varchar(5000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb_hp`
--

CREATE TABLE `_temp_tb_kasus_bb_hp` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB03',
  `ID_BB` varchar(5) NOT NULL,
  `NOSIM` varchar(50) DEFAULT NULL,
  `NOIMEI` varchar(50) DEFAULT NULL,
  `NOIMSI` varchar(50) DEFAULT NULL,
  `MERK` varchar(100) DEFAULT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `WARNA` varchar(50) DEFAULT NULL,
  `POSISI_TERAKHIR` varchar(200) DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb_r2`
--

CREATE TABLE `_temp_tb_kasus_bb_r2` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB02',
  `ID_BB` varchar(5) NOT NULL,
  `NOPOL` varchar(50) DEFAULT NULL,
  `NOKA` varchar(50) DEFAULT NULL,
  `NOSIN` varchar(50) DEFAULT NULL,
  `NOBPKB` varchar(50) DEFAULT NULL,
  `MERK` varchar(100) DEFAULT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `JENIS` varchar(50) DEFAULT NULL,
  `MODEL` varchar(50) DEFAULT NULL,
  `WARNA` varchar(50) DEFAULT NULL,
  `TAHUN` varchar(4) DEFAULT NULL,
  `SILINDER` varchar(50) DEFAULT NULL,
  `NO_FIDUSIA` varchar(50) DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb_r4`
--

CREATE TABLE `_temp_tb_kasus_bb_r4` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB01',
  `ID_BB` varchar(5) NOT NULL,
  `NOPOL` varchar(50) DEFAULT NULL,
  `NOKA` varchar(50) DEFAULT NULL,
  `NOSIN` varchar(50) DEFAULT NULL,
  `NOBPKB` varchar(50) DEFAULT NULL,
  `MERK` varchar(100) DEFAULT NULL,
  `TIPE` varchar(50) DEFAULT NULL,
  `JENIS` varchar(50) DEFAULT NULL,
  `MODEL` varchar(50) DEFAULT NULL,
  `WARNA` varchar(50) DEFAULT NULL,
  `TAHUN` varchar(4) DEFAULT NULL,
  `SILINDER` varchar(50) DEFAULT NULL,
  `NO_FIDUSIA` varchar(50) DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb_sertifikat`
--

CREATE TABLE `_temp_tb_kasus_bb_sertifikat` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB04',
  `ID_BB` varchar(5) NOT NULL,
  `NOSERTIFIKAT` varchar(50) DEFAULT NULL,
  `KELURAHAN` varchar(100) DEFAULT NULL,
  `NAMA_PEMILIK` varchar(50) DEFAULT NULL,
  `TGL_PENERBITAN` date DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb_sertifikat_peralihan`
--

CREATE TABLE `_temp_tb_kasus_bb_sertifikat_peralihan` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB04',
  `ID_BB` varchar(5) NOT NULL,
  `TGL_PERALIHAN` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_bb_surat`
--

CREATE TABLE `_temp_tb_kasus_bb_surat` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_JENIS_BB` varchar(4) NOT NULL DEFAULT 'BB05',
  `ID_BB` varchar(5) NOT NULL,
  `NAMA_DOKUMEN` varchar(50) DEFAULT NULL,
  `AUTHOR` varchar(100) DEFAULT NULL,
  `TGL_PENERBITAN` date DEFAULT NULL,
  `NO_DPM` varchar(50) DEFAULT NULL,
  `NAMA_PEMILIK_BB` varchar(50) DEFAULT NULL,
  `ALAMAT_PEMILIK_BB` varchar(200) DEFAULT NULL,
  `HP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `TELP_PEMILIK_BB` varchar(12) DEFAULT NULL,
  `STATUS_PEMILIK_BB` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_doc_berkas_koreksi`
--

CREATE TABLE `_temp_tb_kasus_doc_berkas_koreksi` (
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_DOC_BERKAS` varchar(5) NOT NULL,
  `BERKAS_KE` int(2) NOT NULL DEFAULT '1',
  `ID_KOREKSI` varchar(10) NOT NULL DEFAULT '',
  `KOREKSI` varchar(300) DEFAULT NULL,
  `TGL_KOREKSI` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_hambatan`
--

CREATE TABLE `_temp_tb_kasus_hambatan` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_HAMBATAN` varchar(5) NOT NULL,
  `TAHAP_LIDIK_SIDIK` varchar(25) NOT NULL DEFAULT 'PENYELIDIKAN',
  `HAMBATAN` varchar(3000) NOT NULL,
  `SOLUSI_PENYELESAIAN` varchar(3000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_kerugian`
--

CREATE TABLE `_temp_tb_kasus_kerugian` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_KERUGIAN` varchar(5) NOT NULL,
  `KERUGIAN` varchar(1000) NOT NULL,
  `JUMLAH_BARANG` varchar(500) NOT NULL,
  `PEMILIK` varchar(500) NOT NULL,
  `KETERANGAN` varchar(10000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_korban`
--

CREATE TABLE `_temp_tb_kasus_korban` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_KORBAN` varchar(5) NOT NULL,
  `NAMA_KORBAN` varchar(100) NOT NULL,
  `ALAMAT_KORBAN` varchar(100) NOT NULL,
  `KONDISI_KORBAN` varchar(500) NOT NULL,
  `USIA_KORBAN` varchar(3) NOT NULL,
  `CIRI_KORBAN` varchar(200) NOT NULL,
  `KETERANGAN` varchar(2000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_opsnal`
--

CREATE TABLE `_temp_tb_kasus_opsnal` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `NRP` varchar(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_saksi`
--

CREATE TABLE `_temp_tb_kasus_saksi` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_SAKSI` varchar(5) NOT NULL,
  `NAMA_SAKSI` varchar(250) NOT NULL,
  `ALAMAT_SAKSI` varchar(1000) DEFAULT NULL,
  `KOTA` varchar(50) DEFAULT NULL,
  `TEMPAT_LAHIR` varchar(50) DEFAULT NULL,
  `TANGGAL_LAHIR` date DEFAULT NULL,
  `PEKERJAAN` varchar(50) DEFAULT NULL,
  `HP_SAKSI` varchar(50) DEFAULT NULL,
  `TELP_SAKSI` varchar(12) DEFAULT NULL,
  `KETERANGAN_SAKSI` varchar(2000) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kasus_tindak`
--

CREATE TABLE `_temp_tb_kasus_tindak` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_TINDAK` varchar(5) NOT NULL,
  `RENCANA_TINDAK` varchar(2550) NOT NULL,
  `HAMBATAN` varchar(2550) DEFAULT NULL,
  `DURASI` varchar(300) NOT NULL,
  `IS_TERLAKSANA` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `_temp_tb_kronologi_kejadian`
--

CREATE TABLE `_temp_tb_kronologi_kejadian` (
  `NRP_PENGINPUT` varchar(10) NOT NULL,
  `ID_KASUS` varchar(10) NOT NULL,
  `ID_KEJADIAN` varchar(10) NOT NULL,
  `TGL_KEJADIAN` datetime DEFAULT NULL,
  `LOKASI_KEJADIAN` varchar(2550) NOT NULL,
  `KEJADIAN` varchar(2550) NOT NULL,
  `KETERANGAN` varchar(2550) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `dates`
--
DROP TABLE IF EXISTS `dates`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dates`  AS  select (curdate() - interval `numbers`.`number` day) AS `date` from `numbers` ;

-- --------------------------------------------------------

--
-- Structure for view `digits`
--
DROP TABLE IF EXISTS `digits`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `digits`  AS  select 0 AS `digit` union all select 1 AS `1` union all select 2 AS `2` union all select 3 AS `3` union all select 4 AS `4` union all select 5 AS `5` union all select 6 AS `6` union all select 7 AS `7` union all select 8 AS `8` union all select 9 AS `9` ;

-- --------------------------------------------------------

--
-- Structure for view `numbers`
--
DROP TABLE IF EXISTS `numbers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `numbers`  AS  select (((`ones`.`digit` + (`tens`.`digit` * 10)) + (`hundreds`.`digit` * 100)) + (`thousands`.`digit` * 1000)) AS `number` from (((`digits` `ones` join `digits` `tens`) join `digits` `hundreds`) join `digits` `thousands`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bobot_temp`
--
ALTER TABLE `bobot_temp`
  ADD PRIMARY KEY (`BOBOT`);

--
-- Indexes for table `capu`
--
ALTER TABLE `capu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `point_berkas_temp`
--
ALTER TABLE `point_berkas_temp`
  ADD PRIMARY KEY (`BOBOT_POINT`);

--
-- Indexes for table `prosentase_berkas_temp`
--
ALTER TABLE `prosentase_berkas_temp`
  ADD PRIMARY KEY (`PROSENTASE`);

--
-- Indexes for table `prosentase_realisasi_temp`
--
ALTER TABLE `prosentase_realisasi_temp`
  ADD PRIMARY KEY (`PROSENTASE`);

--
-- Indexes for table `prosentase_temp`
--
ALTER TABLE `prosentase_temp`
  ADD PRIMARY KEY (`PROSENTASE`);

--
-- Indexes for table `real_lidik_temp`
--
ALTER TABLE `real_lidik_temp`
  ADD PRIMARY KEY (`rab`);

--
-- Indexes for table `real_sidik_temp`
--
ALTER TABLE `real_sidik_temp`
  ADD PRIMARY KEY (`rab`);

--
-- Indexes for table `ren_lidik_temp`
--
ALTER TABLE `ren_lidik_temp`
  ADD PRIMARY KEY (`rab`);

--
-- Indexes for table `ren_sidik_temp`
--
ALTER TABLE `ren_sidik_temp`
  ADD PRIMARY KEY (`rab`);

--
-- Indexes for table `sc_chat_data`
--
ALTER TABLE `sc_chat_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ts` (`ts`);

--
-- Indexes for table `tb_anggota_spkt`
--
ALTER TABLE `tb_anggota_spkt`
  ADD PRIMARY KEY (`NRP`);

--
-- Indexes for table `tb_ba_interogasi`
--
ALTER TABLE `tb_ba_interogasi`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`);

--
-- Indexes for table `tb_ba_interogasi_detail`
--
ALTER TABLE `tb_ba_interogasi_detail`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_interogasi_gambar`
--
ALTER TABLE `tb_ba_interogasi_gambar`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_lit_dokumen`
--
ALTER TABLE `tb_ba_lit_dokumen`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`);

--
-- Indexes for table `tb_ba_lit_dokumen_detail`
--
ALTER TABLE `tb_ba_lit_dokumen_detail`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_lit_dokumen_gambar`
--
ALTER TABLE `tb_ba_lit_dokumen_gambar`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_observasi`
--
ALTER TABLE `tb_ba_observasi`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`);

--
-- Indexes for table `tb_ba_observasi_detail`
--
ALTER TABLE `tb_ba_observasi_detail`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_observasi_gambar`
--
ALTER TABLE `tb_ba_observasi_gambar`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_pembuntutan`
--
ALTER TABLE `tb_ba_pembuntutan`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`);

--
-- Indexes for table `tb_ba_pembuntutan_detail`
--
ALTER TABLE `tb_ba_pembuntutan_detail`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_POSISI`);

--
-- Indexes for table `tb_ba_pembuntutan_gambar`
--
ALTER TABLE `tb_ba_pembuntutan_gambar`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_penyamaran`
--
ALTER TABLE `tb_ba_penyamaran`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`);

--
-- Indexes for table `tb_ba_penyamaran_detail`
--
ALTER TABLE `tb_ba_penyamaran_detail`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_undercover`
--
ALTER TABLE `tb_ba_undercover`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`);

--
-- Indexes for table `tb_ba_undercover_detail`
--
ALTER TABLE `tb_ba_undercover_detail`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_ba_undercover_gambar`
--
ALTER TABLE `tb_ba_undercover_gambar`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_OPSNAL`,`BERKAS_KE`,`NO_URUT`);

--
-- Indexes for table `tb_bobot_kasus`
--
ALTER TABLE `tb_bobot_kasus`
  ADD PRIMARY KEY (`ID_BOBOT`);

--
-- Indexes for table `tb_bobot_kasus_kanit`
--
ALTER TABLE `tb_bobot_kasus_kanit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_BOBOT_ASAL`,`ID_BOBOT`);

--
-- Indexes for table `tb_bobot_kasus_kasat`
--
ALTER TABLE `tb_bobot_kasus_kasat`
  ADD PRIMARY KEY (`ID_KASUS`);

--
-- Indexes for table `tb_bobot_kasus_spkt`
--
ALTER TABLE `tb_bobot_kasus_spkt`
  ADD PRIMARY KEY (`ID_KASUS`);

--
-- Indexes for table `tb_catatan_kriminal`
--
ALTER TABLE `tb_catatan_kriminal`
  ADD KEY `ID_PELAKU` (`ID_PELAKU`),
  ADD KEY `ID_KASUS` (`ID_KASUS`);

--
-- Indexes for table `tb_direktif_dir`
--
ALTER TABLE `tb_direktif_dir`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `tb_direktif_dir_template`
--
ALTER TABLE `tb_direktif_dir_template`
  ADD PRIMARY KEY (`ID_DIREKTIF_TEMPLATE`);

--
-- Indexes for table `tb_direktif_kanit`
--
ALTER TABLE `tb_direktif_kanit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `tb_direktif_kanit_template`
--
ALTER TABLE `tb_direktif_kanit_template`
  ADD PRIMARY KEY (`ID_DIREKTIF_TEMPLATE`);

--
-- Indexes for table `tb_direktif_kasat`
--
ALTER TABLE `tb_direktif_kasat`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `tb_direktif_kasat_template`
--
ALTER TABLE `tb_direktif_kasat_template`
  ADD PRIMARY KEY (`ID_DIREKTIF_TEMPLATE`);

--
-- Indexes for table `tb_direktif_kasubdit`
--
ALTER TABLE `tb_direktif_kasubdit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `tb_direktif_kasubdit_template`
--
ALTER TABLE `tb_direktif_kasubdit_template`
  ADD PRIMARY KEY (`ID_DIREKTIF_TEMPLATE`);

--
-- Indexes for table `tb_direktif_kasubnit`
--
ALTER TABLE `tb_direktif_kasubnit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `tb_direktif_kasubnit_template`
--
ALTER TABLE `tb_direktif_kasubnit_template`
  ADD PRIMARY KEY (`ID_DIREKTIF_TEMPLATE`);

--
-- Indexes for table `tb_doc_berkas`
--
ALTER TABLE `tb_doc_berkas`
  ADD PRIMARY KEY (`ID_DOC_BERKAS`);

--
-- Indexes for table `tb_doc_iden`
--
ALTER TABLE `tb_doc_iden`
  ADD PRIMARY KEY (`ID_DOC_IDEN`);

--
-- Indexes for table `tb_doc_opsnal`
--
ALTER TABLE `tb_doc_opsnal`
  ADD PRIMARY KEY (`ID_DOC_OPSNAL`);

--
-- Indexes for table `tb_doc_pam`
--
ALTER TABLE `tb_doc_pam`
  ADD PRIMARY KEY (`ID_DOC_PAM`);

--
-- Indexes for table `tb_doc_sp2hp`
--
ALTER TABLE `tb_doc_sp2hp`
  ADD PRIMARY KEY (`ID_DOC_SP2HP`);

--
-- Indexes for table `tb_jenis_bb`
--
ALTER TABLE `tb_jenis_bb`
  ADD PRIMARY KEY (`ID_JENIS_BB`);

--
-- Indexes for table `tb_kasus`
--
ALTER TABLE `tb_kasus`
  ADD PRIMARY KEY (`ID_KASUS`);

--
-- Indexes for table `tb_kasus_bb`
--
ALTER TABLE `tb_kasus_bb`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`);

--
-- Indexes for table `tb_kasus_bb_hp`
--
ALTER TABLE `tb_kasus_bb_hp`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`);

--
-- Indexes for table `tb_kasus_bb_r2`
--
ALTER TABLE `tb_kasus_bb_r2`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`);

--
-- Indexes for table `tb_kasus_bb_r4`
--
ALTER TABLE `tb_kasus_bb_r4`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`);

--
-- Indexes for table `tb_kasus_bb_sertifikat`
--
ALTER TABLE `tb_kasus_bb_sertifikat`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`);

--
-- Indexes for table `tb_kasus_bb_sertifikat_peralihan`
--
ALTER TABLE `tb_kasus_bb_sertifikat_peralihan`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`,`TGL_PERALIHAN`);

--
-- Indexes for table `tb_kasus_bb_surat`
--
ALTER TABLE `tb_kasus_bb_surat`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_JENIS_BB`,`ID_BB`);

--
-- Indexes for table `tb_kasus_doc_berkas`
--
ALTER TABLE `tb_kasus_doc_berkas`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_BERKAS`,`BERKAS_KE`);

--
-- Indexes for table `tb_kasus_doc_berkas_koreksi`
--
ALTER TABLE `tb_kasus_doc_berkas_koreksi`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_BERKAS`,`BERKAS_KE`,`ID_KOREKSI`);

--
-- Indexes for table `tb_kasus_doc_berkas_non_acc`
--
ALTER TABLE `tb_kasus_doc_berkas_non_acc`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_BERKAS`,`BERKAS_KE`);

--
-- Indexes for table `tb_kasus_doc_berkas_target`
--
ALTER TABLE `tb_kasus_doc_berkas_target`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_BERKAS`,`BERKAS_KE`);

--
-- Indexes for table `tb_kasus_doc_iden`
--
ALTER TABLE `tb_kasus_doc_iden`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_IDEN`,`NRP`,`BERKAS_KE`);

--
-- Indexes for table `tb_kasus_doc_opsnal`
--
ALTER TABLE `tb_kasus_doc_opsnal`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC`,`NRP`,`BERKAS_KE`,`is_dok_berkas`);

--
-- Indexes for table `tb_kasus_doc_sp2hp`
--
ALTER TABLE `tb_kasus_doc_sp2hp`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_SP2HP`,`BERKAS_KE`);

--
-- Indexes for table `tb_kasus_doc_sp2hp_target`
--
ALTER TABLE `tb_kasus_doc_sp2hp_target`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_SP2HP`);

--
-- Indexes for table `tb_kasus_hambatan`
--
ALTER TABLE `tb_kasus_hambatan`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_HAMBATAN`);

--
-- Indexes for table `tb_kasus_kerugian`
--
ALTER TABLE `tb_kasus_kerugian`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_KERUGIAN`);

--
-- Indexes for table `tb_kasus_korban`
--
ALTER TABLE `tb_kasus_korban`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_KORBAN`);

--
-- Indexes for table `tb_kasus_opsnal`
--
ALTER TABLE `tb_kasus_opsnal`
  ADD PRIMARY KEY (`ID_KASUS`,`NRP`);

--
-- Indexes for table `tb_kasus_ren_lidik_realisasi`
--
ALTER TABLE `tb_kasus_ren_lidik_realisasi`
  ADD PRIMARY KEY (`id_kasus`,`ren_lidik`,`tgl_pelaksanaan`);

--
-- Indexes for table `tb_kasus_ren_lidik_realisasi_ubah`
--
ALTER TABLE `tb_kasus_ren_lidik_realisasi_ubah`
  ADD PRIMARY KEY (`id_kasus`,`ren_lidik`,`tgl_pelaksanaan`,`tgl_perubahan`);

--
-- Indexes for table `tb_kasus_ren_lidik_target`
--
ALTER TABLE `tb_kasus_ren_lidik_target`
  ADD PRIMARY KEY (`id_kasus`,`ren_lidik`,`tgl_pelaksanaan`);

--
-- Indexes for table `tb_kasus_ren_sidik_realisasi`
--
ALTER TABLE `tb_kasus_ren_sidik_realisasi`
  ADD PRIMARY KEY (`id_kasus`,`ren_sidik`,`tgl_pelaksanaan`);

--
-- Indexes for table `tb_kasus_ren_sidik_realisasi_ubah`
--
ALTER TABLE `tb_kasus_ren_sidik_realisasi_ubah`
  ADD PRIMARY KEY (`id_kasus`,`ren_sidik`,`tgl_pelaksanaan`,`tgl_perubahan`);

--
-- Indexes for table `tb_kasus_ren_sidik_target`
--
ALTER TABLE `tb_kasus_ren_sidik_target`
  ADD PRIMARY KEY (`id_kasus`,`ren_sidik`,`tgl_pelaksanaan`);

--
-- Indexes for table `tb_kasus_saksi`
--
ALTER TABLE `tb_kasus_saksi`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_SAKSI`);

--
-- Indexes for table `tb_kasus_status`
--
ALTER TABLE `tb_kasus_status`
  ADD PRIMARY KEY (`ID_KASUS`,`STATUS`);

--
-- Indexes for table `tb_kasus_tindak`
--
ALTER TABLE `tb_kasus_tindak`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_TINDAK`);

--
-- Indexes for table `tb_kejadian_menonjol`
--
ALTER TABLE `tb_kejadian_menonjol`
  ADD PRIMARY KEY (`ID_KEJADIAN`);

--
-- Indexes for table `tb_kelompok`
--
ALTER TABLE `tb_kelompok`
  ADD PRIMARY KEY (`ID_KELOMPOK`);

--
-- Indexes for table `tb_kronologi_kejadian`
--
ALTER TABLE `tb_kronologi_kejadian`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_KEJADIAN`);

--
-- Indexes for table `tb_master_ren_lidik`
--
ALTER TABLE `tb_master_ren_lidik`
  ADD PRIMARY KEY (`ren_lidik`);

--
-- Indexes for table `tb_master_ren_lidik_deskripsi`
--
ALTER TABLE `tb_master_ren_lidik_deskripsi`
  ADD PRIMARY KEY (`ren_lidik`,`deskripsi`);

--
-- Indexes for table `tb_master_ren_sidik`
--
ALTER TABLE `tb_master_ren_sidik`
  ADD PRIMARY KEY (`ren_sidik`);

--
-- Indexes for table `tb_master_ren_sidik_deskripsi`
--
ALTER TABLE `tb_master_ren_sidik_deskripsi`
  ADD PRIMARY KEY (`ren_sidik`,`deskripsi`);

--
-- Indexes for table `tb_nama_kasus`
--
ALTER TABLE `tb_nama_kasus`
  ADD PRIMARY KEY (`nama_kasus`);

--
-- Indexes for table `tb_pam`
--
ALTER TABLE `tb_pam`
  ADD PRIMARY KEY (`ID_PAM`);

--
-- Indexes for table `tb_pangkat`
--
ALTER TABLE `tb_pangkat`
  ADD PRIMARY KEY (`PANGKAT`);

--
-- Indexes for table `tb_pasal_kasus`
--
ALTER TABLE `tb_pasal_kasus`
  ADD PRIMARY KEY (`ID_PASAL`);

--
-- Indexes for table `tb_pelaku`
--
ALTER TABLE `tb_pelaku`
  ADD PRIMARY KEY (`ID_PELAKU`);

--
-- Indexes for table `tb_pelaku_foto`
--
ALTER TABLE `tb_pelaku_foto`
  ADD PRIMARY KEY (`ID_PELAKU`);

--
-- Indexes for table `tb_pelaku_kelompok`
--
ALTER TABLE `tb_pelaku_kelompok`
  ADD PRIMARY KEY (`ID_KELOMPOK`,`ID_PELAKU`);

--
-- Indexes for table `tb_pelaku_spesialisasi`
--
ALTER TABLE `tb_pelaku_spesialisasi`
  ADD PRIMARY KEY (`ID_SPESIALISASI`,`ID_PELAKU`);

--
-- Indexes for table `tb_pelapor`
--
ALTER TABLE `tb_pelapor`
  ADD PRIMARY KEY (`ID_PELAPOR`);

--
-- Indexes for table `tb_pelapor_pernyataan`
--
ALTER TABLE `tb_pelapor_pernyataan`
  ADD PRIMARY KEY (`ID_PELAPOR`,`ID_KASUS`);

--
-- Indexes for table `tb_penghargaan_penyidik`
--
ALTER TABLE `tb_penghargaan_penyidik`
  ADD PRIMARY KEY (`NRP`,`TGL_PENGHARGAAN`);

--
-- Indexes for table `tb_penyidik`
--
ALTER TABLE `tb_penyidik`
  ADD PRIMARY KEY (`NRP`);

--
-- Indexes for table `tb_perubahan_bobot`
--
ALTER TABLE `tb_perubahan_bobot`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_BOBOT_LAMA`,`ID_BOBOT`);

--
-- Indexes for table `tb_perubahan_bobot_point_lama`
--
ALTER TABLE `tb_perubahan_bobot_point_lama`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_BOBOT_LAMA`,`ID_DOC_BERKAS`);

--
-- Indexes for table `tb_perubahan_bobot_point_lama_opsnal`
--
ALTER TABLE `tb_perubahan_bobot_point_lama_opsnal`
  ADD PRIMARY KEY (`ID_KASUS`,`NRP`,`ID_BOBOT_LAMA`,`ID_DOC_OPSNAL`);

--
-- Indexes for table `tb_polsek`
--
ALTER TABLE `tb_polsek`
  ADD PRIMARY KEY (`NAMA_POLSEK`,`NAMA_SATWIL`);

--
-- Indexes for table `tb_realisasi_doc_pam`
--
ALTER TABLE `tb_realisasi_doc_pam`
  ADD PRIMARY KEY (`ID_PAM`,`ID_DOC_PAM`,`NRP`);

--
-- Indexes for table `tb_rekomendasi_lp`
--
ALTER TABLE `tb_rekomendasi_lp`
  ADD PRIMARY KEY (`ID_PELAPOR`,`ID_KASUS`);

--
-- Indexes for table `tb_riwayat_jabatan_penyidik`
--
ALTER TABLE `tb_riwayat_jabatan_penyidik`
  ADD PRIMARY KEY (`NRP`,`TGL_AWAL_RIWAYAT`);

--
-- Indexes for table `tb_satker`
--
ALTER TABLE `tb_satker`
  ADD PRIMARY KEY (`NAMA_SATKER`,`NAMA_SATWIL`,`NAMA_POLSEK`);

--
-- Indexes for table `tb_satwil`
--
ALTER TABLE `tb_satwil`
  ADD PRIMARY KEY (`NAMA_SATWIL`);

--
-- Indexes for table `tb_sidik_jari`
--
ALTER TABLE `tb_sidik_jari`
  ADD PRIMARY KEY (`ID_PELAKU`);

--
-- Indexes for table `tb_spesialisasi`
--
ALTER TABLE `tb_spesialisasi`
  ADD PRIMARY KEY (`ID_SPESIALISASI`);

--
-- Indexes for table `tb_subdit`
--
ALTER TABLE `tb_subdit`
  ADD PRIMARY KEY (`NAMA_SATWIL`,`NAMA_SATKER`,`NAMA_SUBDIT`);

--
-- Indexes for table `tb_subnit`
--
ALTER TABLE `tb_subnit`
  ADD PRIMARY KEY (`NAMA_SATWIL`,`NAMA_SATKER`,`NAMA_SUBDIT`,`NAMA_UNIT`,`NAMA_SUBNIT`);

--
-- Indexes for table `tb_tahanan`
--
ALTER TABLE `tb_tahanan`
  ADD PRIMARY KEY (`ID_PELAKU`,`ID_TAHANAN`);

--
-- Indexes for table `tb_tahanan_pemberhentian`
--
ALTER TABLE `tb_tahanan_pemberhentian`
  ADD PRIMARY KEY (`ID_TAHANAN`,`ID_PEMBERHENTIAN_TAHANAN`);

--
-- Indexes for table `tb_tahanan_perpanjangan`
--
ALTER TABLE `tb_tahanan_perpanjangan`
  ADD PRIMARY KEY (`ID_TAHANAN`,`ID_PERPANJANGAN_TAHANAN`);

--
-- Indexes for table `tb_tahanan_perpindahan`
--
ALTER TABLE `tb_tahanan_perpindahan`
  ADD PRIMARY KEY (`ID_TAHANAN`,`ID_PERPINDAHAN_TAHANAN`);

--
-- Indexes for table `tb_target_doc_sp2hp`
--
ALTER TABLE `tb_target_doc_sp2hp`
  ADD PRIMARY KEY (`ID_BOBOT`,`ID_DOC_SP2HP`);

--
-- Indexes for table `tb_tim_opsnal`
--
ALTER TABLE `tb_tim_opsnal`
  ADD PRIMARY KEY (`ID_TIM_OPSNAL`);

--
-- Indexes for table `tb_tim_opsnal_anggota`
--
ALTER TABLE `tb_tim_opsnal_anggota`
  ADD PRIMARY KEY (`ID_TIM_OPSNAL`,`ANGGOTA_TIM_OPSNAL`);

--
-- Indexes for table `tb_tingkat_kesulitan_grup`
--
ALTER TABLE `tb_tingkat_kesulitan_grup`
  ADD PRIMARY KEY (`ID_GROUP`);

--
-- Indexes for table `tb_tingkat_kesulitan_hasil`
--
ALTER TABLE `tb_tingkat_kesulitan_hasil`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_GROUP`,`ID_TOPIK`,`ID_BOBOT`);

--
-- Indexes for table `tb_tingkat_kesulitan_resume`
--
ALTER TABLE `tb_tingkat_kesulitan_resume`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_BOBOT`);

--
-- Indexes for table `tb_tingkat_kesulitan_rincian`
--
ALTER TABLE `tb_tingkat_kesulitan_rincian`
  ADD PRIMARY KEY (`ID_GROUP`,`ID_TOPIK`,`ID_BOBOT`);

--
-- Indexes for table `tb_tingkat_kesulitan_topik`
--
ALTER TABLE `tb_tingkat_kesulitan_topik`
  ADD PRIMARY KEY (`ID_GROUP`,`ID_TOPIK`);

--
-- Indexes for table `tb_tipe_user`
--
ALTER TABLE `tb_tipe_user`
  ADD PRIMARY KEY (`TIPE_USER`);

--
-- Indexes for table `tb_unit`
--
ALTER TABLE `tb_unit`
  ADD PRIMARY KEY (`NAMA_SATWIL`,`NAMA_SATKER`,`NAMA_SUBDIT`,`NAMA_UNIT`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`ID_USER`),
  ADD UNIQUE KEY `USER_NAME` (`USER_NAME`);

--
-- Indexes for table `_temp_pelaksana`
--
ALTER TABLE `_temp_pelaksana`
  ADD PRIMARY KEY (`nrp`,`id_user`);

--
-- Indexes for table `_temp_tb_catatan_kriminal`
--
ALTER TABLE `_temp_tb_catatan_kriminal`
  ADD KEY `ID_PELAKU` (`ID_PELAKU`);

--
-- Indexes for table `_temp_tb_direktif_dir`
--
ALTER TABLE `_temp_tb_direktif_dir`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `_temp_tb_direktif_kanit`
--
ALTER TABLE `_temp_tb_direktif_kanit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `_temp_tb_direktif_kasat`
--
ALTER TABLE `_temp_tb_direktif_kasat`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `_temp_tb_direktif_kasubdit`
--
ALTER TABLE `_temp_tb_direktif_kasubdit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `_temp_tb_direktif_kasubnit`
--
ALTER TABLE `_temp_tb_direktif_kasubnit`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DIREKTIF`);

--
-- Indexes for table `_temp_tb_kasus_doc_berkas_koreksi`
--
ALTER TABLE `_temp_tb_kasus_doc_berkas_koreksi`
  ADD PRIMARY KEY (`ID_KASUS`,`ID_DOC_BERKAS`,`BERKAS_KE`,`ID_KOREKSI`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `capu`
--
ALTER TABLE `capu`
  MODIFY `id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sc_chat_data`
--
ALTER TABLE `sc_chat_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=354;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
