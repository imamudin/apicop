<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//myroute
$route['login']['put']	= 'permission/login';
$route['logout']['put']	= 'permission/logout';

//berkas interogasi
$route['interogasi/carikasus']['post']			= 'interogasi/getKasus';
$route['interogasi/simpanberkas']['post']		= 'interogasi/setBerkas';
$route['interogasi/getInterogasi']['post']		= 'interogasi/getBerkas';

//berkas observasi
$route['observasi/carikasus']['post']			= 'observasi/getKasus';
$route['observasi/simpanberkas']['post']		= 'observasi/setBerkas';
$route['observasi/getObservasi']['post']		= 'observasi/getBerkas';

//berkas suveillance/pembututan
$route['surveillance/carikasus']['post']		= 'surveillance/getKasus';
$route['surveillance/simpanberkas']['post']		= 'surveillance/setBerkas';
$route['surveillance/getSurveillance']['post']	= 'surveillance/getBerkas';

//berkas suveillance/pembututan
$route['lit_dokumen/carikasus']['post']			= 'lit_dokumen/getKasus';
$route['lit_dokumen/simpanberkas']['post']		= 'lit_dokumen/setBerkas';
$route['lit_dokumen/getLit_dokumen']['post']	= 'lit_dokumen/getBerkas';


//berkas undercover / penyamaran
$route['undercover/carikasus']['post']			= 'undercover/getKasus';
$route['undercover/simpanberkas']['post']		= 'undercover/setBerkas';
$route['undercover/getUndercover']['post']		= 'undercover/getBerkas';

//monitoring
$route['monitor']['post']						= 'monitor/getMonitor';

//berkas saya untuk opsnal
$route['berkas_saya']['post']					= 'opsnal/getBerkasSaya';
$route['kejadian_baru']['post']					= 'opsnal/setKejadianBaru';
$route['getKejadian_baru']['post']				= 'opsnal/getKejadianBaru';




$route['tes']['post']							= 'home/index';