<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Permission extends MY_Controller {
    //method checkApp terletak di MY_Controller

    function __construct(){
        parent::__construct();
        $this->load->model('Permissionmodel');
        //$this->load->model('Test2');
    } 
    public function login()
    {
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';
            $kirim_gcm  = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_name)     ? $username     = $data->user_name      : $username     = '';
            isset($data->user_regId)    ? $userregid    = $data->user_regId     : $userregid    = '';
            isset($data->user_password) ? $userpassword = $data->user_password  : $userpassword = '';

            $userlogin = $this->Permissionmodel->login($username, $userpassword, $userregid);

            if($userlogin){
                if($userlogin[0]->user_regId != ''){
                    //kirim gcm notifikasi ke android sebelumnya
                    $kirim_gcm  = "kirim gcm";
                }
                
                $userupdate = $this->Permissionmodel->simpan_gcm($username, $userpassword, $userregid);
                if($userupdate){
                    $pesan      = 'Login berhasil.'.$kirim_gcm;
                    $data       = $userlogin;
                    $status     = 1;
                }else{
                    $pesan      = 'Gagal mengubah data user.'.$kirim_gcm;
                    $data       = $userlogin;
                    $status     = 1;
                }
            }else{
                $pesan      = 'Username dan password tidak cocok.';
                $data       = null;
                $status     = -1;
            }

            $response   = array(
                    'status'    => $status,
                    'pesan'   => $pesan,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            echo json_encode(false);
        }
    }
    public function logout(){
        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';

            $data = (object)json_decode(file_get_contents('php://input'));
            
            isset($data->user_id)       ? $userid       = $data->user_id        : $userid       = '';
            isset($data->user_regId)    ? $userregid    = $data->user_regId     : $userregid    = '';

            $userlogout     = $this->Permissionmodel->logout($userid,$userregid);
            if($userlogout){
                $pesan      = 'Logout berhasil.';
                $data       = 'null';
                $status     = 1;
            }else{
                $pesan      = 'Logout gagal.';
                $data       = null;
                $status     = -1;
            }

            $response   = array(
                    'status'    => $status,
                    'pesan'     => $pesan,
                    'data'      => $data
                );
            $this->makeOutput($response);
        }else{
            echo json_encode(false);
        }
    }
}