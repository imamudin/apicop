<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permissionmodel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function login($username, $password, $gcm_regId){
        $query = "SELECT ID_USER, USER_NAME, TIPE_USER, b.NAMA_SATWIL, ifnull(b.NAMA_SATKER,'') NAMA_SATKER, ifnull(c.ID_SATKER,'') ID_SATKER, ifnull(b.NAMA_POLSEK,'') NAMA_POLSEK, d.tipe_satwil, user_regId, b.IS_OPSNAL 
            FROM tb_user a, tb_penyidik b left outer join tb_satker c on b.nama_satwil = c.nama_satwil and b.nama_satker = c.nama_satker, tb_satwil d WHERE a.id_user = b.nrp and b.nama_satwil = d.nama_satwil and USER_NAME='$username' AND PASSWORD='$password'";
         
        $data = $this->db->query($query);

        if($data->num_rows()==1){
            return $data->result();
        }else{
            return false;
        }
    }
    public function simpan_gcm($username, $password, $gcm_regId){
        $query = "update tb_user set user_regId='$gcm_regId' 
                    where USER_NAME='$username' and PASSWORD='$password'";

        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    private function hapus_gcm($user_id, $gcm_regId){
        $query = "update tb_user set user_regId='' where ID_USER='".$user_id."'";

        if($this->db->query($query)){
            return true;
        }else{
            return false;
        }
    }
    public function logout($user_id, $gcm_regId){
        $sql    = "select * from tb_user where ID_USER='".$user_id."' and user_regId='".$gcm_regId."' and user_regId <>''";

        $data   = $this->db->query($sql);

        if($data->num_rows()==1){
            if($this->hapus_gcm($user_id, $gcm_regId)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}