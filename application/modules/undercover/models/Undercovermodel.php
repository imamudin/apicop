<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Undercovermodel extends MY_Model {

    public $tb_ba_undercover         = 'tb_ba_undercover';
    public $tb_ba_undercover_detail  = 'tb_ba_undercover_detail';
    public $tb_ba_undercover_gambar  = 'tb_ba_undercover_gambar';
    

    public function __construct() {
        parent::__construct();
    }
    public function total_ba_undercover($id_doc,$id_kasus){
        $sql    ="select ifnull(max(berkas_ke), 0) as total from $this->tb_ba_undercover where ID_DOC_OPSNAL='".$id_doc."' and ID_KASUS='".$id_kasus."'";

        $result = $this->db->query($sql)->result();

        return $result[0]->total;
    }
    public function total_tanya_undercover($id_doc,$nomor_urut_ba, $id_kasus){
        $sql    ="select * from tb_undercover_detil where ID_DOC_OPSNAL='".$id_doc."' and TGL_BA='".$tanggal."'and NO_URUT_BA=".$nomor_urut_ba." and ID_KASUS='".$id_kasus."'";

        return $this->db->query($sql)->num_rows();
    }
    public function cari_kasus($keyword, $keyword_jenis, $offset, $limit){
        $sql    = "SELECT a.ID_KASUS, a.NAMA_KASUS, a.TGL_KEJADIAN, a.NO_LP, b.NAMA_PELAPOR 
                    FROM tb_kasus a, tb_pelapor b
                    WHERE a.ID_PELAPOR=b.ID_PELAPOR
                    and ".$keyword_jenis." like '%".$keyword."%' limit ".$limit." offset ".$offset;

        return $this->db->query($sql)->result();
    }
    public function is_login($user_id, $user_regId)
    {       
        $sql    = "select * from tb_user where ID_USER='".$user_id."' and user_regId='".$user_regId."'";
        $query  = $this->db->query($sql);
        if($query->num_rows()==1){
            return true;
        }else{
            return false;
        }
    }
    public function simpan_ba($kasus_id, $id_doc, $user_id, $berkas_ke, $dasar, $tanggal, $sasaran_tempat, $latitude, $longitude){
        $sql    = "INSERT INTO $this->tb_ba_undercover (ID_DOC_OPSNAL, TGL_BA, BERKAS_KE, NRP, ID_KASUS, DASAR, 
                    SASARAN_TEMPAT, LATITUDE, LONGITUDE) 
                        VALUES ('".$id_doc."', '".$tanggal."',".$berkas_ke.",'".$user_id."','".$kasus_id."',
                        '".$dasar."','".$sasaran_tempat."', '".$latitude."', '".$longitude."')";
        if($this->db->query($sql)){
            return true;
        }else{
            return false;
        }
    }
    public function simpan_detil($kasus_id, $id_doc, $berkas_ke, $no_urut, $tanya, $jawab){
        $sql    = "INSERT INTO $this->tb_ba_undercover_detail (ID_KASUS, ID_DOC_OPSNAL, BERKAS_KE, NO_URUT, IDENTITAS, INFORMASI) 
                        VALUES ('".$kasus_id."', '".$id_doc."',".$berkas_ke.",'".$no_urut."','".$tanya."',
                        '".$jawab."')";
        return $this->db->query($sql);
    }
    public function simpan_gambar($kasus_id, $id_doc, $berkas_ke, $no_urut, $gambar){
        $sql    = "INSERT INTO $this->tb_ba_undercover_gambar (ID_KASUS, ID_DOC_OPSNAL, BERKAS_KE, NO_URUT, GAMBAR) 
                        VALUES ('".$kasus_id."', '".$id_doc."',".$berkas_ke.",'".$no_urut."','".$gambar."')";
        return $this->db->query($sql);
    }
    
    public function getDataBerkas($user_id, $kasus_id, $id_doc, $berkas_ke){
        $sql = "SELECT b.NAMA_PENYIDIK, a.ID_DOC_OPSNAL, c.NAMA_KASUS, a.NRP, a.ID_KASUS, a.BERKAS_KE, a.tgl_masuk_data 
                FROM $this->tb_ba_undercover a, tb_penyidik b, tb_kasus c 
                WHERE a.NRP=b.NRP
                and a.ID_KASUS=c.ID_KASUS
                and a.ID_KASUS='$kasus_id'
                and a.NRP='$user_id'
                and a.ID_DOC_OPSNAL='$id_doc'
                and a.BERKAS_KE=$berkas_ke";

        $data = $this->db->query($sql)->result();
        return $data[0];
    }

    public function getAllUserReceiveNotif($nrp){
        return $this->getAllUserReceiveNotofication($nrp);
    }
    public function cari_berkas($kasus_id, $nrp, $berkas_ke){
        $sql = "SELECT a.*, b.NAMA_KASUS 
                FROM tb_ba_undercover a, tb_kasus b
                WHERE a.ID_KASUS = '$kasus_id'
                AND a.ID_KASUS=b.ID_KASUS
                AND a.NRP = '$nrp'
                AND BERKAS_KE = $berkas_ke";

        return $this->db->query($sql)->result();
    }
    public function cari_berkas_detail($kasus_id, $nrp, $berkas_ke){
        $sql = "SELECT INFORMASI, KELENGKAPAN 
                FROM tb_ba_undercover_detail
                WHERE ID_KASUS = '$kasus_id'
                AND BERKAS_KE = $berkas_ke
                order by no_urut";

        return $this->db->query($sql)->result();
    }
    public function cari_berkas_gambar($kasus_id, $nrp, $berkas_ke){
        $sql = "SELECT GAMBAR
                FROM tb_ba_undercover_gambar
                WHERE ID_KASUS = '$kasus_id'
                AND BERKAS_KE = $berkas_ke
                order by no_urut";

        return $this->db->query($sql)->result();
    }
}