<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opsnalmodel extends MY_Model {

    public function __construct() {
        parent::__construct();
    }
    public function is_login($user_id, $user_regId)
    {  
    	return $this->is_login_parent($user_id, $user_regId);
    }
    public function total_kejadian($id_date){
    	$sql = "select count(*) as total
				from tb_kejadian_menonjol
				where ID_KEJADIAN like 'K$id_date%'";

		$result = $this->db->query($sql)->result();

        return $result[0]->total;
    }
    public function simpan_kejadian($id_kejadian, $nama_kejad, $alamat, $nama_pelaku, $nama_korban, $kerugian, $saksi, $keterangan, $nrp, $latitude, $longitude, $waktu){

    	$sql = "INSERT INTO tb_kejadian_menonjol (ID_KEJADIAN, NAMA_KEJADIAN, TGL_KEJADIAN, LOKASI, PELAKU, KERUGIAN, KORBAN, SAKSI, KETERANGAN, NRP, LATITUDE, LONGITUDE) VALUES ('$id_kejadian', '$nama_kejad', '$waktu', '$alamat', '$nama_pelaku', '$kerugian', '$nama_korban', '$saksi', '$keterangan', '$nrp', '$latitude', '$longitude');";

    	return $this->db->query($sql);
    }
    public function getKejadian($id_kejadian){
    	$sql 	= "select a.*, b.NAMA_PENYIDIK
				from tb_kejadian_menonjol a, tb_penyidik b
				where a.nrp = b.nrp
				and ID_KEJADIAN = '$id_kejadian'";
    	$data 	= $this->db->query($sql)->result();
        return $data[0];
    }

    public function getBerkasSaya($offset, $limit, $nrp){
    	$sql = 'select a.*, u.USER_NAME, k.NAMA_KASUS, k.NO_LP, p.NAMA_PELAPOR, s.NAMA_PENYIDIK
				from (select id_kasus, id_doc_opsnal, berkas_ke, tgl_ba, nrp
				        from tb_ba_interogasi
				        union
				        select id_kasus, id_doc_opsnal, berkas_ke, tgl_ba, nrp
				        from tb_ba_lit_dokumen
				        union
				        select id_kasus, id_doc_opsnal, berkas_ke, tgl_ba, nrp
				        from tb_ba_observasi
				        union
				        select id_kasus, id_doc_opsnal, berkas_ke, tgl_ba, nrp
				        from tb_ba_pembuntutan
				        union
				        select id_kasus, id_doc_opsnal, berkas_ke, tgl_ba, nrp
				        from tb_ba_penyamaran) as a, tb_user u, tb_kasus k, tb_pelapor p, tb_penyidik s
				where a.nrp=u.ID_USER
				and k.ID_KASUS=a.id_kasus
				and p.ID_PELAPOR=k.ID_PELAPOR
				and a.nrp = s.NRP ' . "
				and a.nrp = '$nrp'
				order by tgl_ba desc
				LIMIT $limit OFFSET $offset
				";

		return $this->db->query($sql)->result();
    }
    public function getKejadianBaru($kejadian_id){
    	$sql = "select a.*, b.NAMA_PENYIDIK
				from tb_kejadian_menonjol a, tb_penyidik b
				where a.NRP = b.NRP
				and a.ID_KEJADIAN = '$kejadian_id'";

    	return $this->db->query($sql)->result();
    }
    public function getAllUserNonOpsnallLogin(){
    	return $this->getAllUserNonOpsnallLoginParent();
    }
}