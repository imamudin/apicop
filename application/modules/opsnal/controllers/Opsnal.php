<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Opsnal extends MY_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->model('Opsnalmodel');
        //$this->load->model('Test2');
    }
    public function getBerkasSaya(){
        $limit   = 50; //menampilkan 50 berkas

        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';

            //$data           = $this->input->post();
            $data = (array)json_decode(file_get_contents('php://input'));
            isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
            isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';
            isset($data['offset'])      ? $offset       = $data['offset']       : $offset       =-1;

            if($user_regId != '' && $user_id !='' && $offset !=-1){
                //mengecek data tidak boleh kosong
                if($this->Opsnalmodel->is_login($user_id, $user_regId)){
                    //mengecek akun user
                    $data   = $this->Opsnalmodel->getBerkasSaya($offset, $limit, $user_id);

                    $status = 1;
                    $pesan  = 'Query sukses.';
                }else{
                    $pesan  = 'autentifikasi gagal.';
                    $data   = null;
                }
            }else{
                $pesan  = 'Data tidak boleh kosong.';
                $data   = null;
            }
            $respon = array(
                'status'    => $status,
                'pesan'     => $pesan,
                'data'      => $data
            );

            $this->makeOutput($respon);
        }else {            
            $this->jsonNoRespon();
        }
    }

    public function setKejadianBaru(){
        $status     = -1;
        $pesan      = '';
        $data       = '';
        $waktu      = date('Y-m-d H:i:s');
        $nextId     = 0;

        $data       = (array)json_decode(file_get_contents('php://input'));
        isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
        isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';

        isset($data['nama_kejad'])  ? $nama_kejad   = $data['nama_kejad']   : $nama_kejad   ='';
        isset($data['alamat'])      ? $alamat       = $data['alamat']       : $alamat       ='';
        isset($data['nama_pelaku']) ? $nama_pelaku  = $data['nama_pelaku']  : $nama_pelaku  ='';
        isset($data['nama_korban']) ? $nama_korban  = $data['nama_korban']  : $nama_korban  =''; //type array
        isset($data['kerugian'])    ? $kerugian     = $data['kerugian']     : $kerugian     =''; //type array
        isset($data['saksi'])       ? $saksi        = $data['saksi']        : $saksi        =''; //type array
        isset($data['keterangan'])  ? $keterangan   = $data['keterangan']   : $keterangan   ='';
        isset($data['koordinat'])   ? $koordinat    = $data['koordinat']    : $koordinat    ='';

        //echo Date('ymd');
        $d="05/Feb/2010:14:00:01";
        $dr= date_create_from_format('d/M/Y:H:i:s', $d);
        //echo $dr->format('Y-m-d H:i:s');

        //echo $dr->format('ymd');
        if($user_regId != '' && $user_id != '' && $nama_kejad != '' && $alamat != '' && $koordinat != ''){
            $latitude   = '';
            $longitude  = '';
            if($koordinat!=''){
                $latlong    = explode(';', $koordinat);
                if(count($latlong)==2){
                    $latitude   = $latlong[0];
                    $longitude  = $latlong[1];
                }
            }
            //melakukan autentifikasi
            if($this->Opsnalmodel->is_login($user_id, $user_regId)){
                $id_date = Date('ymd');
                $id_kejadian = '';

                $kejadian_ke  = ($this->Opsnalmodel->total_kejadian($id_date)+1);
                $nextId ++;

                if($kejadian_ke<10){
                    $id_kejadian    = 'K'.$id_date.'00'.$kejadian_ke;
                }else if($kejadian_ke >= 10 && $kejadian_ke < 100){
                    $id_kejadian    = 'K'.$id_date.'0'.$kejadian_ke;
                }else{
                    $id_kejadian    = 'K'.$id_date.''.$kejadian_ke;
                }

                $simpan_kejadian  = $this->Opsnalmodel->simpan_kejadian($id_kejadian, $nama_kejad, $alamat, $nama_pelaku, $nama_korban, $kerugian, $saksi, $keterangan, $user_id, $latitude, $longitude, $waktu);
                if($simpan_kejadian){
                    //mendapatkan data yang berhasil disimpan
                    $data_pesan = $this->Opsnalmodel->getKejadian($id_kejadian);

                    $pesan  = 'Data berhasil disimpan.';
                    $data   = $data_pesan;
                    $status = 1;
                }else{
                    $pesan  = 'Penyimpanan gagal.';
                    $data   = null;
                    $status = 0;
                }
            }else{
                $pesan  = 'Autentifikasi gagal.';
                $data   = null;
            }
        }else{
            $pesan  = 'Data tidak boleh kosong.';
            $data   = null;
            $status = 0;
        }

        //untuk mengirim notifikasi ke non opsnall yang telah login
        if($status==1){
            if($data_pesan!=null){
                //mendapatkan user yang sedang aktif
                $array_regId = null;
                $users = $this->Opsnalmodel->getAllUserNonOpsnallLogin();
                for($i=0;$i<count($users);$i++){
                    $array_regId[] = $users[$i]->user_regId;
                    if(count($array_regId)==COUNT_MAX){
                        $this->sendNotification($array_regId, $data_pesan);
                        $array_regId = null;
                    }
                }
                if($array_regId!=null){
                    $this->sendNotification($array_regId, $data_pesan);
                }
            }
        }

        $respon = array(
            'status'    => $status,
            'pesan'     => $pesan,
            'data'      => $data_pesan
        );

        $this->makeOutput($respon);
    }

    public function getKejadianBaru(){
        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';

            //$data           = $this->input->post();
            $data = (array)json_decode(file_get_contents('php://input'));
            isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
            isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';

            isset($data['kejadian_id']) ? $kejadian_id  = $data['kejadian_id']  : $kejadian_id  ='';
            
            if($user_regId != '' && $user_id != '' && $kejadian_id != ''){
                //mengecek data tidak boleh kosong
                if($this->Opsnalmodel->is_login($user_id, $user_regId)){
                    //mengecek akun user
                    //cari kasus yang sesaui dengan keyword
                    $data_kejadian         = $this->Opsnalmodel->getKejadianBaru($kejadian_id);
                    $data   = $data_kejadian;
                    $status = 1;
                    $pesan  = 'Query sukses.';
                }else{
                    $pesan  = 'autentifikasi gagal.';
                    $data   = null;
                }
            }else{
                $pesan  = 'Data tidak boleh kosong.';
                $data   = null;
            }
            $respon = array(
                'status'    => $status,
                'pesan'     => $pesan,
                'data'      => $data
            );

            $this->makeOutput($respon);
        }else {            
            $this->jsonNoRespon();
        }
    }

}