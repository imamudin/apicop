<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Observasi extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('Observasimodel');
        //$this->load->model('Test2');
    }
    public function getKasus(){
        $limit   = 50; //menampilkan 50 kasus per load

        //untuk mengecek apakah input dari aplikasi android
        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';

            //$data           = $this->input->post();
            $data = (array)json_decode(file_get_contents('php://input'));
            isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
            isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';
            isset($data['keyword'])     ? $keyword      = $data['keyword']      : $keyword      ='';
            isset($data['keyword_jenis'])? $keyword_jenis = $data['keyword_jenis'] : $keyword_jenis='';
            isset($data['offset'])      ? $offset       = $data['offset']       : $offset       ='';

            /*
            keyword jenis ada"NO_LP" dan "NAMA_PELAPOR";
            */
            if($user_regId != '' && $user_id != '' && $keyword_jenis != '' && $offset != ''){
                //mengecek data tidak boleh kosong
                if($this->Observasimodel->is_login($user_id, $user_regId)){
                    //mengecek akun user
                    //cari kasus yang sesaui dengan keyword
                    $data   = $this->Observasimodel->cari_kasus($keyword, $keyword_jenis, $offset, $limit);
                    //echo 'telah login';
                    $status = 1;
                    $pesan  = 'Query sukses.';
                }else{
                    $pesan  = 'autentifikasi gagal.';
                    $data   = null;
                }
            }else{
                $pesan  = 'Data tidak boleh kosong.';
                $data   = null;
            }
            $respon = array(
                'status'    => $status,
                'pesan'     => $pesan,
                'data'      => $data
            );

            $this->makeOutput($respon);
        }else {
            $this->jsonNoRespon();
        }
    }
    public function setBerkas(){
        $status     = -1;
        $pesan      = '';
        $data       = '';
        $tanggal    = date('Y-m-d H:i:s');
        $tanggal_g  = date('Y-m-d');
        $total_ba   = 0;

        $data       = (array)json_decode(file_get_contents('php://input'));
        isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
        isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';

        isset($data['kasus_id'])    ? $kasus_id     = $data['kasus_id']     : $kasus_id     ='';
        isset($data['dasar'])       ? $dasar        = $data['dasar']        : $dasar        ='';
        isset($data['sasaran_tempat'])? $stempat    = $data['sasaran_tempat']: $stempat     ='';
        isset($data['tempats'])     ? $tempats      = $data['tempats']      : $tempats      =null; //type array
        isset($data['gagasans'])    ? $gagasans     = $data['gagasans']     : $gagasans     =null; //type array
        isset($data['gambar'])      ? $gambar       = $data['gambar']       : $gambar       =null; //type array
        isset($data['koordinat'])   ? $koordinat    = $data['koordinat']    : $koordinat    ='';

        $latitude   = '';
        $longitude  = '';
        if($koordinat!=''){
            $latlong    = explode(';', $koordinat);
            if(count($latlong)==2){
                $latitude   = $latlong[0];
                $longitude  = $latlong[1];
            }
        }
        if($user_regId != '' && $user_id != '' && $kasus_id !='' && $dasar != '' && $stempat != '' && count($tempats) !=0 && count($gagasans) != 0){
            if(count($tempats)==count($gagasans)){
                //jumlah tempat harus sama dengan gagasan

                //melakukan autentifikasi
                if($this->Observasimodel->is_login($user_id, $user_regId)){
                    $berkas_ke  = ($this->Observasimodel->total_ba_observasi(ID_DOC_OBSERVASI, $kasus_id)+1);

                    $simpan_ba  = $this->Observasimodel->simpan_ba($kasus_id, ID_DOC_OBSERVASI, $user_id, $berkas_ke, $dasar, $tanggal, $stempat, $latitude, $longitude);
                    if($simpan_ba){
                        //simpan pertanyaan
                        for($i=0;$i<count($tempats);$i++){
                            $this->Observasimodel->simpan_detil($kasus_id, ID_DOC_OBSERVASI, $berkas_ke, ($i+1), $tempats[$i], $gagasans[$i]);
                        }
                        for($i=0;$i<count($gambar);$i++){
                            $nama_gambar    = ID_DOC_OBSERVASI."_".$tanggal_g."_".$berkas_ke."_".$kasus_id."_".($i+1).".jpeg";
                            $path           = PATH_GAMBAR.ID_DOC_OBSERVASI."/".$nama_gambar;
                            $simpan_gambar  = $this->Observasimodel->simpan_gambar($kasus_id, ID_DOC_OBSERVASI, $berkas_ke, ($i+1), $nama_gambar);
                            if($simpan_gambar){
                                $dirname    = PATH_GAMBAR.ID_DOC_OBSERVASI.'/';
                                $filename   = "/folder/" . $dirname . "/";

                                if (!file_exists($dirname)) {
                                    mkdir(PATH_GAMBAR . ID_DOC_OBSERVASI, 0777);
                                }

                                file_put_contents($path,base64_decode($gambar[$i]));
                            }
                        }

                        //mendapatkan data yang berhasil disimpan
                        $data_pesan = $this->Observasimodel->getDataBerkas($user_id, $kasus_id,ID_DOC_OBSERVASI, $berkas_ke);

                        $pesan  = 'Data berhasil disimpan.';
                        $data   = null;
                        $status = 1;

                    }else{
                        $pesan  = 'Penyimpanan gagal.';
                        $data   = null;
                        $status = 0;
                    }
                }else{
                    $pesan  = 'Autentifikasi gagal.';
                    $data   = null;
                }
            }else{
                $pesan  = 'Data informasi kelengkapan tidak lengkap.';
                $data   = null;
                $status = 0;
            }
        }else{
            $pesan  = 'Data tidak boleh kosong.';
            $data   = null;
            $status = 0;
        }

        //untuk mengirim notifikasi ke non opsnall yang telah login
        if($status==1){
            if($data_pesan!=null){
                //mendapatkan user yang sedang aktif
                $array_regId = null;
                $users = $this->Observasimodel->getAllUserReceiveNotofication($user_id);
                for($i=0;$i<count($users);$i++){
                    $array_regId[] = $users[$i]->user_regId;
                    if(count($array_regId)==COUNT_MAX){
                        $this->sendNotification($array_regId, $data_pesan);
                        $array_regId = null;
                    }
                }
                if($array_regId!=null){
                    $this->sendNotification($array_regId, $data_pesan);
                }
            }
        }

        $respon = array(
            'status'    => $status,
            'pesan'     => $pesan,
            'data'      => $data
        );

        $this->makeOutput($respon);
    }
    public function getBerkas(){
        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';

            //$data           = $this->input->post();
            $data = (array)json_decode(file_get_contents('php://input'));
            isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
            isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';

            isset($data['kasus_id'])    ? $kasus_id     = $data['kasus_id']     : $kasus_id     ='';
            isset($data['nrp'])         ? $nrp          = $data['nrp']          : $nrp          ='';
            isset($data['berkas_ke'])   ? $berkas_ke    = $data['berkas_ke']    : $berkas_ke    ='';

            /*
            keyword jenis ada"NO_LP" dan "NAMA_PELAPOR";
            */
            // echo json_encode($data);
            // exit();
            if($user_regId != '' && $user_id != '' && $kasus_id != '' && $nrp != '' && $berkas_ke != ''){
                //mengecek data tidak boleh kosong
                if($this->Observasimodel->is_login($user_id, $user_regId)){
                    //mengecek akun user
                    //cari kasus yang sesaui dengan keyword
                    $observasi         = $this->Observasimodel->cari_berkas($kasus_id, $nrp, $berkas_ke);
                    $observasi_detail  = $this->Observasimodel->cari_berkas_detail($kasus_id, $nrp, $berkas_ke);
                    $observasi_gambar  = $this->Observasimodel->cari_berkas_gambar($kasus_id, $nrp, $berkas_ke);

                    $data = array(
                            'berkas'    => $observasi,
                            'detail'    => $observasi_detail,
                            'gambar'    => $observasi_gambar
                        );
                    //echo 'telah login';
                    $status = 1;
                    $pesan  = 'Query sukses.';
                }else{
                    $pesan  = 'autentifikasi gagal.';
                    $data   = null;
                }
            }else{
                $pesan  = 'Data tidak boleh kosong.';
                $data   = null;
            }
            $respon = array(
                'status'    => $status,
                'pesan'     => $pesan,
                'data'      => $data
            );

            $this->makeOutput($respon);
        }else {            
            $this->jsonNoRespon();
        }
    }
}