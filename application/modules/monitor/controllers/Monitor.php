<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Monitor extends MY_Controller {
    
    function __construct(){
        parent::__construct();
        $this->load->model('Monitormodel');
        //$this->load->model('Test2');
    }
    public function getMonitor(){
        $limit   = 50; //menampilkan 50 berkas

        if($this->checkApp($this->input->get_request_header('appName'))){
            $status     = -1;
            $pesan      = '';
            $data       = '';

            //$data           = $this->input->post();
            $data = (array)json_decode(file_get_contents('php://input'));
            isset($data['user_regId'])  ? $user_regId   = $data['user_regId']   : $user_regId   ='';
            isset($data['user_id'])     ? $user_id      = $data['user_id']      : $user_id      ='';

            isset($data['tgl_mulai'])   ? $tgl_mulai    = $data['tgl_mulai']    : $tgl_mulai    ='';
            isset($data['tgl_akhir'])   ? $tgl_akhir    = $data['tgl_akhir']    : $tgl_akhir    ='';
            isset($data['nama_kasus'])  ? $nama_kasus   = $data['nama_kasus']   : $nama_kasus   ='';
            isset($data['no_lp'])       ? $no_lp        = $data['no_lp']        : $no_lp        ='';
            isset($data['nama_pelapor'])? $nama_pelapor = $data['nama_pelapor'] : $nama_pelapor ='';
            isset($data['offset'])      ? $offset       = $data['offset']       : $offset       =-1;

            isset($data['sort_by'])     ? $sort_by      = $data['sort_by']      : $sort_by      = 'tgl_ba desc';
            isset($data['filter_by'])   ? $filter_by    = $data['filter_by']    : $filter_by    = '';
            isset($data['filter_value'])? $filter_value = $data['filter_value'] : $filter_value = '';

            if($user_regId != '' && $user_id !='' && $offset !=-1){
                //mengecek data tidak boleh kosong
                if($this->Monitormodel->is_login($user_id, $user_regId)){
                    //mengecek akun user

                    //melakukan filter
                    $add_query = '';
                    if($tgl_mulai!='' && $tgl_akhir!=''){
                        $add_query = " and tgl_ba between '$tgl_mulai' and '$tgl_akhir'";
                    }
                    if($filter_value != ''){
                        $add_query = $add_query . " and $filter_by like '%$filter_value%'";
                    }

                    $data   = $this->Monitormodel->getMonitoring($add_query,$offset, $limit, $sort_by);

                    $status = 1;
                    $pesan  = 'Query sukses.';
                }else{
                    $pesan  = 'autentifikasi gagal.';
                    $data   = null;
                }
            }else{
                $pesan  = 'Data tidak boleh kosong.';
                $data   = null;
            }
            $respon = array(
                'status'    => $status,
                'pesan'     => $pesan,
                'data'      => $data
            );

            $this->makeOutput($respon);
        }else {            
            $this->jsonNoRespon();
        }
    }
}